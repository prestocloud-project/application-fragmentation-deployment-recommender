package fragments;


import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.HIGH,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = faas,
        precededBy = {"fragments.FeatureExtractor"},
        cloud_docker_image = "aditess_classifier_grpc:1.0",
        cloud_docker_registry = "presto-public-repository.ubitech.eu",
        cloud_docker_cmd = "--device=/dev/snd",
        cloud_environmental_variables = {
                "AUDIO_ALGO","RFSNS_ROC1",
                "RPU_SERVER_PORT","6666"
        },
        cloud_ports_properties = {
        }

)
public class AudioClassifier {
}
