package fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        onloadable = true,
        offloadable = false,
        elasticity_mechanism = faas,
        sensors_required = {"microphone"}
)
public class AudioCaptor {
}
