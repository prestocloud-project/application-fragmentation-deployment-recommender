package fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = faas,
        dependencyOn = {"test_fragments.VideoStreamer"},
        precededBy = {"test_fragments.VideoStreamer"}
        )
public class VideoTranscoder {
}
