package fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.HIGH,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = faas,
        precededBy = {},
        cloud_docker_image = "facedetector:1.0",
        cloud_docker_registry = "presto-public-repository.ubitech.eu",
        cloud_environmental_variables = {
                "RTSP_FEED","rtsp://admin:admin@10.124.10.100",
        },
        cloud_ports_properties = {
        }

)
public class FaceDetector {

    public void detection_algorithm(){

    }

}
