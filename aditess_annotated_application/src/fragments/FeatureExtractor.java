package fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.HIGH,
        onloadable = true,
        offloadable = false,
        elasticity_mechanism = faas,
        precededBy = {},
        cloud_docker_image = "aditess_afe_java:1.0",
        cloud_docker_registry = "presto-public-repository.ubitech.eu",
        cloud_docker_cmd = "--device=/dev/snd",
        cloud_environmental_variables = {
                "RPU_SERVER_IP","192.168.10.125",
                "RPU_SERVER_PORT","6666"
        },
        cloud_ports_properties = {
        },
        sensors_required = {"usb_sound_card"}

)
public class FeatureExtractor {
}
