package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;
import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.simple_horizontal_no_load_balancing;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        onloadable = true,
        offloadable = true,
        elasticity_mechanism = simple_horizontal_no_load_balancing,
        cloud_docker_registry = "prestocloud.test.eu",
        cloud_docker_image = "video_transcoder_cloud:latest",
        edge_docker_registry = "prestocloud.edge.test.eu",
        edge_docker_image = "video_transcoder_edge:latest",
        dependencyOn = {"test_fragments.VideoStreamer"},
        precededBy = {"test_fragments.VideoStreamer"},
        optimization_cost_weight = 2,
        optimization_distance_weight = 8,
        optimization_providerFriendliness_weights = {"aws", "1",
                "gce", "0",
                "azure", "5"
        }
        )
public class VideoTranscoder {
}
