package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        min_instances = 3,
        elasticity_mechanism = faas,
        onloadable = true,
        offloadable = false,
        edge_docker_registry = "prestocloud.test.eu",
        edge_docker_image = "video_streamer:latest",
        sensors_required = {"video_camera","/dev/video/camera0"},
        edge_environmental_variables = {
                "VIDEO_TRANSCODER_SERVICE","get_property: [deployment_node_test_fragments_VideoTranscoder,host,network,addresses,1]",
                "VIDEO_RESOLUTION","HD1080p"
        },
        public_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCos7rOnd5KO+W/j6tPl3NggkYqkc5ZDWdluWe+7CK0F22ET9G+f+SDaPSQ4xQjM22wmRYsEzeN965pysIyqmGq2453A7Ykb14jZsvnyOBCMTu+xHez/uOSRrSEFB7Eo8UA8vN/1S3QhSDOMIf83ji3W0UeG6MianvIl5/uB02nagr7NCx6vqNA7TwSL7R21e2vGi4pMmGX5D5YwtSO9984EkvRnCmwtK4u+kiqMF3Emb3NHDZSfPooYVdLXjeTQ0b8DoTRiM+8xkYGyB4QIpqtBF45GhLXzzQtpEuTkj6CKNWkFIv/r6hJpTYaF7w2ql1U98P6y1BYcaOkmeHJdaV user"
)
public class VideoStreamer {
}
