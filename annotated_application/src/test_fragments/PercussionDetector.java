package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        elasticity_mechanism = faas,
        onloadable = true,
        offloadable = false,
        edge_docker_registry = "prestocloud.test.eu",
        edge_docker_image = "percussion_detector:latest",
        dependencyOn = {"test_fragments.AudioCaptor"},
        precededBy = {"test_fragments.AudioCaptor"}
        )
public class PercussionDetector {
}


