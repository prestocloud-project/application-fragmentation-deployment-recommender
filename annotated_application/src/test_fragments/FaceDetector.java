package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.*;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        onloadable = true,
        offloadable = true,
        elasticity_mechanism = jppf,
        edge_docker_registry = "local.prestocloud.test.eu",
        edge_docker_image = "face_detector_edge:latest",
        cloud_docker_registry = "prestocloud.test.eu",
        cloud_docker_image = "face_detector_cloud:latest",
        health_check_command = "ps -ef >> /var/log/prestocloud_log",
        cloud_environmental_variables = {"PRECISION","100","ITERATIONS","2"},
        edge_environmental_variables = {"PRECISION","50","ITERATIONS","10"}

)
public class FaceDetector {


}
