package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.faas;
import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.none;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.HIGH,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = none,
        precededBy = {"test_fragments.VideoTranscoder","test_fragments.FaceDetector","test_fragments.VideoStreamer"},
        cloud_docker_registry = "prestocloud.test.eu",
        cloud_docker_image = "multimedia_manager:latest",
        cloud_environmental_variables = {
                "VIDEO_TRANSCODER_SERVICE","get_property: [deployment_node_test_fragments_VideoTranscoder,host,network,addresses,1]",
                "FACE_DETECTOR_SERVICE","get_property: [deployment_node_test_fragments_FaceDetector,host,network,addresses,1]",
                "RUNNING_THREADS","2"
        },
        optimization_cost_weight = 5,
        optimization_distance_weight = 4,
        optimization_providerFriendliness_weights = {"aws", "1",
                "gce", "0",
                "azure", "5"
        },
        public_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1uCM+WTrtr82waAez9ZrPnvIyArAVk5AxOXkS8E4genxu1D7YKK2prLDl8xOzOJmrWkfHtzSSDH+quxAaHCmdz/fMsiDt3CzsRLn1f5GqXy4BM+IvVd8M4585s/hRUmDEGohlt9Ro3gnE65zPAjz35pwkBLIWXZ4tSbhQnUJEjMlKjAtsEXIlJeC1YSTMNhvaBnB1qOMxwZbOUEdhxlbezIVlugLxrdhlEDDJBpR5xwSDgFwEKO/Z9eE+cLAVNEetShenHZevctMHcdEGFBie8MOF2hiKD3kKcPykH6ULPw2hs9seiIz51coGAcX5k3YHxvi7ngVqHjuQsRqUIe97 user"
)
public class MultimediaManager {


}
