package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.none;

/*@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        elasticity_mechanism = none,
        dependencyOn = {"test_fragments.VideoStreamer"},
        precededBy = {"test_fragments.VideoStreamer"},
        edge_docker_registry = "local.prestocloud.test.eu",
        edge_docker_image = "test_fragment_edge:latest",
        cloud_docker_registry = "prestocloud.test.eu",
        cloud_docker_image = "test_fragment_cloud:latest",
        onloadable = true,
        offloadable = true
        )
*/
public class TestFragment {
}
