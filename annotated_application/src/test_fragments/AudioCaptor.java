package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.*;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        onloadable = true,
        offloadable = false,
        elasticity_mechanism = none,
        edge_docker_registry = "prestocloud.test.eu",
        edge_docker_image = "audiocaptor:latest",
        sensors_required = {"microphone","/dev/snd/mic0"},
        health_check_command = "cat /proc/meminfo",
        edge_environmental_variables = {
                "SAMPLING_RATE","22 kHZ"
        }
)
public class AudioCaptor {
}
