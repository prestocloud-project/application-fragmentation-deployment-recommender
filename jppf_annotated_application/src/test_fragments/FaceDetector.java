package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;


@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        onloadable = true,
        offloadable = true,
        elasticity_mechanism = jppf,
        precededBy = {"test_fragments.VideoTranscoder","test_fragments.MultimediaManager","test_fragments.VideoStreamer"}

)
public class FaceDetector {

    public void detection_algorithm(){

    }

}
