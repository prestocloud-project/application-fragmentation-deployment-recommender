package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        elasticity_mechanism = jppf,
        onloadable = true,
        offloadable = false,
        dependencyOn = {"test_fragments.AudioCaptor"},
        precededBy = {"test_fragments.AudioCaptor"}
        )
public class PercussionDetector {
}


