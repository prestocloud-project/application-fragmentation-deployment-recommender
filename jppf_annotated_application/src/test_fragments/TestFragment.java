package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.none;

@PrestoFragmentation(
        offloadable = false,
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        elasticity_mechanism = none,
        dependencyOn = {"test_fragments.VideoStreamer"},
        precededBy = {"test_fragments.VideoStreamer"},
        onloadable = true
        )
public class TestFragment {
}
