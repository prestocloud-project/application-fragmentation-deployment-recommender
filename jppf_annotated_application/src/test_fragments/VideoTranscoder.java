package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;

@PrestoFragmentation(
        onloadable = false,
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.LOW,
        offloadable = true,
        elasticity_mechanism = jppf,
        dependencyOn = {"test_fragments.VideoStreamer"},
        precededBy = {"test_fragments.VideoStreamer"}
        )
public class VideoTranscoder {
}
