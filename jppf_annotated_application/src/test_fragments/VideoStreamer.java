package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;

@PrestoFragmentation(

        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        min_instances = 3,

        elasticity_mechanism = jppf,
        onloadable = true,
        offloadable = false,
        sensors_required = {"video_camera"}
)
public class VideoStreamer {
}
