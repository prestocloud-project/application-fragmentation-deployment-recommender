package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;

@PrestoFragmentation( memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = PrestoFragmentation.CPULoad.LOW, storageLoad = PrestoFragmentation.StorageLoad.MEDIUM, elasticity_mechanism = jppf, precededBy = {"test_fragments.TestFragment"},onloadable = false,offloadable = true)
public class MultimediaManager {

    @PrestoFragmentation( memoryLoad = PrestoFragmentation.MemoryLoad.VERY_HIGH, cpuLoad = PrestoFragmentation.CPULoad.HIGH, storageLoad = PrestoFragmentation.StorageLoad.LOW, elasticity_mechanism = jppf, precededBy = {"test_fragments.TestFragment"},onloadable = true,offloadable = true)
    public static void intensive_multimedia_processor(){

    }

}
