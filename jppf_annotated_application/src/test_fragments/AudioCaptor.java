package test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.jppf;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        onloadable = true,
        offloadable = false,
        elasticity_mechanism = jppf,
        sensors_required = {"microphone"}
)
public class AudioCaptor {
}
