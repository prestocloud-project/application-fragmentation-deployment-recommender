package eu.prestocloud.utilities;

import eu.prestocloud.annotations.PrestoFragmentation;
import org.reflections.util.ClasspathHelper;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.annotated_application_name;
import static eu.prestocloud.configuration.Constants.annotated_application_path;
import static eu.prestocloud.tosca_generator.NodeGenerator.PROCESS_ANNOTATION_NAMES;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.UNDERSCORE;
import static java.util.logging.Level.INFO;

public class AnnotatedFragmentsJarProcessor {

    public static HashMap<String, PrestoFragmentation> get_annotated_fragments() {
        HashMap<String, PrestoFragmentation> annotated_fragments = new HashMap<>();
        JarFile application_jar = null;
        try {
            application_jar = new JarFile(annotated_application_path + annotated_application_name);
            Enumeration<JarEntry> e = application_jar.entries();
            URL[] urls = {new URL("jar:file:" + annotated_application_path + annotated_application_name + "!/")};
            Logger.getAnonymousLogger().log(INFO, "Loading " + annotated_application_path + annotated_application_name + "... ");
            URLClassLoader cl = URLClassLoader.newInstance(urls);
            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();

                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }
                String className = je.getName().substring(0, je.getName().length() - String.valueOf(".class").length());
                className = className.replace('/', '.');
                try {
                    Class<?> clazz = cl.loadClass(className);
                    clazz.getAnnotations();
                    if (clazz.getAnnotationsByType(PrestoFragmentation.class).length > 0) {
                        annotated_fragments.put(clazz.getCanonicalName(), clazz.getAnnotation(PrestoFragmentation.class));
                        for (Method method : clazz.getDeclaredMethods()) {
                            if (method.getAnnotationsByType(PrestoFragmentation.class).length > 0) {
                                annotated_fragments.put(get_processed_method_name(clazz.getCanonicalName(), method.getName(), method.getAnnotation(PrestoFragmentation.class).overloading_tag()), method.getAnnotation(PrestoFragmentation.class));
                            } else {
                                annotated_fragments.put(get_processed_method_name(clazz.getCanonicalName(), method.getName(),""), clazz.getAnnotation(PrestoFragmentation.class));
                            }
                        }
                    } else {
                        continue;
                    }
                } catch (NoClassDefFoundError n) {
                    n.printStackTrace();
                    continue;
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } finally {
            try {
                application_jar.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
       /* try {
            classLoadersList.add(new URLClassLoader(new URL[]{application_jar.toURI().toURL()},System.class.getClassLoader()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
*/
/*

        Reflections reflections = null;

            reflections = new Reflections(new ConfigurationBuilder()
                    .setScanners(new SubTypesScanner(false */
        /* don't exclude Object.class *//*
), new ResourcesScanner(), new TypeAnnotationsScanner())
                    .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                    //.setUrls(ClasspathHelper.forManifest(Arrays.asList(new URL[]{application_jar.toURI().toURL()})))
                    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("eu.prestocloud.annotated_application.test_fragments")).exclude(FilterBuilder.prefix("eu.prestocloud.annotated_application.old_method_level")).exclude(FilterBuilder.prefix("eu.prestocloud.annotated_application.test_fragments"))));



        Set<Class<?>> annotated_classes = reflections.getTypesAnnotatedWith(PrestoFragmentation.class);
*/
        return annotated_fragments;
    }

    private static String get_processed_class_name(String canonicalName) {
        return canonicalName.replace(".",UNDERSCORE);
    }

    /**
     * This method returns the name of a method object, substituting dots for underscores if the relevant variable is set
     *
     * @param canonical_name The canonical name of the parent class of the method, as returned by method.getDeclaringClass().getCanonicalName()
     * @param method_name    The name of the method examined as returned by method.getName()
     * @return The full (processed) name of the method
     */
    private static String get_processed_method_name(String canonical_name, String method_name, String overloading_name_tag) {
        String OVERLOADED_STRING = "overloaded";
        String full_method_name = "";
        String overloading_string = (overloading_name_tag != null && !overloading_name_tag.equals("")) ? UNDERSCORE + OVERLOADED_STRING + UNDERSCORE + overloading_name_tag : "";
        full_method_name = canonical_name + "." + method_name + overloading_string;

        return full_method_name;
    }
}