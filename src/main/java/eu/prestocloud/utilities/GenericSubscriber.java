package eu.prestocloud.utilities;

import consumer.AmqpConsumer;
import exceptions.AlreadySubscribedException;
import exceptions.NoHostException;
import exceptions.SubscribingFailedException;

import java.io.IOException;

public class GenericSubscriber {

    public java.util.function.BiConsumer<String, String> consumerFunction = this::doSomething;

    //java.util.function.BiConsumer<String, String> consumerFunction = this::doSomething;

    //consumer​.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
    public void subscribe() {
        AmqpConsumer consumer = new AmqpConsumer("52.58.107.100",false,consumerFunction);
        consumer.setUsernameAndPassword("nissatech","9r3570cl0ud!");
        try {
            consumer.subscribe("topology_adaptation.#");
        } catch (AlreadySubscribedException | NoHostException | IOException e){
            System.out.println(e.getMessage());
        } catch (SubscribingFailedException s){
                subscribe();
                //waitXminAndRetry(3);
        }
    }
//doSomething is the method that gets executed on every received message
    private void doSomething(String message, String topic) {
    }

    private void  processing_function(String message, String topic){
        System.out.println("A message was received in topic "+topic);
        System.out.println("MESSAGE "+message);
    }

    private void waitXminAndRetry(int minutes){
        try{
            Thread.sleep(minutes*60*1000);
        }catch (InterruptedException i){
            subscribe();
        }
    }
}
