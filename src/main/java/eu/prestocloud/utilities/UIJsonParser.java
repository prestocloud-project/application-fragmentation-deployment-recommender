package eu.prestocloud.utilities;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.tosca_generator.policy_file_requirements.BudgetRequirement;
import eu.prestocloud.tosca_generator.policy_file_requirements.BusinessGoal;
import eu.prestocloud.tosca_generator.policy_file_requirements.FragmentationPolicy;
import eu.prestocloud.tosca_generator.policy_file_requirements.ProviderRequirement;
import eu.prestocloud.tosca_generator.utility_classes.CoordinatorNodesController;
import eu.prestocloud.tosca_generator.utility_classes.OptimizationVariables;
import eu.prestocloud.tosca_generator.utility_classes.Port;
import eu.prestocloud.tosca_generator.utility_classes.fragments.Fragment;
import eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures;
import eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentTypes;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.security.MessageDigest;
import java.util.*;
import java.util.logging.Logger;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.*;
import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.NodeGenerator.*;
import static eu.prestocloud.tosca_generator.topology_template.NodeTemplate.get_mapping_node_name;
import static eu.prestocloud.tosca_generator.utility_classes.CoordinatorNodesController.set_faas_proxy_fragment_node;
import static eu.prestocloud.tosca_generator.utility_classes.CoordinatorNodesController.set_load_balancer_fragment_node;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.SPACE;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.add_fragment;
import static java.util.logging.Level.*;

public class UIJsonParser {

    private static JSONObject ui_deployment_json;

    public static Set<Fragment> get_ui_fragments(){
        HashMap<String, String> component_node_hexId_name_map = new HashMap<>();
        HashMap<String, OptimizationVariables> fragment_optimization_variables = new HashMap<String, OptimizationVariables>();
        Set<Fragment> ui_fragments = new HashSet<Fragment>();
        try {
            JSONParser parser = new JSONParser();
            /*
            //TODO remove below check & code
            if (ui_deployment_json ==null || ui_deployment_json.size()==0) {
                ui_deployment_json = (JSONObject) parser.parse(new FileReader(get_next_UI_file_path()));
            }
            */
            String new_organization_username = ((String) ui_deployment_json.get("organizationName")).toLowerCase();
            if (new_organization_username !=null && !new_organization_username.equals(EMPTY)){
                organization_username = new_organization_username;
            }


            graph_hex_id = (String)((JSONObject) ui_deployment_json.get("jsonDeployment")).get("graphHexID");
            graph_instance_hex_id = (String)((JSONObject) ui_deployment_json.get("jsonDeployment")).get("graphInstanceHexID");
            JSONArray services = (JSONArray) ((JSONObject) ui_deployment_json.get("jsonDeployment")).get("services");

            for (Object service : services) {
                JSONObject service_json_object = (JSONObject) service;
                String fragment_json_name = (String) service_json_object.get("componentNodeName");

                String fragment_hexID = (String) service_json_object.get("componentNodeHexID");
                String processed_fragment_name = fragment_json_name;


                if (fragment_json_name.matches(".*(_edge)[0-9]*(LB)?")) {

                    processed_fragment_name = get_processed_fragment_name(fragment_json_name,"edge");

                    if ((Boolean)(service_json_object.get("loadBalancer"))&& fragment_json_name.endsWith("LB")) {

                        String duplicate_load_balancer_component_node_name =
                                get_processed_fragment_name(identify_possible_duplicate_load_balancer_on_cloud(services, fragment_json_name));

                        if (!duplicate_load_balancer_component_node_name.equals(EMPTY)) {

                            component_node_hexId_name_map.put(fragment_hexID, duplicate_load_balancer_component_node_name);

                            if (get_collocation_dependency_hashmap().get(duplicate_load_balancer_component_node_name)==null) {
                                get_collocation_dependency_hashmap().put(duplicate_load_balancer_component_node_name, new LinkedHashSet<>());
                            }
                            if (get_anti_affinity_hashmap().get(duplicate_load_balancer_component_node_name)==null){

                                get_anti_affinity_hashmap().put(duplicate_load_balancer_component_node_name,new LinkedHashSet<>());
                            }
                            if (get_precedence_dependency_hashmap().get(duplicate_load_balancer_component_node_name) == null) {
                                get_precedence_dependency_hashmap().put(duplicate_load_balancer_component_node_name, new LinkedHashSet<>());
                            }

                            continue; //ignore the edge load balancer fragment, but handle any dependencies pointing to it by redirecting to the cloud load balancer
                        }
                    }


                }
                else if (fragment_json_name.matches(".*(_cloud)[0-9]*(LB)?")) {

                    processed_fragment_name = get_processed_fragment_name(fragment_json_name,"cloud");

                }

                component_node_hexId_name_map.put(fragment_hexID,processed_fragment_name);

                get_collocation_dependency_hashmap().put(processed_fragment_name,new LinkedHashSet<>());
                get_anti_affinity_hashmap().put(processed_fragment_name,new LinkedHashSet<>());
                get_precedence_dependency_hashmap().put(processed_fragment_name,new LinkedHashSet<>());
            }


            JSONArray optimization_variables_list = (JSONArray) (ui_deployment_json.get("optimizationVariables"));

            for (Object optimization_variables_json_object : optimization_variables_list){
                JSONObject optimization_variables = (JSONObject) optimization_variables_json_object;
                String fragment_hexID = (String) optimization_variables.get("componentNodeHexID");
                String fragment_name = component_node_hexId_name_map.get(fragment_hexID);

                String cost_weight_string = ((String) optimization_variables.get("cost"));
                String distance_weight_string = ((String) optimization_variables.get("distance"));
                int cost_weight=1,distance_weight=1;
                if (cost_weight_string!=null && !cost_weight_string.equals(EMPTY)){
                    cost_weight = Integer.parseInt(cost_weight_string);
                }

                if (distance_weight_string!=null && !distance_weight_string.equals(EMPTY)){
                    distance_weight = Integer.parseInt(distance_weight_string);
                }

                String friendliness_values_string = (String)optimization_variables.get("friendliness");
                ArrayList<String> provider_friendliness_list = new ArrayList<>();
                if (friendliness_values_string!=null && !friendliness_values_string.equals(EMPTY)) {
                JSONObject provider_friendliness_list_json_object= (JSONObject) parser.parse(friendliness_values_string);

                    for (Object key : provider_friendliness_list_json_object.keySet()) {
                        provider_friendliness_list.add((String) key);
                        provider_friendliness_list.add(String.valueOf(provider_friendliness_list_json_object.get(key)));
                    }
                }
                fragment_optimization_variables.put(fragment_name, new OptimizationVariables(provider_friendliness_list, cost_weight, distance_weight));
            }


            JSONArray collocation_dependencies_array = (JSONArray) ui_deployment_json.get("collocated");

            for (Object collocation_dependency_pair : collocation_dependencies_array){

                String collocation_dependency_hex_ID_A = (String) ((JSONObject) collocation_dependency_pair).get("componentNodeHexIDA");
                String collocation_dependency_hex_ID_B = (String) ((JSONObject) collocation_dependency_pair).get("componentNodeHexIDB");

                if(get_collocation_dependencies_set(component_node_hexId_name_map,collocation_dependency_hex_ID_A)== null){
                    get_collocation_dependency_hashmap().put(component_node_hexId_name_map.get(collocation_dependency_hex_ID_A),new LinkedHashSet<>());
                }
                get_collocation_dependencies_set(component_node_hexId_name_map,collocation_dependency_hex_ID_A).add(component_node_hexId_name_map.get(collocation_dependency_hex_ID_B));
            }



            JSONArray anti_affinity_constraints_array = (JSONArray) ui_deployment_json.get("antiAffinity");

            for (Object anti_affinity_constraint_pair : anti_affinity_constraints_array){

                String anti_affinity_constraint_hex_ID_A = (String) ((JSONObject) anti_affinity_constraint_pair).get("componentNodeHexIDA");
                String anti_affinity_constraint_hex_ID_B = (String) ((JSONObject) anti_affinity_constraint_pair).get("componentNodeHexIDB");

                if(get_anti_affinity_constraints_set(component_node_hexId_name_map, anti_affinity_constraint_hex_ID_A)==null){
                    get_anti_affinity_hashmap().put(component_node_hexId_name_map.get(anti_affinity_constraint_hex_ID_A), new LinkedHashSet<>());
                }

                get_anti_affinity_constraints_set(component_node_hexId_name_map,anti_affinity_constraint_hex_ID_A).add(component_node_hexId_name_map.get(anti_affinity_constraint_hex_ID_B));

            }

            for (Object service : services) {


                JSONObject service_json_object = (JSONObject) service;
                String fragment_json_name = (String) service_json_object.get("componentNodeName"); //TODO see if there is any difference with componentNodeInstanceName
                String processed_fragment_name = EMPTY;
                FragmentTypes fragment_type;
                Fragment defined_fragment = null;
                boolean onloadable=false, offloadable=false;
                boolean fragment_already_has_cloud_details_specified = false;
                boolean fragment_already_has_edge_details_specified = false;
                boolean no_fragment_definition_exists = true;

                boolean edge_version_of_fragment = false, cloud_version_of_fragment = false;
                int minimum_workers, maximum_workers;
                String fragment_hexID = (String) service_json_object.get("componentNodeHexID");
                String fragment_id = graph_hex_id+":"+graph_instance_hex_id+":"+fragment_hexID;




                JSONObject elasticity_mechanism_object = (JSONObject) service_json_object.get("monitoringElasticity");
                String elasticity_mechanism_string = (String) elasticity_mechanism_object.get("profile");

                PrestoFragmentation.Elasticity_mechanism elasticity_mechanism; //TODO add a category for fragments which are scalable but do not have a Load Balancer defined, and update the error statement as appropriate.
                if (elasticity_mechanism_string!=null && elasticity_mechanism_string.equals("LAMBDA_FUNCTION")){
                    elasticity_mechanism = faas;
                }else if (elasticity_mechanism_string!=null && elasticity_mechanism_string.equals("HORIZONTAL")){
                    elasticity_mechanism = horizontal_load_balanced;
                }else if (elasticity_mechanism_string!=null && elasticity_mechanism_string.equals("JPPF")){
                    elasticity_mechanism = jppf;
                }else if (elasticity_mechanism_string!=null && elasticity_mechanism_string.equals("NONE")){
                    elasticity_mechanism = none;
                }else{
                    Logger.getAnonymousLogger().log(SEVERE,"The elasticity mechanism definition for fragment "+fragment_json_name+ " may be invalid because it was not LAMBDA_FUNCTION or HORIZONTAL or JPPF or NONE (case-sensitive)");
                    elasticity_mechanism = none;
                }

                JSONObject necessary_sensors_json_object = (JSONObject) service_json_object.get("devices");
                LinkedHashMap<String,String> necessary_sensors = new LinkedHashMap<>(); //{"test_sensor_1","test_sensor_2"};
                if (necessary_sensors_json_object!=null) {
                    for (Object key : necessary_sensors_json_object.keySet()) {
                        necessary_sensors.put((String) key, (String) necessary_sensors_json_object.get(key));
                    }
                }

                //explicit declaration of edge-only fragment. TODO Determine if the LB needs to have its deployment location changed
                if (fragment_json_name.matches(".*(_edge)[0-9]*(LB)?$")) {


                    processed_fragment_name = get_processed_fragment_name(fragment_json_name,"edge");

                    if ((Boolean)(service_json_object.get("loadBalancer"))&& fragment_json_name.endsWith("LB")){

                        String duplicate_load_balancer_component_node_name = identify_possible_duplicate_load_balancer_on_cloud(services,fragment_json_name);

                        if (!duplicate_load_balancer_component_node_name.equals(EMPTY)){
                            //TODO insert correctly the required new entry on the hexid and component id map
                            component_node_hexId_name_map.put(fragment_hexID,get_processed_fragment_name(duplicate_load_balancer_component_node_name,"cloud"));
                            continue; //ignore the edge load balancer fragment, but handle any dependencies pointing to it by redirecting to the cloud load balancer
                        }

                        //Load-balancers are assumed to run only on cloud machines
                        cloud_version_of_fragment = true;
                        offloadable = true;

                        String fragment_dependency_hex_id = (String)((JSONObject)((JSONArray) service_json_object.get("dependsOn")).get(0)).get("dependency");
                        String fragment_dependency = component_node_hexId_name_map.get(fragment_dependency_hex_id);
                        //below check should always be false
                        if (!(get_processed_fragment_name(fragment_dependency).equals(processed_fragment_name.replaceAll("_LB$",EMPTY)))) {

                            Logger.getAnonymousLogger().log(WARNING,"Discrepancy found between the naming of the load-balancing fragment "+fragment_json_name+" and the name of the fragment on which it is dependent (most probably the load-balanced fragment?) named "+get_processed_fragment_name(fragment_dependency)+". Choosing the explicitly dependent fragment over the implicitly inferred fragment name");

                        }

                        set_load_balancer_fragment_node(get_processed_fragment_name(fragment_dependency), get_mapping_node_name(processed_fragment_name));
                        minimum_workers = 1; //Load balancer instances do not have a number of workers
                        maximum_workers = 1;
                        fragment_type = FragmentTypes.coordinator_load_balancer;

                    }else{//a non-coordinator fragment, the case of a Lambda Proxy is excluded as its name would start with "LambdaProxy"

                        edge_version_of_fragment = true;
                        onloadable = true;
                        minimum_workers = Integer.parseInt(service_json_object.get("minimumWorkers").toString());
                        maximum_workers = Integer.parseInt(service_json_object.get("maximumWorkers").toString());
                        fragment_type = FragmentTypes.application_fragment;
                    }

                    defined_fragment = FragmentDataStructures.fragments_map.get(processed_fragment_name);
                    if (defined_fragment != null) {
                        synchronized (defined_fragment) {
                            no_fragment_definition_exists = false;
                            if (defined_fragment.getCloud_docker_image() != null && !defined_fragment.getCloud_docker_image().equals(EMPTY)) {
                                fragment_already_has_cloud_details_specified = true; // if a fragment already exists, this means that since now the edge version was detected, the cloud version has already been specified
                            }
                        }
                    }
                }
                //explicit declaration of a cloud-only fragment/LB
                else if (fragment_json_name.matches(".*(_cloud)[0-9]*(LB)?$")) {

                    processed_fragment_name = get_processed_fragment_name(fragment_json_name,"cloud");

                    if ((Boolean)(service_json_object.get("loadBalancer"))&& fragment_json_name.endsWith("LB")){

                        //Load-balancers are assumed to run only on cloud machines

                        cloud_version_of_fragment = true;
                        offloadable = true;
                        //TODO understand the load-balanced fragment from the list of the dependencies of the Loadbalancer
                        String fragment_dependency_hex_id = (String)((JSONObject)((JSONArray) service_json_object.get("dependsOn")).get(0)).get("dependency");
                        String fragment_dependency = component_node_hexId_name_map.get(fragment_dependency_hex_id);
                        //below check should always be false
                        if (!(get_processed_fragment_name(fragment_dependency).equals(processed_fragment_name.replaceAll("_LB$",EMPTY)))) {

                            Logger.getAnonymousLogger().log(WARNING,"Discrepancy found between the naming of the load-balancing fragment "+fragment_json_name+" and the name of the fragment on which it is dependent (most probably the load-balanced fragment?) named "+get_processed_fragment_name(fragment_dependency)+". Choosing the explicitly dependent fragment over the implicitly inferred fragment name");

                        }

                        set_load_balancer_fragment_node(get_processed_fragment_name(fragment_dependency), get_mapping_node_name(processed_fragment_name));
                        minimum_workers = 1; //Load balancer instances do not have a number of workers
                        maximum_workers = 1;
                        fragment_type = FragmentTypes.coordinator_load_balancer;

                    }else{//a non-coordinator fragment
                        cloud_version_of_fragment = true;
                        offloadable = true;

                        minimum_workers = Integer.parseInt(service_json_object.get("minimumWorkers").toString());
                        maximum_workers = Integer.parseInt(service_json_object.get("maximumWorkers").toString());
                        fragment_type = FragmentTypes.application_fragment;
                    }


                    defined_fragment = FragmentDataStructures.fragments_map.get(processed_fragment_name);

                    if (defined_fragment!=null) {
                        synchronized (defined_fragment) {
                            if (defined_fragment.getEdge_docker_image() != null && !defined_fragment.getEdge_docker_image().equals(EMPTY)) {
                                fragment_already_has_edge_details_specified = true; // if a fragment already exists, this means that since now the cloud version was detected, the edge version has already been specified
                            }
                        }
                    }
                }
                //explicit declaration of Lambda Proxy
                else if (fragment_json_name.startsWith("LambdaProxy")
                        /*&& (Boolean)(service_json_object.get("lambdaProxy")*/){

                    //Assumption that the Lambda Proxy will always run in a cloud machine
                    cloud_version_of_fragment = true;
                    offloadable = true;
                    processed_fragment_name = fragment_json_name;
                    ArrayList<String> fragments_serviced = new ArrayList<>();
                    for (Object fragment_serviced : (JSONArray)service_json_object.get("dependsOn")){
                        fragments_serviced.add(component_node_hexId_name_map.get((String)((JSONObject) fragment_serviced).get("dependency")));
                    }
                    for (String fragment_serviced:fragments_serviced) {
                        CoordinatorNodesController.set_faas_proxy_fragment_node(fragment_serviced, get_mapping_node_name(fragment_json_name));
                    }
                    minimum_workers = 1; //Lambda proxy instances do not have minimum/maximum instances in the json file.
                    maximum_workers = 1;
                    fragment_type = FragmentTypes.coordinator_faas_proxy;
                }

                else {
                    //implicit inference of a cloud & edge fragment
                    if (necessary_sensors.size() == 0) {

                        processed_fragment_name = fragment_json_name;

                        if ((Boolean)(service_json_object.get("loadBalancer")) && fragment_json_name.endsWith("LB")){
                            set_load_balancer_fragment_node(fragment_json_name.replaceAll("LB$",EMPTY), get_mapping_node_name(processed_fragment_name));
                            minimum_workers = 1; //Load balancer instances do not have a number of workers
                            maximum_workers = 1;
                            fragment_type = FragmentTypes.coordinator_load_balancer;

                            onloadable = false; //run only on cloud nodes
                            offloadable = true;
                            edge_version_of_fragment = false;
                            cloud_version_of_fragment = true;

                        }else if (fragment_json_name.equals("LambdaProxy")) {

                            set_faas_proxy_fragment_node(fragment_json_name,get_mapping_node_name(processed_fragment_name));
                            minimum_workers = 1; //Load balancer instances do not have a number of workers
                            maximum_workers = 1;
                            fragment_type = FragmentTypes.coordinator_faas_proxy;

                            ArrayList <String> fragments_serviced = new ArrayList();
                            for (Object fragment_serviced : (JSONArray)service_json_object.get("dependsOn")){
                                fragments_serviced.add((String)((JSONObject) fragment_serviced).get("dependency"));
                            }
                            for (String fragment_serviced:fragments_serviced) {
                                CoordinatorNodesController.set_faas_proxy_fragment_node(fragment_serviced, get_mapping_node_name(fragment_json_name));
                            }

                            onloadable = false; //run only on cloud nodes
                            offloadable = true;
                            edge_version_of_fragment = false;
                            cloud_version_of_fragment = true;

                        }else{

                            Logger.getAnonymousLogger().log(INFO, ("The fragment " + fragment_json_name + " is not declared explicitly as a cloud or an edge fragment. As such, it is assumed to belong to both deployment locations, as no sensor dependencies were detected"));
                            edge_version_of_fragment = true;
                            cloud_version_of_fragment = true;
                            onloadable = true;
                            offloadable = true;

                            minimum_workers = Integer.parseInt(service_json_object.get("minimumWorkers").toString());
                            maximum_workers = Integer.parseInt(service_json_object.get("maximumWorkers").toString());
                            fragment_type = FragmentTypes.application_fragment;

                        }

                    }
                    //implicit inference of an edge-only fragment
                    else {

                        Logger.getAnonymousLogger().log(INFO, ("The fragment " + fragment_json_name + " is not declared explicitly as a cloud or an edge fragment, but was inferred to be an edge-only fragment as sensor dependencies were detected"));
                        processed_fragment_name = fragment_json_name;

                        if ((Boolean)(service_json_object.get("loadBalancer")) && fragment_json_name.endsWith("LB")){
                            set_load_balancer_fragment_node(fragment_json_name.replaceAll("LB$",EMPTY), get_mapping_node_name(processed_fragment_name));
                            minimum_workers = 1; //Load balancer instances do not have a number of workers
                            maximum_workers = 1;
                            fragment_type = FragmentTypes.coordinator_load_balancer;
                            try{
                                Logger.getAnonymousLogger().log(SEVERE,"A load balancer instance was inferred to require deployment on the edge");
                                throw new Exception();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            System.exit(0);

                        }else{
                            minimum_workers = Integer.parseInt(service_json_object.get("minimumWorkers").toString());
                            maximum_workers = Integer.parseInt(service_json_object.get("maximumWorkers").toString());
                            fragment_type = FragmentTypes.application_fragment;
                            edge_version_of_fragment = true;
                            onloadable = true;
                        }
                    }
                }

                JSONArray precedence_dependencies = (JSONArray) service_json_object.get("dependsOn");
                if (precedence_dependencies!=null) {
                    for (Object precedence_dependency_object : precedence_dependencies) {
                        String precedence_dependency_node_HexId = (String) ((JSONObject) precedence_dependency_object).get("dependency");
                        String precedence_dependency = component_node_hexId_name_map.get(precedence_dependency_node_HexId);
                        get_precedence_dependency_hashmap().get(processed_fragment_name).add(precedence_dependency);
                    }
                }


                JSONObject health_check = (JSONObject) service_json_object.get("healthCheck");
                int health_check_interval = Integer.parseInt((String)health_check.get("interval"));
                String health_check_command = (String) health_check.get("args");
                String health_check_url = (String) health_check.get("httpURL");
                if (health_check_command==null || health_check_command.equals(EMPTY)){
                    if (health_check_url!=null && !health_check_url.equals(EMPTY)) {
                        health_check_command = "curl "+health_check_url;
                    }
                }

                JSONObject flavor = (JSONObject) service_json_object.get("flavor");
                int low_cpus_required = Integer.parseInt(flavor.get("vCPUs").toString());
                int low_ram_required = Integer.parseInt(flavor.get("ram").toString());
                int low_disk_required = Integer.parseInt(flavor.get("storage").toString());

                int high_cpus_required = 2 * low_cpus_required;
                int high_ram_required = 2 * low_ram_required;
                int high_disk_required = 2 * low_disk_required;

                int min_cpus_edge=0,min_memory_mb_edge=0,min_storage_gb_edge=0,
                    max_cpus_edge=0,max_memory_mb_edge=0,max_storage_gb_edge=0;

                int min_cpus_cloud=0,min_memory_mb_cloud=0,min_storage_gb_cloud=0,
                    max_cpus_cloud=0,max_memory_mb_cloud=0,max_storage_gb_cloud=0;

                if (edge_version_of_fragment){
                    min_cpus_edge = low_cpus_required;
                    max_cpus_edge = high_cpus_required;
                    min_memory_mb_edge = low_ram_required;
                    max_memory_mb_edge = high_ram_required;
                    min_storage_gb_edge = low_disk_required;
                    max_storage_gb_edge = high_disk_required;
                }
                if (cloud_version_of_fragment){
                    min_cpus_cloud= low_cpus_required;
                    max_cpus_cloud = high_cpus_required;
                    min_memory_mb_cloud = low_ram_required;
                    max_memory_mb_cloud = high_ram_required;
                    min_storage_gb_cloud  = low_disk_required;
                    max_storage_gb_cloud = high_disk_required;
                }

                String public_ssh_key;
                String docker_image = EMPTY, cloud_docker_image = EMPTY, edge_docker_image = EMPTY;
                String docker_registry = EMPTY, cloud_docker_registry = EMPTY, edge_docker_registry = EMPTY;
                String docker_registry_username = EMPTY, cloud_docker_registry_username = EMPTY, edge_docker_registry_username = EMPTY;
                String docker_registry_password = EMPTY, cloud_docker_registry_password = EMPTY, edge_docker_registry_password = EMPTY;
                String edge_docker_command = EMPTY, cloud_docker_command = EMPTY;
                JSONObject environmental_variables_json;

                LinkedHashMap<String, String> cloud_environmental_variables = new LinkedHashMap<>();
                LinkedHashMap<String, String> edge_environmental_variables = new LinkedHashMap<>();
                ArrayList<Port> cloud_ports_properties = new ArrayList<>();
                ArrayList<Port> edge_ports_properties = new ArrayList<>();


                String docker_command=EMPTY;
                JSONArray command_json_array = (JSONArray)service_json_object.get("command");
                if (command_json_array!=null && command_json_array.size()>0) {
                    for (Object key : command_json_array) {
                        if (fragment_type.equals(FragmentTypes.coordinator_load_balancer) && ((String)key).startsWith("--consul.endpoint")){
                                docker_command = docker_command + SPACE + "--consul.endpoint=@variables_CONSUL_MASTER_IP:8500 ";
                        }else{
                            docker_command = docker_command + SPACE + key;
                        }
                    }
                }

                if (edge_version_of_fragment && !docker_command.equals(EMPTY)) {
                    edge_docker_command = docker_command;
                }
                if (cloud_version_of_fragment && !docker_command.equals(EMPTY)) {
                    cloud_docker_command = docker_command;
                }

                docker_image = (String) service_json_object.get("image");
                docker_registry = (String) service_json_object.get("registry");
                docker_registry_username = (String) service_json_object.get("dockerUsername");
                docker_registry_password = decrypt((String) service_json_object.get("dockerPassword"));

                public_ssh_key = (String) service_json_object.get("sshKey");
                //devices_json = (JSONObject) service_json_object.get("devices");
                environmental_variables_json = (JSONObject) service_json_object.get("environmentalVariables");

                if (environmental_variables_json != null && environmental_variables_json.size() > 0) {
                    for (Object json_environmental_variable : environmental_variables_json.keySet()) {
                        String environmental_variable_name = json_environmental_variable.toString();
                        String environmental_variable_value = environmental_variables_json.get(environmental_variable_name).toString();

                        if (environmental_variable_value.startsWith("@") && !environmental_variable_value.equals("@IPV4_PRIVATE") && !environmental_variable_value.equals("@IPV6_PRIVATE") && !environmental_variable_value.equals("@IPV4_PUBLIC")){
                            String hex_id_of_referenced_fragment = environmental_variable_value.replaceFirst("@",EMPTY);
                            String component_node_name = component_node_hexId_name_map.get(hex_id_of_referenced_fragment);
                            String get_property_statement = "{ get_property: ["+
                                    TOSCA_MAPPING_NODE+create_processed_fragment_name(component_node_name)+",host,network,addresses,1] }";
                            environmental_variable_value = get_property_statement;
                        }

                        if (edge_version_of_fragment) {
                            edge_environmental_variables.put(environmental_variable_name, environmental_variable_value);
                        }
                        if (cloud_version_of_fragment) {
                            cloud_environmental_variables.put(environmental_variable_name, environmental_variable_value);
                        }
                    }
                }
                /*
                if (devices_json != null && devices_json.size() > 0){
                    for (Object device_json : devices_json.entrySet()) {
                        Map.Entry<Object, Object> device = (Map.Entry<Object, Object>) device_json;
                        String device_name = (String) device.getKey();
                        String device_path = (String) device.getValue();
                        necessary_devices.put("prestocloud_device_" + device_name, device_path);
                    }
                    if (edge_version_of_fragment){
                        edge_environmental_variables.putAll(necessary_devices);
                    }else if (cloud_version_of_fragment){
                        cloud_environmental_variables.putAll(necessary_devices);
                    }
                }
                */

                JSONArray json_ports_properties = (JSONArray) service_json_object.get("ports");

                if (json_ports_properties != null) {
                    for (int i = 0; i < json_ports_properties.size(); i++) {
                        JSONObject json_ports_property = (JSONObject) json_ports_properties.get(i);
                        //basically, because we assume that the received json object consists only of one key-value pair, we will
                        //need to get the first (and only) element in the keyset and the first (and only) element in the value set
                        Object target_port_property = json_ports_property.get("target");
                        int target_port = (target_port_property != null && target_port_property.toString() != "") ? Integer.parseInt(target_port_property.toString()) : -1;

                        String published_port_property = json_ports_property.get("published").toString();
                        int published_port = (published_port_property != null && published_port_property != "") ? Integer.parseInt(json_ports_property.get("published").toString()) : -1;

                        Port.TransportLayer protocol = json_ports_property.get("protocol").toString().equals("BOTH")? Port.TransportLayer.TCP_UDP:Port.TransportLayer.valueOf(json_ports_property.get("protocol").toString());

                        if (target_port < 0) {
                            Logger.getAnonymousLogger().log(SEVERE, "The target port is invalid for a fragment");
                        } else if (published_port < 0) {
                            Logger.getAnonymousLogger().log(SEVERE, "The published port is invalid for a fragment");
                        } else {
                            if (protocol == null) {
                                Logger.getAnonymousLogger().log(SEVERE, "The protocol is null for a fragment");
                            }
                            if (edge_version_of_fragment) {
                                edge_ports_properties.add(new Port(target_port, published_port, protocol));
                            }
                            if (cloud_version_of_fragment) {
                                cloud_ports_properties.add(new Port(target_port, published_port, protocol));
                            }
                        }
                    }
                }

                //parsing the option to use host networking from the interior of a Docker container
                Object host_mode_networking_object = service_json_object.get("networkModeHost");
                if (host_mode_networking_object!=null) {
                    boolean host_mode_networking = Boolean.parseBoolean(String.valueOf(host_mode_networking_object));
                    if (host_mode_networking) {
                        if (edge_version_of_fragment) {
                            edge_ports_properties.add(new Port(-1, -1, Port.TransportLayer.TCP_UDP));
                        }if (cloud_version_of_fragment) {
                            cloud_ports_properties.add(new Port(-1, -1, Port.TransportLayer.TCP_UDP));
                        }
                    }
                }


                if (cloud_version_of_fragment) {
                    if (docker_image!=null) {
                        cloud_docker_image = docker_image;
                    }
                    if (docker_registry!=null) {
                        cloud_docker_registry = docker_registry;
                    }
                    if (docker_registry_username!=null) {
                        cloud_docker_registry_username = docker_registry_username;
                    }
                    if (docker_registry_password!=null) {
                        cloud_docker_registry_password = docker_registry_password;
                    }
                }
                if (edge_version_of_fragment) {
                    if (docker_image!=null) {
                        edge_docker_image = docker_image;
                    }
                    if (docker_registry!=null) {
                        edge_docker_registry = docker_registry;
                    }
                    if (docker_registry_username!=null) {
                        edge_docker_registry_username = docker_registry_username;
                    }
                    if (docker_registry_password!=null) {
                        edge_docker_registry_password = docker_registry_password;
                    }
                }

                if (fragment_already_has_cloud_details_specified) {
                    synchronized (defined_fragment) {

                        defined_fragment.setMin_cpus_edge(min_cpus_edge);
                        defined_fragment.setMax_cpus_edge(max_cpus_edge);

                        defined_fragment.setMin_memory_mb_edge(min_memory_mb_edge);
                        defined_fragment.setMax_memory_mb_edge(max_memory_mb_edge);

                        defined_fragment.setMin_storage_gb_edge(min_storage_gb_edge);
                        defined_fragment.setMax_storage_gb_edge(max_storage_gb_edge);


                        defined_fragment.setEdge_docker_command(edge_docker_command);
                        defined_fragment.setEdge_docker_image(edge_docker_image);
                        defined_fragment.setEdge_docker_registry(edge_docker_registry);
                        defined_fragment.setEdge_environmental_variables(edge_environmental_variables);
                        defined_fragment.setEdge_ports_properties(edge_ports_properties);
                        defined_fragment.setOnloadable(onloadable);

                        //The number of instances specified, will be added to the number of instances set for the other version of the fragment
                        defined_fragment.setUnconditional_minimum_instances(defined_fragment.getUnconditional_minimum_instances()+minimum_workers);
                        defined_fragment.setCurrent_instances(defined_fragment.getCurrent_instances()+minimum_workers);
                        defined_fragment.setMaximum_instances(defined_fragment.getMaximum_instances()+maximum_workers);
                        //TODO add necessary sensors for the operation of the particular fragment.
                    }
                } else if (fragment_already_has_edge_details_specified) {
                    synchronized (defined_fragment){
                        defined_fragment.setMin_cpus_cloud(min_cpus_cloud);
                        defined_fragment.setMax_cpus_cloud(max_cpus_cloud);

                        defined_fragment.setMin_memory_mb_cloud(min_memory_mb_cloud);
                        defined_fragment.setMax_memory_mb_cloud(max_memory_mb_cloud);

                        defined_fragment.setMin_storage_gb_cloud(min_storage_gb_cloud);
                        defined_fragment.setMax_storage_gb_cloud(max_storage_gb_cloud);

                        defined_fragment.setCloud_docker_command(cloud_docker_command);
                        defined_fragment.setCloud_docker_image(cloud_docker_image);
                        defined_fragment.setCloud_docker_registry(cloud_docker_registry);
                        defined_fragment.setCloud_environmental_variables(cloud_environmental_variables);
                        defined_fragment.setCloud_ports_properties(cloud_ports_properties);
                        defined_fragment.setOffloadable(offloadable);

                        //The number of instances specified, will be added to the number of instances set for the other version of the fragment
                        defined_fragment.setUnconditional_minimum_instances(defined_fragment.getUnconditional_minimum_instances()+minimum_workers);
                        defined_fragment.setCurrent_instances(defined_fragment.getCurrent_instances()+minimum_workers);
                        defined_fragment.setMaximum_instances(defined_fragment.getMaximum_instances()+maximum_workers);
                    }

                } else if (no_fragment_definition_exists) {

                    LinkedHashSet<String> fragment_collocation_set = get_collocation_dependency_hashmap().get(processed_fragment_name);
                    String [] fragment_collocation_array = fragment_collocation_set.toArray(new String [fragment_collocation_set.size()]);
                    LinkedHashSet<String> fragment_antiaffinity_constraints_set= get_anti_affinity_hashmap().get(processed_fragment_name);
                    String [] fragment_antiaffinity_array = fragment_antiaffinity_constraints_set.toArray(new String [fragment_antiaffinity_constraints_set.size()]);

                    LinkedHashSet<String> fragment_precedence_set = get_precedence_dependency_hashmap().get(processed_fragment_name);
                    String [] fragment_precedence_array = fragment_precedence_set.toArray(new String[fragment_precedence_set.size()]);


                    LinkedHashMap<String, String> optimization_variables_hash_map = new LinkedHashMap<>();
                    int optimization_cost_weight=1, optimization_distance_weight=1;
                    if(fragment_optimization_variables.get(processed_fragment_name)!=null){
                        if(fragment_optimization_variables.get(processed_fragment_name).getProvider_friendliness_weights()!=null) {
                            optimization_variables_hash_map = convert_key_value_structure_to_linked_hash_map(fragment_optimization_variables.get(processed_fragment_name).getProvider_friendliness_weights());
                        }

                        optimization_cost_weight = fragment_optimization_variables.get(processed_fragment_name).getCost_weight();
                        optimization_distance_weight = fragment_optimization_variables.get(processed_fragment_name).getDistance_weight();
                    }

                    Fragment fragment = new Fragment(
                            processed_fragment_name,
                            fragment_id,
                            fragment_type,
                            min_cpus_cloud, max_cpus_cloud,
                            min_memory_mb_cloud,max_memory_mb_cloud,
                            min_storage_gb_cloud,max_storage_gb_cloud,
                            min_cpus_edge, max_cpus_edge,
                            min_memory_mb_edge, max_memory_mb_edge,
                            min_storage_gb_edge, max_storage_gb_edge,
                            minimum_workers,
                            maximum_workers,
                            onloadable,
                            offloadable,
                            elasticity_mechanism,
                            necessary_sensors,
                            fragment_collocation_array,
                            fragment_antiaffinity_array,
                            fragment_precedence_array,
                            cloud_docker_image,
                            cloud_docker_registry,
                            cloud_docker_registry_username,
                            cloud_docker_registry_password,
                            cloud_ports_properties,
                            cloud_environmental_variables,
                            cloud_docker_command,
                            edge_docker_image,
                            edge_docker_registry,
                            edge_docker_registry_username,
                            edge_docker_registry_password,
                            edge_ports_properties,
                            edge_environmental_variables,
                            edge_docker_command,
                            optimization_variables_hash_map,
                            optimization_cost_weight,
                            optimization_distance_weight,
                            health_check_interval,
                            health_check_command,
                            public_ssh_key
                    );
                    ui_fragments.add(fragment);
                    add_fragment(fragment);
                    //RestCommunicator.uploadSSHkey(SSH_KEY_PREFIX+processed_fragment_name,public_ssh_key);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return  ui_fragments;
    }

    /**Returns the name of a cloud load balancer of a fragment, and is called when an edge load balancer is detected.
     *The goal is to have only one load balancer and redirect all dependencies and references to it.
     * @param services The components included in the json file
     * @param fragment_json_name The json name for the fragment which is an edge load balancer
     * @return
     */
    private static String identify_possible_duplicate_load_balancer_on_cloud(JSONArray services, String fragment_json_name) {

        for (Object service : services){
            JSONObject local_service_json_object = (JSONObject) service;

            if ((Boolean)(local_service_json_object.get("loadBalancer"))&& fragment_json_name.endsWith("LB")) {
                String trimmed_fragment_json_name = fragment_json_name.replaceAll("(_edge).*[0-9]*(LB)$", EMPTY);
                String possible_alternative_load_balancer_name = (String) ((JSONObject) local_service_json_object).get("componentNodeName");
                if ((possible_alternative_load_balancer_name.replaceAll("(_cloud).*[0-9]*(LB)$", EMPTY)).equals(trimmed_fragment_json_name)) {
                    return (String) local_service_json_object.get("componentNodeName");
                }
            }
        }
        return EMPTY;
    }

    /**
     * This method invokes the other overloaded method, specifying exhaustively all (currently cloud and edge) execution location options
     * @param fragment_json_name The original name from the UI json
     * @return
     */
    public static String get_processed_fragment_name(String fragment_json_name) {
        String processed_fragment_name = get_processed_fragment_name(fragment_json_name,"edge");
        processed_fragment_name = get_processed_fragment_name(processed_fragment_name,"cloud");
        return processed_fragment_name;
    }

    /**
     *
     * @param fragment_json_name The original name from the UI json
     * @param fragment_version Either the cloud or the edge version of a fragment
     * @return
     */
    private static String get_processed_fragment_name(String fragment_json_name, String fragment_version) {
        String processed_fragment_name = fragment_json_name.replaceAll("(_"+fragment_version+")[0-9]+(LB)$","_LB");
        processed_fragment_name = processed_fragment_name.replaceAll("(_"+fragment_version+")[0-9]+$",EMPTY);
        return processed_fragment_name;
    }

    private static LinkedHashSet get_anti_affinity_constraints_set(HashMap component_hexID_name_map, String component_hex_id){
        return get_anti_affinity_hashmap().get(component_hexID_name_map.get(component_hex_id));
    }

    private static LinkedHashSet get_collocation_dependencies_set(HashMap component_hexID_name_map, String component_hex_id){
        return get_collocation_dependency_hashmap().get(component_hexID_name_map.get(component_hex_id));
    }

    public static void calculate_dependencies_of_ui_fragments(Set<Fragment> ui_fragments){
        for (Fragment fragment : ui_fragments) {
            initialize_dependencies_of_fragment(fragment);
        }
    }

    public static String get_next_UI_file_path() {
        File folder = new File(repository_path);
        File[] listOfFiles = folder.listFiles();
        String next_ui_deployment_file_path = "";
        int maximum_ui_deployment_id_encountered = 0;

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
                String file_name = listOfFiles[i].getName();
                if (is_ui_deployment_file_name(file_name)) {
                    int ui_deployment_id = Integer.parseInt(file_name.replace(ui_deployment_file_name_prefix, EMPTY).replace(ui_deployment_file_name_suffix,EMPTY));
                    if (ui_deployment_id > maximum_ui_deployment_id_encountered) {
                        maximum_ui_deployment_id_encountered = ui_deployment_id;
                        next_ui_deployment_file_path = file_name;
                    }
                }
            }
        }

        return repository_path+next_ui_deployment_file_path;
    }
    public static boolean is_ui_deployment_file_name(String file_name) {
        if (file_name.startsWith(ui_deployment_file_name_prefix) && file_name.endsWith(ui_deployment_file_name_suffix)) {
            return true;
        }else{
            return false;
        }
    }


    /**
     * //TODO needs a better implementation, based on the information to be retrieved from the UI
     * @return
     */
    public static FragmentationPolicy get_policy_from_ui() {

        int cost_threshold, time_period;
        JSONObject budget_requirement_json;
        JSONArray globally_excluded_providers,globally_preferred_providers; //This limits the possible providers to the list of preferred providers, and excludes from the possible deployment target regions any providers in the excluded providers list

        String business_goal_string;

        budget_requirement_json = ((JSONObject)ui_deployment_json.get("budgetRequirement"));
        cost_threshold = ((Long)budget_requirement_json.get("costThreshold")).intValue();
        time_period = ((Long)budget_requirement_json.get("timePeriod")).intValue();

        BudgetRequirement budget_requirement = new BudgetRequirement();
        budget_requirement.setCostThreshold(cost_threshold);
        budget_requirement.setTimePeriod(time_period);

        business_goal_string = ((String)ui_deployment_json.get("businessGoal"));

        BusinessGoal business_goal;
        if (business_goal_string.equalsIgnoreCase("cost")) {
            business_goal = new BusinessGoal(business_goal_string, "minimize");
        }else{//should never need to enter here
            business_goal = new BusinessGoal(business_goal_string,"optimize");
        }

        globally_excluded_providers = (JSONArray)ui_deployment_json.get("excludeRegions");
        ArrayList<ProviderRequirement> provider_requirements = new ArrayList<>();
        for(Object excluded_provider : globally_excluded_providers){
            ProviderRequirement excluded_provider_requirement = new ProviderRequirement((String)((JSONObject)excluded_provider).get("name"),false, true);
            provider_requirements.add(excluded_provider_requirement);
        }
        globally_preferred_providers = (JSONArray)ui_deployment_json.get("preferredRegions");
        for(Object preferred_provider : globally_preferred_providers){
            ProviderRequirement preferred_provider_requirement = new ProviderRequirement((String)((JSONObject)preferred_provider).get("name"),true,false);
            provider_requirements.add(preferred_provider_requirement);
        }

        FragmentationPolicy fragmentation_policy = new FragmentationPolicy();
        fragmentation_policy.setBusinessGoals(new ArrayList<BusinessGoal>(){{add(business_goal);}});
        fragmentation_policy.setBudgetRequirement(budget_requirement);
        fragmentation_policy.setProviderRequirements(provider_requirements);

        return fragmentation_policy;
    }


    public static JSONObject getUi_deployment_json() {
        return ui_deployment_json;
    }

    public static void setUi_deployment_json(JSONObject ui_deployment_json) {
        UIJsonParser.ui_deployment_json = ui_deployment_json;
    }


    public static String decrypt(String strToDecrypt){
        String decryptedString = EMPTY;
        if (strToDecrypt!=null) {
            String myKey = "A161EEAC15A02166087D02A6C7C4F79FC82E33133A71AA42778EECB38E51854B";//AgentConfiguration.KYE_TOKEN;
            byte[] key;
            SecretKeySpec secretKey;
            MessageDigest sha = null;
            try {
                key = myKey.getBytes("UTF-8");
                sha = MessageDigest.getInstance("SHA-1");
                key = sha.digest(key);
                key = Arrays.copyOf(key, 16);
                secretKey = new SecretKeySpec(key, "AES");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt.getBytes())));
            } catch (Exception e) {
                Logger.getAnonymousLogger().log(SEVERE, e.getMessage(), e);
                e.printStackTrace();
            }
        }
        return decryptedString;
    }
}
