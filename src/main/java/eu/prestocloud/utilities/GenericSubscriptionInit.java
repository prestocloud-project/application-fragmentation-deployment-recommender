package eu.prestocloud.utilities;

public class GenericSubscriptionInit {
    private static final GenericSubscriber subscriber = new GenericSubscriber();
    public static void main(String[] args){
        try {
            subscriber.subscribe();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
