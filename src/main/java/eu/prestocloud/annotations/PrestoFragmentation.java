package eu.prestocloud.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.none;
import static eu.prestocloud.configuration.Constants.DOCKER_ECHO_COMMAND;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface PrestoFragmentation {
    enum MemoryLoad {
        VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH
    }

    enum CPULoad {
        VERY_LOW,LOW, MEDIUM, HIGH, VERY_HIGH
    }

    enum StorageLoad {
        VERY_LOW,LOW, MEDIUM, HIGH,VERY_HIGH
    }

    enum Elasticity_mechanism{
        faas, jppf, horizontal_load_balanced, simple_horizontal_no_load_balancing, none
    }

    //String policyFile();

    String overloading_tag() default "";

    boolean onloadable() ; //whether it can be executed at the edge

    boolean offloadable() ; //whether it can be executed on the cloud

    int max_instances() default Integer.MAX_VALUE;

    int min_instances() default 1;

    Elasticity_mechanism elasticity_mechanism();

    MemoryLoad memoryLoad();

    CPULoad cpuLoad();

    StorageLoad storageLoad();

    String[] cloud_environmental_variables() default {};
    String[] cloud_ports_properties() default {};
    String cloud_docker_image() default "";
    String cloud_docker_registry() default "" ;
    String cloud_docker_registry_username() default "";
    String cloud_docker_registry_password() default "";
    String cloud_docker_cmd() default "";
    String[] edge_environmental_variables() default {};
    String[] edge_ports_properties() default {};
    String edge_docker_image() default "";
    String edge_docker_registry() default "";
    String edge_docker_registry_username() default "";
    String edge_docker_registry_password() default "";
    String edge_docker_cmd() default "";

    int optimization_cost_weight() default 1;
    int optimization_distance_weight() default 1;
    String [] optimization_providerFriendliness_weights() default {};

    int health_check_interval() default 1;
    String health_check_command() default "";

    String public_ssh_key () default "";

    String[]  sensors_required() default {};
    String[]  precededBy() default {};
    String[]  dependencyOn() default {};
    String[]  antiAffinityTo() default {};
}
