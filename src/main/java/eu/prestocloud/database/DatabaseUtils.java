package eu.prestocloud.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

public class DatabaseUtils {

    public static Connection connection;
    static {
        try {
            //Class.forName("org.hsqldb.jdbc.jdbcDriver");
            connection= DriverManager.getConnection(database_url, database_user, database_password);
        }catch (SQLException s ){
            s.printStackTrace();
        }//catch (ClassNotFoundException c){
        //    c.printStackTrace();
        //}
    }


    public static void update_tosca_deployments_db_table(int next_tosca_id, String processing_state) {

        try {
            //First, consult the database to ascertain if the particular id has already been entered to it
            PreparedStatement prepared_selection_sql_statement = connection.prepareStatement("SELECT ID FROM " + tosca_deployments_db_table_name + " WHERE ID = ?");
            prepared_selection_sql_statement.setInt(1, next_tosca_id);
            ResultSet result = prepared_selection_sql_statement.executeQuery();

            if (result.next()) { //if at least one object is return, this means that this id has already been processed and should not be entered again, so an update statement is needed rather than a create statement
                Statement update_sql_statement = connection.createStatement();
                update_sql_statement.executeUpdate("UPDATE " + tosca_deployments_db_table_name + " SET PROCESSING_STATE = \'" + processing_state + "\' WHERE ID = " + next_tosca_id + ";");
            } else {

                PreparedStatement prepared_insertion_sql_statement = connection.prepareStatement("INSERT INTO " + tosca_deployments_db_table_name + "(ID,PROCESSING_STATE) VALUES(?,?);");

                prepared_insertion_sql_statement.setInt(1, next_tosca_id);
                prepared_insertion_sql_statement.setString(2, processing_state);
                prepared_insertion_sql_statement.executeUpdate();
            }
        }catch (SQLException s){
            Logger.getAnonymousLogger().log(SEVERE,"Could not update the current value of the deployed TOSCA file");
            s.printStackTrace();
            return;
        }
    }

    public static int bootstrap_database_table( String db_table_name, boolean initialize_database) {
        int next_tosca_id = 0;
        try {
            boolean table_exists = locate_existing_table(db_table_name);
            if (initialize_database && table_exists){
                drop_db_table(db_table_name);
            }
            if ((!table_exists) || initialize_database){
                create_db_table(db_table_name);
                return next_tosca_id;
            } else {
                //sql_statement.executeQuery("DELETE FROM ACTIVE_TYPE_LEVEL_TOSCA WHERE "+tosca_id+"=1");

                next_tosca_id = get_current_tosca_deployment_id(connection) + 1;
                while ((next_tosca_id - 1) == -1) {
                    TimeUnit.SECONDS.sleep(5);
                    next_tosca_id = get_current_tosca_deployment_id(connection) + 1;
                    Logger.getAnonymousLogger().log(Level.SEVERE, "Cannot determine the id of the type-level TOSCA which has been deployed");
                }
            }
        } catch (SQLException | InterruptedException s){
            s.printStackTrace();
            return -2;
        }
        return next_tosca_id;
    }

    public static boolean locate_existing_table( String db_table_name) throws SQLException {

        boolean table_exists = false;
        try {
            //locate any existing tables
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, tosca_deployments_db_table_name,
                    new String[]{"TABLE"});
            while (res.next()) {
                table_exists = (res.getString("TABLE_NAME") != null && !res.getString("TABLE_NAME").equals(""));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return table_exists;
    }

    public static void create_db_table( String db_table_name) {
        try {
            Statement sql_statement = connection.createStatement();
            Logger.getAnonymousLogger().log(INFO,"Table does not exist, creating...");
            sql_statement.executeQuery("CREATE TABLE " + db_table_name + "(id INT NOT NULL, PROCESSING_STATE VARCHAR("+number_of_characters_in_sql_varchar+") NOT NULL, PRIMARY KEY (id));");
            Logger.getAnonymousLogger().log(INFO,"Table Created");
        }catch (SQLException s){
            Logger.getAnonymousLogger().log(Level.SEVERE,"Error while trying to create new database table "+db_table_name);
            s.printStackTrace();
        }
    }

    public static void drop_db_table( String db_table_name) {
        try {
            Statement sql_statement = connection.createStatement();
            Logger.getAnonymousLogger().log(INFO,"Table exists, dropped as requested...");
            sql_statement.executeQuery("DROP TABLE " + db_table_name + ";");
            Logger.getAnonymousLogger().log(INFO,"Table dropped");
        }catch (SQLException s){
            Logger.getAnonymousLogger().log(Level.SEVERE,"Error while trying to drop database table "+db_table_name);
            s.printStackTrace();
        }
    }



    public static int get_current_tosca_deployment_id(Connection connection) {
        int current_id = -1;
        try {
            Statement sql_statement = connection.createStatement();
            ResultSet current_tosca_id_result = sql_statement.executeQuery("SELECT * FROM " + tosca_deployments_db_table_name + " WHERE id = (SELECT MAX(id) FROM " + tosca_deployments_db_table_name + ") ;");
            String processing_state = "";
            if (current_tosca_id_result.next()) {
                current_id = current_tosca_id_result.getInt(1);
                processing_state = current_tosca_id_result.getString(2);
                Logger.getAnonymousLogger().log(INFO,"The already existing id is the following: " + current_id);
                Logger.getAnonymousLogger().log(INFO,"The processing state for the particular id is: " + processing_state);
            }
            return current_id;
        }catch (SQLException s){
            s.printStackTrace();
        }
        return current_id;
    }

    public static void backup_map_to_database ( String new_database_table_name, String first_column_name, String second_column_name, Map map) {

        try {
            //Connection connection = DriverManager.getConnection(database_url, database_user, database_password);

            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, new_database_table_name.toUpperCase(),
                    new String[]{"TABLE"});
            boolean table_exists = false, drop_table = true;
            while (res.next()) {
                table_exists = (res.getString("TABLE_NAME") != null && !res.getString("TABLE_NAME").equals(""));
            }


            if (drop_table && table_exists) {
                //Drops the table to start fresh
                Logger.getAnonymousLogger().log(INFO,"Table exists, therefore will be dropped");
                Statement drop_table_sql_statement = connection.createStatement();
                drop_table_sql_statement.executeQuery("DROP TABLE "+ new_database_table_name+" ;"); //Parameter replacing did not work for drop statement
                table_exists = false;
            }

            if (!table_exists) {

                String create_table_query = "CREATE TABLE "+new_database_table_name+" (id INT NOT NULL, "+first_column_name+" VARCHAR("+number_of_characters_in_sql_varchar+") NOT NULL, "+second_column_name+" VARCHAR("+number_of_characters_in_sql_varchar+") NOT NULL, PRIMARY KEY (id));";

                Statement prepared_create_table_sql_statement = connection.createStatement();
                prepared_create_table_sql_statement.executeQuery(create_table_query);


                String insert_query = "INSERT INTO "+new_database_table_name+" (id, "+first_column_name+", "+second_column_name+") VALUES (?,?,?);";
                PreparedStatement prepared_insert_sql_statement = connection.prepareStatement(insert_query);
                int id = 0;
                for (Object entry : map.entrySet()){
                    Map.Entry map_entry = (Map.Entry)entry;

                    String key = map_entry.getKey().toString();
                    String value = map_entry.getValue().toString();
                    prepared_insert_sql_statement.setInt(1,id++);
                    prepared_insert_sql_statement.setString(2,key);
                    prepared_insert_sql_statement.setString(3,value);

                    prepared_insert_sql_statement.executeUpdate();

                }

                Logger.getAnonymousLogger().log(INFO,"The database has been created");
            }
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void retrieve_database_table_contents ( String database_table_name){
        //Statement select_all_statement =new Sta;
        try {

            DatabaseMetaData meta = connection.getMetaData();
            Logger.getAnonymousLogger().log(INFO,"Successfully contacted database");
            ResultSet res = meta.getTables(null, null, database_table_name.toUpperCase(),
                    new String[]{"TABLE"});
            boolean table_exists = false;
            while (res.next()) {
                table_exists = (res.getString("TABLE_NAME") != null && !res.getString("TABLE_NAME").equals(""));
            }
            Logger.getAnonymousLogger().log(INFO,"Determined whether table exists");

            Statement get_columns_statement = connection.createStatement();
            ResultSet columns_result = get_columns_statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'"+tosca_deployments_db_table_name+"\'");

            ArrayList<String> column_names = new ArrayList<>();
            while (columns_result.next()){
                column_names.add(columns_result.getString(4)); //this element contains the name of the database table
            }

            if (table_exists) {
                Statement select_all_statement = connection.createStatement();
                ResultSet result = select_all_statement.executeQuery("SELECT * FROM "+database_table_name);

                //Print the names of the columns
                for (int i=0; i< column_names.size();i++){
                    System.out.print(" "+column_names.get(i)+"\t ");
                }
                Logger.getAnonymousLogger().log(INFO,"");

                while(result.next()){
                    for (int i=1; i<= column_names.size();i++){
                        System.out.print(" "+result.getString(i)+"\t\t ");
                    }
                    Logger.getAnonymousLogger().log(INFO,"");
                }

            }
        }catch (SQLException s){
            s.printStackTrace();
        }
    }

    public static void restore_database_table_to_map( String database_table_name, int source_column_for_keys, int source_column_for_values, HashMap map){
        //Statement select_all_statement =new Sta;
        //Statement select_all_statement =new Sta;
        try {

            DatabaseMetaData meta = connection.getMetaData();
            Logger.getAnonymousLogger().log(INFO,"Successfully contacted database");
            ResultSet res = meta.getTables(null, null, database_table_name.toUpperCase(),
                    new String[]{"TABLE"});
            boolean table_exists = false;
            while (res.next()) {
                table_exists = (res.getString("TABLE_NAME") != null && !res.getString("TABLE_NAME").equals(""));
            }
            Logger.getAnonymousLogger().log(INFO,"Determined whether table exists");
            //print_available_columns(connection,database_table_name);

            if (table_exists) {
                Statement select_all_statement = connection.createStatement();
                ResultSet result = select_all_statement.executeQuery("SELECT * FROM "+database_table_name);

                //  Logger.getAnonymousLogger().log(INFO,column_names.get(source_column_for_keys)+"\t"+column_names.get(source_column_for_values));

                while(result.next()){
                    map.put(result.getString(source_column_for_keys),result.getString(source_column_for_values));
                    Logger.getAnonymousLogger().log(INFO,"");
                }
            }
        }catch (SQLException s){
            s.printStackTrace();
        }
    }

    public static void print_available_columns( String database_table_name) {
        try {
            Statement get_columns_statement = connection.createStatement();

            ResultSet columns_result = get_columns_statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'" + database_table_name + "\'");


            ArrayList<String> column_names = new ArrayList<>();
            while (columns_result.next()) {
                column_names.add(columns_result.getString(4)); //this element contains the name of the database table columns
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
