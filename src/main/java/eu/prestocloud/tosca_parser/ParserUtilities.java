package eu.prestocloud.tosca_parser;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Collectors;

import static eu.prestocloud.configuration.Constants.*;

public class ParserUtilities {
    public static String get_next_instance_tosca_file_path(){
        File folder = new File(repository_path);
        File[] listOfFiles = folder.listFiles();
        String next_instance_tosca_file_path = "";
        int maximum_instance_tosca_id_encountered = 0;

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
                String file_name = listOfFiles[i].getName();
                if (is_instance_tosca_file_name(file_name)) {
                    int instance_tosca_id = Integer.parseInt(file_name.replace(instance_tosca_file_name_prefix, "").replace(".yaml","").replace(".yml",""));
                    if (instance_tosca_id > maximum_instance_tosca_id_encountered) {
                        maximum_instance_tosca_id_encountered = instance_tosca_id;
                        next_instance_tosca_file_path = file_name;
                    }
                }
            }
        }

        return repository_path+next_instance_tosca_file_path;
    }

    public static boolean is_instance_tosca_file_name(String file_name) {
        if (file_name.startsWith(instance_tosca_file_name_prefix) &&
                Arrays.asList(instance_tosca_file_name_suffix).stream().filter(current_yaml_extension -> file_name.endsWith(current_yaml_extension)).collect(Collectors.toList()).size() > 0) {
            //(file_name.endsWith(".yaml") || file_name.endsWith(".yml"))) {
            return true;
        }
        else{
            return false;
        }
    }
}
