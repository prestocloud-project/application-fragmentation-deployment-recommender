package eu.prestocloud.tosca_parser;

import eu.prestocloud.tosca_generator.utility_classes.fragments.Fragment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.fragments_map;
import static eu.prestocloud.tosca_parser.ParserUtilities.get_next_instance_tosca_file_path;

public class SimpleParser {

    public static HashMap<String,String> host_type = new HashMap<>(); //whether the host is installed in the cloud or the edge
    public static HashMap<String,ArrayList<String>> fragment_hosts = new HashMap<>();
    static int [] line_count = {0,1};

    public static void parse(){
        String path = get_next_instance_tosca_file_path();//instance_level_tosca_path;

        // ""C:\\Users\\user\\IdeaProjects\\CodeIntrospector\\policy_files\\";
        File type_level_tosca_file = new File(path);

        BufferedReader br = null;
        FileReader fr = null;

        boolean host_found = false;

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(type_level_tosca_file);
            br = new BufferedReader(fr);

            String sCurrentLine,sNextLine;

            boolean processing_nodes_segment=false;
            boolean inside_processing_node = false;


            while ((sCurrentLine = read_next_actual_tosca_line(br,0)) != null) {
                sCurrentLine = sCurrentLine.trim();
                br.mark(100000);
                if ((sNextLine = read_next_actual_tosca_line(br,1))!=null){
                    sNextLine = sNextLine.trim();
                }

                if (sCurrentLine.equals("node_templates:")) {
                    processing_nodes_segment = true;
                    br.reset();
                    continue;
                }

                if (processing_nodes_segment){

                    String fragment_name;

                    if (sNextLine!=null && sNextLine.startsWith("type: ") && (sNextLine.replaceAll("type: ","").trim().equals(prestocloud_faas_agent) || sNextLine.replaceAll("type: ","").trim().equals(prestocloud_jppf_agent))){

                        fragment_name = sCurrentLine.replaceFirst(TOSCA_MAPPING_NODE,"").replaceAll("_\\d:$","");


                        sCurrentLine= read_next_actual_tosca_line(br).trim();
                        sNextLine= read_next_actual_tosca_line(br).trim();


                        if (sNextLine!=null && sCurrentLine !=null &&
                            sCurrentLine.equals("requirements:") &&
                            sNextLine.startsWith("- host: ")){ // This gets the host on which the fragment will be deployed.

                            host_found = true;
                            String current_host = sNextLine.replaceAll("^- host: ","");

                            ArrayList<String> current_fragment_hosts = fragment_hosts.get(fragment_name);
                            if (current_fragment_hosts == null) {
                                current_fragment_hosts = new ArrayList<>();
                                fragment_hosts.put(fragment_name,current_fragment_hosts);
                            }
                            current_fragment_hosts.add(current_host);

                        }

                        br.reset();
                        continue;
                    }

                    if (sNextLine!=null &&
                            sNextLine.replaceAll("type:","").trim().startsWith(prestocloud_compute)) {

                        String current_host = sCurrentLine.replaceAll(":$","");
                        //String fragment_hosted = fragment_hosts.get(current_host);
                        //read_next_actual_tosca_line(br); //Insert this in case a description field
                        sCurrentLine= read_next_actual_tosca_line(br).trim(); //properties
                        sNextLine= read_next_actual_tosca_line(br).trim();//type
                        if (sCurrentLine != null && sNextLine != null &&
                                sCurrentLine.equals("properties:") && sNextLine.startsWith("type: ")){

                            String execution_location = sNextLine.replaceAll("^type: ","");
                            host_type.put(current_host,execution_location);
                            Logger.getAnonymousLogger().log(Level.INFO,"Added host "+current_host+" to "+execution_location+" hosts");
                        }
                        br.reset();
                        continue;
                    }
                }

                br.reset();
                /*
                if (processing_nodes_segment){
                    if (sCurrentLine.startsWith(TOSCA_PROCESSING_NODE)){
                        fragment_name = sCurrentLine.replaceFirst(TOSCA_PROCESSING_NODE,"").replaceAll("_\\d:$","");
                        inside_processing_node = true;
                        continue;
                    }
                    if (inside_processing_node){
                        if (sCurrentLine.startsWith("occurrences")){
                            String[] occurences_string = sCurrentLine.split(":\\s");
                            Fragment current_fragment = FragmentDataStructures.fragments_map.get(fragment_name);
                            current_fragment.setCurrent_instances(Integer.valueOf(occurences_string[1]));
                        }
                    }
                }*/
            }


            //Updates of the Fragment data maps

            for (Map.Entry<String,ArrayList<String>> fragment_entry: fragment_hosts.entrySet()) {

                String fragment_name = fragment_entry.getKey();
                Fragment current_fragment = fragments_map.get(fragment_name);
                //int current_instances = current_fragment.getCurrent_instances();
                //current_fragment.setCurrent_instances(fragment_instances.get(fragment_name));
                synchronized (current_fragment) {
                    current_fragment.setCurrent_instances(fragment_entry.getValue().size());
                }

                //ArrayList<String> fragment_cloud_hosts = new ArrayList<>();
                //ArrayList<String> fragment_edge_hosts = new ArrayList<>();

                for (String current_host : fragment_entry.getValue()){

                    Logger.getAnonymousLogger().log(Level.INFO, "Found host " + current_host + " for fragment " + fragment_name);

                    String execution_location = host_type.get(current_host);
                    if (execution_location.equals("cloud")) {
                        //fragment_cloud_hosts.add(current_host);
                        current_fragment.increase_cloud_host_instances();
                    } else if (execution_location.equals("edge")) {
                        //fragment_edge_hosts.add(current_host);
                        current_fragment.increase_edge_host_instances();
                    }
                }

                //current_fragment.setCloud_hosts(fragment_cloud_hosts);
                //current_fragment.setEdge_hosts(fragment_edge_hosts);


            }



        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
            host_type = new HashMap<>();
            fragment_hosts = new HashMap<>();
        }

    }


    private static String read_next_actual_tosca_line(BufferedReader buffered_reader) {
        String next_line = null;
        try {
            do {
                next_line = buffered_reader.readLine();
            }while (next_line!=null && (next_line.startsWith("#") || next_line.trim().equals("")) );
        }catch (Exception e){
            e.printStackTrace();
        }
        return next_line;
    }

    private static String read_next_actual_tosca_line(BufferedReader buffered_reader, int line_count_index) {
        String next_line = null;
        try {
            do {
                next_line = buffered_reader.readLine();
                line_count[line_count_index]++;
            }while (next_line!=null && (next_line.trim().startsWith("#") || next_line.trim().equals("")));
        }catch (Exception e){
            e.printStackTrace();
        }
        return next_line;
    }
}
