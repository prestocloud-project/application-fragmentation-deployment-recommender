package eu.prestocloud.annotated_application.test_fragments;


import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.faas;

@OldPrestoFragmentation(policyFile = "demonstrator.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, max_instances = 10, elasticity_mechanism = faas, onloadable = true, offloadable = true)
public class VideoTranscoder {
}
