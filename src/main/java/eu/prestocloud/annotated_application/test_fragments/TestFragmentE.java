package eu.prestocloud.annotated_application.test_fragments;


import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.faas;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, dependencyOn = {"eu.prestocloud.annotated_application.test_fragments.TestFragmentB", "eu.prestocloud.annotated_application.test_fragments.TestFragmentC"}, max_instances = 5,elasticity_mechanism = faas, onloadable = true, offloadable = true)
public class TestFragmentE extends AbstractTask<Object> {
}
