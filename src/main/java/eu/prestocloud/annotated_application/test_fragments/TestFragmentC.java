package eu.prestocloud.annotated_application.test_fragments;


import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.faas;

@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.MEDIUM, storageLoad = OldPrestoFragmentation.StorageLoad.MEDIUM, antiAffinityTo = {"eu.prestocloud.annotated_application.test_fragments.TestFragmentD","eu.prestocloud.annotated_application.test_fragments.TestFragmentH"}, max_instances = 3,elasticity_mechanism = faas, offloadable = false)
public class TestFragmentC extends AbstractTask<Object> {
}
