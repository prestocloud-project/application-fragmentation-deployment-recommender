package eu.prestocloud.annotated_application.test_fragments;


import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.faas;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, max_instances = 5,elasticity_mechanism = faas, offloadable = true)
public class TestFragmentD extends AbstractTask<Object> {
}
