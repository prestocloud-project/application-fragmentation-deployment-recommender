package eu.prestocloud.annotated_application.test_fragments;

import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.HIGH, cpuLoad = OldPrestoFragmentation.CPULoad.VERY_HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.MEDIUM, dependencyOn = {"eu.prestocloud.annotated_application.test_fragments.TestFragmentA"}, max_instances = 4,elasticity_mechanism = jppf, onloadable = true, offloadable = true)
public class TestFragmentB extends AbstractTask<Object> {
}
