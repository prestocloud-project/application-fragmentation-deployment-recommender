package eu.prestocloud.annotated_application.test_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;
import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.VERY_LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW, max_instances = 2,elasticity_mechanism = jppf, offloadable = true)
public class TestFragmentI extends AbstractTask<Object> {
}
