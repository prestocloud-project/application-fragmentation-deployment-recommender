package eu.prestocloud.annotated_application.test_fragments;


import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, max_instances = 5, dependencyOn = {"eu.prestocloud.annotated_application.test_fragments.TestFragmentG"},elasticity_mechanism = jppf, onloadable = false, offloadable = true)
public class TestFragmentF extends AbstractTask<Object> {
}
