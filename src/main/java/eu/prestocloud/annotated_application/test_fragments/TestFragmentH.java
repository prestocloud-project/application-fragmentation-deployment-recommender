package eu.prestocloud.annotated_application.test_fragments;

import org.jppf.node.protocol.AbstractTask;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.VERY_LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW, dependencyOn = {"eu.prestocloud.annotated_application.test_fragments.TestFragmentI", "eu.prestocloud.annotated_application.test_fragments.TestFragmentJ"}, max_instances = 2,elasticity_mechanism = jppf, onloadable = true, offloadable = false)
public class TestFragmentH extends AbstractTask<Object> {
}
