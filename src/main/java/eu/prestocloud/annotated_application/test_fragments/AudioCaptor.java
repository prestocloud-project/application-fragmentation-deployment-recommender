package eu.prestocloud.annotated_application.test_fragments;


import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.faas;

@OldPrestoFragmentation(policyFile = "demonstrator.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.VERY_LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW, max_instances = 10, onloadable = true, offloadable = false, elasticity_mechanism = faas)
public class AudioCaptor {
}
