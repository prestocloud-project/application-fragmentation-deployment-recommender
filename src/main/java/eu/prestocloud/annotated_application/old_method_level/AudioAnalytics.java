package eu.prestocloud.annotated_application.old_method_level;



import eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, max_instances = 5,elasticity_mechanism = jppf, offloadable = true)
public class AudioAnalytics {

    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, overloading_tag = "1",elasticity_mechanism = jppf, offloadable = false, onloadable = true)
	public AudioAnalytics(Object a){

    }

    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, overloading_tag = "2",elasticity_mechanism = jppf, offloadable = false, onloadable = true)
    public AudioAnalytics(){

    }

	@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.HIGH, cpuLoad = OldPrestoFragmentation.CPULoad.HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, dependencyOn = {"AudioAnalytics.detectSilence"}, max_instances = 3,elasticity_mechanism = jppf, offloadable = true)
	public static boolean detectShout(Object a) {
		// ...
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.MEDIUM, storageLoad = OldPrestoFragmentation.StorageLoad.HIGH, dependencyOn = {"AudioAnalytics.getValue","AudioAnalytics.runAlgorithm"}, max_instances = 5,elasticity_mechanism = jppf, offloadable = true)
	public static boolean detectActiveIncidents(Object a) {
		// ....
        Object data = getData(new Object());
		return runAlgorithm(data);
	}


	@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.MEDIUM, max_instances = 4,elasticity_mechanism = jppf, offloadable = true)
	public static Object getData(Object a) {
		// ....
		return new Object();
	}



	@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.MEDIUM, storageLoad = OldPrestoFragmentation.StorageLoad.HIGH, antiAffinityTo = {"AudioAnalytics.detectShout"}, max_instances = 3,elasticity_mechanism = jppf, offloadable = true)
	public static boolean runAlgorithm(Object a) {
		// ....
		return false;
	}


	@OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW, dependencyOn = {"AudioAnalytics.testMethod1", "AudioAnalytics.testMethod2"},elasticity_mechanism = jppf, offloadable = true)
	public static boolean detectSilence(Object a) {
		// ...
		return false;
	}
    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
    public static boolean testMethod1(Object a){
        return false;
    }
    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.VERY_LOW, dependencyOn = {"AudioAnalytics.detectShout","eu.prestocloud.annotated_application.AudioAnalytics_overloaded_1"},elasticity_mechanism = jppf, offloadable = true)
    public static boolean testMethod2 (Object a){
        return false;
    }

	public static boolean doNothing (){
	    return false;
    }
}