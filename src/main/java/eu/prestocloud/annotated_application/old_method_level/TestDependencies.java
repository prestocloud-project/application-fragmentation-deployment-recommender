package eu.prestocloud.annotated_application.old_method_level;

import eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation;
import eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.*;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;


@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.MEDIUM, cpuLoad = CPULoad.HIGH, storageLoad = StorageLoad.LOW,elasticity_mechanism = jppf, offloadable = true)
public class TestDependencies {

    @OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", memoryLoad = MemoryLoad.MEDIUM, cpuLoad = CPULoad.HIGH, storageLoad = StorageLoad.LOW, overloading_tag = "1",elasticity_mechanism = jppf, offloadable = true, onloadable = true)
	public TestDependencies(Object a){

    }

    @OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", memoryLoad = MemoryLoad.MEDIUM, cpuLoad = CPULoad.HIGH, storageLoad = StorageLoad.LOW, overloading_tag = "2",elasticity_mechanism = jppf, offloadable = true, onloadable = true)
    public TestDependencies(){

    }

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.HIGH, cpuLoad = CPULoad.HIGH, storageLoad = StorageLoad.LOW, dependencyOn = {"TestDependencies.B","TestDependencies.E"},elasticity_mechanism = jppf, offloadable = true)
	public boolean A(Object a) {
		// ...
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.MEDIUM, cpuLoad = CPULoad.MEDIUM, storageLoad = StorageLoad.HIGH,elasticity_mechanism = jppf, offloadable = true)
	public boolean B(Object a) {
		// ....
        Object data = C(new Object());
		return D(data);
	}


	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.MEDIUM, dependencyOn = {"TestDependencies.E","TestDependencies.D"},elasticity_mechanism = jppf, offloadable = true)
	public Object C(Object a) {
		// ....
		return new Object();
	}



	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.MEDIUM, cpuLoad = CPULoad.MEDIUM, storageLoad = StorageLoad.HIGH,elasticity_mechanism = jppf, offloadable = true)
	public boolean D(Object a) {
		// ....
		return false;
	}


	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
	public boolean E(Object a) {
		// ...
		return false;
	}
	

    @OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW, dependencyOn = {"TestDependencies.G","TestDependencies.H"},elasticity_mechanism = jppf, offloadable = true)
    public boolean F (Object a){
        return false;
    }

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
	public boolean G (Object a){
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW, dependencyOn = {"TestDependencies.I"},elasticity_mechanism = jppf, offloadable = true)
	public boolean H (Object a){
		return false;
	}
	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW, dependencyOn = {"TestDependencies.J","TestDependencies.K"},elasticity_mechanism = jppf, offloadable = true)
	public boolean I (Object a){
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
	public boolean J (Object a){
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
	public boolean K (Object a){
		return false;
	}


	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW, dependencyOn = {"TestDependencies.M","TestDependencies.C"},elasticity_mechanism = jppf, offloadable = true)
	public boolean L (Object a){
		return false;
	}

	@OldPrestoFragmentation(policyFile = "basic_policy_file.policyfile", onloadable = true, memoryLoad = MemoryLoad.LOW, cpuLoad = CPULoad.LOW, storageLoad = StorageLoad.VERY_LOW,elasticity_mechanism = jppf, offloadable = true)
	public boolean M (Object a){
		return false;
	}

}