package eu.prestocloud.annotated_application.old_method_level;


import eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation;

import static eu.prestocloud.annotated_application.test_fragments.OldPrestoFragmentation.Elasticity_mechanism.jppf;

public class SimpleTest {
    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.HIGH, cpuLoad = OldPrestoFragmentation.CPULoad.HIGH, storageLoad = OldPrestoFragmentation.StorageLoad.LOW, dependencyOn = {"SimpleTest.detectActiveIncidents"}, max_instances = 3,elasticity_mechanism = jppf, offloadable = true)
    public boolean detectShout(Object a) {
        // ...
        return false;
    }

    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.MEDIUM, cpuLoad = OldPrestoFragmentation.CPULoad.MEDIUM, storageLoad = OldPrestoFragmentation.StorageLoad.HIGH, max_instances = 5,elasticity_mechanism = jppf, offloadable = true)
    public boolean detectActiveIncidents(Object a) {
        // ....
        Object data = getData(new Object());
        return true;
    }


    @OldPrestoFragmentation(policyFile = "basic_policy_file_v2.policyfile", onloadable = true, memoryLoad = OldPrestoFragmentation.MemoryLoad.LOW, cpuLoad = OldPrestoFragmentation.CPULoad.LOW, storageLoad = OldPrestoFragmentation.StorageLoad.MEDIUM, max_instances = 4,  antiAffinityTo = {"SimpleTest.detectActiveIncidents"},elasticity_mechanism = jppf, offloadable = true)
    public Object getData(Object a) {
        // ....
        return new Object();
    }
}
