package eu.prestocloud.configuration;

import java.util.UUID;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;

public class Constants {

    //debugging variables
    public static final boolean SIMULATE_NETWORK = false; // Bypasses all network communications through direct method calls - for release, set false

    //topic variables
    //public static final String DEPLOYMENT_PERFORMED = "deployment.ack";
    //public static final String SCALE_OUT_TOPIC = "topology_adaptation.scale_out";
    //public static final String SCALE_IN_TOPIC = "topology_adaptation.scale_in";
    //public static final String EXCLUDED_DEVICES_REQ_TOPIC = "topology_adaptation.excluded_devices.req";
    //public static final String EXCLUDED_DEVICES_ACK_TOPIC = "topology_adaptation.excluded_devices.ack";
    //public static final String FRAGMENT_DATA_TOPIC = "deployment.fragment_hosts";

    //one-time configuration variables
    public static final boolean SSH_KEYS_IN_TOSCA = false;

    //sql-related variables
    public static final String tosca_deployments_db_table_name ="ACTIVE_TYPE_LEVEL_TOSCA";
    public static final int number_of_characters_in_sql_varchar = 100;
    public static final String database_url = "jdbc:hsqldb:file:src\\main\\java\\eu\\prestocloud\\database\\diafdrecom_db";
    public static final String database_user = "tosca_generator";
    public static final String database_password = "";

    //operational logic variables
    public static final String TYPE_LEVEL_TOSCA_SENT_FOR_DEPLOYMENT = "sent_for_deployment";
    public static final String TYPE_LEVEL_TOSCA_DEPLOYED = "DEPLOYED";
    public static final String ERROR_OCCURRED = "ERROR_OCCURRED";

    //file_naming
    public static final String instance_tosca_file_name_prefix = "instance_tosca_";
    public static final String [] instance_tosca_file_name_suffix = {".yaml",".yml"};
    public static final String ui_deployment_file_name_prefix = "ui_deployment_";
    public static final String ui_deployment_file_name_suffix = ".json";

    //configuration variables
    public static String operational_input_mode;
    //public static String use_cjdns_overlay;
    public static String graph_hex_id=UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
    public static String graph_instance_hex_id;
    public static String base_policy_file_path;
    public static String base_generated_tosca_path;
    public static String public_type_level_tosca_path;
    public static String repository_path;
    public static String instance_level_tosca_parser;
    public static String annotated_application_path;
    public static String annotated_application_name;
    public static String type_level_tosca_file_type = ".yaml";
    public static String type_level_tosca_filename_suffix;

    //broker configuration
    public static String broker_IP_address;
    public static String broker_username;
    public static String broker_password;
    public static boolean enforce_ssl;
    public static String key_storage_endpoint;
    //public static String organization_username = EMPTY;
    public static String organization_username;
    public static String key_storage_password;
    public static String vault_address;
    public static String vault_token;
    public static String topic_to_trigger_actual_deployment;
    public static String topic_to_request_excluded_devices;
    public static String topic_to_receive_actual_deployment_state;
    public static String topic_for_application_undeployment;
    public static String topic_to_receive_excluded_devices;
    public static String topic_to_receive_deployment_json;
    public static String topic_for_scale_out_adaptation;
    public static String topic_for_scale_in_adaptation;
    public static String topic_for_fragment_hosting_data;
    public static String [] adaptation_topics;

    //coordinator node configuration
    public static String coordinator_node_docker_image;
    public static String coordinator_node_docker_registry;
    public static String coordinator_node_docker_registry_username;
    public static String coordinator_node_docker_registry_password;
    public static String coordinator_node_ssh_key;
    public static String [] coordinator_node_ports_properties;
    public static String [] coordinator_node_environmental_variables;
    public static String coordinator_node_health_check_command;
    public static int coordinator_node_health_check_interval;

    //misc configuration
    public static String active_policy_file;
    public static int adaptation_interval_seconds;
    public static Boolean initialize_database;
    public static Boolean run_basic_tests;
    public static Boolean use_full_dependency_ordering;

    //policy-file parsing
    public static final String exceptionDescribedInLog = "Exception occurred during policy compilation. See log for details";
    public static final int bufferReaderReadAheadLimit = 100;

    //numerical constants
    public static final int int_infinity=Integer.MAX_VALUE;
    public static final double double_infinity=Double.POSITIVE_INFINITY;

    //performance constants
    public static final int EDGE_CPU_REQUIREMENT_MULTIPLIER = 1;
    public static final int EDGE_RAM_REQUIREMENT_MULTIPLIER = 1;
    public static final int EDGE_DISK_REQUIREMENT_MULTIPLIER = 1;
    //CONSTANT String resources

    public static final String TOSCA_PROCESSING_NODE = "processing_node_";
    public static final String TOSCA_PROCESSING_NODE_DESCRIPTION = "A TOSCA representation of a processing node";
    public static final String TOSCA_MAPPING_NODE = "deployment_node_";
    public static final String TOSCA_DEF_VERSION = "tosca_prestocloud_mapping_1_2";

    public static final String DOCKER_IMAGE_STRING = "docker_img_";
    public static final String DOCKER_ECHO_COMMAND= "echo Hello World";
    public static final String SSH_KEY_PREFIX = "prestocloud_key_";

    //NODE TYPES
    public static final String tosca_compute = "tosca.nodes.Compute";
    public static final String tosca_root = "tosca.nodes.Root";
    public static final String prestocloud_compute = "prestocloud.nodes.compute";
    public static final String prestocloud_abstract = "prestocloud.nodes.abstract";
    public static final String prestocloud_jppf_agent = "prestocloud.nodes.agent.jppf";
    public static final String prestocloud_faas_agent = "prestocloud.nodes.agent.faas";
    public static final String prestocloud_load_balanced_agent = "prestocloud.nodes.agent.loadBalanced";
    public static final String prestocloud_nodes_agent = "prestocloud.nodes.agent";

    public static final String prestocloud_jppf_master = "prestocloud.nodes.master.jppf";
    public static final String prestocloud_load_balancer = "prestocloud.nodes.proxy";
    public static final String prestocloud_faas_proxy = "prestocloud.nodes.proxy.faas";

    //APPLICATION FRAGMENTS
    public static final String prestocloud_generic_fragment = "prestocloud.nodes.fragment";
    public static final String prestocloud_jppf_fragment = "prestocloud.nodes.fragment.jppf";
    public static final String prestocloud_faas_fragment = "prestocloud.nodes.fragment.faas";
    public static final String prestocloud_load_balanced_fragment = "prestocloud.nodes.fragment.loadBalanced";

    //CAPABILITIES

    public static final String tosca_capabilities_root = "tosca.capabilities.Root";
    public static final String tosca_capabilities_container = "tosca.capabilities.Container";
    public static final String tosca_capabilities_network_PortDef = "tosca.datatypes.network.PortDef";
    public static final String prestocloud_fragmentExecution = "prestocloud.capabilities.jppf.fragmentExecution";

    //RELATIONSHIPS
    public static final String tosca_relationships_root = "tosca.relationships.Root";
    public static final String tosca_relationships_hostedOn = "tosca.relationships.HostedOn";
    public static final String prestocloud_relationships_executedBy = "prestocloud.relationships.jppf.ExecutedBy";
    public static final String prestocloud_relationships_connectsTo = "prestocloud.relationships.jppf.ConnectsTo";

    //DATATYPES
    public static final String tosca_datatypes_credential = "tosca.datatypes.Credential";

    //POLICIES
    public static final String prestocloud_collocation_policy = "prestocloud.policies.Collocation";
    public static final String prestocloud_anti_affinity_policy = "prestocloud.policies.AntiAffinity";
    public static final String prestocloud_exclude_devices_policy = "prestocloud.placement.Ban";
    public static final String prestocloud_btrplace_spread_policy = "prestocloud.placement.Spread";
    public static final String prestocloud_btrplace_gather_policy = "prestocloud.placement.Gather";
    public static final String prestocloud_btrplace_precedence_policy = "prestocloud.placement.Precedence";
}
