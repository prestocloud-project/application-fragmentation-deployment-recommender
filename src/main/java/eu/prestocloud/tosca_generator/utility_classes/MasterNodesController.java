package eu.prestocloud.tosca_generator.utility_classes;

import eu.prestocloud.tosca_generator.NodeGenerator;
import eu.prestocloud.tosca_generator.node_types.NodeType;
import eu.prestocloud.tosca_generator.utility_classes.fragments.Fragment;

import java.util.ArrayList;

public class MasterNodesController {

    private static int master_id =0;
    public static final int nodes_per_master = Integer.MAX_VALUE;
    private static ArrayList<NodeType> master_nodes = new ArrayList<>();
    private static NodeType active_master_node = NodeGenerator.create_jppf_master_node("jppf_master_node");
    private static int nodes_supported_by_active_master = 0;

    static{
        master_nodes.add(active_master_node);
    }

    public static String get_coordinating_master_node( Fragment fragment){
        if (nodes_supported_by_active_master<nodes_per_master) {
            nodes_supported_by_active_master++;
        }
        else{
            active_master_node = NodeGenerator.create_jppf_master_node("jppf_master_node");
            master_nodes.add(active_master_node);
        }
        return active_master_node.getName();
    }

    public static ArrayList<NodeType> getMaster_nodes(){
        return master_nodes;
    }


}
