package eu.prestocloud.tosca_generator.utility_classes;


import static eu.prestocloud.configuration.Constants.int_infinity;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;
import static eu.prestocloud.tosca_generator.utility_classes.RelationStrings.equal;
import static eu.prestocloud.tosca_generator.utility_classes.RelationStrings.equal_less_than;
import static eu.prestocloud.tosca_generator.utility_classes.RelationStrings.greater_or_equal;

public class AttributeRange {
    private int lower_bound;
    private int higher_bound;
    private String measurement_unit;
    private final boolean enforce_list_format;
    private final boolean simple_range_format;

    /** This constructor returns a new Attribute range
     */
    public AttributeRange (){
        this(false,false);
    }

    /** This constructor returns a new Attribute range
     * @param enforce_list_format A boolean variable describing if the output will follow strictly the list format or not
     * @param simple_range_format A boolean variable describing if the representation of the range will be simple or including curly braces, 'in_range' statements etc.
    */
    public AttributeRange(boolean enforce_list_format,boolean simple_range_format){
        this.enforce_list_format = enforce_list_format;
        this.simple_range_format = simple_range_format;
    }

    /**
     * This constructor returns a new Attribute range
     * @param lower_bound The integer lower bound of the range
     * @param higher_bound The integer higher bound of the range
     */
    public AttributeRange(int lower_bound,int higher_bound){
        this(lower_bound,higher_bound,EMPTY,false,false);
    }

    public AttributeRange(int lower_bound,int higher_bound,String measurement_unit){
        this(lower_bound,higher_bound,measurement_unit,false,false);
    }

    /**
     * This constructor returns a new Attribute range
     * @param lower_bound The integer lower bound of the range
     * @param higher_bound The integer higher bound of the range
     * @param enforce_list_format A boolean variable describing if the output will follow strictly the list format or not
     * @param simple_range_format A boolean variable describing if the representation of the range will be simple or including curly braces, 'in_range' statements etc.
     */
    public AttributeRange(int lower_bound,int higher_bound,String measurement_unit,boolean enforce_list_format,boolean simple_range_format){
        this.higher_bound = higher_bound;
        this.lower_bound = lower_bound;
        this.measurement_unit = measurement_unit;
        this.enforce_list_format = enforce_list_format;
        this.simple_range_format = simple_range_format;
    }

    public int getLower_bound() {
        return lower_bound;
    }

    public void setLower_bound(int lower_bound) {
        this.lower_bound = lower_bound;
    }

    public int getHigher_bound() {
        return higher_bound;
    }

    public void setHigher_bound(int higher_bound) {
        this.higher_bound = higher_bound;
    }

    public boolean is_list_format_enforced() {
        return enforce_list_format;
    }

    public boolean is_range_format_simple() {
        return simple_range_format;
    }

    //    @Override
//    public String toString(){
//        StringBuilder string_representation  = new StringBuilder();
//        string_representation.append(Formatting.LIST_START).append(lower_bound).append(LIST_SEP).append(higher_bound).append(LIST_END);
//        return string_representation.toString();
//    }


    @Override
    public String toString(){
        return toString(0);
    }

    public String toString(int indentation_level) {
        StringBuilder string_representation  = new StringBuilder(indent(indentation_level));

        if (lower_bound == 0) {
            if (lower_bound != higher_bound) {
                string_representation
                        .append(C_LIST_START).append(SPACE)
                        .append(equal_less_than).append(COLON_DELIMITER).append(SPACE)
                        .append(higher_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                        .append(SPACE).append(C_LIST_END);
            } else {
                System.out.println("Possible error value in input, as both lowest and highest values have been set to 0");
                //string_representation.append(indent(indentation_level)+core_number).append(COLON_DELIMITER).append(higher_bound);
            }
        }
        else
        if (higher_bound == int_infinity){
            string_representation.
                    append(C_LIST_START).append(SPACE)
                    .append(greater_or_equal).append(COLON_DELIMITER).append(SPACE)
                    .append(lower_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                    .append(SPACE).append(C_LIST_END);
        }
        else {
            if ((lower_bound != higher_bound)|| enforce_list_format) {
                if (simple_range_format){
                    string_representation
                            .append(LIST_START).append(SPACE)
                            .append(lower_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                            .append(LIST_SEP).append(SPACE)
                            .append(higher_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                            .append(SPACE).append(LIST_END);
                }
                else {
                    string_representation
                            .append(C_LIST_START).append(SPACE)
                            .append(IN_RANGE).append(COLON_DELIMITER).append(SPACE)
                            .append(LIST_START).append(SPACE)
                            .append(lower_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                            .append(LIST_SEP).append(SPACE)
                            .append(higher_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                            .append(SPACE).append(LIST_END)
                            .append(SPACE).append(C_LIST_END);
                }
            }
            else { //lower_bound equals higher_bound
                string_representation
                        .append(C_LIST_START).append(SPACE)
                        .append(equal).append(COLON_DELIMITER).append(SPACE)
                        .append(lower_bound).append((!measurement_unit.equals(EMPTY))?SPACE:EMPTY).append(measurement_unit)
                        .append(SPACE).append(C_LIST_END);
            }
        }

        return string_representation.toString();
    }
}
