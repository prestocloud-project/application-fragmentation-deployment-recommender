package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

/**
 * This class represents a named DataContainer object, which can contain multiple DataContainer objects.
 */

public class DataContainerList extends DataContainer {
    private String name = "";
    private ArrayList<DataContainer> attributes = new ArrayList<>();

    private String name_prefix = EMPTY;
    private String entry_prefix = EMPTY;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DataContainer> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<DataContainer> attributes) {
        this.attributes = attributes;
    }

    public void add (DataContainer data_container){
        attributes.add(data_container);
    }

    public void add_all_attributes (DataContainerList data_container_list){
        for (DataContainer data_container : data_container_list.getAttributes()){
            attributes.add(data_container);
        }
    }

    public String getEntry_prefix() {
        return entry_prefix;
    }

    public void setEntry_prefix(String entry_prefix) {
        this.entry_prefix = entry_prefix;
    }

    public String getName_prefix() {
        return name_prefix;
    }

    public void setName_prefix(String name_prefix) {
        this.name_prefix = name_prefix;
    }

    @Override
    public String toString(){
        return toString(0);
    }
    @Override
    public String toString(int indentation_level){

        StringBuilder string_representation = new StringBuilder();

        if (name!=null && !name.equals("")) {
            string_representation.append(indent(indentation_level)).append(name_prefix).append(name).append(COLON_DELIMITER).append(NEWLINE);
            indentation_level++;
        }

        if (attributes!=null && attributes.size()>0) {
            if (!entry_prefix.equals(EMPTY)) {
                for (DataContainer attribute : attributes) {
                    string_representation.append(indent(indentation_level)).append(entry_prefix).append(attribute.toString());
                }
            }else {
                for (DataContainer attribute : attributes) {
                    string_representation.append(attribute.toString(indentation_level));
                }
            }
        }
        return string_representation.toString();
    }

}
