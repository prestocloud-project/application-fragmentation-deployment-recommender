package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class StringTupleDataContainer extends DataContainer {
    private String name="";
    private String value="";
    private String name_prefix="";
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName_prefix() {
        return name_prefix;
    }

    public void setName_prefix(String name_prefix) {
        this.name_prefix = name_prefix;
    }

    public StringTupleDataContainer(){

    }
    public StringTupleDataContainer(String name, String value){
        this.name = name;
        this.value = value;
    }

    public StringTupleDataContainer(String name,String value,String name_prefix) {
        this.name = name;
        this.value = value;
        this.name_prefix = name_prefix;
    }

    @Override
    public String toString(){
        return toString(0);
    }
    @Override
    public String toString(int indentation_level) {
        if (name!=null && value !=null && value!="" && name!="") {
            return (indent(indentation_level) + name_prefix + name + COLON_DELIMITER + SPACE + value + NEWLINE);
        }else{
            return "";
        }
    }

}
