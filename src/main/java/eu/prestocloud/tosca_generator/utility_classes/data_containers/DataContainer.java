package eu.prestocloud.tosca_generator.utility_classes.data_containers;

/**
 * This class is the parent type for all data types (containers) used in the context of the TOSCA generator. It requires the implementation of two functions - the empty_container_definition() and the toString(int indentation level).
 */

public abstract class DataContainer {

    @Override
    public String toString(){
        return toString(0);
    }

    public abstract String toString(int indentation_level);

}
