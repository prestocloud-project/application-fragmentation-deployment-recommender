package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import java.util.LinkedHashSet;
import java.util.Set;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class SetDataContainer <T> extends DataContainer {

    protected String name;
    protected LinkedHashSet<T> data = new LinkedHashSet<>();

    private String prefix = "";
    private String entry_prefix  = EMPTY;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<T> getData() {
        return data;
    }

    public void setData(LinkedHashSet<T> attributes) {
        this.data = attributes;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getEntry_prefix() {
        return entry_prefix;
    }

    public void setEntry_prefix(String entry_prefix) {
        this.entry_prefix = entry_prefix;
    }

    public SetDataContainer(LinkedHashSet<T> data , String name, String entry_prefix){
        this.name = name;
        this.data = data;
        this.entry_prefix = entry_prefix;
    }

    public SetDataContainer(LinkedHashSet<T> data , String name){
        this.name = name;
        this.data = data;
        this.entry_prefix = EMPTY;
    }

    public SetDataContainer(){

    }

    public void add (T value){
        data.add(value);
    }


    @Override
    public String toString(){
        return toString(0);
    }

    public String toString(int indentation_level){

        StringBuilder string_representation = new StringBuilder();

        /*Below implementation does not terminate with a NEWLINE (responsibility of the parent object to do this if desired)
        *
        Iterator<Map.Entry<Object, Object>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Object, Object> entry = it.next();
            Object key = entry.getKey();
            Object value = entry.getValue();
            string_representation.append(indent(indentation_level)+key.toString()+COLON_DELIMITER+SPACE+value.toString());
            if (it.hasNext()){
                string_representation.append(NEWLINE);
            }
        }
        *
        */
        //Below implementation does terminate with a NEWLINE
        if (name!=null && name!="") {
            string_representation.append(indent(indentation_level)).append(prefix).append(name).append(COLON_DELIMITER).append(NEWLINE);
            indentation_level++;
        }

        for (Object entry : data) {
            string_representation.append(indent(indentation_level)).append(entry_prefix).append(entry.toString()).append(NEWLINE);
        }

        return string_representation.toString();

    }

}
