package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

/**
 * @name The name of the container
 * @data The data included in the container
 * @prefix The String to be prefixed before the name of the container
 * @entry_prefix The String to be prefixed before each entry of the container
 * @list_element A boolean describing whether the container is part of a list or not
 */
public class  MapDataContainer  <T> extends DataContainer {

    protected String name="";
    protected LinkedHashMap<T, T> data = new LinkedHashMap<>();

    private boolean format_data_single_line = false;
    private boolean brace_format = false;

    private String prefix = EMPTY;
    private String entry_prefix  = EMPTY;

    private boolean list_element = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<T, T> getData() {
        return data;
    }

    public void setData(LinkedHashMap<T, T> attributes) {
        this.data = attributes;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getEntry_prefix() {
        return entry_prefix;
    }

    public void setEntry_prefix(String entry_prefix) {
        this.entry_prefix = entry_prefix;
    }

    public boolean isList_element() {
        return list_element;
    }

    public void setList_element(boolean list_element) {
        this.list_element = list_element;
    }

    public MapDataContainer (LinkedHashMap<T,T> data , String name, String entry_prefix){
        this.name = name;
        this.data = data;
        this.entry_prefix = entry_prefix;
    }

    public MapDataContainer (LinkedHashMap<T,T> data , String name){
        this.name = name;
        this.data = data;
        this.entry_prefix = EMPTY;
    }

    public MapDataContainer(){

    }

    public void add (T name, T value){
        data.put(name,value);
    }

    public void setFormat_data_single_line(boolean format_data_single_line) {
        this.format_data_single_line = format_data_single_line;
    }

    public void setBrace_format(boolean brace_format) {
        this.brace_format = brace_format;
    }
    @Override
    public String toString(){
        return toString(0);
    }

    public String toString(int indentation_level){

        StringBuilder string_representation = new StringBuilder();

        /*Below implementation does not terminate with a NEWLINE (responsibility of the parent object to do this if desired)
        *
        Iterator<Map.Entry<Object, Object>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Object, Object> entry = it.next();
            Object key = entry.getKey();
            Object value = entry.getValue();
            string_representation.append(indent(indentation_level)+key.toString()+COLON_DELIMITER+SPACE+value.toString());
            if (it.hasNext()){
                string_representation.append(NEWLINE);
            }
        }
        *
        */
        //Below implementation does terminate with a NEWLINE
        if (name!=null && !name.equals("")) {
            string_representation.append(indent(indentation_level)).append(prefix).append(name).append(COLON_DELIMITER).append(brace_format?SPACE+C_LIST_START+SPACE:NEWLINE);
            indentation_level++;
        }

        boolean first_map_element = true;

        if (data==null || data.size()==0){
            if (format_data_single_line){
                string_representation.append(C_LIST_END+NEWLINE);
            }else{
                string_representation.append(NEWLINE);
            }
        }

        for (Iterator<T> iterator = data.keySet().iterator(); iterator.hasNext();)/* Map.Entry<T, T> entry : data.entrySet())*/ {
            T current_key = iterator.next();
            T current_value = data.get(current_key);
            if (current_key==null || current_key.toString().equals("")||current_value==null || current_value.toString().equals("")){
                continue;
            }else {
                string_representation.
                        append(format_data_single_line ? EMPTY : indent(indentation_level)).
                        append(list_element && first_map_element ? entry_prefix : list_element ? entry_prefix.replaceAll(".",SPACE):entry_prefix).
                        append(brace_format?quoted(current_key.toString()):current_key.toString()).
                        append(COLON_DELIMITER).
                        append(SPACE).
                        append(brace_format?quoted(current_value.toString()):current_value.toString()).
                        append(format_data_single_line && iterator.hasNext() ? COMMA_DELIMITER+SPACE :
                              !format_data_single_line ? NEWLINE : SPACE+C_LIST_END+NEWLINE);
                first_map_element = false;
            }
        }

        return string_representation.toString();

    }

}
