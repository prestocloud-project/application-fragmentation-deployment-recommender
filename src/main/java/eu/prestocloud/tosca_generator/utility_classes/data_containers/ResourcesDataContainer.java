package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.tosca_generator.policy_file_requirements.AttributeMapping;
import eu.prestocloud.tosca_generator.utility_classes.AttributeRange;

import java.util.LinkedHashMap;

import static eu.prestocloud.tosca_generator.policy_file_requirements.MappingRequirement.translate;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.LIST_PRFX;

public class ResourcesDataContainer extends MapDataContainer {

    private final AttributeRange mem_size;
    private final AttributeRange num_cpus;
    private final AttributeRange storage_size;

    public ResourcesDataContainer(String name,PrestoFragmentation.CPULoad cpuLoad, PrestoFragmentation.MemoryLoad memoryLoad, PrestoFragmentation.StorageLoad storageLoad, AttributeMapping cpuLoadMapping, AttributeMapping ramLoadMapping, AttributeMapping storageLoadMapping) {

        super.name = name;
        super.setEntry_prefix(LIST_PRFX);
        num_cpus = translate(cpuLoad, cpuLoadMapping);
        mem_size = translate(memoryLoad, ramLoadMapping);
        storage_size = translate(storageLoad, storageLoadMapping);
        serializeMapDataContainer();

    }

    public ResourcesDataContainer(String name,AttributeRange num_cpus,AttributeRange mem_size, AttributeRange storage_size){

        super.name = name;
        super.setEntry_prefix(LIST_PRFX);
        this.num_cpus = num_cpus;
        this.mem_size = mem_size;
        this.storage_size = storage_size;
        serializeMapDataContainer();
    }

    public AttributeRange getMem_size() {
        return mem_size;
    }

    public AttributeRange getNum_cpus() {
        return num_cpus;
    }

    public AttributeRange getStorage_size() {
        return storage_size;
    }

    private void serializeMapDataContainer(){

        LinkedHashMap <Object,Object> data = new LinkedHashMap<>();
        data.put("num_cpus",num_cpus);
        data.put("mem_size",mem_size);
        data.put("storage_size",storage_size);
        this.data = data;

    }

}
