package eu.prestocloud.tosca_generator.utility_classes.data_containers;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.indent;

/**
 * This class is used to depict requirements which do not follow the name-value pair system, but rather have a single String as their value
 */
public class StringDataContainer extends DataContainer {

    String value="";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public StringDataContainer(String value){
        this.value = value;
    }

    public StringDataContainer(){

    }

    @Override
    public String toString(){
        return toString(0);
    }

    @Override
    public String toString(int indentation_level) {
        StringBuilder string_representation = new StringBuilder();
        string_representation.append(indent(indentation_level)).append(value).append(NEWLINE);
        return string_representation.toString();
    }
}
