package eu.prestocloud.tosca_generator.utility_classes.data_containers;


import static eu.prestocloud.tosca_generator.utility_classes.Formatting.COLON_DELIMITER;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.indent;

/**
 * The ExtendedDataContainer class is comprised of a regular DataContainer, along with a 'name' String. It can be used as a regular DataContainer, if its name is set to "" or null, since then the toString method will return the output of the toString method of DataContainer.
 */

public class ExtendedDataContainer extends DataContainer {

    private DataContainer data;
    private String extended_data_container_name = "";
    private String prefix = "";

    public ExtendedDataContainer (){

    }

    public ExtendedDataContainer (String extended_data_container_name, DataContainer data){
        this.extended_data_container_name = extended_data_container_name;
        this.data = data;
    }

    public ExtendedDataContainer (String extended_data_container_name){
        this(extended_data_container_name,null);
    }

    public String getExtended_data_container_name() {
        return extended_data_container_name;
    }

    public void setExtended_data_container_name(String extended_data_container_name) {
        this.extended_data_container_name = extended_data_container_name;
    }

    public DataContainer getData() {
        return data;
    }

    public void setData(DataContainer data) {
        this.data = data;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String toString(){
        return toString(0);
    }

    public String toString (int indentation_level){

        StringBuilder string_representation = new StringBuilder();

        if (extended_data_container_name!=null && !extended_data_container_name.equals("")){

            string_representation.append(indent(indentation_level)).append(prefix).append(extended_data_container_name).append
(COLON_DELIMITER).append(NEWLINE);
            indentation_level++;

        }

        string_representation.append(data.toString(indentation_level)); //indentation level has increased if the name is not null or an empty String.

        return string_representation.toString();
    }
}
