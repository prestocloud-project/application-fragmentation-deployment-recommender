package eu.prestocloud.tosca_generator.utility_classes;

public enum OsDistribution {

    coreOS("coreOS"),debian("debian"),raspbian("raspbian"),arch_linux("arch_linux"),red_hat_linux("red_hat_linux"),fedora("fedora"),ubuntu("ubuntu"),windows_xp("windows_xp"),windows_vista("windows_vista"),windows_7("windows_7"),windows_8("windows_8"),windows_8_1("windows_8.1"),windows_10("windows_10"),snow_leopard("snow_leopard");

    private final String fieldDescription;

    OsDistribution(String value) {
        fieldDescription = value;
    }

    @Override
    public String toString() {
        return fieldDescription;
    }
}
