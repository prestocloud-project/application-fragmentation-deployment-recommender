package eu.prestocloud.tosca_generator.utility_classes;

public enum OsArchitecture {
    i386("i386"),x86_64("x86_64"),arm64("arm64"),armel("armel"),armhf("armhf"),mips("mips"),mips64el("mips64el"),mipsel("mipsel"),ppc64el("ppc64el"),s390ex("s390ex");
    private final String fieldDescription;

    OsArchitecture(String value) {
        fieldDescription = value;
    }

    @Override
    public String toString() {
        return fieldDescription;
    }
}
