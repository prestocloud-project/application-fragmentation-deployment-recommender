package eu.prestocloud.tosca_generator.utility_classes;

import java.util.ArrayList;

/**
 * This class holds the optimization variables which can be defined for each fragment.
 * The provider_friendliness_weights field is a String array with pairs containing the names of the providers and their respective friendliness weights
 * The cost weight reflects the importance of the cost minimization
 * The distance weight reflects the importance of the minimization of the distance between the deployment of the fragment and the edge cloud centroid
 */
public class OptimizationVariables {

    private ArrayList<String> provider_friendliness_weights;
    private int cost_weight;
    private int distance_weight;


    public OptimizationVariables (ArrayList<String> provider_friendliness_weights, int cost_weight, int distance_weight){
        this.provider_friendliness_weights = provider_friendliness_weights;
        this.cost_weight = cost_weight;
        this.distance_weight = distance_weight;
    }

    public ArrayList<String> getProvider_friendliness_weights() {
        return provider_friendliness_weights;
    }

    public void setProvider_friendliness_weights(ArrayList<String> provider_friendliness_weights) {
        this.provider_friendliness_weights = provider_friendliness_weights;
    }

    public int getCost_weight() {
        return cost_weight;
    }

    public void setCost_weight(int cost_weight) {
        this.cost_weight = cost_weight;
    }

    public int getDistance_weight() {
        return distance_weight;
    }

    public void setDistance_weight(int distance_weight) {
        this.distance_weight = distance_weight;
    }
}
