package eu.prestocloud.tosca_generator.utility_classes;

public class SynchronizedBoolean extends Object {
    private Boolean value = false;
    public void setFalse(){
        this.value = false;
    }
    public void setTrue(){
        this.value = true;
    }

    public Boolean getValue(){
        return value;
    }

    public SynchronizedBoolean(){

    }
    public SynchronizedBoolean(Boolean value){
        this.value = value;
    }


}
