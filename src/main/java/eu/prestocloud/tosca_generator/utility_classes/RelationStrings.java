package eu.prestocloud.tosca_generator.utility_classes;

public class RelationStrings {
    public static final String greater_than = "greater_than";
    public static final String greater_or_equal = "greater_or_equal";
    public static final String equal = "equal";
    public static final String less_than = "less_than";
    public static final String equal_less_than = "equal_less_than";
}
