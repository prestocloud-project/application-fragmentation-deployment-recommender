package eu.prestocloud.tosca_generator.utility_classes;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.COLON_DELIMITER;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.SPACE;

public class Port {
    private final int target_port;
    private final int published_port;
    public enum TransportLayer {UDP,TCP,TCP_UDP}
    private final TransportLayer protocol;

    public int getTarget_port() {
        return target_port;
    }

    public int getPublished_port() {
        return published_port;
    }

    public TransportLayer getProtocol() {
        return protocol;
    }

    public Port (int target_port, int published_port, TransportLayer protocol){
        this.target_port = target_port;
        this.published_port = published_port;
        this.protocol = protocol;
    }


    @Override
    public String toString(){
        StringBuilder string_representation = new StringBuilder();
        string_representation.append("target").append(COLON_DELIMITER).append(target_port).append(SPACE).append(NEWLINE);
        string_representation.append("published").append(COLON_DELIMITER).append(published_port).append(SPACE).append(NEWLINE);
        string_representation.append("protocol").append(COLON_DELIMITER).append(protocol).append(SPACE).append(NEWLINE);
        return string_representation.toString();
    }
}
