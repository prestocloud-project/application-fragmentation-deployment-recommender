package eu.prestocloud.tosca_generator.utility_classes.dependencies;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.ForwardPrecedenceDependencies.clean_precedence_dependencies;

/**
 * This class contains programming logic which creates reverse dependencies, i.e if fragment A depends on B, then B is
 * also deemed to be dependent on A.
 */
public class BidirectionalCollocationDependencies {

    public static DataContainerList proximity_policies;
    public static final boolean DEBUG_MESSAGES = true;

    public static DataContainerList perform_dependency_processing() {
        set_visited_dependencies(new LinkedHashSet<>());
        set_explored_dependencies(new LinkedHashSet<>());
        set_included_dependencies(new LinkedHashSet<>());
        prepare_dependencies();
        proximity_policies = ForwardCollocationDependencies.perform_dependency_processing();
        return proximity_policies;
    }

    private static LinkedHashMap<String,LinkedHashSet<String>> additional_method_dependencies = new LinkedHashMap<>();

    /**
     * This method creates reverse dependencies based on the existing dependencies.
     */
    private static void prepare_dependencies() {
        for (HashMap.Entry<String,LinkedHashSet<String>> fragment: get_collocation_dependency_hashmap().entrySet()){
            String fragment_name = fragment.getKey();
            for (String dependency: fragment.getValue()){
                LinkedHashSet previous_dependencies_of_dependency = get_collocation_dependency_hashmap().get(dependency);
                try{
                    previous_dependencies_of_dependency.add(fragment_name); //adding the reverse dependency
                    additional_method_dependencies.put(dependency, previous_dependencies_of_dependency);
                }catch (Exception e){
                    Logger.getAnonymousLogger().log(Level.SEVERE,"We tried to insert the dependency "+fragment_name + ", to fragment "+dependency+" but no such fragment (or dependency) could be found. Please check the code annotations to ensure that all fragments referred in them, exist.");
                    e.printStackTrace();
                }
            }
        }

        for (HashMap.Entry<String,LinkedHashSet<String>> fragment : get_collocation_dependency_hashmap().entrySet()){
            LinkedHashSet <String> additional_dependencies_for_fragment = additional_method_dependencies.get(fragment.getKey());
            if (additional_dependencies_for_fragment!=null) {
                fragment.getValue().addAll(additional_dependencies_for_fragment);
            }
        }

    }

    public static void cleanDependencies(){
        clean_precedence_dependencies();
        additional_method_dependencies.clear();
    }
}