package eu.prestocloud.tosca_generator.utility_classes.dependencies;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.ExtendedDataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.MapDataContainer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.prestocloud_btrplace_precedence_policy;
import static eu.prestocloud.configuration.Constants.use_full_dependency_ordering;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.ForwardCollocationDependencies.BTRPLACE_CONSTRAINTS;

public class ForwardPrecedenceDependencies {
    private static boolean DEBUG_MESSAGES = false;
    private static LinkedHashSet<String> precedence_dependencies = new LinkedHashSet<>();
    private static LinkedHashSet<String> visited_simple_dependencies = new LinkedHashSet<>();

    /**
     * This method performs a processing on precedence dependencies. It first adds to a global LinkedHashSet the fragments which have no precedence dependencies, and then the fragments whose precedence dependencies are already added to the global LinkedHashSet. The process stops when all  code fragments have been added. In order to retrieve the fragments which have no precedence dependencies, the fragments are traversed using DFS algorithm. The method currently returns a single precedence policy.
     * @return A DataContainerList object is returned, containing the calculated precedence policies
     */
    public static DataContainerList perform_dependency_processing () {

        set_visited_dependencies(new LinkedHashSet<>());
        set_explored_dependencies(new LinkedHashSet<>());
        set_included_dependencies(new LinkedHashSet<>());

        //1. Calculate the precedence dependencies
        DataContainerList precedence_policies = new DataContainerList();
        int precedence_group_counter = 0;
        HashSet<LinkedHashSet<String>> precedence_dependencies_per_fragment = new HashSet<>(); //only used when partial dependency ordering is desired

        //in this for loop, each call to the dfs_traverse_precedence_dependencies function could create a separate independent precedence policy group (by properly modifying the function to accept an arraylist containing all fragments added until now)
        for (HashMap.Entry<String, LinkedHashSet<String>> fragment : get_precedence_dependency_hashmap().entrySet()) {
            if (use_full_dependency_ordering) {
                dfs_traverse_precedence_dependencies(fragment.getKey());
            }else{ //partial dependency ordering
                //set_visited_dependencies(new LinkedHashSet<>());
                //while (!get_explored_dependencies().contains(fragment.getKey())) {
                    precedence_dependencies.clear();
                    dfs_traverse_precedence_dependencies(fragment.getKey());
                    precedence_dependencies_per_fragment.add((LinkedHashSet<String>) precedence_dependencies.clone());
                //}
                //get_visited_dependencies().remove(fragment.getKey()); //remove the fragment from the list of visited dependencies, to allow other fragments to point to it as a dependency. Since the dependencies of this fragment have been marked as visited, other fragments pointing to it will only use the name of this fragment and will not repeat the whole dependency ordering again
            }
        }

        for (HashMap.Entry<String, LinkedHashSet<String>> fragment : get_precedence_dependency_hashmap().entrySet()) {
            if (!use_full_dependency_ordering){
                while (!get_explored_dependencies().contains(fragment.getKey())) {
                    precedence_dependencies.clear();
                get_simple_dependencies(fragment.getKey());
                precedence_dependencies_per_fragment.add((LinkedHashSet<String>) precedence_dependencies.clone());
                }
            }

        }

            //2. Create the precedence dependency String group ([a,b,c,d])
        if (use_full_dependency_ordering) {
            String precedence_group = "";
            int counter = 1;
            for (String fragment_name : precedence_dependencies) {

                if (counter == precedence_dependencies.size()) { //don't append a comma to the last element of the policy list
                    precedence_group = precedence_group + fragment_name;
                } else {
                    precedence_group = precedence_group + fragment_name + ",";
                }
                counter++;
            }

            //3. Create the precedence policy

            ExtendedDataContainer precedence_policy = get_precedence_policy(precedence_group_counter++, precedence_group, prestocloud_btrplace_precedence_policy);

            if (precedence_policy != null) {
                precedence_policies.add(precedence_policy);
            } else {
                precedence_group_counter--;
            }
        }else {

            for (LinkedHashSet<String> fragment_whose_dependencies_are_examined : precedence_dependencies_per_fragment) {

                String precedence_group = "";
                int counter = 1;

                LinkedHashSet<String> fragment_precedence_dependencies = fragment_whose_dependencies_are_examined;
                if (fragment_precedence_dependencies.size()>1) { //there is no reason to create a precedence policy if only one element is to be specified
                    for (String fragment_name : fragment_precedence_dependencies) {

                        if (counter == fragment_precedence_dependencies.size()) { //don't append a comma to the last element of the policy list
                            precedence_group = precedence_group + fragment_name;
                        } else {
                            precedence_group = precedence_group + fragment_name + ",";
                        }
                        counter++;
                    }
                    //3. Create the precedence policy

                    ExtendedDataContainer precedence_policy = get_precedence_policy(precedence_group_counter++, precedence_group, prestocloud_btrplace_precedence_policy);

                    if (precedence_policy != null) {
                        precedence_policies.add(precedence_policy);
                    } else {
                        precedence_group_counter--;
                    }
                }
            }
        }

        return precedence_policies;
    }

    private static void dfs_traverse_precedence_dependencies(String fragment_name){

        if (!get_visited_dependencies().contains(fragment_name)) {
            get_visited_dependencies().add(fragment_name);
            int number_of_simple_dependencies = 0;

            LinkedHashSet<String> fragment_precedence_dependencies = get_precedence_dependency_hashmap().get(fragment_name);

            //In the case when full dependency ordering is not desired, start processing first the longest dependencies (exclude simple dependencies - dependencies which have no dependencies of their own)
            try{
                for (String precedence_dependency_fragment_name : fragment_precedence_dependencies) {
                    if (((!use_full_dependency_ordering)  && get_precedence_dependency_hashmap().get(precedence_dependency_fragment_name).size()>0)
                            || use_full_dependency_ordering){
                        dfs_traverse_precedence_dependencies(precedence_dependency_fragment_name);
                    }
                }
            }catch (NullPointerException n){
                Logger.getAnonymousLogger().log(Level.SEVERE,"Fragment "+fragment_name+" has not been registered");
            }

            //once all precedence dependencies of the particular fragment have been added to the HashSet, the fragment itself is added. The fragment is explored if no additional simple dependencies exist
            //if (use_full_dependency_ordering) {
                //precedence_dependencies.add(fragment_name);
            //}
            //get_explored_dependencies().add(fragment_name);
        }
        precedence_dependencies.add(fragment_name);
    }

    private static void get_simple_dependencies(String fragment_name){
            LinkedHashSet<String> fragment_precedence_dependencies = get_precedence_dependency_hashmap().get(fragment_name);
            if (!use_full_dependency_ordering) {
                for(String fragment_dependency : fragment_precedence_dependencies){
                    if ((get_precedence_dependency_hashmap().get(fragment_dependency).size() == 0) && !visited_simple_dependencies.contains(fragment_dependency)/*&& !get_visited_dependencies().contains(fragment_dependency)*/)
                    {
                        get_visited_dependencies().add(fragment_dependency);
                        visited_simple_dependencies.add(fragment_dependency);
                        precedence_dependencies.add(fragment_dependency);
                        precedence_dependencies.add(fragment_name);
                        return;
                    }
                }
                visited_simple_dependencies.clear();
            }


            precedence_dependencies.add(fragment_name);
            get_explored_dependencies().add(fragment_name);
    }

    /**
     * The method below can only process precedence dependency groups which are guided towards one direction (one-way) e.g A->(B,C->D), which will yield group [A,B,C,D]. It cannot process however two-way dependency groups e.g A->B<-C will result in two policies, [A,B] and [C,B].
     * @return
     */
    public static DataContainerList perform_one_way_dependency_processing (){

        int precedence_group_counter = 0;
        DataContainerList precedence_policies = new DataContainerList();

        set_visited_dependencies(new LinkedHashSet<>());
        set_explored_dependencies(new LinkedHashSet<>());
        set_included_dependencies(new LinkedHashSet<>());

        for (HashMap.Entry<String, LinkedHashSet<String>> fragment : get_precedence_dependency_hashmap().entrySet()) {
            if (!(get_visited_dependencies().contains(fragment.getKey()))) {
                String fragment_name = fragment.getKey();

                //save all current first-level dependencies to a new LinkedHashSet.
                LinkedHashSet<String> first_level_precedence_dependencies = new LinkedHashSet<>();
                first_level_precedence_dependencies.addAll(fragment.getValue());

                get_visited_dependencies().add(fragment_name);
                //fragment.getValue().addAll(get_additional_precedence_dependencies(first_level_precedence_dependencies));

                //Traverse all current first-level dependencies, adding more as they appear *directly* to the precedence dependency hashmap (thus tackling concurrent modification problems, if changes were made to the new LinkedHashSet)

                if (DEBUG_MESSAGES) {
                    Logger.getAnonymousLogger().log(Level.INFO, "Processing fragment " + fragment_name + " ...");
                    Logger.getAnonymousLogger().log(Level.INFO, "Fragment " + fragment_name + " has these dependencies: " + first_level_precedence_dependencies);
                }

                for (String precedence_dependency: first_level_precedence_dependencies){

                    LinkedHashSet <String> additional_precedence_dependencies = get_additional_precedence_dependencies(precedence_dependency);

                    if (DEBUG_MESSAGES) {
                        Logger.getAnonymousLogger().log(Level.INFO, "The fragment has these additional dependencies from fragment " + precedence_dependency + " :" + additional_precedence_dependencies);
                    }

                    additional_precedence_dependencies.addAll(fragment.getValue()); //add to the beginning the additional dependencies since these should be first instantiated for the already existing dependencies to be started.
                    fragment.setValue(additional_precedence_dependencies);

                    if (DEBUG_MESSAGES) {
                        Logger.getAnonymousLogger().log(Level.INFO,"The intermediate dependencies of the fragment are the following: " +fragment.getValue()+"\n");
                    }
                }

                if (DEBUG_MESSAGES) {
                    Logger.getAnonymousLogger().log(Level.INFO, "The final dependencies of the fragment are the following: " + fragment.getValue() + "\n");
                }
                get_explored_dependencies().add(fragment_name);
            }
        }



        if (BTRPLACE_CONSTRAINTS){
            for (HashMap.Entry<String,LinkedHashSet<String>> precedence_dependency : get_precedence_dependency_hashmap().entrySet()){
                if (!get_included_dependencies().contains(precedence_dependency.getKey())) {
                    String precedence_group = get_precedence_group(precedence_dependency);
                    ExtendedDataContainer precedence_policy = get_precedence_policy(precedence_group_counter++, precedence_group, prestocloud_btrplace_precedence_policy);

                    if (precedence_policy != null) {
                        precedence_policies.add(precedence_policy);
                    } else {
                        precedence_group_counter--;
                    }
                }

            }
        }
        return precedence_policies;
    }

    /**
     * This method is complementary to the `perform_one_way_dependency_processing` method above. It retrieves one-way additional dependencies for fragments.
     * @param first_level_fragment_dependency The name of the fragment for which to find additional dependencies
     * @return Returns a LinkedHashSet containing additional precedence dependencies
     */
    private static LinkedHashSet<String> get_additional_precedence_dependencies(String first_level_fragment_dependency) {

        LinkedHashSet<String> additional_precedence_dependencies = new LinkedHashSet<>();


        if (first_level_fragment_dependency != null && !first_level_fragment_dependency.equals("")) {
                if (!(get_visited_dependencies().contains(first_level_fragment_dependency))) {
                    get_included_dependencies().add(first_level_fragment_dependency);
                    get_visited_dependencies().add(first_level_fragment_dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = get_precedence_dependencies(first_level_fragment_dependency);
                    try {
                        for (String dependency_of_dependency : first_level_dependencies_of_dependency) {
                            additional_precedence_dependencies.addAll(get_additional_precedence_dependencies(dependency_of_dependency));
                            if (DEBUG_MESSAGES) {
                                Logger.getAnonymousLogger().log(Level.INFO, "A dependency of dependency : " + dependency_of_dependency + " and the additional deps : " + additional_precedence_dependencies);
                            }
                        }

                    } catch (NullPointerException n) {
                        Logger.getAnonymousLogger().log(Level.SEVERE, "An unknown fragment was referenced during precedence dependency handling. Please make sure that the fragment has been annotated, and that it has been included for processing");
                        n.printStackTrace();
                        System.exit(0);
                    }

                    get_explored_dependencies().add(first_level_fragment_dependency);
                    //Add the first-level dependencies of the dependency to the additional dependencies list to be returned
                    additional_precedence_dependencies.addAll(first_level_dependencies_of_dependency);


                    if (DEBUG_MESSAGES) {
                        Logger.getAnonymousLogger().log(Level.INFO, "Dependencies calculated on *if* for fragment " + first_level_fragment_dependency + " : " + additional_precedence_dependencies);
                    }

                } else if (get_explored_dependencies().contains(first_level_fragment_dependency)) {
                    get_included_dependencies().add(first_level_fragment_dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = get_precedence_dependencies(first_level_fragment_dependency);
                    additional_precedence_dependencies.addAll(first_level_dependencies_of_dependency);
                    if (DEBUG_MESSAGES) {
                        Logger.getAnonymousLogger().log(Level.INFO, "Dependencies calculated on *else if* for fragment " + first_level_fragment_dependency + " : " + additional_precedence_dependencies);
                    }
                }
            }

        return additional_precedence_dependencies;
    }

    private static LinkedHashSet<String> get_precedence_dependencies (String dependency) {
        //consult the main hashmap here and return a meaningful value
        return get_precedence_dependency_hashmap().get(dependency);
    }


    private static String get_precedence_group(HashMap.Entry<String, LinkedHashSet<String>> fragment_dependency_entry) {

        String precedence_group = "";
        if (DEBUG_MESSAGES) {
            System.out.println("- The currently examined fragment is " + fragment_dependency_entry.getKey());
        }

        //Add the name of the fragment to the end of the precedence dependencies
        //(fragments which should be firstly installed before installing this fragment),
        // but ONLY if precedence dependencies exist.

        if (fragment_dependency_entry.getValue().size()>0) {
            fragment_dependency_entry.getValue().add(fragment_dependency_entry.getKey());
        }

        int dependency_counter = 0;

        for (String precedence_dependency : fragment_dependency_entry.getValue()) { //iterate through the LinkedHashSet containing the fragment dependencies
            if (dependency_counter > 0) {
                precedence_group = precedence_group + ", " + precedence_dependency;
            } else {
                precedence_group = precedence_dependency;
            }
            dependency_counter++;
        }

        if (DEBUG_MESSAGES) {
            System.out.println("Dependency group:\n" + precedence_group + "\n");
        }
        return precedence_group;
    }




    public static ExtendedDataContainer get_precedence_policy(int precedence_group_counter, String dependency_group, String policy_type) {

        if (dependency_group != null && !dependency_group.equals("")) {

            String collocation_policy_name = "precedence_policy_group_" + precedence_group_counter;
            ExtendedDataContainer collocation_policy = new ExtendedDataContainer(collocation_policy_name);
            MapDataContainer collocation_policy_data = new MapDataContainer();
            collocation_policy_data.add("type", policy_type);
            collocation_policy_data.add("targets", LIST_START + SPACE + dependency_group + SPACE + LIST_END);
            collocation_policy.setPrefix(LIST_PRFX);
            collocation_policy.setData(collocation_policy_data);

            return collocation_policy;
        }
        return null;
    }
    public static void clean_precedence_dependencies(){
        precedence_dependencies.clear();
    }

}
