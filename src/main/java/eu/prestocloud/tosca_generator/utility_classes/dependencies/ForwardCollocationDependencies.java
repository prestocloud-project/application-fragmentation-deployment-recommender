package eu.prestocloud.tosca_generator.utility_classes.dependencies;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.ExtendedDataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.MapDataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.SetDataContainer;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class ForwardCollocationDependencies {


    public static final boolean DEBUG_MESSAGES = true;
    public static final boolean BTRPLACE_CONSTRAINTS = true;

    public static DataContainerList perform_dependency_processing() {
        /**
         *Below follows the logic for the processing of developer-annotated dependencies.
         *
         * During the processing of each annotated method in every class, we have created two preliminary hashmaps - method dependency hashmap and method anti-dependency hashmap - reflecting dependencies and anti-affinity statements respectively.
         *
         * Now, we seek to extend those data structures, so that their information gets depicted in TOSCA, in a format useable
         * by BtrPlace. For this reason:
         *
         * - Concerning dependencies, for each fragment we collect every dependency of its dependencies in a large group, and
         * so define different dependency groups.
         * - Concerning anti-dependencies, for each fragment we collect every dependency of its *anti-dependencies* in an
         * large anti-dependency group and so define different anti-dependency groups.
         *
         * Subsequently, the hashmaps send their data to properly created Strings which reflect the TOSCA groups to be created.
         */

        int collocation_group_counter = 0;
        int anti_affinity_group_counter = 0;
        DataContainerList proximity_policies = new DataContainerList();

        set_visited_dependencies(new LinkedHashSet<>());
        set_explored_dependencies(new LinkedHashSet<>());
        set_included_dependencies(new LinkedHashSet<>());

        for (HashMap.Entry<String, LinkedHashSet<String>> fragment : get_collocation_dependency_hashmap().entrySet()) {
            if (!(get_visited_dependencies().contains(fragment.getKey()))) {
                String fragment_name = fragment.getKey();
                LinkedHashSet<String> first_level_fragment_dependencies = fragment.getValue();
                get_visited_dependencies().add(fragment_name);
                fragment.getValue().addAll(get_additional_fragment_dependencies(first_level_fragment_dependencies));
                get_explored_dependencies().add(fragment_name);
            }
        }

        //To handle anti-dependencies, the visited and explored anti-dependencies are used only in the get_additional_method_anti_dependencies method, as they basically track and merge dependencies of the dependencies. The logic here is to get the dependencies of the dependencies for each fragment, but without merging in the highest level.

        for (HashMap.Entry<String, LinkedHashSet<String>> fragment : get_anti_affinity_hashmap().entrySet()) {
            String fragment_name = fragment.getKey();
            LinkedHashSet<String> first_level_fragment_anti_dependencies = fragment.getValue();
            fragment.getValue().addAll(get_additional_fragment_anti_dependencies(first_level_fragment_anti_dependencies));
            try {
                if (fragment.getValue().contains(fragment_name)) throw new Exception();
            } catch (Exception e) {
                Logger logger = Logger.getAnonymousLogger();
                logger.log(Level.SEVERE, "A parsing error occurred. You have specified (directly or most likely indirectly) that fragment " + fragment_name + " should not be collocated with itself");
                e.printStackTrace();
                System.exit(0);
            }
        }


        //The TOSCA representation of the dependency and anti-affinity groups
        ExtendedDataContainer collocation_policy, anti_affinity_policy;

        for (HashMap.Entry<String, LinkedHashSet<String>> fragment_dependency : get_collocation_dependency_hashmap().entrySet()) {
            if (!get_included_dependencies().contains(fragment_dependency.getKey())) { //print only groups for methods which are not included in any other sub-group
                String collocation_group = get_collocation_group(fragment_dependency);
                collocation_policy = BTRPLACE_CONSTRAINTS ? get_collocation_policy(collocation_group_counter++, collocation_group, prestocloud_btrplace_gather_policy) : get_collocation_policy(collocation_group_counter++, collocation_group, prestocloud_collocation_policy);
                if (collocation_policy != null) {
                    proximity_policies.add(collocation_policy);
                } else {
                    collocation_group_counter--;
                }
            }
        }


        if (BTRPLACE_CONSTRAINTS) {
            for (HashMap.Entry<String, LinkedHashSet<String>> fragment_anti_dependency : get_anti_affinity_hashmap().entrySet()) {
                SetDataContainer<String> anti_dependency_groups = get_anti_dependencies_as_multiple_groups(fragment_anti_dependency);
                for (String anti_dependency_group : anti_dependency_groups.getData()) {
                    anti_affinity_policy = get_anti_affinity_policy(anti_affinity_group_counter++, anti_dependency_group, prestocloud_btrplace_spread_policy);
                    if (anti_affinity_policy != null) {
                        proximity_policies.add(anti_affinity_policy);
                    } else {
                        anti_affinity_group_counter--;
                    }
                }
            }
        } else {
            for (HashMap.Entry<String, LinkedHashSet<String>> fragment_anti_dependency : get_anti_affinity_hashmap().entrySet()) {
                String anti_dependency_group = get_anti_dependencies_as_single_group(fragment_anti_dependency);
                anti_affinity_policy = get_anti_affinity_policy(anti_affinity_group_counter++, anti_dependency_group, prestocloud_anti_affinity_policy);
                if (anti_affinity_policy != null) {
                    proximity_policies.add(anti_affinity_policy);
                } else {
                    anti_affinity_group_counter--;
                }
            }
        }
        return proximity_policies;
    }


    private static LinkedHashSet<String> get_dependencies(String dependency) {
        //consult the main hashmap here and return a meaningful value
        return get_collocation_dependency_hashmap().get(dependency);
    }


    private static LinkedHashSet<String> get_additional_fragment_dependencies(LinkedHashSet<String> first_level_fragment_dependencies) {

        LinkedHashSet<String> additional_dependencies = new LinkedHashSet<>();


        if (first_level_fragment_dependencies != null) {
            for (String dependency : first_level_fragment_dependencies) {
                if (!(get_visited_dependencies().contains(dependency))) {
                    get_included_dependencies().add(dependency);
                    get_visited_dependencies().add(dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = get_dependencies(dependency);
                    try {
                        additional_dependencies.addAll(first_level_dependencies_of_dependency);
                        additional_dependencies.addAll(get_additional_fragment_dependencies(first_level_dependencies_of_dependency));
                    }catch (NullPointerException n){
                        Logger.getAnonymousLogger().log(Level.SEVERE,"An unknown fragment was referenced during dependency handling. Please make sure that the fragment has been annotated, and that it has been included for processing");
                        n.printStackTrace();
                        System.exit(0);
                    }
                    get_explored_dependencies().add(dependency);
                } else if (get_explored_dependencies().contains(dependency)) {
                    get_included_dependencies().add(dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = get_dependencies(dependency);
                    additional_dependencies.addAll(first_level_dependencies_of_dependency);
                }
            }
        }


        return additional_dependencies;
    }

    private static LinkedHashSet<String> get_additional_fragment_anti_dependencies(LinkedHashSet<String> first_level_fragment_anti_dependencies) {
        LinkedHashSet<String> additional_anti_dependencies = new LinkedHashSet<>();
        if (first_level_fragment_anti_dependencies != null) {
            for (String anti_dependency : first_level_fragment_anti_dependencies) {
                if (!(get_visited_anti_dependencies().contains(anti_dependency))) {
                    get_included_anti_dependencies().add(anti_dependency);
                    get_visited_anti_dependencies().add(anti_dependency);
                    LinkedHashSet<String> first_level_dependencies_of_anti_dependency = get_dependencies(anti_dependency);
                    try{
                        additional_anti_dependencies.addAll(first_level_dependencies_of_anti_dependency);
                        additional_anti_dependencies.addAll(get_additional_fragment_anti_dependencies(first_level_dependencies_of_anti_dependency));
                    }catch (NullPointerException n){
                        Logger.getAnonymousLogger().log(Level.SEVERE,"An unknown fragment was referenced during anti-dependency handling. Please make sure that the fragment has been annotated, and that it has been included for processing");
                        n.printStackTrace();
                        System.exit(0);
                    }
                    get_explored_anti_dependencies().add(anti_dependency);
                } else if (get_explored_anti_dependencies().contains(anti_dependency)){
                    get_included_dependencies().add(anti_dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = get_dependencies(anti_dependency);
                    additional_anti_dependencies.addAll(first_level_dependencies_of_dependency);
                }
            }
        }
        return additional_anti_dependencies;
    }


    private static String get_collocation_group(HashMap.Entry<String, LinkedHashSet<String>> fragment_dependency_entry) {

        String dependency_group = "";
        if (DEBUG_MESSAGES) {
            System.out.println("- The currently examined fragment is " + fragment_dependency_entry.getKey());
        }

        if (fragment_dependency_entry.getValue().size() > 0) { //add the method name only if there are also other dependencies. Adding the name to the LinkedHashSet in order to take advantage of its deduplication capabilities.
            fragment_dependency_entry.getValue().add(fragment_dependency_entry.getKey());
        }
        int dependency_counter = 0;
        for (String dependency : fragment_dependency_entry.getValue()) { //iterate through the LinkedHashSet containing the fragment dependencies
            if (dependency_counter > 0) {
                dependency_group = dependency_group + ", " + dependency;
            } else {
                dependency_group = dependency;
            }
            dependency_counter++;
        }

        if (DEBUG_MESSAGES) {
            System.out.println("Dependency group:\n" + dependency_group + "\n");
        }
        return dependency_group;
    }


    private static ExtendedDataContainer get_collocation_policy(int collocation_group_counter, String dependency_group, String policy_type) {

        if (dependency_group != null && !dependency_group.equals("")) {

            String collocation_policy_name = "collocation_policy_group_" + collocation_group_counter;
            ExtendedDataContainer collocation_policy = new ExtendedDataContainer(collocation_policy_name);
            MapDataContainer collocation_policy_data = new MapDataContainer();
            collocation_policy_data.add("type", policy_type);
            collocation_policy_data.add("targets", LIST_START + SPACE + dependency_group + SPACE + LIST_END);
            collocation_policy.setPrefix(LIST_PRFX);
            collocation_policy.setData(collocation_policy_data);

            return collocation_policy;
        }
        return null;
    }

    private static ExtendedDataContainer get_anti_affinity_policy(int anti_affinity_group_counter, String anti_affinity_group, String anti_affinity_policy_type) {

        if (anti_affinity_group != null && !anti_affinity_group.equals("")) {

            StringBuilder anti_affinity_policy_name = new StringBuilder("anti_affinity_group_").append(anti_affinity_group_counter);
            StringBuilder anti_affinity_policy_targets = new StringBuilder(LIST_START).append(SPACE).append(anti_affinity_group).append(SPACE).append(LIST_END);

            ExtendedDataContainer anti_affinity_policy = new ExtendedDataContainer(anti_affinity_policy_name.toString());
            MapDataContainer anti_affinity_policy_data = new MapDataContainer();

            anti_affinity_policy_data.add("type", anti_affinity_policy_type);
            anti_affinity_policy_data.add("targets", anti_affinity_policy_targets.toString());

            anti_affinity_policy.setPrefix(LIST_PRFX);
            anti_affinity_policy.setData(anti_affinity_policy_data);

            return anti_affinity_policy;
        }
        return null;
    }

    private static String get_anti_dependencies_as_single_group(HashMap.Entry<String, LinkedHashSet<String>> method_anti_dependency_entry) {

        String anti_dependency_group = "";
        if (DEBUG_MESSAGES) {
            System.out.println("- The currently examined fragment is " + method_anti_dependency_entry.getKey());
        }
        String anti_dependencies = "";

        for (String anti_dependency : method_anti_dependency_entry.getValue()) {
            anti_dependencies = anti_dependencies + ", " + anti_dependency;
        }
        if (!anti_dependencies.equals("")) {
            anti_dependency_group = method_anti_dependency_entry.getKey() + anti_dependencies; //Warning! The method is not anti-dependent on itself! We insert though its name in order to specify that the next methods are anti-dependent on it.
        }
        if (DEBUG_MESSAGES) {
            System.out.println("Anti_dependency group:\n" + anti_dependency_group + "\n");
        }
        return anti_dependency_group;
    }

    private static SetDataContainer<String> get_anti_dependencies_as_multiple_groups(Map.Entry<String, LinkedHashSet<String>> method_anti_dependency_entry) {
        SetDataContainer<String> anti_dependency_groups = new SetDataContainer<>();
        if (DEBUG_MESSAGES) {
            System.out.println("- The currently examined fragment is " + method_anti_dependency_entry.getKey());
        }

        for (String anti_dependency : method_anti_dependency_entry.getValue()) {
            anti_dependency_groups.add(method_anti_dependency_entry.getKey()+LIST_SEP+anti_dependency);
            if (DEBUG_MESSAGES) {
                System.out.println("Anti_dependency group:\n" + method_anti_dependency_entry.getKey() + "," + anti_dependency + "\n");
            }
        }

        return anti_dependency_groups;
    }
}