package eu.prestocloud.tosca_generator.utility_classes.dependencies;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class DependenciesDataStructures {
    private static LinkedHashMap<String,LinkedHashSet<String>> collocation_dependencies_hashmap;
    private static LinkedHashMap<String,LinkedHashSet<String>> anti_affinity_constraints_hashmap;
    private static LinkedHashMap<String,LinkedHashSet<String>> precedence_dependencies_hashmap;
    private static LinkedHashSet<String> visited_dependencies ;
    private static LinkedHashSet<String> visited_anti_dependencies ;
    private static LinkedHashSet<String> explored_dependencies ;
    private static LinkedHashSet<String> explored_anti_dependencies ;
    private static LinkedHashSet<String> included_dependencies;
    private static LinkedHashSet<String> included_anti_dependencies;

    public static void initialize_dependency_data_structures(){
        collocation_dependencies_hashmap = new LinkedHashMap<>();
        anti_affinity_constraints_hashmap = new LinkedHashMap<>();
        precedence_dependencies_hashmap = new LinkedHashMap<>();
        visited_anti_dependencies = new LinkedHashSet<>();
        visited_dependencies = new LinkedHashSet<>();
        explored_anti_dependencies= new LinkedHashSet<>();
        explored_dependencies = new LinkedHashSet<>();
        included_anti_dependencies = new LinkedHashSet<>();
        included_dependencies = new LinkedHashSet<>();
    }

    public static LinkedHashMap<String, LinkedHashSet<String>> get_collocation_dependency_hashmap() {
        return collocation_dependencies_hashmap;
    }

    public static LinkedHashMap<String, LinkedHashSet<String>> get_anti_affinity_hashmap() {
        return anti_affinity_constraints_hashmap;
    }

    public static LinkedHashSet<String> get_visited_dependencies() {
        return visited_dependencies;
    }

    public static void set_visited_dependencies(LinkedHashSet<String> visited_dependencies) {
        DependenciesDataStructures.visited_dependencies = visited_dependencies;
    }

    public static LinkedHashSet<String> get_visited_anti_dependencies() {
        return visited_anti_dependencies;
    }

    public static void set_visited_anti_dependencies(LinkedHashSet<String> visited_anti_dependencies) {
        DependenciesDataStructures.visited_anti_dependencies = visited_anti_dependencies;
    }

    public static LinkedHashSet<String> get_explored_dependencies() {
        return explored_dependencies;
    }

    public static void set_explored_dependencies(LinkedHashSet<String> explored_dependencies) {
        DependenciesDataStructures.explored_dependencies = explored_dependencies;
    }

    public static LinkedHashSet<String> get_explored_anti_dependencies() {
        return explored_anti_dependencies;
    }

    public static void set_explored_anti_dependencies(LinkedHashSet<String> explored_anti_dependencies) {
        DependenciesDataStructures.explored_anti_dependencies = explored_anti_dependencies;
    }

    public static LinkedHashSet<String> get_included_dependencies() {
        return included_dependencies;
    }

    public static void set_included_dependencies(LinkedHashSet<String> included_dependencies) {
        DependenciesDataStructures.included_dependencies = included_dependencies;
    }

    public static LinkedHashSet<String> get_included_anti_dependencies() {
        return included_anti_dependencies;
    }

    public static void set_included_anti_dependencies(LinkedHashSet<String> included_anti_dependencies) {
        DependenciesDataStructures.included_anti_dependencies = included_anti_dependencies;
    }

    public static LinkedHashMap<String, LinkedHashSet<String>> get_precedence_dependency_hashmap() {
        return precedence_dependencies_hashmap;
    }
}
