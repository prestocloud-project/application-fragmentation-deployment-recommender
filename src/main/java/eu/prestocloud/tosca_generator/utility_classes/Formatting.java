package eu.prestocloud.tosca_generator.utility_classes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Formatting {
    /**
     * Describes useful properties to be used to format the policy file, the instance TOSCA parser output, and the type-level TOSCA document.
     */
    public static final String INDENTER = "   ";//3-space identation
    public static final String DOUBLE_QUOTE = "\"";
    public static final String SINGLE_QUOTE = "'";
    public static final String SPACE = " ";
    public static final String EMPTY = "";
    public static final String UNDERSCORE = "_";
    public static final String COMMA_DELIMITER = ",";
    public static final String SEMICOLON_DELIMITER = ";";
    public static final String COLON_DELIMITER = ":";
    public static final String POLICYFILE_DELIMITER = COLON_DELIMITER;
    public static final int    POLICYFILE_INDENTATION_DEPTH = 4;
    public static final String INSTANCE_TOSCA_PARSER_RESULT_STATEMENT = "RESULT:";

    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String C_LIST_START = "{";
    public static final String C_LIST_END = "}";

    public static final String LIST_PRFX = "- ";
    public static final String LIST_START = "[";
    public static final String LIST_SEP = ",";
    public static final String LIST_END = "]";

    public static final String EQUALS = "equal";
    public static final String GREATER_THAN = "greater_than";
    public static final String GREATER_THAN_EQUALS = "greater_than_or_equal";
    public static final String LESS_THAN = "less_than";
    public static final String LESS_THAN_OR_EQUALS = "less_than_or_equal";
    public static final String IN_RANGE = "in_range";
    public static final String VALID_VALUES = "valid_values";


    public static String indent(int indentation_level){
        StringBuilder indentation_StringBuilder = new StringBuilder("");
        for (int i=0;i<indentation_level;i++){
            indentation_StringBuilder.append(INDENTER);
        }
        return indentation_StringBuilder.toString();
    }

    public static void mapPrintingMethod(StringBuilder string_representation,int indentation_level, Map<String,String> requirements) {
        for (String key: requirements.keySet()) {
            string_representation.append(indent(indentation_level)).append(LIST_PRFX).append(key).append(COLON_DELIMITER).append(SPACE).append(requirements.get(key)).append(NEWLINE);
            //System.out.println(key);
            //System.out.println("value : " + metadata.get(key));
        }
    }

    /** Returns an instance of a string enclosed in escaped quotes
     *
     * @param s The string to be quoted
     * @return A new string enclosed in quotes
     */
    public static String quoted(String s){
        return new StringBuilder(DOUBLE_QUOTE).append(s).append(DOUBLE_QUOTE).toString();
    }


    /*
    *Used for curly-brace lists
    *@param comparison_operator: The comparison operator to be used (equality, greater_than etc.)
    *@param value: The actual value in the operator
    */
    public static String brace_list_format (String comparison_operator, String value){
        StringBuilder string_representation = new StringBuilder();
        string_representation.append(C_LIST_START);
        string_representation.append(SPACE);
        string_representation.append(comparison_operator);
        string_representation.append(COLON_DELIMITER);
        string_representation.append(SPACE);
        string_representation.append(value);
        string_representation.append(SPACE);
        string_representation.append(C_LIST_END);
        return string_representation.toString();
    }

    public static <T> String brace_list_format (ArrayList<T> data_list){

        String value;
        StringBuilder string_representation = new StringBuilder("");

        if  (data_list.size()>1)  {
            value = data_list.toString().replace("[","["+SPACE).replace("]",SPACE+"]");

            string_representation.append(C_LIST_START);
            string_representation.append(SPACE);
            string_representation.append(VALID_VALUES);
            string_representation.append(COLON_DELIMITER);
            string_representation.append(SPACE);
            string_representation.append(value);
            string_representation.append(SPACE);
            string_representation.append(C_LIST_END);
        }else{
            value = data_list.toString().replace("[","").replace("]","");

            string_representation.append(C_LIST_START);
            string_representation.append(SPACE);
            string_representation.append(EQUALS);
            string_representation.append(COLON_DELIMITER);
            string_representation.append(SPACE);
            string_representation.append(value);
            string_representation.append(SPACE);
            string_representation.append(C_LIST_END);
        }

        return string_representation.toString();
    }

    public static ArrayList empty_arraylist(){
        return new ArrayList();
    }
    public static LinkedHashMap empty_hashmap(){
        return  new LinkedHashMap();
    }
}
