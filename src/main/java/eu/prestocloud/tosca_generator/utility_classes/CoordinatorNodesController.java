package eu.prestocloud.tosca_generator.utility_classes;

import java.util.HashMap;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;

public class CoordinatorNodesController {
    private static HashMap<String,String> fragment_faas_proxy_nodes = new HashMap<>();
    private static HashMap<String,String> fragment_load_balancer_nodes = new HashMap<>();
    private static HashMap<String,String> fragment_jppf_master_nodes = new HashMap<>();

    private static String jppf_master_fragment_node = EMPTY;
    private static String faas_proxy_fragment_node = EMPTY;
    private static String load_balancer_fragment_node = EMPTY;

    public static void initialize_coordinator_nodes(){
        fragment_faas_proxy_nodes = new HashMap<>();
        fragment_load_balancer_nodes = new HashMap<>();
        fragment_jppf_master_nodes = new HashMap<>();
    }

    public static String get_load_balancer_fragment_node(String fragment_name) {
        if (!load_balancer_fragment_node.equals(EMPTY)){
            return load_balancer_fragment_node;
        }
        return fragment_load_balancer_nodes.get(fragment_name);
    }
    public static String get_load_balancer_fragment_node() {
        return load_balancer_fragment_node;
    }

    public static String get_faas_fragment_proxy_node(String fragment_name){
        if (!faas_proxy_fragment_node.equals(EMPTY)){
            return faas_proxy_fragment_node;
        }
        return fragment_faas_proxy_nodes.get(fragment_name);
    }

    public static String get_faas_fragment_proxy_node(){
        return faas_proxy_fragment_node;
    }

    public static String get_jppf_master_fragment_node(String fragment_name){
        if (!jppf_master_fragment_node.equals(EMPTY)){
            return jppf_master_fragment_node;
        }
        return fragment_jppf_master_nodes.get(fragment_name);
    }

    public static String get_jppf_master_fragment_node(){
        return jppf_master_fragment_node;
    }




    public static void set_faas_proxy_fragment_node(String faas_proxy_node)
    {
        faas_proxy_fragment_node = faas_proxy_node;
    }

    public static void set_faas_proxy_fragment_node(String fragment_name, String faas_proxy_node)
    {
        fragment_faas_proxy_nodes.put(fragment_name,faas_proxy_node);
    }


    public static void set_load_balancer_fragment_node(String load_balancer_node){
            load_balancer_fragment_node = load_balancer_node;
    }

    public static void set_load_balancer_fragment_node(String fragment_name, String load_balancer_node){
        fragment_load_balancer_nodes.put(fragment_name,load_balancer_node);
    }


    public static void set_jppf_master_fragment_node(String jppf_master_node){
            jppf_master_fragment_node = jppf_master_node;
        }

    public static void set_jppf_master_fragment_node(String fragment_name, String jppf_master_node){
        fragment_jppf_master_nodes.put(fragment_name,jppf_master_node);
    }

}
