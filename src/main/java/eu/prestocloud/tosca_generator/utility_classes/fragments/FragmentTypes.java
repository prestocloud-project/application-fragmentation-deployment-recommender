package eu.prestocloud.tosca_generator.utility_classes.fragments;

public enum FragmentTypes {
    coordinator_faas_proxy, coordinator_load_balancer, coordinator_jppf_master,application_fragment
}
