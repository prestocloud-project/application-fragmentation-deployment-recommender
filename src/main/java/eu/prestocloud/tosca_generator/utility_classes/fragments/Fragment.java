package eu.prestocloud.tosca_generator.utility_classes.fragments;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.tosca_generator.policy_file_requirements.AttributeMapping;
import eu.prestocloud.tosca_generator.utility_classes.Port;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static eu.prestocloud.tosca_generator.NodeGenerator.create_processed_fragment_name;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;

public class Fragment {

    //CONSTANTS
    public static final boolean PROCESS_ANNOTATION_NAMES = true;


    //CLASS FIELDS
    public static int processing_node_counter = 0;
    public static int fragment_node_counter = 0;

    //INSTANCE FIELDS

    // ---INITIAL FIELDS
    private String fragment_type; //TODO delete this field once method-level fragments are discarded. The allowed values are method/constructor.

    //private PrestoFragmentation annotation;

    private String original_fragment_name;
    private String processed_fragment_name;
    private String fragment_id;

    private FragmentTypes special_fragment_type;

    private String public_ssh_key;

    private String [] fragment_collocation_dependencies;
    private String [] fragment_anti_affinity_constraints;
    private String [] fragment_precedence_dependencies;

    private AttributeMapping cpu_mapping;
    private AttributeMapping memory_mapping;
    private AttributeMapping disk_mapping;

    private PrestoFragmentation.Elasticity_mechanism elasticity_mechanism;

    private int min_cpus_cloud;
    private int max_cpus_cloud;
    private int min_memory_mb_cloud;
    private int max_memory_mb_cloud;
    private int min_storage_gb_cloud;
    private int max_storage_gb_cloud;

    private int min_cpus_edge;
    private int max_cpus_edge;
    private int min_memory_mb_edge;
    private int max_memory_mb_edge;
    private int min_storage_gb_edge;
    private int max_storage_gb_edge;

    private boolean onloadable;
    private boolean offloadable;

    private boolean adaptation_pending;

    private int unconditional_minimum_instances; //a fragment should never have less than this number of instances
    private int current_instances;
    //private int current_maximum_instances;
    private int maximum_instances;

    //Formerly used by Simple parser to pinpoint exact cloud and edge hosts. Can be reinstated if necessary, with the appropriate changes on the parsing logic followed in the parser of Activeeon. Getters & Setters have been deleted

    //private ArrayList<String>edge_hosts = new ArrayList<>();
    //private ArrayList<String>cloud_hosts = new ArrayList<>();

    private int cloud_host_instances;
    private int edge_host_instances;

    private ArrayList<String> excluded_edge_device_identifiers = new ArrayList<>();
    private LinkedHashMap<String,String> required_sensors;
    private String cloud_docker_image;
    private String cloud_docker_registry;
    private String cloud_docker_registry_username;
    private String cloud_docker_registry_password;
    private ArrayList<Port> cloud_ports_properties;
    private LinkedHashMap<String,String> cloud_environmental_variables;
    private String cloud_docker_command;

    private String edge_docker_image;
    private String edge_docker_registry;
    private String edge_docker_registry_username;
    private String edge_docker_registry_password;
    private ArrayList<Port> edge_ports_properties;
    private LinkedHashMap<String,String> edge_environmental_variables;
    private String edge_docker_command;

    private LinkedHashMap<String, String> optimization_provider_friendliness_weights;
    private int optimization_cost_weight;
    private int optimization_distance_weight;

    private int health_check_interval;
    private String health_check_command;

    public Fragment (String original_fragment_name,
                     String fragment_id,
                     FragmentTypes special_fragment_type,
                     int min_cpus_cloud, int max_cpus_cloud,
                     int min_memory_mb_cloud, int max_memory_mb_cloud,
                     int min_storage_gb_cloud, int max_storage_gb_cloud,
                     int min_cpus_edge, int max_cpus_edge,
                     int min_memory_mb_edge, int max_memory_mb_edge,
                     int min_storage_gb_edge, int max_storage_gb_edge,
                     int minimum_instances,
                     int maximum_instances,
                     boolean onloadable,
                     boolean offloadable,
                     PrestoFragmentation.Elasticity_mechanism elasticity_mechanism,
                     LinkedHashMap<String,String> required_sensors,

                     String [] fragment_collocation_dependencies,
                     String [] fragment_anti_affinity_constraints,
                     String [] fragment_precedence_dependencies,

                     String cloud_docker_image,
                     String cloud_docker_registry,
                     String cloud_docker_registry_username,
                     String cloud_docker_registry_password,
                     ArrayList<Port> cloud_ports_properties,
                     LinkedHashMap<String,String> cloud_environmental_variables,
                     String cloud_docker_command,

                     String edge_docker_image,
                     String edge_docker_registry,
                     String edge_docker_registry_username,
                     String edge_docker_registry_password,
                     ArrayList<Port> edge_ports_properties,
                     LinkedHashMap<String,String> edge_environmental_variables,
                     String edge_docker_command,

                     LinkedHashMap<String, String> optimization_provider_friendliness_weights,
                     int optimization_cost_weight,
                     int optimization_distance_weight,

                     int health_check_interval,
                     String health_check_command,



                     String public_ssh_key

    ){
        this.original_fragment_name = original_fragment_name;
        this.processed_fragment_name = create_processed_fragment_name(original_fragment_name);
        this.fragment_id = fragment_id.equals(EMPTY)?String.valueOf(fragment_node_counter++):fragment_id;

        this.special_fragment_type = special_fragment_type;

        this.current_instances = minimum_instances;
        //this.current_maximum_instances = maximum_instances;
        this.unconditional_minimum_instances = minimum_instances;
        this.maximum_instances = maximum_instances;
        //this.load_balancing_required = maximum_instances >1;

        this.required_sensors = required_sensors;

        this.cloud_docker_image = cloud_docker_image;
        this.cloud_docker_registry = cloud_docker_registry;
        this.cloud_docker_registry_username = cloud_docker_registry_username;
        this.cloud_docker_registry_password = cloud_docker_registry_password;
        this.cloud_ports_properties = cloud_ports_properties;
        this.cloud_environmental_variables = cloud_environmental_variables;
        this.cloud_docker_command = cloud_docker_command;

        this.edge_docker_image = edge_docker_image;
        this.edge_docker_registry = edge_docker_registry;
        this.edge_docker_registry_username = edge_docker_registry_username;
        this.edge_docker_registry_password = edge_docker_registry_password;
        this.edge_ports_properties = edge_ports_properties;
        this.edge_environmental_variables = edge_environmental_variables;
        this.edge_docker_command = edge_docker_command;

        this.elasticity_mechanism = elasticity_mechanism;

        this.min_cpus_cloud = min_cpus_cloud;
        this.min_cpus_edge = min_cpus_edge;
        this.max_cpus_cloud = max_cpus_cloud;
        this.max_cpus_edge = max_cpus_edge;

        this.min_storage_gb_cloud = min_storage_gb_cloud;
        this.min_storage_gb_edge = min_storage_gb_edge;
        this.max_storage_gb_cloud = max_storage_gb_cloud;
        this.max_storage_gb_edge = max_storage_gb_edge;

        this.min_memory_mb_cloud = min_memory_mb_cloud;
        this.min_memory_mb_edge = min_memory_mb_edge;
        this.max_memory_mb_cloud = max_memory_mb_cloud;
        this.max_memory_mb_edge = max_memory_mb_edge;

        this.optimization_distance_weight = optimization_distance_weight;
        this.optimization_cost_weight = optimization_cost_weight;
        this.optimization_provider_friendliness_weights = optimization_provider_friendliness_weights;

        this.health_check_interval = health_check_interval;
        this.health_check_command = health_check_command;

        this.onloadable = onloadable;
        this.offloadable = offloadable;

        this.fragment_collocation_dependencies = fragment_collocation_dependencies;
        this.fragment_anti_affinity_constraints = fragment_anti_affinity_constraints;
        this.fragment_precedence_dependencies = fragment_precedence_dependencies;

        this.public_ssh_key = public_ssh_key;

    }

    public int getMin_cpus_cloud() {
        return min_cpus_cloud;
    }

    public void setMin_cpus_cloud(int min_cpus_cloud) {
        this.min_cpus_cloud = min_cpus_cloud;
    }

    public int getMax_cpus_cloud() {
        return max_cpus_cloud;
    }

    public void setMax_cpus_cloud(int max_cpus_cloud) {
        this.max_cpus_cloud = max_cpus_cloud;
    }

    public int getMin_memory_mb_cloud() {
        return min_memory_mb_cloud;
    }

    public void setMin_memory_mb_cloud(int min_memory_mb_cloud) {
        this.min_memory_mb_cloud = min_memory_mb_cloud;
    }

    public int getMax_memory_mb_cloud() {
        return max_memory_mb_cloud;
    }

    public void setMax_memory_mb_cloud(int max_memory_mb_cloud) {
        this.max_memory_mb_cloud = max_memory_mb_cloud;
    }

    public int getMin_storage_gb_cloud() {
        return min_storage_gb_cloud;
    }

    public void setMin_storage_gb_cloud(int min_storage_gb_cloud) {
        this.min_storage_gb_cloud = min_storage_gb_cloud;
    }

    public int getMax_storage_gb_cloud() {
        return max_storage_gb_cloud;
    }

    public void setMax_storage_gb_cloud(int max_storage_gb_cloud) {
        this.max_storage_gb_cloud = max_storage_gb_cloud;
    }

    public int getMin_cpus_edge() {
        return min_cpus_edge;
    }

    public void setMin_cpus_edge(int min_cpus_edge) {
        this.min_cpus_edge = min_cpus_edge;
    }

    public int getMax_cpus_edge() {
        return max_cpus_edge;
    }

    public void setMax_cpus_edge(int max_cpus_edge) {
        this.max_cpus_edge = max_cpus_edge;
    }

    public int getMin_memory_mb_edge() {
        return min_memory_mb_edge;
    }

    public void setMin_memory_mb_edge(int min_memory_mb_edge) {
        this.min_memory_mb_edge = min_memory_mb_edge;
    }

    public int getMax_memory_mb_edge() {
        return max_memory_mb_edge;
    }

    public void setMax_memory_mb_edge(int max_memory_mb_edge) {
        this.max_memory_mb_edge = max_memory_mb_edge;
    }

    public int getMin_storage_gb_edge() {
        return min_storage_gb_edge;
    }

    public void setMin_storage_gb_edge(int min_storage_gb_edge) {
        this.min_storage_gb_edge = min_storage_gb_edge;
    }

    public int getMax_storage_gb_edge() {
        return max_storage_gb_edge;
    }

    public void setMax_storage_gb_edge(int max_storage_gb_edge) {
        this.max_storage_gb_edge = max_storage_gb_edge;
    }

    public boolean isAdaptation_pending() {
        return adaptation_pending;
    }

    public void setAdaptation_pending(boolean adaptation_pending) {
        this.adaptation_pending = adaptation_pending;
    }

    public static int getProcessing_node_counter() {
        return processing_node_counter;
    }

    public static void setProcessing_node_counter(int processing_node_counter) {
        Fragment.processing_node_counter = processing_node_counter;
    }

    public static int getFragment_node_counter() {
        return fragment_node_counter;
    }

    public static void setFragment_node_counter(int fragment_node_counter) {
        Fragment.fragment_node_counter = fragment_node_counter;
    }

    public String getFragment_type() {
        return fragment_type;
    }

    public void setFragment_type(String fragment_type) {
        this.fragment_type = fragment_type;
    }

    //public PrestoFragmentation getAnnotation() {
    //    return annotation;
    //}

    //public void setAnnotation(PrestoFragmentation annotation) {
    //    this.annotation = annotation;
    //}

    public String getOriginal_fragment_name() {
        return original_fragment_name;
    }

    public void setOriginal_fragment_name(String original_fragment_name) {
        this.original_fragment_name = original_fragment_name;
    }

    public String getProcessed_fragment_name() {
        return processed_fragment_name;
    }

    public void setProcessed_fragment_name(String processed_fragment_name) {
        this.processed_fragment_name = processed_fragment_name;
    }

    public String getFragment_id() {
        return fragment_id;
    }

    public void setFragment_id(String fragment_id) {
        this.fragment_id = fragment_id;
    }

    public String[] getFragment_collocation_dependencies() {
        return fragment_collocation_dependencies;
    }

    public void setFragment_collocation_dependencies(String[] fragment_collocation_dependencies) {
        this.fragment_collocation_dependencies = fragment_collocation_dependencies;
    }

    public String[] getFragment_anti_affinity_constraints() {
        return fragment_anti_affinity_constraints;
    }

    public void setFragment_anti_affinity_constraints(String[] fragment_anti_affinity_constraints) {
        this.fragment_anti_affinity_constraints = fragment_anti_affinity_constraints;
    }

    public boolean isOnloadable() {
        return onloadable;
    }

    public void setOnloadable(boolean onloadable) {
        this.onloadable = onloadable;
    }

    public int getCurrent_instances() {
        return current_instances;
    }

    public void setCurrent_instances(int current_instances) {
        this.current_instances = current_instances;
    }

    public int getMaximum_instances() {
        return maximum_instances;
    }

    public void setMaximum_instances(int maximum_instances) {
        this.maximum_instances = maximum_instances;
    }



    public PrestoFragmentation.Elasticity_mechanism getElasticity_mechanism() {
        return elasticity_mechanism;
    }

    public void setElasticity_mechanism(PrestoFragmentation.Elasticity_mechanism elasticity_mechanism) {
        this.elasticity_mechanism = elasticity_mechanism;
    }
    public boolean isOffloadable() {
        return offloadable;
    }

    public void setOffloadable(boolean offloadable) {
        this.offloadable = offloadable;
    }

    public int getUnconditional_minimum_instances() {
        return unconditional_minimum_instances;
    }

    public void setUnconditional_minimum_instances(int unconditional_minimum_instances) {
        this.unconditional_minimum_instances = unconditional_minimum_instances;
    }

    public int getCloud_host_instances() {
        return cloud_host_instances;
    }

    public void increase_cloud_host_instances() {
        this.cloud_host_instances++;
    }

    public int getEdge_host_instances() {
        return edge_host_instances;
    }

    public void increase_edge_host_instances() {
        this.edge_host_instances++;
    }

    public void setCloud_host_instances(int cloud_host_instances) {
        this.cloud_host_instances = cloud_host_instances;
    }

    public void setEdge_host_instances(int edge_host_instances) {
        this.edge_host_instances = edge_host_instances;
    }

    public ArrayList<String> getExcluded_edge_device_identifiers() {
        return excluded_edge_device_identifiers;
    }

    public void setExcluded_edge_device_identifiers(ArrayList<String> excluded_edge_device_identifiers) {
        this.excluded_edge_device_identifiers = excluded_edge_device_identifiers;
    }

    public LinkedHashMap<String,String> getRequired_sensors() {
        return required_sensors;
    }

    public void setRequired_sensors(LinkedHashMap<String,String> required_sensors) {
        this.required_sensors = required_sensors;
    }

    public String getCloud_docker_image() {
        return cloud_docker_image;
    }

    public void setCloud_docker_image(String cloud_docker_image) {
        this.cloud_docker_image = cloud_docker_image;
    }

    public String getCloud_docker_registry() {
        return cloud_docker_registry;
    }

    public void setCloud_docker_registry(String cloud_docker_registry) {
        this.cloud_docker_registry = cloud_docker_registry;
    }

    public String getCloud_docker_registry_username() {
        return cloud_docker_registry_username;
    }

    public void setCloud_docker_registry_username(String cloud_docker_registry_username) {
        this.cloud_docker_registry_username = cloud_docker_registry_username;
    }

    public String getCloud_docker_registry_password() {
        return cloud_docker_registry_password;
    }

    public void setCloud_docker_registry_password(String cloud_docker_registry_password) {
        this.cloud_docker_registry_password = cloud_docker_registry_password;
    }

    public String getEdge_docker_image() {
        return edge_docker_image;
    }

    public void setEdge_docker_image(String edge_docker_image) {
        this.edge_docker_image = edge_docker_image;
    }

    public String getEdge_docker_registry() {
        return edge_docker_registry;
    }

    public void setEdge_docker_registry(String edge_docker_registry) {
        this.edge_docker_registry = edge_docker_registry;
    }

    public String getEdge_docker_registry_username() {
        return edge_docker_registry_username;
    }

    public void setEdge_docker_registry_username(String edge_docker_registry_username) {
        this.edge_docker_registry_username = edge_docker_registry_username;
    }

    public String getEdge_docker_registry_password() {
        return edge_docker_registry_password;
    }

    public void setEdge_docker_registry_password(String edge_docker_registry_password) {
        this.edge_docker_registry_password = edge_docker_registry_password;
    }

    public LinkedHashMap<String, String> getCloud_environmental_variables() {
        return cloud_environmental_variables;
    }

    public void setCloud_environmental_variables(LinkedHashMap<String, String> cloud_environmental_variables) {
        this.cloud_environmental_variables = cloud_environmental_variables;
    }


    public String getCloud_docker_command() {
        return cloud_docker_command;
    }

    public void setCloud_docker_command(String cloud_docker_command) {
        this.cloud_docker_command = cloud_docker_command;
    }

    public ArrayList<Port> getEdge_ports_properties() {
        return edge_ports_properties;
    }

    public void setEdge_ports_properties(ArrayList<Port> edge_ports_properties) {
        this.edge_ports_properties = edge_ports_properties;
    }

    public LinkedHashMap<String, String> getEdge_environmental_variables() {
        return edge_environmental_variables;
    }

    public void setEdge_environmental_variables(LinkedHashMap<String, String> edge_environmental_variables) {
        this.edge_environmental_variables = edge_environmental_variables;
    }

    public String getEdge_docker_command() {
        return edge_docker_command;
    }

    public void setEdge_docker_command(String edge_docker_command) {
        this.edge_docker_command = edge_docker_command;
    }

    public String[] getFragment_precedence_dependencies() {
        return fragment_precedence_dependencies;
    }

    public void setFragment_precedence_dependencies(String[] fragment_precedence_dependencies) {
        this.fragment_precedence_dependencies = fragment_precedence_dependencies;
    }

    public ArrayList<Port> getCloud_ports_properties() {
        return cloud_ports_properties;
    }

    public void setCloud_ports_properties(ArrayList<Port> cloud_ports_properties) {
        this.cloud_ports_properties = cloud_ports_properties;
    }

    public LinkedHashMap<String, String> getOptimization_provider_friendliness_weights() {
        return optimization_provider_friendliness_weights;
    }

    public void setOptimization_provider_friendliness_weights(LinkedHashMap<String,String> optimization_provider_friendliness_weights) {
        this.optimization_provider_friendliness_weights = optimization_provider_friendliness_weights;
    }

    public int getOptimization_cost_weight() {
        return optimization_cost_weight;
    }

    public void setOptimization_cost_weight(int optimization_cost_weight) {
        this.optimization_cost_weight = optimization_cost_weight;
    }

    public int getOptimization_distance_weight() {
        return optimization_distance_weight;
    }

    public void setOptimization_distance_weight(int optimization_distance_weight) {
        this.optimization_distance_weight = optimization_distance_weight;
    }

    public int getHealth_check_interval() {
        return health_check_interval;
    }

    public void setHealth_check_interval(int health_check_interval) {
        this.health_check_interval = health_check_interval;
    }

    public String getHealth_check_command() {
        return health_check_command;
    }

    public void setHealth_check_command(String health_check_command) {
        this.health_check_command = health_check_command;
    }

    public String getPublic_ssh_key() {
        return public_ssh_key;
    }

    public void setPublic_ssh_key(String public_ssh_key) {
        this.public_ssh_key = public_ssh_key;
    }

    public FragmentTypes getSpecial_fragment_type() {
        return special_fragment_type;
    }

    public void setSpecial_fragment_type(FragmentTypes special_fragment_type) {
        this.special_fragment_type = special_fragment_type;
    }

    private static String[] process_annotation_names(String[] strings) {
        String [] new_names = new String[strings.length];
        int name_counter = 0;
        for (String s: strings){
            new_names[name_counter++]=s.replace('.','_');
        }
        return new_names;
    }

}
