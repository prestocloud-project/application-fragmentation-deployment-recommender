package eu.prestocloud.tosca_generator.utility_classes.fragments;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.tosca_generator.policy_file_requirements.AttributeMapping;
import eu.prestocloud.tosca_generator.policy_file_requirements.MappingRequirement;
import eu.prestocloud.tosca_generator.utility_classes.AttributeRange;

public class FragmentResources {

    private final AttributeMapping cpu_mapping;
    private final AttributeMapping memory_mapping;
    private final AttributeMapping disk_mapping;
    private PrestoFragmentation.CPULoad cpuLoad;
    private PrestoFragmentation.MemoryLoad memoryLoad;
    private PrestoFragmentation.StorageLoad storageLoad;
    private boolean onloadable;
    private int lower_instances;
    private int higher_instances;

    public FragmentResources(PrestoFragmentation prestoannotation, MappingRequirement mapping, AttributeRange instances) {
        this.cpuLoad = prestoannotation.cpuLoad();
        this.memoryLoad = prestoannotation.memoryLoad();
        this.storageLoad = prestoannotation.storageLoad();

        this.cpu_mapping = mapping.getCpu_mapping();
        this.memory_mapping = mapping.getMemory_mapping();
        this.disk_mapping = mapping.getStorage_mapping();

        this.lower_instances = instances.getLower_bound();
        this.higher_instances = instances.getHigher_bound();

        this.onloadable = prestoannotation.onloadable();

    }

    public PrestoFragmentation.CPULoad getCpuLoad() {
        return cpuLoad;
    }

    public void setCpuLoad(PrestoFragmentation.CPULoad cpuLoad) {
        this.cpuLoad = cpuLoad;
    }

    public PrestoFragmentation.MemoryLoad getMemoryLoad() {
        return memoryLoad;
    }

    public void setMemoryLoad(PrestoFragmentation.MemoryLoad memoryLoad) {
        this.memoryLoad = memoryLoad;
    }

    public PrestoFragmentation.StorageLoad getStorageLoad() {
        return storageLoad;
    }

    public void setStorageLoad(PrestoFragmentation.StorageLoad storageLoad) {
        this.storageLoad = storageLoad;
    }

    public boolean isOnloadable() {
        return onloadable;
    }

    public void setOnloadable(boolean onloadable) {
        this.onloadable = onloadable;
    }

    public int getLower_instances() {
        return lower_instances;
    }

    public void setLower_instances(int lower_instances) {
        this.lower_instances = lower_instances;
    }

    public int getHigher_instances() {
        return higher_instances;
    }

    public void setHigher_instances(int higher_instances) {
        this.higher_instances = higher_instances;
    }

}
