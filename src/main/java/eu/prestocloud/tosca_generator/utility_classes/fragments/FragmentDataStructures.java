package eu.prestocloud.tosca_generator.utility_classes.fragments;



import java.util.HashMap;
import java.util.LinkedHashMap;

public class FragmentDataStructures {

    public static HashMap<String, Fragment> fragments_map = new LinkedHashMap<>();

    public static void add_fragment(Fragment fragment){
        fragments_map.put(fragment.getProcessed_fragment_name(),fragment);
    }

}
