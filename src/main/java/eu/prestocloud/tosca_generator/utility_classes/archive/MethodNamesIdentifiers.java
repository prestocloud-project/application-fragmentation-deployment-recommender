package eu.prestocloud.tosca_generator.utility_classes.archive;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static eu.prestocloud.tosca_generator.NodeGenerator.PROCESS_ANNOTATION_NAMES;
import static eu.prestocloud.tosca_generator.NodeGenerator.counter;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.UNDERSCORE;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.BidirectionalCollocationDependencies.DEBUG_MESSAGES;
import static java.lang.StrictMath.abs;

public class MethodNamesIdentifiers {
    private static String get_original_method_name(Constructor constructor) {
        return constructor.getName();
    }

    private static String get_original_method_name(Method method) {
        return method.getDeclaringClass().getCanonicalName()+"."+method.getName();
    }

    private static String get_method_hashcode(Constructor constructor) {
        int hashcode = constructor.hashCode();
        if (constructor.getParameterTypes()!=null) {
            for (Class parametertype : constructor.getParameterTypes()) {
                if (DEBUG_MESSAGES) {
                    System.out.println("PARAMETER TYPE\n" + parametertype + " : " + parametertype.hashCode() + "\n----------");
                }
                hashcode += parametertype.hashCode();
            }
        }
        return String.valueOf(abs(hashcode));
    }

    private static String get_method_hashcode(Method method) {
        int hashcode = method.hashCode();
        if (method.getParameterTypes()!=null) {
            for (Class parametertype : method.getParameterTypes()) {
                if (DEBUG_MESSAGES) {
                    System.out.println("PARAMETER TYPE\n" + parametertype + " : " + parametertype.hashCode() + "\n----------");
                }
                hashcode += parametertype.hashCode();
            }
        }
        return String.valueOf(abs(hashcode));
    }

    private static String get_method_id(Constructor constructor) {
        //Complex alternative, using the hashcodes of the parameters of the constructor: return get_method_hashcode (constructor)

        //Simpler alternative follows, hashcodes though do not have a constant number of digits. This means that if 123 has a conflict, and the two conflicting fragments have their id's renamed to be 1230 and 1231,there might be a problem because another fragment may have a (non-conflicting with another overloaded instance of itself, and thus non-detectable) hash of 1230

        /*
        StringBuffer hashcode_value = new StringBuffer(String.valueOf(constructor.hashCode()));
        Integer overloaded_fragment_counter = fragment_IDs.get(String.valueOf(hashcode_value));
        if (overloaded_fragment_counter==null ){
            overloaded_fragment_counter = 0;
            fragment_IDs.put(hashcode_value.toString(),overloaded_fragment_counter);
        }else {
            overloaded_fragment_counter++;
            fragment_IDs.replace(hashcode_value.toString(),overloaded_fragment_counter);
        }
        String overloaded_value = String.valueOf(overloaded_fragment_counter);
        return hashcode_value.append(overloaded_value).toString();
        */

        //final approach uses the value of counter

        return String.valueOf(counter);
    }

    private static String get_method_id(Method method) {
        //Complex alternative, using the hashcodes of the parameters of the method: return get_method_hashcode (method)

        //Simpler alternative follows, hashcodes though do not have a constant number of digits. This means that if 123 has a conflict, and the two conflicting fragments have their id's renamed to be 1230 and 1231,there might be a problem because another fragment may have a (non-conflicting with another overloaded instance of itself, and thus non-detectable) hash of 1230

        /*
        StringBuffer hashcode_value = new StringBuffer(String.valueOf(method.hashCode()));
        Integer overloaded_fragment_counter = fragment_IDs.get(hashcode_value);
        if (overloaded_fragment_counter==null ){
            overloaded_fragment_counter = 0;
            fragment_IDs.put(hashcode_value.toString(),overloaded_fragment_counter);
        }else {
            overloaded_fragment_counter++;
        }
        String overloaded_value = String.valueOf(overloaded_fragment_counter);
        */

        //final approach uses the value of counter

        return String.valueOf(counter);
    }

    private static String[] process_annotation_names(String[] strings) {
        String [] new_names = new String[strings.length];
        int name_counter = 0;
        for (String s: strings){
            new_names[name_counter++]=s.replace('.','_');
        }
        return new_names;
    }

        /*
    private static void add_fragment_ID(String method_name){
        fragment_IDs.put(method_name,String.valueOf(method_name.hashCode()));
    }


    public static String getConstructorID(Constructor constructor) {
        //StringBuilder constructor_single_identifier = new StringBuilder(constructor.getName().replace(".","_")).append(UNDERSCORE).append(abs(constructor.hashCode()));
        StringBuilder constructor_single_identifier = new StringBuilder(constructor.getName().replace(".","_"));
        return constructor_single_identifier.toString();
    }

    public static String getMethodID(Method method) {

        // Old method, using method_name+hashcode in order to differentiate between different overloaded instances of a method. Will not be used as the developer cannot know at coding time what is the hashcode of a method to properly annotate.
        // StringBuilder method_single_identifier = new StringBuilder(method.getName()).append(UNDERSCORE).append(method.hashCode());

        StringBuilder method_single_identifier = new StringBuilder(method.getName());
        return method_single_identifier.toString();
    }
    */
    /**
     * This method returns the name of a method object, substituting dots for underscores if the relevant variable is set
     * @param method The method object of the method examined
     * @return The full (processed) name of the method
     */
    private static String get_processed_method_name(Method method, String overloading_name_tag) {
        String OVERLOADED_STRING = "overloaded";
        String full_method_name=EMPTY;
        String overloading_string = (overloading_name_tag!=null && !overloading_name_tag.equals(EMPTY))? UNDERSCORE + OVERLOADED_STRING + UNDERSCORE + overloading_name_tag : EMPTY ;
        String canonical_name = method.getDeclaringClass().getCanonicalName();

        if (PROCESS_ANNOTATION_NAMES) {
            String converted_name = canonical_name.replace(".", UNDERSCORE);
            full_method_name = converted_name + UNDERSCORE + method.getName()+overloading_string;
        }else {
            full_method_name = canonical_name+"."+method.getName()+overloading_string;
        }
        return full_method_name;
    }

    /**
     * This method returns the name of a constructor object, substituting dots for underscores if the relevant variable is set
     * @param constructor The constructor object of the method examined
     * @return The full (processed) name of the constructor
     */
    private static String get_processed_method_name(Constructor constructor, String overloading_name_tag) {
        String OVERLOADED_STRING = "overloaded";
        String full_method_name=EMPTY;
        String overloading_string = (overloading_name_tag!=null && !overloading_name_tag.equals(EMPTY))? UNDERSCORE + OVERLOADED_STRING + UNDERSCORE + overloading_name_tag : EMPTY ;
        String canonical_name = constructor.getDeclaringClass().getCanonicalName();

        if (PROCESS_ANNOTATION_NAMES) {
            String converted_name = canonical_name.replace(".", UNDERSCORE);
            full_method_name =  converted_name+overloading_string; //was: full_method_name =  converted_name + "_" + constructor.getName();

        }else {
            full_method_name = canonical_name+overloading_string;
            //full_method_name =  constructor.getDeclaringClass().getCanonicalName()+"."+constructor.getName()+"."+constructor.getParameterTypes();
        }
        return full_method_name;
    }

     /*
    private static String get_fragment_id(String fragment_name, String fragment_hashcode) {
        String fragment_id = fragment_IDs.get(fragment_name);
        if (fragment_id!=null){
            return fragment_id;
        }else{
            fragment_id = fragment_IDs.get(fragment_name+UNDERSCORE+fragment_hashcode);
            if (fragment_id!=null){
                return fragment_id;
            }
            else {
                try {
                    throw new Exception("The fragment name cannot be recognized, perhaps there is an overloading/constructor annotation issue") ;
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("The fragment which was examined had the following name"+COLON_DELIMITER+NEWLINE+fragment_name+" , and the following hashcode"+COLON_DELIMITER+NEWLINE);
                }
                return null;
            }
        }
    }
    */
    /**
     * This method returns the name of a constructor object, substituting dots for underscores if the relevant variable is set
     * @param class_name The name of the constructor class
     * @return The full (processed) name of the constructor
     */
    private static String get_processed_constructor_name(String class_name, String overloading_name_tag) {
        String OVERLOADED_STRING = "overloaded";
        String full_fragment_name="";
        String overloading_string = (overloading_name_tag!=null && !overloading_name_tag.equals(""))? UNDERSCORE + OVERLOADED_STRING + UNDERSCORE + overloading_name_tag : "" ;

        if (PROCESS_ANNOTATION_NAMES) {
            String converted_name = class_name.replace(".", UNDERSCORE);
            full_fragment_name =  converted_name+overloading_string;
        }else {
            full_fragment_name = class_name+overloading_string;
        }
        return full_fragment_name;
    }


    /**
     * This method returns the name of a method object, substituting dots for underscores if the relevant variable is set
     * @param class_name The name of the method class
     * @return The full (processed) name of the method
     */
    private String get_processed_method_name(String class_name,String original_fragment_name,  String overloading_name_tag) {

        String OVERLOADED_STRING = "overloaded";
        String full_method_name="";
        String overloading_string = (overloading_name_tag!=null && !overloading_name_tag.equals(""))? UNDERSCORE + OVERLOADED_STRING + UNDERSCORE + overloading_name_tag : "" ;

        if (PROCESS_ANNOTATION_NAMES) {
            String converted_name = class_name.replace(".", UNDERSCORE);
            full_method_name = converted_name + UNDERSCORE + /*method.getName()*/original_fragment_name+overloading_string;
        }else {
            full_method_name = class_name+"."+original_fragment_name+overloading_string;
        }
        return full_method_name;
    }

}
