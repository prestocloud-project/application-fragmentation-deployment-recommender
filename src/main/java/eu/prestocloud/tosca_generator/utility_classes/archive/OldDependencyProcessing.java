package eu.prestocloud.tosca_generator.utility_classes.archive;

import eu.prestocloud.tosca_generator.NodeGenerator;

import java.util.*;

import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;

public class OldDependencyProcessing {

    private static LinkedHashSet<String> get_additional_method_dependencies(String fragment_name, LinkedHashSet<String> first_level_fragment_dependencies) {
        LinkedHashSet<String> additional_dependencies = new LinkedHashSet<>();
        if (first_level_fragment_dependencies!=null) {
            for (String dependency : first_level_fragment_dependencies) {
                if (!get_visited_dependencies().contains(dependency)){
                    get_included_dependencies().add(dependency);
                }
                if (!(get_visited_dependencies().contains(dependency) || get_explored_dependencies().contains(dependency))) {
                    get_visited_dependencies().add(dependency);
                    LinkedHashSet<String> first_level_dependencies_of_dependency = NodeGenerator.get_dependencies(dependency);
                    additional_dependencies.addAll(get_additional_method_dependencies(dependency, first_level_dependencies_of_dependency));
                    get_explored_dependencies().add(dependency);
                }


                /* NEWER

                if (get_collocation_dependency_hashmap().get(dependency)!=null && get_collocation_dependency_hashmap().get(dependency).contains(fragment_name)) { //if the *dependency* has already been examined, and *fragment_name* has been found to be a dependency of *dependency*
                    if (!primary_fragments.contains(dependency)) {
                        primary_fragments.add(fragment_name);
                        get_included_dependencies().add(dependency); //marks the nodes which are dependencies of another node. Removed support for it as the point is to define as many consistent groups as possible and let BtrPlace optimize the placement of resources.However, below we add some code that will prevent publishing groups which are a subset of another group
                    }else{
                        get_included_dependencies().add(fragment_name); //maybe this should be done when fragment is examined as a dependency, but adding this here to prevent possible deadlocks.
                    }
                }
               */

                /* OLD CODE, with parents list

                LinkedHashSet<String> new_parents;
                if (parents.get(fragment_name)!=null){
                    new_parents = parents.get(fragment_name);
                }else {
                    new_parents = new LinkedHashSet<>();
                    new_parents.add(fragment_name);
                    parents.put(fragment_name,new_parents);
                }

                boolean there_are_no_parents = parents.get(dependency)==null || parents.get(dependency).size()==0;
                boolean parent_dependency_is_itself_but_not_same_with_parent_fragment = parents.get(dependency)!=null && parents.get(dependency).contains(dependency) && !parents.get(dependency).equals(new_parents);

                if (there_are_no_parents || parent_dependency_is_itself_but_not_same_with_parent_fragment){ //if there existed some parents before, add them to the new ones
                    get_included_dependencies().add(dependency);
                }
                else {
                    new_parents.addAll(parents.get(dependency));
                }
                parents.put(dependency,new_parents);

                OLD CODE ENDS HERE */


            }
        }
        return additional_dependencies;
    }

    private static LinkedHashSet<String> get_additional_method_anti_dependencies(String fragment_name, LinkedHashSet<String> first_level_fragment_anti_dependencies) {
        LinkedHashSet<String> additional_anti_dependencies = new LinkedHashSet<>();
        if (first_level_fragment_anti_dependencies!=null) {
            for (String anti_dependency : first_level_fragment_anti_dependencies) {
                if (!get_visited_anti_dependencies().contains(anti_dependency)){
                    get_included_anti_dependencies().add(anti_dependency);
                }
                if (!(get_visited_anti_dependencies().contains(anti_dependency) || get_explored_anti_dependencies().contains(anti_dependency))) {
                    get_visited_anti_dependencies().add(anti_dependency);
                    LinkedHashSet<String> first_level_dependencies_of_anti_dependency = NodeGenerator.get_dependencies(anti_dependency);
                    additional_anti_dependencies.addAll(get_additional_method_anti_dependencies(anti_dependency, first_level_dependencies_of_anti_dependency));
                    get_explored_anti_dependencies().add(anti_dependency);
                }


                /* OLD CODE
                // get_included_dependencies().add(anti_dependency); //marks the nodes which are dependencies of another node. Removed support for it as the point is to define as many consistent groups as possible and let BtrPlace optimize the placement of resources.However, below we add some code that will prevent publishing groups which are a subset of another group



                LinkedHashSet<String> new_parents;
                if (parents.get(fragment_name)!=null){
                    new_parents = parents.get(fragment_name);
                }else {
                    new_parents = new LinkedHashSet<>();
                    new_parents.add(fragment_name);
                    parents.put(fragment_name,new_parents);
                }

                boolean there_are_no_parents = parents.get(anti_dependency)==null || parents.get(anti_dependency).size()==0;
                boolean parent_of_anti_dependency_is_itself_but_not_same_with_parent_of_fragment = parents.get(anti_dependency)!=null && parents.get(anti_dependency).contains(anti_dependency) && !parents.get(anti_dependency).equals(new_parents);

                if (there_are_no_parents || parent_of_anti_dependency_is_itself_but_not_same_with_parent_of_fragment){ //if there existed some parents before, add them to the new ones
                    get_included_anti_dependencies().add(anti_dependency);
                }
                else {
                    new_parents.addAll(parents.get(anti_dependency));
                }
                parents.put(anti_dependency,new_parents);

                 OLD CODE ENDS HERE */
            }
        }
        return additional_anti_dependencies;
    }


    private static LinkedHashMap<String, LinkedHashSet<String>> get_additional_method_dependencies_old(LinkedHashMap<String, LinkedHashSet<String>> method_dependency_hashmap) {

        LinkedHashMap<String, LinkedHashSet<String>> global_dependency_hashMap = new LinkedHashMap<>();
        HashSet<String> explored_dependencies = new LinkedHashSet<>();
        HashSet<String> additional_dependencies = new LinkedHashSet<>();

        for (HashMap.Entry<String, LinkedHashSet<String>> entry : method_dependency_hashmap.entrySet()) {
            do {
                entry.getValue().addAll(additional_dependencies); //add to the master dependency list the dependencies found during the last round
                additional_dependencies = new LinkedHashSet<>(); //re-initialize_dependency_data_structures

                for (String dependency : entry.getValue()) { //search for every dependency of the current method examined in the above for loop
                    boolean dependency_has_not_been_visited = !explored_dependencies.contains(dependency);
                    if (dependency_has_not_been_visited) {
                        if (method_dependency_hashmap.get(dependency)!=null) { //if the dependency has dependent elements, add all of them to the additional dependencies and mark the dependency as explored
                            LinkedHashSet<String> dependencies_of_dependency = new LinkedHashSet<>();
                            dependencies_of_dependency.addAll(method_dependency_hashmap.get(dependency));
                            additional_dependencies.addAll(dependencies_of_dependency);
                        }
                        explored_dependencies.add(dependency);
                    }
                }
            }while (additional_dependencies.size() > 0);
        }

        global_dependency_hashMap = get_collocation_dependency_hashmap();
        return global_dependency_hashMap;

    }

/*

        HashMap<String, LinkedHashSet<String>> global_dependency_hashSet = new LinkedHashMap<>();
        for (HashMap.Entry<String,LinkedHashSet<String>> entry: get_collocation_dependency_hashmap().entrySet()){
            String method_name = entry.getKey();

            LinkedHashSet<String> global_method_dependencies = entry.getValue() ,dependencies = entry.getValue();
            LinkedHashSet<String> additional_dependencies = new LinkedHashSet<>();

            while (dependencies.size()>0) {
                for (String dependency : dependencies) {
                    if (get_collocation_dependency_hashmap().get(dependency)!=null && get_collocation_dependency_hashmap().get(dependency).size()>0) {
                        additional_dependencies.addAll(get_collocation_dependency_hashmap().get(dependency));
                    }
                }
                dependencies = additional_dependencies;
                global_method_dependencies.addAll(additional_dependencies);
                additional_dependencies = new LinkedHashSet<>();
            }

            global_dependency_hashSet.put(method_name,global_method_dependencies);
        }
        return global_dependency_hashSet;
    }
    */

    private static void process_dependencies_of_fragment(String fragment_name, String[] annotated_method_dependencies, String[] annotated_method_anti_affinity_group) {

        LinkedHashSet<String> fragment_dependency_set = new LinkedHashSet<>(Arrays.asList(annotated_method_dependencies));
        LinkedHashSet<String> fragment_anti_affinity_set = new LinkedHashSet<>(Arrays.asList(annotated_method_anti_affinity_group));
        get_collocation_dependency_hashmap().put(fragment_name, fragment_dependency_set);
        get_anti_affinity_hashmap().put(fragment_name, fragment_anti_affinity_set);

    }
}
