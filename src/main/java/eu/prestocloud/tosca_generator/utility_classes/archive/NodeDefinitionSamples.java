package eu.prestocloud.tosca_generator.utility_classes.archive;

import eu.prestocloud.tosca_generator.node_types.NodeCapability;
import eu.prestocloud.tosca_generator.node_types.NodeType;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.MapDataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.StringTupleDataContainer;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.LIST_PRFX;

public class NodeDefinitionSamples {
    /**
     * A very basic example using the new format of Activeeon,
     * in which the basic definition of a JPPF-node is provided.
     *
     * @return A single node defining the basic attributes of a JPPF agent node
     */
    public static NodeType define_jppf_agent_node() {
        //TEST methods follow

        NodeType nodeType = new NodeType();
        nodeType.setDescription("A basic JPPF-agent node");
        nodeType.setDerived_from(tosca_root);
        nodeType.setName("prestocloud.nodes.jppf.Agent");

        //Capabilities list creation

        StringTupleDataContainer fragments_capability_data = new StringTupleDataContainer();
        fragments_capability_data.setName("type");
        fragments_capability_data.setValue(prestocloud_fragmentExecution);

        ArrayList<DataContainer> fragments_capability_attributes = new ArrayList<>();
        fragments_capability_attributes.add(fragments_capability_data);

        NodeCapability fragments_capability = new NodeCapability();
        fragments_capability.setName("fragments");
        fragments_capability.setAttributes(fragments_capability_attributes);

        ArrayList<NodeCapability> general_node_capabilities = new ArrayList<>();
        general_node_capabilities.add(fragments_capability);

        //Properties list creation

        LinkedHashMap<Object, Object> node_property_data_map = new LinkedHashMap<>();
        node_property_data_map.put("type", "version");
        node_property_data_map.put("required", "false");

        MapDataContainer node_property_data = new MapDataContainer();
        node_property_data.setData(node_property_data_map);
        node_property_data.setName("component_version");

        ArrayList<DataContainer> node_property_attributes = new ArrayList<>();
        node_property_attributes.add(node_property_data);
/*
        Property general_node_property = new Property();
        general_node_property.setName("component_version");
        general_node_property.setAttributes(node_property_attributes);

        ArrayList<Property> general_node_properties = new ArrayList<>();
        general_node_properties.add_node(general_node_property);*/


        //Requirements list creation

        DataContainerList general_node_requirements = new DataContainerList();
        DataContainerList general_node_requirement = new DataContainerList();

        MapDataContainer node_requirement_data = new MapDataContainer();
        node_requirement_data.add("capability", "prestocloud.capabilities.jppf.endpoint");
        node_requirement_data.add("node", "prestocloud.nodes.jppf.Master");
        node_requirement_data.add("relationship", prestocloud_relationships_connectsTo);

        //do not limit the maximum amount of active jpppf agents
        //node_requirement_data.add("occurrences", new AttributeRange(1, MAX_SCALE_OUT,true,true).toString());

        ArrayList<DataContainer> node_requirement_attributes = new ArrayList<>();
        node_requirement_attributes.add(node_requirement_data);

        general_node_requirement.setName("master");
        general_node_requirement.setName_prefix(LIST_PRFX);
        general_node_requirement.setAttributes(node_requirement_attributes);
        general_node_requirements.add(general_node_requirement);


        nodeType.setCapabilities(general_node_capabilities);
        nodeType.setProperties(node_property_attributes);
        nodeType.setRequirements(general_node_requirements);

        return nodeType;
    }

    public static NodeType define_jppf_master_node() {
        NodeType nodeType = new NodeType();
        nodeType.setDescription("A basic JPPF-master node");
        nodeType.setDerived_from(tosca_root);
        nodeType.setName("prestocloud.nodes.jppf.Master");

        //Capabilities list creation

        StringTupleDataContainer fragments_capability_data = new StringTupleDataContainer();
        fragments_capability_data.setName("type");
        fragments_capability_data.setValue("prestocloud.capabilities.jppf.endpoint");

        ArrayList<DataContainer> fragments_capability_attributes = new ArrayList<>();
        fragments_capability_attributes.add(fragments_capability_data);

        NodeCapability fragments_capability = new NodeCapability();
        fragments_capability.setName("jppf_endpoint");
        fragments_capability.setAttributes(fragments_capability_attributes);

        ArrayList<NodeCapability> general_node_capabilities = new ArrayList<>();
        general_node_capabilities.add(fragments_capability);

        //Properties list creation

        LinkedHashMap<Object, Object> node_property_data_map = new LinkedHashMap<>();
        node_property_data_map.put("type", "version");
        node_property_data_map.put("required", "false");

        MapDataContainer node_property_data = new MapDataContainer();
        node_property_data.setData(node_property_data_map);
        node_property_data.setName("component_version");

        ArrayList<DataContainer> node_property_attributes = new ArrayList<>();
        node_property_attributes.add(node_property_data);
/*
        Property general_node_property = new Property();
        general_node_property.setName("component_version");
        general_node_property.setAttributes(node_property_attributes);

        ArrayList<Property> general_node_properties = new ArrayList<>();
        general_node_properties.add_node(general_node_property);*/


        //Requirements list creation

        DataContainerList general_node_requirements = new DataContainerList();
        DataContainerList general_node_requirement = new DataContainerList();


        MapDataContainer node_requirement_data = new MapDataContainer();
        node_requirement_data.add("node", "tosca.nodes.Compute");
        node_requirement_data.add("capability", "tosca.capabilities.Container");
        node_requirement_data.add("relationship", "tosca.relationships.HostedOn");


        general_node_requirement.setName("host");
        general_node_requirement.setName_prefix(LIST_PRFX);
        general_node_requirement.add(node_requirement_data);
        general_node_requirements.add(general_node_requirement);


        nodeType.setCapabilities(general_node_capabilities);
        nodeType.setProperties(node_property_attributes);
        nodeType.setRequirements(general_node_requirements);

        return nodeType;
    }

    private static NodeType define_jppf_fragment() {
        NodeType nodeType = new NodeType();
        nodeType.setDescription("A TOSCA description of an annotated_application fragment");
        nodeType.setDerived_from(tosca_root);
        nodeType.setName("prestocloud.nodes.fragment");


        //Properties list creation

        MapDataContainer id_property_data = new MapDataContainer();
        id_property_data.setName("id");
        id_property_data.add("type","integer");
        id_property_data.add("required","true");

        MapDataContainer name_property_data = new MapDataContainer();
        name_property_data.setName("name");
        name_property_data.add("type","string");
        name_property_data.add("required","true");

        MapDataContainer onloadable_property_data = new MapDataContainer();
        onloadable_property_data.setName("onloadable");
        onloadable_property_data.add("type","boolean");
        onloadable_property_data.add("required","true");


        ArrayList<DataContainer> node_property_attributes = new ArrayList<>();
        node_property_attributes.add(id_property_data);
        node_property_attributes.add(name_property_data);
        node_property_attributes.add(onloadable_property_data);
/*
    Property general_node_property = new Property();
    general_node_property.setName("component_version");
    general_node_property.setAttributes(node_property_attributes);

    ArrayList<Property> general_node_properties = new ArrayList<>();
    general_node_properties.add_node(general_node_property);*/


        //Requirements list creation

        DataContainerList general_node_requirements = new DataContainerList();
        DataContainerList general_node_requirement = new DataContainerList();

        LinkedHashMap <Object,Object> node_requirement_data_map = new LinkedHashMap<>();
        node_requirement_data_map.put("capability", prestocloud_fragmentExecution);
        node_requirement_data_map.put("node", prestocloud_jppf_agent );
        node_requirement_data_map.put("relationship", prestocloud_relationships_executedBy);

        //Do not limit the maximum amount of available fragments
        //node_requirement_data_map.put("occurrences", new AttributeRange(1, MAX_SCALE_OUT,true,true).toString());



        MapDataContainer node_requirement_data = new MapDataContainer();
        node_requirement_data.setData(node_requirement_data_map);

        ArrayList<DataContainer> node_requirement_attributes = new ArrayList<>();
        node_requirement_attributes.add(node_requirement_data);

        general_node_requirement.setName("execute");
        general_node_requirement.setName_prefix(LIST_PRFX);
        general_node_requirement.setAttributes(node_requirement_attributes);
        general_node_requirements.add(general_node_requirement);

        nodeType.setProperties(node_property_attributes);
        nodeType.setRequirements(general_node_requirements);

        return nodeType;
    }
}
