package eu.prestocloud.tosca_generator.utility_classes;

public enum OsType {
    windows("windows"),mac_os("mac_os"),linux("linux"),solaris("solaris"),bsd("bsd");
    private final String fieldDescription;

    OsType(String value) {
        fieldDescription = value;
    }

    @Override
    public String toString() {
        return fieldDescription;
    }
}
