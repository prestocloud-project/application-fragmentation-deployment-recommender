package eu.prestocloud.tosca_generator.utility_classes;

public enum ExecutionLocation {

    all("all"),edge("edge"),cloud("cloud");

    private final String fieldDescription;

    ExecutionLocation(String value) {
        fieldDescription = value;
    }

    @Override
    public String toString() {
        return fieldDescription;
    }
}
