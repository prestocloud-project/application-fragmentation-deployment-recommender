package eu.prestocloud.tosca_generator.node_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class NodeCapability {
    private String name;
    private ArrayList<DataContainer> attributes = new ArrayList<>();

    public ArrayList<DataContainer> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<DataContainer> attributes) {
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addAttribute(DataContainer attribute){
        attributes.add(attribute);
    }

    public NodeCapability (){

    }
    public NodeCapability (String name){
        this.name = name;
    }

    @Override
    public String toString(){
        return toString(0);
    }
    public String toString(int indentation_level){

        StringBuilder string_representation = new StringBuilder();
        string_representation.append(indent(indentation_level)).append(LIST_PRFX).append(name).append(COLON_DELIMITER).append(NEWLINE);
        if (attributes!=null && attributes.size()>0) {
            for (DataContainer attribute:attributes) {
                string_representation.append(attribute.toString(indentation_level + 1));
            }
        }
        return string_representation.toString();

    }

}
