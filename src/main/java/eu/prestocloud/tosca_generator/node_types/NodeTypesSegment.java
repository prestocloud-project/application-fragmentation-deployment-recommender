package eu.prestocloud.tosca_generator.node_types;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

/**
 * This class is used to represent the node types segment of the TOSCA deployment file. It contains the initial and more elaborate definitions of the nodes that will be described in their final detail inside the topology_template segment.
 *
 * definition_node_types - The nodes that define the basic properties for each type of node.
 * concrete_nodes - The nodes that have more concrete properties (primarily through the use of a Nodefilter), and can be used for the description of the actual topology in the topology_template segment.
 */

public class NodeTypesSegment {


    ArrayList<NodeType> definition_node_types = new ArrayList<>();
    ArrayList<NodeType> processing_node_types = new ArrayList<>();



    public void addDefinition_node_type(NodeType nodeType){
        definition_node_types.add(nodeType);
    }

    public void addProcessing_node_type(NodeType nodeType){
        processing_node_types.add(nodeType);
    }

    @Override
    public String toString() {
        return toString(0);
    }
    public String toString(int indentation_level) {
        StringBuilder string_representation = new StringBuilder();

        //The text field is added in the ToscaDocument toString() method
        //string_representation.append(indent(indentation_level)).append("node_types").append(COLON_DELIMITER).append(NEWLINE);

        if (definition_node_types.size()>0) {
            string_representation.append(indent(indentation_level)).append("#Node definitions").append(COLON_DELIMITER).append(NEWLINE);
        }
        for (NodeType definition_node_type: definition_node_types){
            string_representation.append(definition_node_type.toString(indentation_level));
            string_representation.append(NEWLINE);
        }

        if (processing_node_types.size()>0) {
            string_representation.append(indent(indentation_level)).append("#Processing node selection").append(COLON_DELIMITER).append(NEWLINE);
        }
        for (NodeType processing_node_type: processing_node_types){
            string_representation.append(processing_node_type.toString(indentation_level));
            string_representation.append(NEWLINE);
        }

        return string_representation.toString();
    }

}
