package eu.prestocloud.tosca_generator.node_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class NodeType {
    private String description=EMPTY;
    private String derived_from=EMPTY;
    private String name=EMPTY;
    private ArrayList<NodeCapability> capabilities;
    private ArrayList<DataContainer> properties;
    private DataContainerList requirements;

    public DataContainerList getRequirements() {
        return requirements;
    }

    public void setRequirements(DataContainerList requirements) {
        this.requirements = requirements;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDerived_from() {
        return derived_from;
    }

    public void setDerived_from(String derived_from) {
        this.derived_from = derived_from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<NodeCapability> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(ArrayList<NodeCapability> capabilities) {
        this.capabilities = capabilities;
    }

    public ArrayList<DataContainer> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<DataContainer> properties) {
        this.properties = properties;
    }

    public void add(DataContainer property){
        properties.add(property);
    }

    public void add(NodeCapability capability){
        capabilities.add(capability);
    }

    public void add (DataContainerList requirement){
        requirements.add(requirement);
    }

    @Override
    public String toString(){
        return toString(0);
    }

    public String toString(int indentation_level) {
        StringBuilder string_representation = new StringBuilder(indent(indentation_level)+name+ COLON_DELIMITER +NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("description").append(COLON_DELIMITER).append(SPACE).append(description).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("derived_from").append(COLON_DELIMITER).append(SPACE).append(derived_from).append(NEWLINE);
        //mapPrintingMethod(string_representation,indentation_level+2,requirements);

        if (capabilities!=null && capabilities.size()>0) {
            string_representation.append(indent(indentation_level + 1)).append("capabilities").append(COLON_DELIMITER).append(NEWLINE);

            for (NodeCapability capability : capabilities) {
                string_representation.append(capability.toString(indentation_level + 2));
            }
        }

        if (properties!=null && properties.size()>0) {
            string_representation.append(indent(indentation_level + 1)).append("properties").append(COLON_DELIMITER).append(NEWLINE);

            for (DataContainer nodeProperty : properties) {
                string_representation.append(nodeProperty.toString(indentation_level + 2));
            }
        }

        if (requirements!=null && requirements.getAttributes().size()>0) {
            string_representation.append(indent(indentation_level + 1)).append("requirements").append(COLON_DELIMITER).append(NEWLINE);

            for (DataContainer requirement : requirements.getAttributes()) {
                string_representation.append(requirement.toString(indentation_level + 2));
            }
        }

        return string_representation.toString();
    }

}
