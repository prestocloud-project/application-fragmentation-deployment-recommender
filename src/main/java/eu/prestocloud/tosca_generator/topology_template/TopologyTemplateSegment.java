package eu.prestocloud.tosca_generator.topology_template;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainerList;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.ExtendedDataContainer;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

/**
 * This class contains the nodes described in the topology template segment of the architecture.
 * @field node_templates This field contains the Instance nodes which represent the final description of the nodes to be deployed.
 * jppf_agents - The names of the jppf agents which are currently active in the topology
 * jppf_masters - The names of the jppf masters which are currently active in the topology
 */
public class TopologyTemplateSegment {

    ArrayList<NodeTemplate> node_templates = new ArrayList<>();
    DataContainerList policies = new DataContainerList();
    DataContainerList inputs = new DataContainerList();

    ArrayList<String> jppf_masters = new ArrayList<>();
    int jppf_master_counter = 0;
    ArrayList<String> jppf_agents = new ArrayList<>();
    int jppf_agent_counter = 0;

    public ArrayList<NodeTemplate> getNode_templates() {
        return node_templates;
    }

    public void setNode_templates(ArrayList<NodeTemplate> node_templates) {
        this.node_templates = node_templates;
    }

    public void add_node(NodeTemplate node_template){
        node_templates.add(node_template);
    }

    public void add_nodes(ArrayList<NodeTemplate> new_node_templates){
        for (NodeTemplate node_template : new_node_templates){
            node_templates.add(node_template);
        }
    }

    public void addJPPFMaster (NodeTemplate instanceNode){
        jppf_masters.add(instanceNode.getName());
    }

    public void addJPPFAgent (NodeTemplate instanceNode){
        jppf_agents.add(instanceNode.getName());
    }

    public void add_policy(ExtendedDataContainer policy) {
        policies.add(policy);
    }

    public DataContainerList getPolicies() {
        return policies;
    }

    public void addPolicies (DataContainerList policies){
        for (DataContainer policy : policies.getAttributes()){
            this.policies.add(policy);
        }
    }
    public void setPolicies(DataContainerList policies) {
        this.policies = new DataContainerList();
        for (DataContainer policy : policies.getAttributes()){
            this.policies.add(policy);
        }
    }

    public ArrayList<String> getJppf_masters() {
        return jppf_masters;
    }

    public void setJppf_masters(ArrayList<String> jppf_masters) {
        this.jppf_masters = jppf_masters;
    }

    public int getJppf_master_counter() {
        return jppf_master_counter;
    }

    public void setJppf_master_counter(int jppf_master_counter) {
        this.jppf_master_counter = jppf_master_counter;
    }

    public ArrayList<String> getJppf_agents() {
        return jppf_agents;
    }

    public void setJppf_agents(ArrayList<String> jppf_agents) {
        this.jppf_agents = jppf_agents;
    }

    public int getJppf_agent_counter() {
        return jppf_agent_counter;
    }

    public void setJppf_agent_counter(int jppf_agent_counter) {
        this.jppf_agent_counter = jppf_agent_counter;
    }

    public DataContainerList getInputs() {
        return inputs;
    }

    public void setInputs(DataContainerList inputs) {
        this.inputs = inputs;
    }

    public void addInputs(DataContainerList inputs) {
        this.inputs = inputs;
    }
    @Override
    public String toString(){
        return toString(0);
    }

    /**
     * The String representation of a topology template firstly presents the required inputs, then the policies to be respected, and finally the deployment nodes
     * @param indentation_level
     * @return
     */
    public String toString(int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        if (inputs.getAttributes().size()>0) {
            string_representation.append(indent(indentation_level)).append("inputs").append(COLON_DELIMITER).append(NEWLINE);
            for (DataContainer input: inputs.getAttributes()) {
                string_representation.append(input.toString(indentation_level + 1)).append(NEWLINE); //Purposedly adding a new line to separate the policies. The policy datacontainerlist should be always prefixed by a LIST_PRFX.
            }
        }

        if (policies.getAttributes().size()>0) {
            string_representation.append(indent(indentation_level)).append("policies").append(COLON_DELIMITER).append(NEWLINE);
            for (DataContainer policy : policies.getAttributes()) {
                string_representation.append(policy.toString(indentation_level + 1)).append(NEWLINE); //Purposedly adding a new line to separate the policies. The policy datacontainerlist should be always prefixed by a LIST_PRFX.
            }
        }
        if (node_templates.size()>0) {
            string_representation.append(indent(indentation_level)).append("node_templates").append(COLON_DELIMITER).append(NEWLINE);
            for (NodeTemplate node_template : node_templates) {
                string_representation.append(node_template.toString(indentation_level + 1)).append(NEWLINE); //Purposedly adding a new line to seperate the node templates from one another.
            }
        }
        return string_representation.toString();
    }
}
