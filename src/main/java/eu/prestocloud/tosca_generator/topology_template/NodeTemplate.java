package eu.prestocloud.tosca_generator.topology_template;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;

import java.util.ArrayList;

import static eu.prestocloud.configuration.Constants.TOSCA_MAPPING_NODE;
import static eu.prestocloud.tosca_generator.NodeGenerator.create_processed_fragment_name;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

/**
 * This class contains the definition for an instance node - a node which describes the interactions between the concrete nodes mentioned in the node_types layer. The properties field is of type "DataContainer Arraylist", but could be changed to become an ExtendedDataContainer
 */

public class NodeTemplate {

    private String name;
    private String type;
    private ArrayList<DataContainer> properties= new ArrayList<>();
    private ArrayList<DataContainer> requirements= new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<DataContainer> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<DataContainer> properties) {
        this.properties = properties;
    }

    public ArrayList<DataContainer> getRequirements() {
        return requirements;
    }

    public void setRequirements(ArrayList<DataContainer> requirements) {
        this.requirements = requirements;
    }

    public void addProperty (DataContainer property){
        properties.add(property);
    }

    public void addRequirement (DataContainer requirement){
        requirements.add(requirement);
    }

    public static String get_mapping_node_name(String fragment_name){
        StringBuilder mapping_node_name = new StringBuilder(TOSCA_MAPPING_NODE).append(create_processed_fragment_name(fragment_name));
        return mapping_node_name.toString();
    }

    @Override
    public String toString (){
        return toString(0);
    }

    public String toString (int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        string_representation.append(indent(indentation_level)).append(name).append(COLON_DELIMITER).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("type").append(COLON_DELIMITER).append(SPACE).append(type).append(NEWLINE);


        if (properties.size() > 0){
            string_representation.append(indent(indentation_level + 1)).append("properties").append(COLON_DELIMITER).append(NEWLINE);
        }
        for (DataContainer property:properties){
            string_representation.append(property.toString(indentation_level + 2 ));
        }


        if (requirements.size() > 0){
            string_representation.append(indent(indentation_level + 1)).append("requirements").append(COLON_DELIMITER).append(NEWLINE);
        }
        for (DataContainer requirement : requirements){
            string_representation.append(requirement.toString(indentation_level + 2));
        }

        return string_representation.toString();
    }
}
