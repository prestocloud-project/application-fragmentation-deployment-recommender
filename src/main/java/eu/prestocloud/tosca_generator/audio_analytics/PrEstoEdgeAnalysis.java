package eu.prestocloud.tosca_generator.audio_analytics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import audioanalytics.OnBoardAnalysis;
import eu.prestocloud.annotations.PrestoFragmentation;

/**
 *
 * @author aditess
 */

public class PrEstoEdgeAnalysis {
    
    OnBoardAnalysis analysis;

    public PrEstoEdgeAnalysis(String model){
         analysis= new OnBoardAnalysis("ClassificationModels/"+model+".model");           
    }

    @PrestoFragmentation(elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.jppf, onloadable = true, memoryLoad = PrestoFragmentation.MemoryLoad.HIGH, cpuLoad = PrestoFragmentation.CPULoad.MEDIUM, storageLoad = PrestoFragmentation.StorageLoad.LOW,offloadable = true)
    public String Classify(double[] params){
        return analysis.Classify(params);
    }
}
