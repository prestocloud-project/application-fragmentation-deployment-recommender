package eu.prestocloud.tosca_generator.audio_analytics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import audioanalytics.OnCloudAnalysis;
import eu.prestocloud.annotations.PrestoFragmentation;

/**
 *
 * @author aditess
 */
public class PrEstoCloudAnalysis {
    OnCloudAnalysis cloudAnalysis;

    @PrestoFragmentation(onloadable = false, memoryLoad = PrestoFragmentation.MemoryLoad.HIGH, cpuLoad = PrestoFragmentation.CPULoad.HIGH, storageLoad = PrestoFragmentation.StorageLoad.LOW,elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.jppf, offloadable = true)
    public PrEstoCloudAnalysis(String model){
        cloudAnalysis = new OnCloudAnalysis("ClassificationModels/"+model+".model");
    }

    @PrestoFragmentation(onloadable = false, memoryLoad = PrestoFragmentation.MemoryLoad.HIGH, cpuLoad = PrestoFragmentation.CPULoad.VERY_HIGH, storageLoad = PrestoFragmentation.StorageLoad.MEDIUM,elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.jppf, offloadable = true)

    public String Classify(double[] params){
        return cloudAnalysis.Classify(params);
    }
}
