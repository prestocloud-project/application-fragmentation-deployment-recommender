package eu.prestocloud.tosca_generator.audio_analytics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aditess
 */
public class PrEstoAudioAnalytics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            
           
            double[] params ={
                -17.33636,2.2873,-0.688164,-1.001091,-1.048297,
                0.023298,-0.39139,-0.028932,-0.264,0.312893,
                0.414248,-0.129492,-0.102943,30.938196,281};
            
            /*Edge Side Analysis Test*/
            
            PrEstoEdgeAnalysis edgeAnalysis = new PrEstoEdgeAnalysis("RFSNS_ROC1");

            long startTime=System.nanoTime();
            edgeAnalysis.Classify(params);
            long elapsedTime=System.nanoTime()-startTime;
            System.out.println("[Edge Side] Elapsed Time (ns) "+ elapsedTime);
            
            /*OnCloudAnalysis Test*/
            PrEstoCloudAnalysis cloudAnalysis = new PrEstoCloudAnalysis("NBU_ROC1");

            startTime=System.nanoTime();
            cloudAnalysis.Classify(params);
            elapsedTime=System.nanoTime()-startTime;
            System.out.println("[Cloud Side] Elapsed Time (ns) "+ String.valueOf(elapsedTime));
            
        } catch (Exception ex) {
            Logger.getLogger(PrEstoAudioAnalytics.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
