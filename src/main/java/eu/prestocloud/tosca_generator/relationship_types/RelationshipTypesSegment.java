package eu.prestocloud.tosca_generator.relationship_types;

import java.util.ArrayList;

public class RelationshipTypesSegment {

    private ArrayList<RelationshipType> relationship_types = new ArrayList<>();


    @Override
    public String toString (){
        return toString(0);
    }

    public void add (RelationshipType relationship) {
        relationship_types.add(relationship);
    }

    public String toString (int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        for (RelationshipType relationship: relationship_types){
            string_representation.append(relationship.toString(indentation_level));
        }

        return string_representation.toString();
    }
}
