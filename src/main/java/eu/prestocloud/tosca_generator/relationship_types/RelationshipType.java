package eu.prestocloud.tosca_generator.relationship_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class RelationshipType {
    private String name;
    private String derived_from;
    private String valid_target_types;
    private ArrayList<DataContainer> properties = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDerived_from() {
        return derived_from;
    }

    public void setDerived_from(String derived_from) {
        this.derived_from = derived_from;
    }

    public String getValid_target_types() {
        return valid_target_types;
    }

    public void setValid_target_types(String valid_target_types) {
        this.valid_target_types = valid_target_types;
    }

    public ArrayList<DataContainer> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<DataContainer> properties) {
        this.properties = properties;
    }

    public void add (DataContainer property){
        properties.add(property);
    }

    public RelationshipType(String name){
        this.name = name;
    }

    public RelationshipType(){

    }

    @Override
    public String toString(){
        return  name;
    }

    public String toString(int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        string_representation.append(indent(indentation_level)).append(name).append(COLON_DELIMITER).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1 )).append("derived_from").append(COLON_DELIMITER).append(SPACE).append(derived_from).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("valid_target_types").append(COLON_DELIMITER).append(SPACE).append(LIST_START).append(SPACE).append(valid_target_types).append(SPACE).append(LIST_END).append(NEWLINE);

        if (properties.size() > 0) string_representation.append(indent(indentation_level+1)).append("properties").append(COLON_DELIMITER).append(NEWLINE);
        for (DataContainer property:properties){
            string_representation.append(property.toString(indentation_level+2));
        }

        return string_representation.toString();
    }
}
