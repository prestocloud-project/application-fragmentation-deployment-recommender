package eu.prestocloud.tosca_generator.relationship_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.MapDataContainer;

import static eu.prestocloud.configuration.Constants.*;

public class RelationshipGenerator {
    public static RelationshipTypesSegment create_relationship_types_segment() {

        RelationshipTypesSegment relationship_types_segment = default_relationship_types_segment() ;

        return relationship_types_segment;
    }



    private static RelationshipTypesSegment default_relationship_types_segment(){
        RelationshipTypesSegment relationship_types_segment = new RelationshipTypesSegment();
        relationship_types_segment.add(create_jppf_connectsTo());
        relationship_types_segment.add(create_jppf_ExecutedBy());
        return relationship_types_segment;
    }
    private static RelationshipType create_jppf_connectsTo() {
        RelationshipType relationship = new RelationshipType();
        MapDataContainer attributes_data = new MapDataContainer();

        //credential property
        attributes_data.add("type",tosca_datatypes_credential);
        attributes_data.add("required","false");
        attributes_data.setName("credential");

        relationship.add(attributes_data);

        relationship.setName(prestocloud_relationships_connectsTo);
        relationship.setDerived_from(tosca_relationships_root);
        relationship.setValid_target_types("prestocloud.nodes.jppf.endpoint");

        return relationship;
    }
    private static RelationshipType create_jppf_ExecutedBy() {
        RelationshipType relationship = new RelationshipType();

        relationship.setName(prestocloud_relationships_executedBy);
        relationship.setDerived_from(tosca_relationships_root);
        relationship.setValid_target_types(prestocloud_fragmentExecution);

        return relationship;
    }
}
