package eu.prestocloud.tosca_generator;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.messaging.RestCommunicator;
import eu.prestocloud.tosca_generator.node_types.NodeType;
import eu.prestocloud.tosca_generator.node_types.NodeTypesSegment;
import eu.prestocloud.tosca_generator.policy_file_requirements.MappingRequirement;
import eu.prestocloud.tosca_generator.topology_template.NodeTemplate;
import eu.prestocloud.tosca_generator.topology_template.TopologyTemplateSegment;
import eu.prestocloud.tosca_generator.utility_classes.*;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.*;
import eu.prestocloud.tosca_generator.utility_classes.dependencies.BidirectionalCollocationDependencies;
import eu.prestocloud.tosca_generator.utility_classes.dependencies.ForwardPrecedenceDependencies;
import eu.prestocloud.tosca_generator.utility_classes.fragments.Fragment;
import eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures;
import eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentTypes;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static eu.prestocloud.annotations.PrestoFragmentation.Elasticity_mechanism.*;
import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.ToscaGenerator.FIRST_RUN;
import static eu.prestocloud.tosca_generator.policy_file_requirements.MappingRequirement.translate;
import static eu.prestocloud.tosca_generator.topology_template.NodeTemplate.get_mapping_node_name;
import static eu.prestocloud.tosca_generator.utility_classes.CoordinatorNodesController.*;
import static eu.prestocloud.tosca_generator.utility_classes.ExecutionLocation.cloud;
import static eu.prestocloud.tosca_generator.utility_classes.ExecutionLocation.edge;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.BidirectionalCollocationDependencies.proximity_policies;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.*;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.add_fragment;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.fragments_map;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentTypes.*;
import static eu.prestocloud.utilities.AnnotatedFragmentsJarProcessor.get_annotated_fragments;
import static eu.prestocloud.utilities.UIJsonParser.calculate_dependencies_of_ui_fragments;
import static eu.prestocloud.utilities.UIJsonParser.get_ui_fragments;
import static java.util.logging.Level.*;

/**
 * This class contains methods which can be used to instantiate new TOSCA nodes, aligned to the PrEstoCloud format.
 * The first section contains a basic example of a manually-coded processing node, and a basic example of a node definition.
 * Then, follow some abstraction methods which allow the creation of processing nodes with specific characteristics.
 * The last section contains two lower-level methods which can be used to instantiate processing nodes
 */

public class NodeGenerator {

    //Debug variables
    private static final boolean ADD_PROXY_INFO_NODE_LEVEL = true;
    public static final boolean PROCESS_ANNOTATION_NAMES = true;
    //public static int MAX_TOPOLOGY_SCALE_OUT = 50;
    //public static int CURRENT_TOPOLOGY_NODES = 0;
    //public static int MAX_FRAG_SCALE_OUT = 10;
    public static int MIN_FRAG_SCALE_IN = 1; //this variable is only set here
    private static final boolean DEBUG_MESSAGES = false;
    private static final boolean IMPLICIT_CLASS_ANNOTATIONS_ACCEPTED = true;
    //private static int MAX_JPPF_MASTER_INSTANCES = Integer.MAX_VALUE;
    //private static int MAX_FAAS_PROXY_INSTANCES = Integer.MAX_VALUE;
    //private static int MAX_LOAD_BALANCER_INSTANCES = 1;
    private static MappingRequirement mapping_requirement;

    static ToscaDocument core_tosca_document = new ToscaDocument();
    static NodeTypesSegment node_types_segment = new NodeTypesSegment();
    static TopologyTemplateSegment topology_template_segment = new TopologyTemplateSegment();
    private static Map<String,String> fragment_processing_node_names = new HashMap<>();
    private static Map<String,Integer> fragment_IDs = new HashMap<>();
    public static int counter=0;
    public static final SynchronizedBoolean ui_deployment_ready  = new SynchronizedBoolean(false);
    public static int excluded_devices_policy_counter = 0;
    public static boolean ui_based_deployment=false, annotations_based_deployment=false;
/*
    public NodeGenerator(DeploymentRequirement deployment_requirement, MappingRequirement mapping_requirement){

        MAX_FRAG_SCALE_OUT = deployment_requirement.getMax_fragment_instances();
        MAX_JPPF_MASTER_INSTANCES = deployment_requirement.getMax_master_instances();
        this.mapping_requirement = mapping_requirement;

    }*/

    public static void initialize_node_generator(MappingRequirement new_mapping_requirement){
        //MAX_FRAG_SCALE_OUT = new_deployment_requirement.getMax_fragment_instances();
        //MAX_TOPOLOGY_SCALE_OUT = new_deployment_requirement.getMax_instances();
        //MAX_JPPF_MASTER_INSTANCES = new_deployment_requirement.getMax_master_instances();
        mapping_requirement = new_mapping_requirement;
    }


    /**
     * This is the main method used to create processing nodes for all fragments.
     * @param elasticity_mechanism The framework used for the elasticity of the fragments (jppf/faas or none)
     * @param min_cpus_cloud The minimum amount of cpus in a cloud processing node
     * @param max_cpus_cloud The maximum amount of cpus in a cloud processing node
     * @param min_memory_mb_cloud The minimum amount of memory in a cloud processing node
     * @param max_memory_mb_cloud The maximum amount of memory in a cloud processing node
     * @param min_storage_gb_cloud The minimum amount of storage in a cloud processing node
     * @param max_storage_gb_cloud The maximum amount of storage in a cloud processing node
     * @param min_cpus_edge The minimum amount of cpus in an edge processing node
     * @param max_cpus_edge The maximum amount of cpus in an edge processing node
     * @param min_memory_mb_edge The minimum amount of memory in an edge processing node
     * @param max_memory_mb_edge The maximum amount of memory in an edge processing node
     * @param min_storage_gb_edge The minimum amount of storage in an edge processing node
     * @param max_storage_gb_edge The maximum amount of storage in an edge processing node
     * @param edge_processing_node Whether this processing node can be hosted on cloud machines
     * @param cloud_processing_node Whether this processing node can be hosted on edge machines
     * @param excluded_edge_device_identifiers The identifiers of edge devices which should not be used as hosts of this processing node
     * @param necessary_sensors The sensor types (e.g microphone, camera etc.) which should be enabled on the target deployment device
     * @param processing_node_name The name of the processing node in the generated TOSCA file
     * @param processing_node_type The TOSCA type of the processing node
     * @param processing_node_description A description of the processing node
     * @return A TOSCA node describing the processing node which should undertake processing of a particular fragment
     */


    public static NodeType create_tosca_linux_processing_node(
            int min_cpus_cloud, int max_cpus_cloud,
            int min_memory_mb_cloud, int max_memory_mb_cloud,
            int min_storage_gb_cloud, int max_storage_gb_cloud,
            int min_cpus_edge, int max_cpus_edge,
            int min_memory_mb_edge, int max_memory_mb_edge,
            int min_storage_gb_edge, int max_storage_gb_edge,
            boolean edge_processing_node, boolean cloud_processing_node,
            ArrayList<String> excluded_edge_device_identifiers,
            LinkedHashMap<String,String> necessary_sensors,
            String processing_node_name,
            String processing_node_type,
            String processing_node_description
    ){

        DataContainerList processing_node_requirements = new DataContainerList();
        //--- Node metadata
        String node_name = processing_node_name;


        
        if (cloud_processing_node) {
            ResourcesDataContainer cloud_resources_data_container = new ResourcesDataContainer("properties",new AttributeRange(min_cpus_cloud,max_cpus_cloud),new AttributeRange(min_memory_mb_cloud,max_memory_mb_cloud,"MB"),new AttributeRange(min_storage_gb_cloud,max_storage_gb_cloud,"GB"));
            cloud_resources_data_container.setEntry_prefix(LIST_PRFX);

            ArrayList<OsArchitecture> node_architectures = new ArrayList<>();
            LinkedHashMap<Object, Object> processing_agent_location = new LinkedHashMap<>();
            ArrayList<ExecutionLocation> execution_placement_locations= new ArrayList<>();
            execution_placement_locations.add(cloud);
            
            node_architectures.add(OsArchitecture.x86_64);
            if (!(cloud_resources_data_container.getMem_size().getLower_bound() > 4096)) { //the 32-bit architecture should be added only if less than 4 GB of RAM will be used in the machine
                node_architectures.add(OsArchitecture.i386);
            }
            processing_agent_location.put("type", brace_list_format(execution_placement_locations));

            //--- Node OS properties
            LinkedHashMap<Object, Object> processing_agent_os_map = new LinkedHashMap<>();
            processing_agent_os_map.put("architecture", brace_list_format(node_architectures));
            processing_agent_os_map.put("type", brace_list_format(EQUALS, OsType.linux.toString()));
            processing_agent_os_map.put("distribution", brace_list_format(EQUALS, OsDistribution.ubuntu.toString()));

            //--- NodeFilter metadata
            LinkedHashMap<Object, Object> processing_host_metadata_map = new LinkedHashMap<>();
            processing_host_metadata_map.put("capability", tosca_capabilities_container);
            processing_host_metadata_map.put("node", prestocloud_compute);
            processing_host_metadata_map.put("relationship", tosca_relationships_hostedOn);

            MapDataContainer processing_host_metadata  = new MapDataContainer();
            processing_host_metadata.setData(processing_host_metadata_map);

            //-- Nodefilter definition

            DataContainerList node_filter = get_node_filter(cloud_resources_data_container,processing_agent_os_map,processing_agent_location,new LinkedHashMap<>());

            //- Node definition

            ArrayList<DataContainer> cloud_attributes_defined = new ArrayList<>();
            cloud_attributes_defined.add(processing_host_metadata);
            cloud_attributes_defined.add(node_filter);

            DataContainerList cloud_processing_node_requirement = new DataContainerList();
            cloud_processing_node_requirement.setName("host");
            cloud_processing_node_requirement.setName_prefix(LIST_PRFX);
            cloud_processing_node_requirement.setAttributes(cloud_attributes_defined);

            processing_node_requirements.add(cloud_processing_node_requirement);

        }

        if (edge_processing_node){


            ResourcesDataContainer edge_resources_data_container = new ResourcesDataContainer("properties",new AttributeRange(min_cpus_edge,max_cpus_edge),new AttributeRange(min_memory_mb_edge,max_memory_mb_edge,"MB"),new AttributeRange(min_storage_gb_edge,max_storage_gb_edge,"GB"));


            edge_resources_data_container.setEntry_prefix(LIST_PRFX);
            
            //--- Edge/Cloud execution placement properties
            ArrayList<OsArchitecture> node_architectures = new ArrayList<>();
            ArrayList<String> operating_systems = new ArrayList<>();
            LinkedHashMap<Object, Object> processing_agent_location = new LinkedHashMap<>();
            ArrayList<ExecutionLocation> execution_placement_locations= new ArrayList<>();
            LinkedHashMap<Object, Object> sensors_required = new LinkedHashMap<>();

            operating_systems.add(OsDistribution.raspbian.toString());
            operating_systems.add(OsDistribution.ubuntu.toString());
            execution_placement_locations.add(edge);
            node_architectures.add(OsArchitecture.arm64);
            node_architectures.add(OsArchitecture.armel);
            node_architectures.add(OsArchitecture.armhf);
            node_architectures.add(OsArchitecture.x86_64);
            node_architectures.add(OsArchitecture.i386);

            processing_agent_location.put("type", brace_list_format(execution_placement_locations));

            for (HashMap.Entry <String,String> sensor_entry : necessary_sensors.entrySet()) {
                sensors_required.put(sensor_entry.getKey(), brace_list_format(RelationStrings.equal,quoted(sensor_entry.getValue())));
            }

            //--- Node OS properties
            LinkedHashMap<Object, Object> processing_agent_os_map = new LinkedHashMap<>();
            processing_agent_os_map.put("architecture", brace_list_format(node_architectures));
            processing_agent_os_map.put("type", brace_list_format(EQUALS, OsType.linux.toString()));
            processing_agent_os_map.put("distribution", brace_list_format(operating_systems));

            //--- NodeFilter metadata
            LinkedHashMap<Object, Object> processing_host_metadata_map = new LinkedHashMap<>();
            processing_host_metadata_map.put("capability", tosca_capabilities_container);
            processing_host_metadata_map.put("node", prestocloud_compute);
            processing_host_metadata_map.put("relationship", tosca_relationships_hostedOn);

            //--- Excluded edge devices information
            LinkedHashMap<Object, Object> excluded_edge_devices = new LinkedHashMap<>();
            if (excluded_edge_device_identifiers.size()>0) {
                excluded_edge_devices.put("identifier", brace_list_format(excluded_edge_device_identifiers));
            }

            //-- Nodefilter definition

            DataContainer node_filter = get_node_filter(edge_resources_data_container,processing_agent_os_map,processing_agent_location,sensors_required);
;

            MapDataContainer processing_host_metadata  = new MapDataContainer();
            processing_host_metadata.setData(processing_host_metadata_map);
//- Node definition

            ArrayList<DataContainer> edge_attributes_defined = new ArrayList<>();
            edge_attributes_defined.add(processing_host_metadata);
            edge_attributes_defined.add(node_filter);

            DataContainerList edge_processing_node_requirement = new DataContainerList();
            edge_processing_node_requirement.setName("host");
            edge_processing_node_requirement.setName_prefix(LIST_PRFX);
            edge_processing_node_requirement.setAttributes(edge_attributes_defined);

            processing_node_requirements.add(edge_processing_node_requirement);

        }


        /*
        if (processing_node_type.equals(prestocloud_jppf_agent)) {
            StringTupleDataContainer master_node_requirement = new StringTupleDataContainer();
            master_node_requirement.setName("master");
            master_node_requirement.setValue(MasterNodesController.get_coordinating_master_node(FragmentDataStructures.fragments_map.get(node_name)));
            master_node_requirement.setName_prefix(LIST_PRFX);
            //Logger.getAnonymousLogger().log(Level.INFO, "An attempt was made to associate fragment " + node_name + " with a master node");
            processing_node_requirements.add(master_node_requirement);
        }
        */

        NodeType processing_node = new NodeType();
        processing_node.setName(node_name);
        processing_node.setDescription(processing_node_description);
        processing_node.setDerived_from(processing_node_type);
        processing_node.setRequirements(processing_node_requirements);

        return processing_node;
    }

    private static DataContainerList get_node_filter(DataContainer resources_data_container, LinkedHashMap<Object,Object>processing_agent_os_map, LinkedHashMap<Object,Object> processing_agent_location, LinkedHashMap<Object,Object> sensors_required) {
        //--- Host properties

        ExtendedDataContainer host_properties = get_node_filter_capabilities_host_properties(resources_data_container);
        //--- OS properties

        ExtendedDataContainer os_properties =  get_node_filter_capabilities_OS_properties(processing_agent_os_map);

        //--- Execution placement location properties

        ExtendedDataContainer execution_properties = get_node_filter_capabilities_execution_location_properties(processing_agent_location);

        //--- Sensor presence constraints

        ExtendedDataContainer sensor_properties = get_node_filter_capabilities_sensor_properties(sensors_required);

       //-- Capabilities definition
        
        ArrayList<ExtendedDataContainer> properties = new ArrayList<>();
        properties.add(host_properties);
        properties.add(os_properties);
        properties.add(execution_properties);
        properties.add(sensor_properties);
        
        DataContainerList node_filter_capabilities = new DataContainerList();

        for (ExtendedDataContainer property:properties) {
            if (property.getData()!=null) {
                node_filter_capabilities.add(property);
            }
        }
        node_filter_capabilities.setName("capabilities");
        
        DataContainerList node_filter = new DataContainerList();
        node_filter.add(node_filter_capabilities);
        node_filter.setName("node_filter");
        
        return  node_filter;
    }


    private static DataContainerList get_node_filter_capabilities(ExtendedDataContainer... properties) {
        DataContainerList node_filter_capabilities = new DataContainerList();

        for (ExtendedDataContainer property:properties) {
            if (property.getData()!=null) {
                node_filter_capabilities.add(property);
            }
        }
        node_filter_capabilities.setName("capabilities");
        return node_filter_capabilities;
    }

    private static ExtendedDataContainer get_node_filter_capabilities_OS_properties(LinkedHashMap<Object, Object> host_node_os_map) {
        MapDataContainer os_properties_data =  new MapDataContainer();
        os_properties_data.setName("properties");
        os_properties_data.setData(host_node_os_map);
        os_properties_data.setEntry_prefix(LIST_PRFX);

        ExtendedDataContainer node_capability= new ExtendedDataContainer();
        node_capability.setExtended_data_container_name("os");
        node_capability.setData(os_properties_data);
        node_capability.setPrefix(LIST_PRFX);
        return node_capability;
    }

    private static ExtendedDataContainer get_node_filter_capabilities_execution_location_properties(LinkedHashMap<Object, Object> execution_location_map) {
        MapDataContainer execution_location_properties_data =  new MapDataContainer();
        execution_location_properties_data.setName("properties");
        execution_location_properties_data.setData(execution_location_map);
        execution_location_properties_data.setEntry_prefix(LIST_PRFX);

        ExtendedDataContainer execution_location_properties= new ExtendedDataContainer();
        execution_location_properties.setExtended_data_container_name("resource");
        execution_location_properties.setData(execution_location_properties_data);
        execution_location_properties.setPrefix(LIST_PRFX);
        return execution_location_properties;
    }

    private static ExtendedDataContainer get_node_filter_capabilities_sensor_properties(LinkedHashMap<Object, Object> sensors_required) {
        MapDataContainer sensor_properties_data =  new MapDataContainer();
        sensor_properties_data.setName("properties");
        sensor_properties_data.setData(sensors_required);
        sensor_properties_data.setEntry_prefix(LIST_PRFX);

        ExtendedDataContainer sensor_properties= new ExtendedDataContainer();
        sensor_properties.setExtended_data_container_name("sensors");
        if (sensors_required.keySet().size()>0 && sensors_required.values().size()>0) {
            sensor_properties.setData(sensor_properties_data);
        }
        sensor_properties.setPrefix(LIST_PRFX);
        return sensor_properties;
    }



    private static ExtendedDataContainer get_node_filter_capabilities_host_properties(DataContainer host_node_resource_data) {
        ExtendedDataContainer node_capability= new ExtendedDataContainer();
        node_capability.setExtended_data_container_name("host");
        node_capability.setData(host_node_resource_data);
        node_capability.setPrefix(LIST_PRFX);
        return node_capability;
    }

    /**
     * A method which creates a simple JPPF coordinator node, with constant requirements.
     * @param name The name of the coordinator node
     * @return A coordinator node instance
     */
    public static NodeType create_jppf_master_node(String name) {
        return create_tosca_linux_processing_node(
                2,4,
                4096,16384,
                20,1000,
                0,0,0,0,0,0,
                false,true,
                empty_arraylist(),
                empty_hashmap(),
                name+UNDERSCORE+String.valueOf(counter++),
                prestocloud_jppf_master,
                "A JPPF master node of the topology");
    }

    /**
     * A method which creates a simple coordinator node, with constant requirements.
     * @param name The name of the coordinator node
     * @return A coordinator node instance
     */
    public static NodeType create_faas_proxy_processing_tosca_node(String name) {
        return  create_tosca_linux_processing_node(
                2,4,
                4096,16384,
                20,1000,
                0,0,0,0,0,0,
                false,true,
                empty_arraylist(),
                empty_hashmap(),
                name+UNDERSCORE+String.valueOf(counter++),
                prestocloud_faas_proxy,
                "A faas proxy node of the topology");
    }

    public static NodeType create_load_balancing_processing_tosca_node(String name){
        return  create_tosca_linux_processing_node(
                2,4,
                2048,4096,
                20,100,
                0,0,0,0,0,0,
                false,true,
                empty_arraylist(),
                empty_hashmap(),
                name+UNDERSCORE+String.valueOf(counter++),
                prestocloud_load_balancer,
                "A load balancing node of the topology");
    }

    public static ToscaDocument create_core_TOSCA_document(int tosca_deployment_counter){
        //Logger.getAnonymousLogger().log(Level.INFO,"New tosca document is being prepared. The deployment counter is "+tosca_deployment_counter);
        HashMap<String,PrestoFragmentation> annotated_fragments = new HashMap<>();
        DataContainerList ssh_keys = new DataContainerList();

        if ((tosca_deployment_counter<=1) || (FIRST_RUN)) {

            initialize_dependency_data_structures();

            //TODO choose the method used to create fragments - annotations or the UI. As things stand, this choice is supposed to be done passively, if no annotations are supplied, or no JSON deployment file has been generated from Ubitech, and if annotated fragments exist, only they will be parsed, even if UI fragments have been detected.

            annotations_based_deployment = operational_input_mode.equals("annotations") ? true :false ;
            ui_based_deployment = operational_input_mode.equals("json") ? true: false;

            if (annotations_based_deployment) {
                annotated_fragments =  get_annotated_fragments();
                if (annotated_fragments.size() > 0) {
                    parse_annotated_fragments(annotated_fragments);
                }
                upload_ssh_keys();
                create_coordinator_fragments();
            }

            else if (ui_based_deployment){
                boolean ui_deployment_fragments_retrieved = false;
                synchronized (ui_deployment_ready) {
                    do {
                        while (!ui_deployment_ready.getValue()) {
                            try {
                                ui_deployment_ready.wait();
                            } catch (InterruptedException i) {
                                i.printStackTrace();
                                Logger.getAnonymousLogger().log(SEVERE, "Interrupted while waiting for a new json to arrive");
                            }
                        }
                        ui_deployment_ready.setFalse();
                        Set<Fragment> ui_fragments = get_ui_fragments();
                        if (ui_fragments.size() > 0) {
                            calculate_dependencies_of_ui_fragments(ui_fragments);
                            upload_ssh_keys();
                            upload_docker_registry_usernames_passwords();
                            ui_deployment_fragments_retrieved = true;
                        } else {
                            try {
                                throw new Exception("No annotated classes were found, neither UI fragments");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } while (!ui_deployment_fragments_retrieved);
                }


            }
            BidirectionalCollocationDependencies.perform_dependency_processing();
            topology_template_segment.setPolicies(proximity_policies);
        }
        generate_all_tosca_nodes_for_fragments();

        DataContainerList excluded_device_policies = get_excluded_device_policies();
        DataContainerList precedence_policies = ForwardPrecedenceDependencies.perform_dependency_processing();

        topology_template_segment.setPolicies(proximity_policies);
        topology_template_segment.addPolicies(precedence_policies);
        topology_template_segment.addPolicies(excluded_device_policies);

        if (SSH_KEYS_IN_TOSCA) {
            ssh_keys = retrieve_ssh_keys_descriptions();
            topology_template_segment.addInputs(ssh_keys);
        }

        core_tosca_document.setTopology_template_segment(topology_template_segment);
        core_tosca_document.setNode_types_segment(node_types_segment);

        //initialization in order to be ready for the second deployment
        //System.out.println(topology_template_segment.toString());
        topology_template_segment = new TopologyTemplateSegment();
        node_types_segment = new NodeTypesSegment();

        counter = 0;
        return core_tosca_document;
    }

    private static void create_coordinator_fragments() {

        boolean add_jppf_master=false,add_faas_proxy=false,add_load_balancer=false;

        int jppf_fragments_counter = 0;
        int faas_fragments_counter = 0;
        int load_balancer_fragments_counter = 0;

        String [] jppf_coordinator_precedence_dependencies = new String[fragments_map.size()];
        String [] faas_coordinator_precedence_dependencies = new String[fragments_map.size()];
        ArrayList<String> load_balanced_fragments = new ArrayList<>();

        for (Fragment fragment : fragments_map.values()) {
            synchronized (fragment) {
                if (fragment.getElasticity_mechanism().equals(jppf)) {
                    add_jppf_master = true;
                    jppf_coordinator_precedence_dependencies[jppf_fragments_counter] = fragment.getProcessed_fragment_name();
                    jppf_fragments_counter++;

                } else if (fragment.getElasticity_mechanism().equals(faas)) {
                    add_faas_proxy = true;
                    faas_coordinator_precedence_dependencies[faas_fragments_counter] = fragment.getProcessed_fragment_name();
                    faas_fragments_counter++;
                } else if (fragment.getElasticity_mechanism().equals(horizontal_load_balanced)) {
                    add_load_balancer = true;
                    load_balanced_fragments.add(fragment.getProcessed_fragment_name());
                    load_balancer_fragments_counter++;
                }
            }
        }


        if (add_jppf_master) {
            String jppf_master_node_name = "jppf_master_node";
            Fragment jppf_master_fragment = new Fragment(
                    jppf_master_node_name,
                    graph_hex_id+":"+graph_instance_hex_id+":"+UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10),
                    coordinator_jppf_master,
                    2,4,
                    4,16,
                    10,50,
                    0,0,
                    0,0,
                    0,0,
                    1,
                    1,
                    false,
                    true,
                    none,
                    new LinkedHashMap<>(),
                    new String[0],
                    new String[0],
                    jppf_coordinator_precedence_dependencies,
                    coordinator_node_docker_image,
                    coordinator_node_docker_registry,
                    coordinator_node_docker_registry_username,
                    coordinator_node_docker_registry_password,
                    get_port_forwardings_list(coordinator_node_ports_properties),//new ArrayList<>(),
                    get_environmental_variables_map(coordinator_node_environmental_variables, jppf_master_node_name),//new LinkedHashMap<>(),
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    new ArrayList<>(),
                    new LinkedHashMap<>(),
                    EMPTY,
                    new LinkedHashMap<>(),
                    1,
                    1,
                    0,
                    EMPTY,
                    coordinator_node_ssh_key
            );

            fragments_map.put(jppf_master_fragment.getProcessed_fragment_name(),jppf_master_fragment);

            //TODO check if below steps are required, or if it can be automatically added


        }else if (add_faas_proxy){
            String faas_proxy_fragment_name = "LambdaProxy";
            Fragment faas_proxy_fragment = new Fragment(
                    faas_proxy_fragment_name,
                    graph_hex_id+":"+graph_instance_hex_id+":"+UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10),
                    coordinator_faas_proxy,
                    2,4,
                    4,16,
                    10,50,
                    0,0,
                    0,0,
                    0,0,
                    1,
                    1,
                    false,
                    true,
                    none,
                    new LinkedHashMap<>(),
                    new String[0],
                    new String[0],
                    faas_coordinator_precedence_dependencies,
                    coordinator_node_docker_image,
                    coordinator_node_docker_registry,
                    coordinator_node_docker_registry_username,
                    coordinator_node_docker_registry_password,
                    get_port_forwardings_list(coordinator_node_ports_properties),//new ArrayList<>(),
                    get_environmental_variables_map(coordinator_node_environmental_variables, faas_proxy_fragment_name),//new LinkedHashMap<>(),
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    EMPTY,
                    new ArrayList<>(),
                    new LinkedHashMap<>(),
                    EMPTY,
                    new LinkedHashMap<>(),
                    1,
                    1,
                    0,
                    EMPTY,
                    coordinator_node_ssh_key
            );
            fragments_map.put(faas_proxy_fragment.getProcessed_fragment_name(),faas_proxy_fragment);


        }else if (add_load_balancer){ //For each load-balanced fragment, add a new load-balancer fragment depending on the load-balanced fragment

            for (String load_balanced_fragment_name : load_balanced_fragments) {
                Fragment load_balancer_fragment = new Fragment(
                        load_balanced_fragment_name+"_LB",
                        graph_hex_id+":"+graph_instance_hex_id+":"+UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10),
                        coordinator_load_balancer,
                        2, 4,
                        4, 16,
                        10, 50,
                        0, 0,
                        0, 0,
                        0, 0,
                        1,
                        1,
                        false,
                        true,
                        none,
                        new LinkedHashMap<>(),
                        new String[0],
                        new String[0],
                        new String[]{load_balanced_fragment_name},
                        coordinator_node_docker_image,
                        coordinator_node_docker_registry,
                        coordinator_node_docker_registry_username,
                        coordinator_node_docker_registry_password,
                        get_port_forwardings_list(coordinator_node_ports_properties),//new ArrayList<>(),
                        get_environmental_variables_map(coordinator_node_environmental_variables, load_balanced_fragment_name),//new LinkedHashMap<>(),
                        EMPTY,
                        EMPTY,
                        EMPTY,
                        EMPTY,
                        EMPTY,
                        new ArrayList<>(),
                        new LinkedHashMap<>(),
                        EMPTY,
                        new LinkedHashMap<>(),
                        1,
                        1,
                        0,
                        EMPTY,
                        coordinator_node_ssh_key
                );
                fragments_map.put(load_balancer_fragment.getProcessed_fragment_name(),load_balancer_fragment);
            }
        }

    }

    /**
     * This method assumes that a public key has already been stored in the keyvault, and that this
     * key is referenced in the type-level TOSCA
     * @return
     */
    private static DataContainerList retrieve_ssh_keys_descriptions() {
        DataContainerList ssh_keys = new DataContainerList();
        //ssh_keys.add(new MapDataContainer<String>(){{setName("ssh_pub_key_1");add("type","string"); add("description","The public ssh key");}});
        //ssh_keys.add(new MapDataContainer<String>(){{setName("ssh_priv_key_1");add("type","string"); add("description","The private ssh key");}});
        for (HashMap.Entry <String, Fragment> fragment_entry: fragments_map.entrySet()){
            Fragment fragment = fragment_entry.getValue();
            if (fragment.getPublic_ssh_key()!=null && !fragment.getPublic_ssh_key().equals(EMPTY)) {
                ssh_keys.add(new MapDataContainer<String>() {{
                    setName(SSH_KEY_PREFIX+fragment_entry.getKey());
                    add("type", "string");
                    add("description", "The public ssh key");
                }});
            }
        }
        return ssh_keys;
    }
    private static void upload_ssh_keys(){
        for (HashMap.Entry <String, Fragment> fragment_entry: fragments_map.entrySet()){
            Fragment fragment = fragment_entry.getValue();
            if (fragment.getPublic_ssh_key()!=null && !fragment.getPublic_ssh_key().equals(EMPTY)) {
                 RestCommunicator.uploadSSHkey(SSH_KEY_PREFIX+fragment_entry.getKey(), fragment.getPublic_ssh_key());
            }
        }
    }

    private static void upload_docker_registry_usernames_passwords(){
        HashMap<String,String> docker_registry_names_credentials = new HashMap<>();
        for (HashMap.Entry<String,Fragment> fragment_entry: fragments_map.entrySet()){
            Fragment fragment = fragment_entry.getValue();
            if (fragment.getCloud_docker_registry()!=null && !fragment.getCloud_docker_registry().equals(EMPTY)){
                docker_registry_names_credentials.put(fragment.getCloud_docker_registry(),fragment.getCloud_docker_registry_username()+","+fragment.getCloud_docker_registry_password());
            }
            if (fragment.getEdge_docker_registry()!=null && !fragment.getEdge_docker_registry().equals(EMPTY)){
                docker_registry_names_credentials.put(fragment.getEdge_docker_registry(),fragment.getEdge_docker_registry_username()+","+fragment.getEdge_docker_registry_password());
            }
        }
        for (HashMap.Entry<String,String> registry_credentials_entry: docker_registry_names_credentials.entrySet()){
            String docker_registry_name = registry_credentials_entry.getKey();
            String docker_registry_username = registry_credentials_entry.getValue().split(",")[0];
            String docker_registry_password = registry_credentials_entry.getValue().split(",")[1];
            RestCommunicator.uploadDockerRegistryUsernamePassword(docker_registry_name,docker_registry_username,docker_registry_password);
        }


    }



    public static void parse_annotated_fragments(HashMap<String,PrestoFragmentation> annotated_fragments/*Class... annotated_fragments*/) {

        // Uncomment if needed. Definitions of the nodes are added by Activeeon
        // add_definition_nodes();

        for (HashMap.Entry<String,PrestoFragmentation> annotated_fragment : annotated_fragments.entrySet()){

            PrestoFragmentation prestoannotation = (PrestoFragmentation) annotated_fragment.getValue();
            //mapping_requirement.
            Fragment fragment = new Fragment(
                    annotated_fragment.getKey(),
                    graph_hex_id+":"+graph_instance_hex_id+":"+annotated_fragment.getKey(),
                    application_fragment,
                    translate(prestoannotation.cpuLoad(),mapping_requirement.getCpu_mapping()).getLower_bound(),
                    translate(prestoannotation.cpuLoad(),mapping_requirement.getCpu_mapping()).getHigher_bound(),
                    translate(prestoannotation.memoryLoad(),mapping_requirement.getMemory_mapping()).getLower_bound(),
                    translate(prestoannotation.memoryLoad(),mapping_requirement.getMemory_mapping()).getHigher_bound(),
                    translate(prestoannotation.storageLoad(),mapping_requirement.getStorage_mapping()).getLower_bound(),
                    translate(prestoannotation.storageLoad(),mapping_requirement.getStorage_mapping()).getHigher_bound(),
                    translate(prestoannotation.cpuLoad(),mapping_requirement.getCpu_mapping()).getLower_bound()*EDGE_CPU_REQUIREMENT_MULTIPLIER,
                    translate(prestoannotation.cpuLoad(),mapping_requirement.getCpu_mapping()).getHigher_bound()*EDGE_CPU_REQUIREMENT_MULTIPLIER,
                    translate(prestoannotation.memoryLoad(),mapping_requirement.getMemory_mapping()).getLower_bound()*EDGE_RAM_REQUIREMENT_MULTIPLIER,
                    translate(prestoannotation.memoryLoad(),mapping_requirement.getMemory_mapping()).getHigher_bound()*EDGE_RAM_REQUIREMENT_MULTIPLIER,
                    translate(prestoannotation.storageLoad(),mapping_requirement.getStorage_mapping()).getLower_bound()*EDGE_DISK_REQUIREMENT_MULTIPLIER,
                    translate(prestoannotation.storageLoad(),mapping_requirement.getStorage_mapping()).getHigher_bound()*EDGE_DISK_REQUIREMENT_MULTIPLIER,
                    prestoannotation.min_instances(),
                    prestoannotation.max_instances(),
                    prestoannotation.onloadable(),
                    prestoannotation.offloadable(),
                    prestoannotation.elasticity_mechanism(),
                    convert_key_value_structure_to_linked_hash_map(prestoannotation.sensors_required()),
                    prestoannotation.dependencyOn(),
                    prestoannotation.antiAffinityTo(),
                    prestoannotation.precededBy(),

                    prestoannotation.cloud_docker_image(),
                    prestoannotation.cloud_docker_registry(),
                    prestoannotation.cloud_docker_registry_username(),
                    prestoannotation.cloud_docker_registry_password(),
                    get_port_forwardings_list(prestoannotation.cloud_ports_properties()),
                    get_environmental_variables_map(prestoannotation.cloud_environmental_variables(), annotated_fragment.getKey()),
                    prestoannotation.cloud_docker_cmd(),

                    prestoannotation.edge_docker_image(),
                    prestoannotation.edge_docker_registry(),
                    prestoannotation.edge_docker_registry_username(),
                    prestoannotation.edge_docker_registry_password(),
                    get_port_forwardings_list(prestoannotation.edge_ports_properties()),
                    get_environmental_variables_map(prestoannotation.edge_environmental_variables(), annotated_fragment.getKey()),
                    prestoannotation.edge_docker_cmd(),
                    convert_key_value_structure_to_linked_hash_map(prestoannotation.optimization_providerFriendliness_weights()),
                    prestoannotation.optimization_cost_weight(),
                    prestoannotation.optimization_distance_weight(),
                    prestoannotation.health_check_interval(),
                    prestoannotation.health_check_command(),
                    prestoannotation.public_ssh_key()
            );

            add_fragment(fragment);
            initialize_dependencies_of_fragment(fragment);

        }
    }

    public static LinkedHashMap<String, String> get_environmental_variables_map(String[] environmental_variables, String fragment_name)  {
        LinkedHashMap<String,String> environmental_variables_map = new LinkedHashMap<>();
        if (environmental_variables.length>1 && environmental_variables.length%2!=1) {
            for (int i = 0; i< environmental_variables.length; i+=2){
                if (environmental_variables[i + 1].startsWith("get_property:")) {
                    String[] initial_get_property_statement = environmental_variables[i + 1]
                            .replace("get_property:", "")
                            .replaceAll("\\[|\\]", "")
                            .trim()
                            .split(",");
                    String targeted_fragment_name = initial_get_property_statement[0];
                    String final_get_property_statement = "{ get_property: ["
                            + get_mapping_node_name(targeted_fragment_name)
                            + environmental_variables[i + 1]
                            .replace("get_property:", "")
                            .replaceFirst("\\[" + targeted_fragment_name, "")
                            .trim()
                            + " }";
                    environmental_variables_map.put(environmental_variables[i], final_get_property_statement);
                } else {
                    environmental_variables_map.put(environmental_variables[i], environmental_variables[i + 1]);
                }
            }
        } else if (environmental_variables.length>0) {
                Logger.getAnonymousLogger().log(WARNING, "Not taking into account the environmental variables for fragment " + fragment_name + " which has " + environmental_variables.length + " environmental variables, the following:\n"+Arrays.stream(environmental_variables).collect(Collectors.joining()));
        }
        return environmental_variables_map;
    }

    public static <T> LinkedHashMap<T, T> convert_key_value_structure_to_linked_hash_map(T [] data){
        LinkedHashMap<T, T> map = new LinkedHashMap<>();
        for (int i=0; i<data.length; i+=2 ){
            map.put(data[i],data[i+1]);
        }
        return map;
    }

    public static <T> LinkedHashMap<T, T> convert_key_value_structure_to_linked_hash_map(ArrayList<T> data){
        LinkedHashMap<T, T> map = new LinkedHashMap<>();
        for (int i=0; i<data.size(); i+=2 ){
            map.put(data.get(i),data.get(i+1));
        }
        return map;
    }


    private static ArrayList<Port> get_port_forwardings_list(String[] port_forwardings) {
        ArrayList<Port> port_forwardings_list = new ArrayList<>();
        for (int i=0; i<port_forwardings.length; i+=3){
            Port port = new Port(Integer.parseInt(port_forwardings[i]),Integer.parseInt(port_forwardings[i+1]), Port.TransportLayer.valueOf(port_forwardings[i+2]));
            port_forwardings_list.add(port);
        }
        return  port_forwardings_list;
    }

    /**
     * This method is responsible to create the type-level TOSCA nodes which represent the processing fragments, and the required JPPF masters or Load balancers, or FaaS Lambda-proxy servers.
     */
    public static void generate_all_tosca_nodes_for_fragments(){

        for (Fragment fragment : fragments_map.values()){
            synchronized (fragment) {
                if (!fragment.getSpecial_fragment_type().equals(application_fragment)) { //coordinator fragments should be tackled first, in order to create the necessary deployment nodes which will be referenced by the rest of the fragments
                    create_nodes_from_fragment(fragment);
                }
            }
        }

        for (Fragment fragment : fragments_map.values()){
            synchronized (fragment) {
                if (fragment.getSpecial_fragment_type().equals(application_fragment)) { // non-coordinator fragments can be handled once all coordinator fragments have been processed
                    create_nodes_from_fragment(fragment);
                    fragment.setAdaptation_pending(false);
                }
            }
        }
    }



    public static void initialize_dependencies_of_fragment(Fragment fragment) {

        String [] fragment_collocation_dependencies = fragment.getFragment_collocation_dependencies().clone();
        String [] fragment_anti_affinity_constraints = fragment.getFragment_anti_affinity_constraints().clone();
        String [] fragment_precedence_dependencies = fragment.getFragment_precedence_dependencies().clone();

        for (int i=0;i<fragment_collocation_dependencies.length;i++){
            fragment_collocation_dependencies[i] = create_processed_fragment_name(fragment_collocation_dependencies[i]);
        }
        for (int i=0;i<fragment_anti_affinity_constraints.length;i++){
            fragment_anti_affinity_constraints[i] = create_processed_fragment_name(fragment_anti_affinity_constraints[i]);
        }
        for (int i=0;i<fragment_precedence_dependencies.length;i++){
            fragment_precedence_dependencies[i] = create_processed_fragment_name(fragment_precedence_dependencies[i]);
        }
        
        LinkedHashSet<String> fragment_collocation_dependencies_set = new LinkedHashSet<>(Arrays.asList(fragment_collocation_dependencies));
        LinkedHashSet<String> fragment_anti_affinity_constraints_set = new LinkedHashSet<>(Arrays.asList(fragment_anti_affinity_constraints));
        LinkedHashSet<String> fragment_precedence_dependencies_set = new LinkedHashSet<>(Arrays.asList(fragment_precedence_dependencies));

        get_collocation_dependency_hashmap().put(fragment.getProcessed_fragment_name(),fragment_collocation_dependencies_set);
        get_anti_affinity_hashmap().put(fragment.getProcessed_fragment_name(),fragment_anti_affinity_constraints_set);
        get_precedence_dependency_hashmap().put(fragment.getProcessed_fragment_name(),fragment_precedence_dependencies_set);
    }

    public static String create_processed_fragment_name(String fragment_name) {
        String processed_fragment_name = PROCESS_ANNOTATION_NAMES ? 
                new String(fragment_name).replace(".",UNDERSCORE) : fragment_name;
        return processed_fragment_name;
    }


    /**
     * Returns the dependencies of a particular fragment
     * @param dependency The name of the fragment whose dependencies are requested
     * @return
     */
    public static LinkedHashSet<String> get_dependencies(String dependency){
        return get_collocation_dependency_hashmap().get(dependency);
    }

    /**
     *  The method below instantiates an application fragment node based on information gathered either through the developer annotations or the topology json
     * @param mapping_node The node on which the fragment will be executed
     * @param coordinator_fragment_node_name
     * @param fragment_node_name The name of the fragment, which depends on the name of the method which it represents.
     * @param real_fragment_name
     * @param onloadable A boolean variable which determines whether the fragment can be executed on edge devices or not
     * @param offloadable
     * @param elasticity_mechanism The elasticity mechanism employed by the particular fragment
     * @param occurrences
     * @param dependency_fragments An array of fragment names which it is necessary to collocate with this fragment
     * @param anti_dependency_fragments An array of fragment names which it is necessary to place away from this fragment
     * @param fragment_id
     * @param cloud_ports_properties The properties of the ports which should be used in a cloud installation
     * @param cloud_environmental_variables The environmental variables which should be used in a cloud installation
     * @param cloud_docker_registry The docker registry which should be used in a cloud installation
     * @param cloud_docker_image The docker image which should be used in a cloud installation
     * @param cloud_docker_cmd The docker command which should be used in a cloud installation
     * @param edge_ports_properties The properties of the ports which should be used in an edge installation
     * @param edge_environmental_variables  The environmental variables which should be used in an edge installation
     * @param edge_docker_registry The docker registry which should be used in an edge installation
     * @param edge_docker_image The docker image which should be used in an edge installation
     * @param edge_docker_cmd The docker command which should be used in an edge installation
     * @param optimization_provider_friendliness_weights The importance of using different cloud providers
     * @param optimization_cost_weight The importance of optimizing the cost of instances
     * @param optimization_distance_weight The importance of optimizing the distance from the centroid of edge devices
     * @param health_check_interval The interval at which the health check should be performed
     * @param health_check_command The command which should be performed in each health check
     * @param fragment_type The type of the coordinator fragment
     * @return
     */
    private static NodeTemplate create_application_fragment_node(
            NodeTemplate mapping_node,
            String coordinator_fragment_node_name,
            String fragment_node_name,
            String real_fragment_name,
            boolean onloadable,
            boolean offloadable,
            PrestoFragmentation.Elasticity_mechanism elasticity_mechanism,
            int occurrences, String[] dependency_fragments, //TODO remove attribute range if only the minimum number of instances will be sent
            String[] anti_dependency_fragments,
            String fragment_id,
            
            ArrayList<Port> cloud_ports_properties,
            LinkedHashMap<String, String> cloud_environmental_variables,
            String cloud_docker_registry,
            String cloud_docker_image,
            String cloud_docker_cmd,

            ArrayList<Port> edge_ports_properties,
            LinkedHashMap<String, String> edge_environmental_variables,
            String edge_docker_registry,
            String edge_docker_image,
            String edge_docker_cmd,

            LinkedHashMap<String, String> optimization_provider_friendliness_weights,
            int optimization_cost_weight,
            int optimization_distance_weight,

            int health_check_interval,
            String health_check_command,
            FragmentTypes fragment_type
     ) {


        DataContainerList edge_docker_configuration_property = new DataContainerList();
        DataContainerList cloud_docker_configuration_property = new DataContainerList();


        /**
         * Creation of the cloud docker description of the fragment
         *
         * The relevant data are:
         * - the ports_properties -> assigned to ports_properties container.
         * - the cloud_environmental_variables -> assigned to environmental_variables_container.
         *
         */
        if (offloadable) {
            DataContainerList cloud_ports_properties_container = new DataContainerList();
            cloud_ports_properties_container.setName("ports");
            if (cloud_ports_properties != null && cloud_ports_properties.size() > 0) {
                for (Port port : cloud_ports_properties) {
                    MapDataContainer port_configuration = new MapDataContainer();
                    port_configuration.setList_element(true);
                    port_configuration.setEntry_prefix(LIST_PRFX);
                    port_configuration.add("target", port.getTarget_port());
                    port_configuration.add("published", port.getPublished_port());
                    port_configuration.add("protocol", port.getProtocol());
                    cloud_ports_properties_container.add(port_configuration);
                }
            }

            MapDataContainer environmental_variables_container = new MapDataContainer();
            environmental_variables_container.setName("variables");
            environmental_variables_container.setFormat_data_single_line(true);
            environmental_variables_container.setBrace_format(true);
            environmental_variables_container.setData(cloud_environmental_variables);
            
            cloud_docker_configuration_property.add(new StringTupleDataContainer("image", quoted(cloud_docker_image)));
            if (cloud_docker_registry!=null && !cloud_docker_registry.equals(EMPTY)) {
                cloud_docker_configuration_property.add(new StringTupleDataContainer("registry", quoted(cloud_docker_registry)));
            }
            if (cloud_docker_cmd!=null && !cloud_docker_cmd.equals(EMPTY)) {
                cloud_docker_configuration_property.add(new StringTupleDataContainer("cmd", quoted(cloud_docker_cmd)));
            }
            if (environmental_variables_container.getData() != null && environmental_variables_container.getData().size() > 0) {
                cloud_docker_configuration_property.add(environmental_variables_container);
            }
            if (cloud_ports_properties_container.getAttributes() != null && cloud_ports_properties_container.getAttributes().size() > 0) {
                cloud_docker_configuration_property.add(cloud_ports_properties_container);
            }
            cloud_docker_configuration_property.setName("docker_cloud");
            
        }
        
        /**
         * Creation of the edge docker description of the fragment
         *
         * The relevant data are:
         * - the ports_properties -> assigned to ports_properties container.
         * - the edge_environmental_variables -> assigned to edge_environmental_variables_container.
         *
         */
        
        if (onloadable){
            
            DataContainerList edge_ports_properties_container = new DataContainerList();
            edge_ports_properties_container.setName("ports");
            if (edge_ports_properties != null && edge_ports_properties.size() > 0) {
                for (Port port : edge_ports_properties) {
                    MapDataContainer port_configuration = new MapDataContainer();
                    port_configuration.setList_element(true);
                    port_configuration.setEntry_prefix(LIST_PRFX);
                    port_configuration.add("target", port.getTarget_port());
                    port_configuration.add("published", port.getPublished_port());
                    port_configuration.add("protocol", port.getProtocol());
                    edge_ports_properties_container.add(port_configuration);
                }
            }
            
            MapDataContainer edge_environmental_variables_container = new MapDataContainer();
            edge_environmental_variables_container.setName("variables");
            edge_environmental_variables_container.setFormat_data_single_line(true);
            edge_environmental_variables_container.setBrace_format(true);
            edge_environmental_variables_container.setData(edge_environmental_variables);

            edge_docker_configuration_property.add(new StringTupleDataContainer("image", quoted(edge_docker_image)));
            if(edge_docker_registry!=null && !edge_docker_registry.equals(EMPTY)) {
                edge_docker_configuration_property.add(new StringTupleDataContainer("registry", quoted(edge_docker_registry)));
            }
            if (edge_docker_cmd!=null && !edge_docker_cmd.equals(EMPTY)) {
                edge_docker_configuration_property.add(new StringTupleDataContainer("cmd", quoted(edge_docker_cmd)));
            }
            if (edge_environmental_variables_container.getData() != null && edge_environmental_variables_container.getData().size() > 0) {
                edge_docker_configuration_property.add(edge_environmental_variables_container);
            }
            if (edge_ports_properties_container.getAttributes() != null && edge_ports_properties_container.getAttributes().size() > 0) {
                edge_docker_configuration_property.add(edge_ports_properties_container);
            }
            edge_docker_configuration_property.setName("docker_edge");

        }

        /**
         * Creation of the other fragment properties, and attachment of the docker configuration details to them
         */
        StringTupleDataContainer fragment_id_property = new StringTupleDataContainer("id",fragment_id);
        //perhaps eventually comment out the name attribute, as the fragment node is already appropriately named
        StringTupleDataContainer fragment_name_property = new StringTupleDataContainer ("name",real_fragment_name) ;
        StringTupleDataContainer fragment_scalable_property = new StringTupleDataContainer("scalable",String.valueOf(!elasticity_mechanism.equals(none)));
        StringTupleDataContainer fragment_occurrences_property = new StringTupleDataContainer("occurrences",String.valueOf(occurrences)); //TODO change/simplify this as needed

        DataContainerList application_fragment_properties = new DataContainerList();
        application_fragment_properties.add(fragment_id_property);
        application_fragment_properties.add(fragment_name_property);
        application_fragment_properties.add(fragment_scalable_property);
        application_fragment_properties.add(fragment_occurrences_property);
        if (onloadable) {
            application_fragment_properties.add(edge_docker_configuration_property);
        }
        if (offloadable){
            application_fragment_properties.add(cloud_docker_configuration_property);
        }

        MapDataContainer optimization_friendliness_weights_container =new MapDataContainer();
        optimization_friendliness_weights_container.setName("friendliness");
        optimization_friendliness_weights_container.setFormat_data_single_line(true);
        optimization_friendliness_weights_container.setBrace_format(true);
        optimization_friendliness_weights_container.setData(optimization_provider_friendliness_weights);

        DataContainerList fragment_optimization_variables_property = new DataContainerList();
        fragment_optimization_variables_property.setName("optimization_variables");
        fragment_optimization_variables_property.add(new StringTupleDataContainer("cost",String.valueOf(optimization_cost_weight)));
        fragment_optimization_variables_property.add(new StringTupleDataContainer("distance",String.valueOf(optimization_distance_weight)));
        fragment_optimization_variables_property.add(optimization_friendliness_weights_container);

        application_fragment_properties.add(fragment_optimization_variables_property);

        if (health_check_command!=null && !health_check_command.equals(EMPTY)) {
            MapDataContainer fragment_healthcheck_property = new MapDataContainer();
            fragment_healthcheck_property.setName("health_check");
            fragment_healthcheck_property.add("interval", String.valueOf(health_check_interval));
            fragment_healthcheck_property.add("cmd", quoted(health_check_command));
            application_fragment_properties.add(fragment_healthcheck_property);
        }

        //MapDataContainer application_fragment_requirements = new MapDataContainer();
        DataContainerList application_fragment_requirements = new DataContainerList();

        StringBuilder dependency_fragments_string_format = new StringBuilder(LIST_START);
        if (dependency_fragments.length>0) {
            for (String fragment_depended_upon : dependency_fragments) {
                if (dependency_fragments_string_format.length() == 1) {
                    dependency_fragments_string_format.append(fragment_depended_upon);
                } else {
                    dependency_fragments_string_format.append(LIST_SEP).append(fragment_depended_upon);
                }
            }
            dependency_fragments_string_format.append(LIST_END);
        }
        else{
            dependency_fragments_string_format.append(LIST_END);
        }

        StringBuilder anti_dependency_fragments_string_format = new StringBuilder(LIST_START);
        if (anti_dependency_fragments.length>0) {
            for (String fragment_anti_depended_upon : anti_dependency_fragments) {
                if (anti_dependency_fragments_string_format.length() == 1) {
                    anti_dependency_fragments_string_format.append(fragment_anti_depended_upon);
                } else {
                    anti_dependency_fragments_string_format.append(LIST_SEP).append(fragment_anti_depended_upon);
                }
            }
            anti_dependency_fragments_string_format.append(LIST_END);
        }
        else{
            anti_dependency_fragments_string_format.append(LIST_END);
        }

        //add execute statement for the deployment node of the fragment

        if ((mapping_node!=null) && (mapping_node.getName()!=EMPTY)) {
            application_fragment_requirements.add(new StringTupleDataContainer("execute", mapping_node.getName()));
        }

        //add proxy requirement on faas fragments
        if (coordinator_fragment_node_name!=null && coordinator_fragment_node_name!=EMPTY ) {
            if (elasticity_mechanism == faas) {
                application_fragment_requirements.add(new StringTupleDataContainer("proxy", coordinator_fragment_node_name));
            }else if (elasticity_mechanism == jppf){
                application_fragment_requirements.add(new StringTupleDataContainer("master", coordinator_fragment_node_name));
            }else if (elasticity_mechanism == horizontal_load_balanced){
                application_fragment_requirements.add(new StringTupleDataContainer("balanced_by", coordinator_fragment_node_name));
            }

        }
        application_fragment_requirements.setEntry_prefix(LIST_PRFX);

        NodeTemplate application_fragment_node = new NodeTemplate();
        application_fragment_node.setName(fragment_node_name);
        if (elasticity_mechanism == jppf) {
            application_fragment_node.setType(prestocloud_jppf_fragment);
        }else if (elasticity_mechanism == faas){
            application_fragment_node.setType(prestocloud_faas_fragment);
        }else if (elasticity_mechanism == horizontal_load_balanced){
            application_fragment_node.setType(prestocloud_load_balanced_fragment);
        }else if (elasticity_mechanism == none || elasticity_mechanism == simple_horizontal_no_load_balancing){
            application_fragment_node.setType(prestocloud_generic_fragment);
        }
        //application_fragment_node.setType(tosca_generic_fragment);
        application_fragment_node.addProperty(application_fragment_properties);
        application_fragment_node.addRequirement(application_fragment_requirements);
        return application_fragment_node;
    }




    private static void create_nodes_from_fragment (Fragment fragment){

        String node_type=EMPTY,node_description=TOSCA_PROCESSING_NODE_DESCRIPTION;
        if (fragment.getElasticity_mechanism().equals(jppf)){
            node_type = prestocloud_jppf_agent;
        }
        else if(fragment.getElasticity_mechanism().equals(faas)){
            node_type = prestocloud_faas_agent;
        }
        else if(fragment.getElasticity_mechanism().equals(horizontal_load_balanced)){
            node_type = prestocloud_load_balanced_agent;
        }
        else if (fragment.getElasticity_mechanism().equals(simple_horizontal_no_load_balancing)){

            node_type = prestocloud_nodes_agent;
        }
        else if (fragment.getElasticity_mechanism().equals(none)){

            if (!fragment.getSpecial_fragment_type().equals(application_fragment)){

                switch (fragment.getSpecial_fragment_type()){
                    case coordinator_faas_proxy:
                        node_type = prestocloud_faas_proxy;
                        break;
                    case coordinator_jppf_master:
                        node_type = prestocloud_jppf_master;
                        break;
                    case coordinator_load_balancer:
                        node_type = prestocloud_load_balancer;
                        break;
                }
            }else{
                node_type = prestocloud_nodes_agent;
            }



        }


        NodeType processing_node=new NodeType(),cloud_processing_node=new NodeType(), edge_processing_node=new NodeType();
        NodeTemplate mapping_node = null;

        if (fragment.isOffloadable()&&fragment.isOnloadable()) {//can run both on the cloud and the edge
            String processing_node_name = TOSCA_PROCESSING_NODE+fragment.getProcessed_fragment_name()+UNDERSCORE+counter++;

            processing_node = create_tosca_linux_processing_node(
                    fragment.getMin_cpus_cloud(), fragment.getMax_cpus_cloud(),
                    fragment.getMin_memory_mb_cloud(), fragment.getMax_memory_mb_cloud(),
                    fragment.getMin_storage_gb_cloud(), fragment.getMax_storage_gb_cloud(),
                    fragment.getMin_cpus_cloud(), fragment.getMax_cpus_cloud(),
                    fragment.getMin_memory_mb_cloud(), fragment.getMax_memory_mb_cloud(),
                    fragment.getMin_storage_gb_cloud(), fragment.getMax_storage_gb_cloud(),
                    true, true,
                    fragment.getExcluded_edge_device_identifiers(),
                    fragment.getRequired_sensors(),
                    processing_node_name,
                    node_type, node_description
            );

            node_types_segment.addProcessing_node_type(processing_node);
            if (processing_node!=null && !processing_node.equals(EMPTY)) {
                 mapping_node = create_mapping_node(
                 processing_node.getName(),
                 get_mapping_node_name(fragment.getOriginal_fragment_name()));
            }
        }
        else {
            if (fragment.isOffloadable()) {//can run in the cloud
                String processing_node_name = TOSCA_PROCESSING_NODE + fragment.getProcessed_fragment_name() + UNDERSCORE + counter++;

                cloud_processing_node = create_tosca_linux_processing_node(
                        fragment.getMin_cpus_cloud(), fragment.getMax_cpus_cloud(),
                        fragment.getMin_memory_mb_cloud(), fragment.getMax_memory_mb_cloud(),
                        fragment.getMin_storage_gb_cloud(), fragment.getMax_storage_gb_cloud(),
                        0, 0, 0, 0, 0, 0,
                        false, true,
                        fragment.getExcluded_edge_device_identifiers(),
                        fragment.getRequired_sensors(),
                        processing_node_name,
                        node_type, node_description
                );

                node_types_segment.addProcessing_node_type(cloud_processing_node);
                if (cloud_processing_node!=null && !cloud_processing_node.equals(EMPTY)) {
                    mapping_node = create_mapping_node(
                    cloud_processing_node.getName(),
                    get_mapping_node_name(fragment.getOriginal_fragment_name()));
                }
            }

            else if (fragment.isOnloadable()) { //can run on the edge
                String processing_node_name = TOSCA_PROCESSING_NODE + fragment.getProcessed_fragment_name() + UNDERSCORE + counter++;

                edge_processing_node = create_tosca_linux_processing_node(
                        0, 0, 0, 0, 0, 0,
                        fragment.getMin_cpus_edge(), fragment.getMax_cpus_edge(),
                        fragment.getMin_memory_mb_edge(), fragment.getMax_memory_mb_edge(),
                        fragment.getMin_storage_gb_edge(), fragment.getMax_storage_gb_edge(),
                        true, false,
                        fragment.getExcluded_edge_device_identifiers(),
                        fragment.getRequired_sensors(),
                        processing_node_name,
                        node_type, node_description
                );
                node_types_segment.addProcessing_node_type(edge_processing_node);
                if (edge_processing_node!=null && !edge_processing_node.equals(EMPTY)) {
                    mapping_node = create_mapping_node(
                    edge_processing_node.getName(),
                    get_mapping_node_name(fragment.getOriginal_fragment_name()));
                }
            }
        }

        String active_coordinator_node = EMPTY;

        //The processed name of the fragment is used below, because the processed name is also used in precedence dependencies, from which the coordinator node was able to infer the type of the load-balanced nodes.
        if (fragment.getElasticity_mechanism().equals(jppf)){
            active_coordinator_node = get_jppf_master_fragment_node(fragment.getProcessed_fragment_name());
        }else if(fragment.getElasticity_mechanism().equals(faas)){
            active_coordinator_node = get_faas_fragment_proxy_node(fragment.getProcessed_fragment_name());
        }else if (fragment.getElasticity_mechanism().equals(horizontal_load_balanced)){
            active_coordinator_node = get_load_balancer_fragment_node(fragment.getProcessed_fragment_name());
        }else{
            if (fragment.getElasticity_mechanism().equals(none)){

                if (fragment.getSpecial_fragment_type().equals(coordinator_jppf_master)&&
                    annotations_based_deployment
                ){

                    set_jppf_master_fragment_node(get_mapping_node_name(fragment.getProcessed_fragment_name()));


                }else if (
                        fragment.getSpecial_fragment_type().equals(coordinator_faas_proxy) &&
                        annotations_based_deployment
                ){

                    set_faas_proxy_fragment_node(get_mapping_node_name(fragment.getProcessed_fragment_name()));

                }else if (
                        fragment.getSpecial_fragment_type().equals(coordinator_load_balancer) &&
                        annotations_based_deployment
                ){

                    set_load_balancer_fragment_node(fragment.getFragment_precedence_dependencies()[0], get_mapping_node_name(fragment.getProcessed_fragment_name()));

                }else {
                    Logger.getAnonymousLogger().log(INFO, "An empty coordinator node will be selected for fragment " + fragment.getProcessed_fragment_name() + " which has no elasticity profile");
                }
            }
            else if (!fragment.getElasticity_mechanism().equals(simple_horizontal_no_load_balancing)){
                Logger.getAnonymousLogger().log(SEVERE, "An empty coordinator node will be selected for fragment " + fragment.getProcessed_fragment_name()+ " which has an unknown elasticity profile");
            }
            active_coordinator_node = EMPTY;
        }

        NodeTemplate application_fragment = create_application_fragment_node(
                mapping_node,
                active_coordinator_node,
                fragment.getProcessed_fragment_name(),
                fragment.getOriginal_fragment_name(),
                fragment.isOnloadable(),
                fragment.isOffloadable(),
                fragment.getElasticity_mechanism(),
                fragment.getCurrent_instances(),
                fragment.getFragment_collocation_dependencies(),
                fragment.getFragment_anti_affinity_constraints(),
                fragment.getFragment_id(),
                
                fragment.getCloud_ports_properties(),
                fragment.getCloud_environmental_variables(),
                fragment.getCloud_docker_registry(),
                fragment.getCloud_docker_image(),
                fragment.getCloud_docker_command(),

                fragment.getEdge_ports_properties(),
                fragment.getEdge_environmental_variables(),
                fragment.getEdge_docker_registry(),
                fragment.getEdge_docker_image(),
                fragment.getEdge_docker_command(),

                fragment.getOptimization_provider_friendliness_weights(),
                fragment.getOptimization_cost_weight(),
                fragment.getOptimization_distance_weight(),

                fragment.getHealth_check_interval(),
                fragment.getHealth_check_command(),
                fragment.getSpecial_fragment_type()
        );

        topology_template_segment.add_node(mapping_node);
        topology_template_segment.add_node(application_fragment);
    }


    /**
     * This method creates a mapping node which pairs the application fragment node with the relevant processing node for the particular fragment.
     * @param processing_node The name of the processing node which this instance should be deployed on
     * @param name The name which should be given to the mapping node
     * @return A mapping node having the designated parameterst
     */
    private static NodeTemplate create_mapping_node(String processing_node, String name) {

        NodeTemplate mapping_node = new NodeTemplate();
        mapping_node.setType(processing_node);
        mapping_node.setName(name);
        return mapping_node;

    }

/**This method creates a mapping node for an already existing coordinator (faas proxy, load balancer, jppf master) processing node
 * @param name_of_processing_node The name of the processing node undertaking the processing of this deployment node
 * @param name The name of the deployment node
 */
    public static NodeTemplate create_coordinator_mapping_node(String name_of_processing_node, String name){
        NodeTemplate mapping_node = new NodeTemplate();
        mapping_node.setType(name_of_processing_node);
        mapping_node.setName(name);
        return mapping_node;
    }



    public static DataContainerList get_excluded_device_policies(){
        DataContainerList excluded_devices_policies = new DataContainerList();

        for (Fragment fragment : FragmentDataStructures.fragments_map.values()){

            if (fragment.getExcluded_edge_device_identifiers().size()>0){
                String excluded_devices_policy_name = "exclude_fragment_from_devices_" + excluded_devices_policy_counter++;
                ExtendedDataContainer excluded_devices_policy = new ExtendedDataContainer(excluded_devices_policy_name);
                excluded_devices_policy.setPrefix(LIST_PRFX);

                //MapDataContainer excluded_devices_data = new MapDataContainer();
                DataContainerList excluded_devices_policy_data = new DataContainerList();

                /*Type*/ StringTupleDataContainer policy_type = new StringTupleDataContainer("type", prestocloud_exclude_devices_policy);
                excluded_devices_policy_data.add(policy_type);

                /*Properties*/ ExtendedDataContainer excluded_devices_properties = new ExtendedDataContainer();
                excluded_devices_properties.setExtended_data_container_name("properties");
                SetDataContainer excluded_devices_properties_map = new SetDataContainer();
                excluded_devices_properties_map.setName("excluded_devices");
                excluded_devices_properties_map.setEntry_prefix(LIST_PRFX);

                for (String excluded_device_id : fragment.getExcluded_edge_device_identifiers()) {
                    excluded_devices_properties_map.add(excluded_device_id);
                }

                excluded_devices_properties.setData(excluded_devices_properties_map);
                excluded_devices_policy_data.add(excluded_devices_properties);

                /*Targets*/ StringTupleDataContainer targets_type = new StringTupleDataContainer("targets", LIST_START+SPACE+fragment.getProcessed_fragment_name()+SPACE+LIST_END);
                excluded_devices_policy_data.add(targets_type);

                excluded_devices_policy.setData(excluded_devices_policy_data);
                excluded_devices_policies.add(excluded_devices_policy);
            }
        }

        excluded_devices_policy_counter  = 0;
        return excluded_devices_policies;
    }


}
