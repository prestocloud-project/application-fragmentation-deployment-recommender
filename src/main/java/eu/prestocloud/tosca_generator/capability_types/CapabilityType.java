package eu.prestocloud.tosca_generator.capability_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.ExtendedDataContainer;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class CapabilityType {

    private String description;
    private String derived_from;
    private String name;
    ArrayList<ExtendedDataContainer> properties= new ArrayList<>();
    private String valid_source_types;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDerived_from() {
        return derived_from;
    }

    public void setDerived_from(String derived_from) {
        this.derived_from = derived_from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValid_source_types() {
        return valid_source_types;
    }

    public void setValid_source_types(String valid_source_types) {
        this.valid_source_types = valid_source_types;
    }

    public ArrayList<ExtendedDataContainer> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<ExtendedDataContainer> properties) {
        this.properties = properties;
    }

    public void add (ExtendedDataContainer property){
        properties.add(property);
    }


    @Override
    public String toString (){
        return toString(0);
    }

    public String toString (int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        string_representation.append(indent(indentation_level)).append(name).append(COLON_DELIMITER).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("description").append(COLON_DELIMITER).append(SPACE).append(description).append(NEWLINE);
        string_representation.append(indent(indentation_level + 1)).append("derived_from").append(COLON_DELIMITER).append(SPACE).append(derived_from).append(NEWLINE);


        string_representation.append(indent(indentation_level + 1)).append("properties").append(COLON_DELIMITER).append(NEWLINE);
        for (ExtendedDataContainer property:properties){
            string_representation.append(property.toString(indentation_level + 2));
        }

        string_representation.append(indent(indentation_level + 1)).append("valid_source_types").append(COLON_DELIMITER).append(SPACE).append(LIST_START).append(SPACE).append(valid_source_types).append(SPACE).append(LIST_END).append(NEWLINE);
        string_representation.append(NEWLINE); //to differentiate between sections of TOSCA code

        return string_representation.toString();
    }
}
