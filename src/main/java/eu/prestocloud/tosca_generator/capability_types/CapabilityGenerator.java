package eu.prestocloud.tosca_generator.capability_types;

import eu.prestocloud.tosca_generator.utility_classes.data_containers.DataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.ExtendedDataContainer;
import eu.prestocloud.tosca_generator.utility_classes.data_containers.MapDataContainer;

import java.util.ArrayList;

import static eu.prestocloud.configuration.Constants.tosca_capabilities_root;

public class CapabilityGenerator {



    public static CapabilityTypesSegment create_capability_types_segment() {

        CapabilityTypesSegment capability_types_segment =  new CapabilityTypesSegment();
        capability_types_segment.add(create_jppf_endpoint());
        capability_types_segment.add(create_jppf_fragmentExecution());

        return capability_types_segment;
    }

    private static CapabilityType create_jppf_endpoint() {
        CapabilityType capability = new CapabilityType();
        MapDataContainer attributes_data = new MapDataContainer();
        ExtendedDataContainer property = new ExtendedDataContainer();

        //protocol property
        attributes_data.add("type","string");
        attributes_data.add("required","true");
        attributes_data.add("default","tcp");

        property = new ExtendedDataContainer();
        property.setExtended_data_container_name("protocol");
        property.setData(attributes_data);

        capability.add(property);

        //port property
        attributes_data = new MapDataContainer();
        attributes_data.add("type","tosca.datatypes.network.PortDef");
        attributes_data.add("required","false");

        property = new ExtendedDataContainer();
        property.setExtended_data_container_name("port");
        property.setData(attributes_data);

        capability.add(property);

        //secure property
        attributes_data = new MapDataContainer();
        attributes_data.add("type","boolean");
        attributes_data.add("required","false");
        attributes_data.add("default","false");

        property = new ExtendedDataContainer();
        property.setExtended_data_container_name("secure");
        property.setData(attributes_data);

        capability.add(property);

        //ip_address property
        attributes_data = new MapDataContainer();
        attributes_data.add("type","string");
        attributes_data.add("required","false");

        property = new ExtendedDataContainer();
        property.setExtended_data_container_name("ip_address");
        property.setData(attributes_data);

        capability.add(property);

        capability.setName("prestocloud.capabilities.jppf.endpoint");
        capability.setDerived_from(tosca_capabilities_root);
        capability.setDescription("Provide JPPF master endpoint");
        capability.setValid_source_types("prestocloud.nodes.jppf.Agent");

        return capability;
    }

    private static CapabilityType create_jppf_fragmentExecution() {
        CapabilityType capability = new CapabilityType();
        MapDataContainer attributes_data = new MapDataContainer();
        ArrayList<DataContainer> attributes = new ArrayList<>();
        ExtendedDataContainer property = new ExtendedDataContainer();

        //jvm installable property - no prefix
        /*
        attributes_data.add("type","boolean");
        attributes_data.add("required","true");

        attributes.add(attributes_data);

        property = new ExtendedDataContainer();
        property.setExtended_data_container_name("jvm_installable");
        property.setFalse(attributes_data);

        capability.add(property);
        */

        capability.setName("prestocloud.capabilities.jppf.fragmentExecution");
        capability.setDerived_from(tosca_capabilities_root);
        capability.setDescription("Capability to execute a fragment");
        capability.setValid_source_types("prestocloud.nodes.fragment");

        return capability;    }
}
