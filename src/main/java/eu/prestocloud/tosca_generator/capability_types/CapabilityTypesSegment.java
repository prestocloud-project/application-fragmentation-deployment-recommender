package eu.prestocloud.tosca_generator.capability_types;


import java.util.ArrayList;

public class CapabilityTypesSegment {

    private ArrayList<CapabilityType> capability_types= new ArrayList<>();

    public void add (CapabilityType capability){
        capability_types.add(capability);
    }

    @Override
    public String toString (){
        return toString(0);
    }

    public String toString (int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        for (CapabilityType capability:capability_types){
            string_representation.append(capability.toString(indentation_level));
        }

        return string_representation.toString();
    }
}
