package eu.prestocloud.tosca_generator;

import eu.prestocloud.tosca_generator.capability_types.CapabilityTypesSegment;
import eu.prestocloud.tosca_generator.node_types.NodeTypesSegment;
import eu.prestocloud.tosca_generator.relationship_types.RelationshipTypesSegment;
import eu.prestocloud.tosca_generator.topology_template.TopologyTemplateSegment;

import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.TOSCA_DEF_VERSION;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class ToscaDocument {
    private ToscaMetadata metadata;
    private String description;
    private ToscaImports tosca_imports;
    private NodeTypesSegment node_types_segment;
    private CapabilityTypesSegment capability_types_segment;
    private RelationshipTypesSegment relationship_types_segment;
    private TopologyTemplateSegment topology_template_segment;

    public ToscaMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ToscaMetadata metadata) {
        this.metadata = metadata;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ToscaImports getTosca_imports() {
        return tosca_imports;
    }

    public void setTosca_imports(ToscaImports tosca_imports) {
        this.tosca_imports = tosca_imports;
    }

    public NodeTypesSegment getNode_types_segment() {
        return node_types_segment;
    }

    public void setNode_types_segment(NodeTypesSegment node_types_segment) {
        this.node_types_segment = node_types_segment;
    }

    public CapabilityTypesSegment getCapability_types_segment() {
        return capability_types_segment;
    }

    public void setCapability_types_segment(CapabilityTypesSegment capability_types_segment) {
        this.capability_types_segment = capability_types_segment;
    }

    public RelationshipTypesSegment getRelationship_types_segment() {
        return relationship_types_segment;
    }

    public void setRelationship_types_segment(RelationshipTypesSegment relationship_types_segment) {
        this.relationship_types_segment = relationship_types_segment;
    }

    public TopologyTemplateSegment getTopology_template_segment() {
        return topology_template_segment;
    }

    public void setTopology_template_segment(TopologyTemplateSegment topology_template_segment) {
        this.topology_template_segment = topology_template_segment;
    }

    @Override
    public String toString(){
        StringBuilder tosca_output = new StringBuilder("tosca_definitions_version");

        tosca_output.append(COLON_DELIMITER).append(SPACE).append(TOSCA_DEF_VERSION).append(NEWLINE).append(NEWLINE);//append two newline characters in order to leave one line blank between the sections.

        if (metadata!=null && !metadata.toString().equals("")) {
            tosca_output.append("metadata").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(metadata.toString(1)).append(NEWLINE);
        }else{
            Logger.getAnonymousLogger().log(Level.SEVERE,"No metadata segment created");
            //System.exit(0);
        }

        if (description!=null && !description.equals("")) {
            tosca_output.append("description").append(COLON_DELIMITER).append(SPACE).append(description);
            tosca_output.append(NEWLINE).append(NEWLINE);
        }

        if (tosca_imports!=null && !tosca_imports.toString().equals("")) {
            tosca_output.append("imports").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(tosca_imports.toString(1)).append(NEWLINE);
        }

        if (node_types_segment!=null && !node_types_segment.toString().equals("")) {
            tosca_output.append("node_types").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(node_types_segment.toString(1)).append(NEWLINE);
        }else{
            Logger.getAnonymousLogger().log(Level.SEVERE,"No node types segment created");
            //System.exit(0);
        }

        if (capability_types_segment!=null && !capability_types_segment.toString().equals("")) {
            tosca_output.append("capability_types").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(capability_types_segment.toString(1)).append(NEWLINE);
        }
        if (relationship_types_segment!=null && !relationship_types_segment.toString().equals("")) {
            tosca_output.append("relationship_types").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(relationship_types_segment.toString(1)).append(NEWLINE);
        }
        if (topology_template_segment!=null && !topology_template_segment.toString().equals("")) {
            tosca_output.append("topology_template").append(COLON_DELIMITER).append(NEWLINE);
            tosca_output.append(topology_template_segment.toString(1)).append(NEWLINE);
        }else{
            Logger.getAnonymousLogger().log(Level.SEVERE,"No topology segment created");
            //System.exit(0);
        }


        return tosca_output.toString();
    }

    public boolean isNotEmpty() {
        if ((node_types_segment!=null && !node_types_segment.toString().equals(""))  ||
            (topology_template_segment!=null && !topology_template_segment.toString().equals(""))){
            return true;
        }else{
            return false;
        }
    }
}
