package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.ArrayList;

public class FragmentationPolicy {


    private ArrayList<BusinessGoal> businessGoals = new ArrayList<>();
    private BudgetRequirement budgetRequirement = new BudgetRequirement();
    private MappingRequirement mappingRequirement;
    private ArrayList<ScalabilityRequirement> scalabilityRequirements = new ArrayList<>();
    private DeploymentRequirement deploymentRequirement;
    private ArrayList<CollocationRequirement> collocationRequirements = new ArrayList<>();
    private ArrayList<ProviderRequirement> providerRequirements = new ArrayList<>();

    public MappingRequirement getMappingRequirement() {
        return mappingRequirement;
    }

    public void setMappingRequirement(MappingRequirement mappingRequirement) {
        this.mappingRequirement = mappingRequirement;
    }

    public void setBusinessGoals(ArrayList<BusinessGoal> businessGoals) {
        this.businessGoals = businessGoals;
    }

    public void setBudgetRequirement(BudgetRequirement budgetRequirement) {
        this.budgetRequirement = budgetRequirement;
    }


    public void setScalabilityRequirements(ArrayList scalabilityRequirements) {
        this.scalabilityRequirements = scalabilityRequirements;
    }

    public void setDeploymentRequirement(DeploymentRequirement deploymentRequirement) {
        this.deploymentRequirement = deploymentRequirement;
    }

    public void setCollocationRequirements(ArrayList<CollocationRequirement> collocationRequirements) {
        this.collocationRequirements = collocationRequirements;
    }

    public void setProviderRequirements(ArrayList<ProviderRequirement> providerRequirements) {
        this.providerRequirements = providerRequirements;
    }


    public FragmentationPolicy (){
    }


    public ArrayList<BusinessGoal> getBusinessGoals() {
        return businessGoals;
    }

    public void addBusinessGoal(BusinessGoal businessGoal) {
        businessGoals.add(businessGoal);
    }

    public BudgetRequirement getBudgetRequirement() {
        return budgetRequirement;
    }

    public ArrayList<ScalabilityRequirement> getScalabilityRequirements() {
        return scalabilityRequirements;
    }

    public DeploymentRequirement getDeploymentRequirement() {
        return deploymentRequirement;
    }
    /*
    public void addScalabilityRequirement(DeploymentRequirement deploymentRequirement) {
        scalabilityRequirements.add(deploymentRequirement);
    }
    */
    public ArrayList<CollocationRequirement> getCollocationRequirements() {
        return collocationRequirements;
    }

    public void addCollocationRequirement(CollocationRequirement collocationRequirement) {
        collocationRequirements.add(collocationRequirement);
    }

    public ArrayList<ProviderRequirement> getProviderRequirements() {
        return providerRequirements;
    }

    public void addProviderRequirement(ProviderRequirement providerRequirement) {
        providerRequirements.add(providerRequirement);

    }

    @Override
    public String toString(){
        StringBuilder string_representation =new StringBuilder("");
        string_representation.append(mappingRequirement);
        return  string_representation.toString();
    }


}
