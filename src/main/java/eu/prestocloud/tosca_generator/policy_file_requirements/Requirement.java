package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.Map;

public abstract  class Requirement {
    public  Requirement(){

    }
    public abstract Map<String,String> serializeToMap ();
}
