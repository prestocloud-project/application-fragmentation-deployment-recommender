package eu.prestocloud.tosca_generator.policy_file_requirements;

public class ScalabilityRequirement {
    private String scalabilityAction_uri;
    private String id;

    public ScalabilityRequirement(){

    }
    public ScalabilityRequirement(String id,String uri){
        this.id = id;
        this.scalabilityAction_uri = uri;
    }

    public String getScalabilityAction_uri() {
        return scalabilityAction_uri;
    }

    public void setScalabilityAction_uri(String scalabilityAction_uri) {
        this.scalabilityAction_uri = scalabilityAction_uri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
