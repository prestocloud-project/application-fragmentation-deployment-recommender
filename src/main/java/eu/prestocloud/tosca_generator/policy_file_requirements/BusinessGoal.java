package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.COLON_DELIMITER;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;

public class BusinessGoal extends Requirement {

    private String MetricToMinimize;
    private String MetricToMaximize;
    private String Metric;
    private String Threshold;
    private String Operator;

    public String getMetric() {
        return Metric;
    }
    public String getThreshold() {
        return Threshold;
    }
    public String getOperator() {
        return Operator;
    }
    public void setMetric(String metric) {
        this.Metric = metric;
    }
    public void setThreshold(String threshold) {
        this.Threshold = threshold;
    }
    public void setOperator(String operator) {
        this.Operator = operator;
    }
    public String getMetricToMinimize() {
        return MetricToMinimize;
    }
    public String getMetricToMaximize() {
        return MetricToMaximize;
    }
    public void setMetricToMaximize(String metricToMaximize) {
        MetricToMaximize = metricToMaximize;
    }
    public void setMetricToMinimize(String metricToMinimize) {
        MetricToMinimize = metricToMinimize;
    }

    public BusinessGoal(String metric,String threshold,String operator){
        Metric = metric;
        Threshold = threshold;
        Operator = operator;
    }

    public BusinessGoal(String metric, String objective){
        if (objective.equals("maximize")){
            MetricToMaximize = metric;
        }else if (objective.equals("minimize")){
            MetricToMinimize = metric;
        }
    }



    @Override
    public String toString() {
        Logger logger = Logger.getLogger(BusinessGoal.class.getName());
        if (MetricToMaximize!=null){
            return ("MetricToMaximize"+ COLON_DELIMITER +MetricToMaximize+NEWLINE);
        }else if (MetricToMinimize!=null){
            return ("MetricToMinimize"+ COLON_DELIMITER +MetricToMinimize+NEWLINE);
        }else if ((Metric!=null)&&(Threshold!=null)&&(Operator!=null)){
            return ("Metric "+Metric+"should be "+Operator+"the threshold value"+ COLON_DELIMITER +NEWLINE);
        }else {
            logger.log(Level.SEVERE,"Invalid BusinessGoal object, having none of the two accepted forms, asked to be converted to String");
            return "";
        }
    }

    @Override
    public Map<String, String> serializeToMap() {
        Map <String,String> serializeMap = new LinkedHashMap<>();
        if (MetricToMinimize!=null && !Objects.equals(MetricToMinimize, "")) {
            serializeMap.put("MetricToMinimize", MetricToMinimize);
        }
        if ((MetricToMaximize!=null)&&(!Objects.equals(MetricToMaximize, ""))){
            serializeMap.put("MetricToMaximize", MetricToMaximize);
        }
        if (Metric!=null && !Objects.equals(Metric, "")){
            if (Threshold!=null && !Objects.equals(Threshold, "")){
                serializeMap.put("Metric",Metric);
                serializeMap.put("Threshold",Threshold);
            }
            if (Operator!=null && !Objects.equals(Operator, "")){
                serializeMap.put("Metric",Metric);
                serializeMap.put("Operator",Operator);
            }
        }
        return serializeMap;
    }
}




