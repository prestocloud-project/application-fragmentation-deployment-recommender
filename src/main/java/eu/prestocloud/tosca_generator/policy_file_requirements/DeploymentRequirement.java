package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.LinkedHashMap;
import java.util.Map;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;

public class DeploymentRequirement extends Requirement{
    private int max_instances = Integer.MAX_VALUE; //max active instances of all fragments
    private int max_fragment_instances = Integer.MAX_VALUE; //max active instances of one fragment
    private int max_master_instances = Integer.MAX_VALUE; //max active instances of a JPPF master

    public int getMax_instances() {
        return max_instances;
    }

    public void setMax_instances(int max_instances) {
        this.max_instances = max_instances;
    }

    public int getMax_fragment_instances() {
        return max_fragment_instances;
    }

    public void setMax_fragment_instances(int max_fragment_instances) {
        this.max_fragment_instances = max_fragment_instances;
    }

    public int getMax_master_instances() {
        return max_master_instances;
    }

    public void setMax_master_instances(int max_master_instances) {
        this.max_master_instances= max_master_instances;
    }

    @Override
    public String toString(){
        StringBuilder string_representation = new StringBuilder();
        if (max_instances!=0){
            string_representation.append("MaxInstances").append(max_instances).append(NEWLINE);
        }
        if (max_fragment_instances!=0){
            string_representation.append("MaxFragmentInstances").append(max_fragment_instances).append(NEWLINE);
        }
        return string_representation.toString();
    }

    @Override
    public Map<String, String> serializeToMap() {
        Map <String,String> serializeMap = new LinkedHashMap<>();
        //serializeMap.put("ScalabilityAction",ScalabilityAction);
        if (max_instances!=0 && max_instances<Integer.MAX_VALUE) {
            serializeMap.put("MaxInstances", String.valueOf(max_instances));
        }
        /* Will not add the maximum number of fragment instances to the map, as they will be added to each fragment separately
        if (max_fragment_instances!=0) {
            serializeMap.put("MaxFragmentInstances", String.valueOf(max_fragment_instances));
        }*/
        return serializeMap;
    }
}
