package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.ArrayList;
import java.util.Map;

public class HostingRequirement extends Requirement {
    ArrayList<Integer> hasNumberOfCores;
    ArrayList<Integer> hasRAM;
    ArrayList<Integer> hasDisk;
    ArrayList<Integer> hasBandwidth;

    public HostingRequirement(){

    }

    @Override
    public Map<String, String> serializeToMap() {
        return null;
    }

    public ArrayList<Integer> getHasNumberOfCores() {
        return hasNumberOfCores;
    }

    public void setHasNumberOfCores(ArrayList<Integer> hasNumberOfCores) {
        this.hasNumberOfCores = hasNumberOfCores;
    }

    public ArrayList<Integer> getHasRAM() {
        return hasRAM;
    }

    public void setHasRAM(ArrayList<Integer> hasRAM) {
        this.hasRAM = hasRAM;
    }

    public ArrayList<Integer> getHasDisk() {
        return hasDisk;
    }

    public void setHasDisk(ArrayList<Integer> hasDisk) {
        this.hasDisk = hasDisk;
    }

    public ArrayList<Integer> getHasBandwidth() {
        return hasBandwidth;
    }

    public void setHasBandwidth(ArrayList<Integer> hasBandwidth) {
        this.hasBandwidth = hasBandwidth;
    }

}
