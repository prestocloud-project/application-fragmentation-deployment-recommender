package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class BudgetRequirement extends Requirement {
    private Integer CostThreshold;
    private Integer TimePeriod;
    public Integer getCostThreshold() {
        return CostThreshold;
    }

    public void setCostThreshold(Integer costThreshold) {
        CostThreshold = costThreshold;
    }

    public Integer  getTimePeriod() {
        return TimePeriod;
    }

    public void setTimePeriod(Integer  timePeriod) {
        TimePeriod = timePeriod;
    }
    public BudgetRequirement(Integer time_period, Integer cost_threshold){
        this.CostThreshold = cost_threshold;
        this.TimePeriod = time_period;
    }

    public BudgetRequirement(){

    }

    @Override
    public Map<String, String> serializeToMap() {
        Map <String,String> serializeMap = new LinkedHashMap<>();
        serializeMap.put("CostThreshold",CostThreshold.toString());
        if (TimePeriod!=null && !Objects.equals(TimePeriod, "")) {
            serializeMap.put("TimePeriod", String.valueOf(TimePeriod));
        }
        return serializeMap;
    }
}
