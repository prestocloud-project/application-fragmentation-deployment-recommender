package eu.prestocloud.tosca_generator.policy_file_requirements;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;

public class AttributeMapping {
    private final int VERY_HIGH;
    private final int HIGH;
    private final int MEDIUM;
    private final int LOW;
    private final int VERY_LOW;


    public AttributeMapping (ProcessingAttribute processing_attribute){
        switch (processing_attribute){
            case cpu:
                VERY_HIGH = 16;
                HIGH = 8;
                MEDIUM = 4;
                LOW = 2;
                VERY_LOW = 1;
                break;
            case memory:
                VERY_HIGH = 32000;
                HIGH = 16000;
                MEDIUM = 4000;
                LOW = 2000;
                VERY_LOW = 500;
                break;
            case storage:
                VERY_HIGH = 4000;
                HIGH = 1000;
                MEDIUM = 100;
                LOW = 20;
                VERY_LOW = 4;
                break;
                default:
                    try{
                        throw new Exception("Tried to initialize a processing attribute not enumerated among possible processing attributes (e.g cpu, memory, storage)");
                    }catch (Exception e){
                        e.printStackTrace();
                        System.exit(-100);
                    }
                    VERY_HIGH = -1;
                    HIGH = -1;
                    MEDIUM = -1;
                    LOW = -1;
                    VERY_LOW = -1;
                    break;

        }
    }

    public AttributeMapping (int very_low, int low, int medium, int high, int very_high){
        VERY_HIGH = very_high;
        HIGH = high;
        MEDIUM = medium;
        LOW = low;
        VERY_LOW = very_low;
    }

    public int getVERY_HIGH() {
        return VERY_HIGH;
    }

    public int getHIGH() {
        return HIGH;
    }

    public int getMEDIUM() {
        return MEDIUM;
    }

    public int getLOW() {
        return LOW;
    }

    public int getVERY_LOW() {
        return VERY_LOW;
    }

    @Override
    public String toString(){
        String High = "High:"+getHIGH()+ NEWLINE;
        String VeryHigh = "Very High:"+getVERY_HIGH()+ NEWLINE;
        String Medium = "Medium:"+getMEDIUM()+ NEWLINE;
        String Low = "Low:"+getLOW()+ NEWLINE;
        String VeryLow = "Very low:"+getVERY_LOW()+ NEWLINE;
        return "Value of translator:"+NEWLINE+VeryHigh+High+Medium+Low+VeryLow;
    }

}
