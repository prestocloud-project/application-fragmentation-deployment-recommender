package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;

public class CollocationRequirement extends Requirement{
    private String AffinityTo;
    private String AntiAffinityTo;
    private Boolean DeployOnSameResource;

    public CollocationRequirement(String AffinityTo,String AntiAffinityTo,boolean DeployOnSameResource){
        this.AffinityTo = AffinityTo;
        this.AntiAffinityTo = AntiAffinityTo;
        this.DeployOnSameResource = DeployOnSameResource;
    }
    public CollocationRequirement(){

    }

    @Override
    public Map<String, String> serializeToMap() {
        Map <String,String> serializeMap = new LinkedHashMap<>();
        if (AffinityTo!=null && !Objects.equals(AffinityTo, "")) {
            serializeMap.put("AffinityTo", AffinityTo);
        }
        if (AntiAffinityTo!=null && !Objects.equals(AntiAffinityTo, "")){
            serializeMap.put("AntiAffinityTo",AntiAffinityTo);
        }
        if (DeployOnSameResource!=null) {
            serializeMap.put("DeployOnSameResource", DeployOnSameResource.toString());
        }
        return serializeMap;
    }

    public String getAffinityTo() {
        return AffinityTo;
    }

    public void setAffinityTo(String affinityTo) {
        AffinityTo = affinityTo;
    }

    public String getAntiAffinityTo() {
        return AntiAffinityTo;
    }

    public void setAntiAffinityTo(String antiAffinityTo) {
        AntiAffinityTo = antiAffinityTo;
    }

    public Boolean getDeployOnSameResource() {
        return DeployOnSameResource;
    }

    public void setDeployOnSameResource(boolean deployOnSameResource) {
        DeployOnSameResource = deployOnSameResource;
    }

    @Override
    public String toString(){
        StringBuilder string_representation = new StringBuilder("");
        if (AffinityTo!=null){
            string_representation.append("Has affinity to: ").append(AffinityTo).append(NEWLINE);
        }
        if (AntiAffinityTo!=null){
            string_representation.append("Has anti-affinity to: ").append(AntiAffinityTo).append(NEWLINE);
        }
        //TODO check if this field will always be specified, or will be assumed to be false if not specified
        string_representation.append("Should deploy on same resource ").append(DeployOnSameResource).append(NEWLINE);
        return string_representation.toString();
    }

}
