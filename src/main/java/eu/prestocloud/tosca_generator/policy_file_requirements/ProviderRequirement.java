package eu.prestocloud.tosca_generator.policy_file_requirements;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.UNDERSCORE;

public class ProviderRequirement extends Requirement {

    private String providerName;
    private Boolean required;
    private Boolean excluded;
    private static int id_counter = 0;
    private int this_requirement_id;

    public Boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Boolean isExcluded() {
        return excluded;
    }

    public void setExcluded(boolean excluded) {
        this.excluded = excluded;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String provider) {
        this.providerName = provider;
    }

    /**
     * This method needs to be called whenever the provider requirements have been parsed, so that the next parsing will render the proper
     */
    public static void zero_requirement_id_counter(){
        id_counter = 0;
    }

    public  ProviderRequirement(String provider_requirement, boolean required, boolean excluded){
        this.providerName = provider_requirement;
        this.required = required;
        this.excluded = excluded;
        this_requirement_id = id_counter ++;
    }
    public ProviderRequirement(){
        this_requirement_id = id_counter ++;
    }

    @Override
    public Map<String, String> serializeToMap() {
        Map<String, String> serializeMap = new LinkedHashMap<>();
        if (providerName != null && !Objects.equals(providerName, "")) {
            serializeMap.put("ProviderName"+UNDERSCORE+ this_requirement_id, providerName);
        }
        if (required != null){
            serializeMap.put("ProviderRequired"+UNDERSCORE+this_requirement_id, required.toString());
        }
        if (excluded != null){
            serializeMap.put("ProviderExcluded"+UNDERSCORE+this_requirement_id,excluded.toString());
        }
        return serializeMap;
    }

    @Override
    public String toString(){
        StringBuilder string_representation = new StringBuilder("");
        string_representation.append("Provider required is: ").append(providerName).append(NEWLINE);
        string_representation.append("This provider should absolutely be followed: ").append(required).append(NEWLINE);
        string_representation.append("This provider should absolutely not be used: ").append(excluded).append(NEWLINE);

        return string_representation.toString();
    }

}
