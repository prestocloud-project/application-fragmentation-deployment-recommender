package eu.prestocloud.tosca_generator.policy_file_requirements;

import eu.prestocloud.annotations.PrestoFragmentation;
import eu.prestocloud.tosca_generator.utility_classes.AttributeRange;

import java.util.Map;

import static eu.prestocloud.configuration.Constants.int_infinity;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.NEWLINE;

public class MappingRequirement extends Requirement{
    private final AttributeMapping cpu_mapping;
    private final AttributeMapping memory_mapping;
    private final AttributeMapping storage_mapping;
    private static String annotations_accepted = "standard 5-level";

    public MappingRequirement(){
        cpu_mapping =new AttributeMapping(ProcessingAttribute.cpu);
        memory_mapping =new AttributeMapping(ProcessingAttribute.memory);
        storage_mapping =new AttributeMapping(ProcessingAttribute.storage);
    }
    public MappingRequirement(AttributeMapping cpu_mapping, AttributeMapping memory_mapping, AttributeMapping storage_mapping){
        this.cpu_mapping = cpu_mapping;
        this.memory_mapping = memory_mapping;
        this.storage_mapping = storage_mapping;
    }

    public AttributeMapping getCpu_mapping() {
        return cpu_mapping;
    }

    public AttributeMapping getMemory_mapping() {
        return memory_mapping;
    }

    public AttributeMapping getStorage_mapping() {
        return storage_mapping;
    }

    public static AttributeRange translate(PrestoFragmentation.CPULoad cpuLoad, AttributeMapping cpuLoadMapping) {
        AttributeRange num_cpus = new AttributeRange();
        switch (cpuLoad) {
            case VERY_HIGH:
                num_cpus = new AttributeRange(cpuLoadMapping.getVERY_HIGH(), int_infinity);
                break;
            case HIGH:
                num_cpus = new AttributeRange(cpuLoadMapping.getHIGH(), cpuLoadMapping.getVERY_HIGH());
                break;
            case MEDIUM:
                num_cpus = new AttributeRange(cpuLoadMapping.getMEDIUM(), cpuLoadMapping.getHIGH());
                break;
            case LOW:
                num_cpus = new AttributeRange(cpuLoadMapping.getLOW(), cpuLoadMapping.getMEDIUM());
                break;
            case VERY_LOW:
                num_cpus = new AttributeRange(cpuLoadMapping.getVERY_LOW(), cpuLoadMapping.getLOW());
                break;
            default:
                System.out.println("Invalid cpu load annotation - only " + annotations_accepted + " annotations are accepted");
                break;
        }
        return num_cpus;
    }
    public static AttributeRange translate(PrestoFragmentation.CPULoad cpuLoad) {
        AttributeRange num_cpus = new AttributeRange();
        switch (cpuLoad) {
            case VERY_HIGH:
                num_cpus = new AttributeRange(16, int_infinity);
                break;
            case HIGH:
                num_cpus = new AttributeRange(8, 16);
                break;
            case MEDIUM:
                num_cpus = new AttributeRange(4, 8);
                break;
            case LOW:
                num_cpus = new AttributeRange(2, 4);
                break;
            case VERY_LOW:
                num_cpus = new AttributeRange(1, 2);
                break;
            default:
                System.out.println("Invalid cpu load annotation - only " + annotations_accepted + " annotations are accepted");
                break;
        }
        return num_cpus;
    }

    public static AttributeRange translate(PrestoFragmentation.MemoryLoad memoryLoad, AttributeMapping ramLoadMapping) {
        AttributeRange mem_size = new AttributeRange();
        switch (memoryLoad) {
            case VERY_HIGH:
                mem_size = new AttributeRange(ramLoadMapping.getVERY_HIGH(), int_infinity);
            case HIGH:
                mem_size = new AttributeRange(ramLoadMapping.getHIGH(), ramLoadMapping.getVERY_HIGH());
                break;
            case MEDIUM:
                mem_size = new AttributeRange(ramLoadMapping.getMEDIUM(), ramLoadMapping.getHIGH());
                break;
            case LOW:
                mem_size = new AttributeRange(ramLoadMapping.getLOW(), ramLoadMapping.getMEDIUM());
                break;
            case VERY_LOW:
                mem_size = new AttributeRange(ramLoadMapping.getVERY_LOW(), ramLoadMapping.getLOW());
                break;
            default:
                System.out.println("Invalid memory load annotation - only " + annotations_accepted + " annotations are accepted");
                break;
        }
        return mem_size;
    }
    public static AttributeRange translate(PrestoFragmentation.MemoryLoad memoryLoad) {
        AttributeRange mem_size = new AttributeRange();
        switch (memoryLoad) {
            case VERY_HIGH:
                mem_size = new AttributeRange(32, int_infinity);
            case HIGH:
                mem_size = new AttributeRange(16, 32);
                break;
            case MEDIUM:
                mem_size = new AttributeRange(8, 16);
                break;
            case LOW:
                mem_size = new AttributeRange(4, 8);
                break;
            case VERY_LOW:
                mem_size = new AttributeRange(1, 4);
                break;
            default:
                System.out.println("Invalid memory load annotation - only high, medium and low values are expected");
                break;
        }
        return mem_size;
    }


    public static AttributeRange translate(PrestoFragmentation.StorageLoad storageLoad, AttributeMapping storageLoadMapping) {
        AttributeRange storage_size = new AttributeRange();
        switch (storageLoad) {
            case VERY_HIGH:
                storage_size = new AttributeRange(storageLoadMapping.getVERY_HIGH(), int_infinity);
            case HIGH:
                storage_size = new AttributeRange(storageLoadMapping.getHIGH(), storageLoadMapping.getVERY_HIGH());
                break;
            case MEDIUM:
                storage_size = new AttributeRange(storageLoadMapping.getMEDIUM(), storageLoadMapping.getHIGH());
                break;
            case LOW:
                storage_size = new AttributeRange(storageLoadMapping.getLOW(), storageLoadMapping.getMEDIUM());
                break;
            case VERY_LOW:
                storage_size = new AttributeRange(storageLoadMapping.getVERY_LOW(), storageLoadMapping.getLOW());
                break;
            default:
                System.out.println("Invalid storage load annotation - only " + annotations_accepted + " annotations are accepted");
                break;
        }
        return storage_size;
    }
    public static AttributeRange translate(PrestoFragmentation.StorageLoad storageLoad) {
        AttributeRange storage_size = new AttributeRange();
        switch (storageLoad) {
            case VERY_HIGH:
                storage_size = new AttributeRange(32, int_infinity);
            case HIGH:
                storage_size = new AttributeRange(16, 32);
                break;
            case MEDIUM:
                storage_size = new AttributeRange(8, 16);
                break;
            case LOW:
                storage_size = new AttributeRange(4, 8);
                break;
            case VERY_LOW:
                storage_size = new AttributeRange(1, 4);
                break;
            default:
                System.out.println("Invalid storage load annotation - only " + annotations_accepted + " annotations are accepted");
        }
        return storage_size;
    }



    @Override
    public String toString(){
            return cpu_mapping +NEWLINE+ memory_mapping +NEWLINE+ storage_mapping;
    }

    @Override
    public Map<String, String> serializeToMap() {
        return null;
    }
}
