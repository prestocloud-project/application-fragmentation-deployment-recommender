package eu.prestocloud.tosca_generator;

import java.util.ArrayList;

import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;

public class ToscaImports {
    private ArrayList<String> import_list= new ArrayList<>();


    public void add (String import_statement){
        import_list.add(import_statement);
    }

    public ArrayList<String> getImport_list() {
        return import_list;
    }

    public void setImport_list(ArrayList<String> import_list) {
        this.import_list = import_list;
    }

    @Override
    public String toString(){
        return toString(0);
    }

    public String toString(int indentation_level){
        StringBuilder string_representation = new StringBuilder();

        for (String import_statement: import_list) {
            string_representation.append(indent(indentation_level)).append(LIST_PRFX).append(import_statement).append(NEWLINE);
        }
        return string_representation.toString();
    }
}
