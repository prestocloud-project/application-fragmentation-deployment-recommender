package eu.prestocloud.tosca_generator;

import eu.prestocloud.tosca_generator.policy_file_requirements.*;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static eu.prestocloud.configuration.Constants.graph_hex_id;
import static eu.prestocloud.configuration.Constants.graph_instance_hex_id;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;
import static eu.prestocloud.utilities.UIJsonParser.getUi_deployment_json;

public class ToscaMetadata {
    private Map<String,String> metadata = new LinkedHashMap<>();
    public void addPair (String key, String value){
        metadata.put(key,value);
    }
    public void addMap (Map<String,String> map){
        metadata.putAll(map);
    }

    public void addDefaultValues(){
        addPair("template_name","ICCS generated types definition");
        addPair("template_author","ICCS");
        addPair("template_version","1.0.0-SNAPSHOT");
    }

    public void should_use_cjdns_overlay(String value){
        addPair("use_cjdns_overlay", value);
    }

    public void addCustomGraphValues(){
        addPair("graphHexID",graph_hex_id);
        addPair("graphInstanceHexID",graph_instance_hex_id);
    }
    public void addGraphValues(){

        graph_hex_id = (String)((JSONObject) getUi_deployment_json().get("jsonDeployment")).get("graphHexID");
        graph_instance_hex_id = (String)((JSONObject) getUi_deployment_json().get("jsonDeployment")).get("graphInstanceHexID");
        addPair("graphHexID",graph_hex_id);
        addPair("graphInstanceHexID",graph_instance_hex_id);
    }

    @Override
    public String toString() {
        return toString(0);
    }
    public String toString(int indentation_level){

        StringBuilder string_representation = new StringBuilder();

        for (String key: metadata.keySet()) {
            string_representation.append(indent(indentation_level)).append(key).append(COLON_DELIMITER).append(SPACE).append(metadata.get(key)).append(NEWLINE);
        }
            //System.out.println(key);
            //System.out.println("value : " + metadata.get(key));
        return string_representation.toString();
    }

    public void addRequirements(FragmentationPolicy fragmentationPolicy) {

        BudgetRequirement budgetRequirement = fragmentationPolicy.getBudgetRequirement();
        this.addMap(budgetRequirement.serializeToMap());


        ArrayList<ProviderRequirement> providerRequirements = fragmentationPolicy.getProviderRequirements();
        for (ProviderRequirement providerRequirement : providerRequirements) {
            this.addMap(providerRequirement.serializeToMap());
        }

        ArrayList<CollocationRequirement> collocationRequirements = fragmentationPolicy.getCollocationRequirements();
        for (CollocationRequirement collocationRequirement : collocationRequirements) {
            this.addMap(collocationRequirement.serializeToMap());
        }

        /*
        ArrayList<DeploymentRequirement> scalabilityRequirements = fragmentationPolicy.getScalabilityRequirements();
        for (DeploymentRequirement deploymentRequirement : scalabilityRequirements) {
            this.addMap(deploymentRequirement.serializeToMap());
        }

        DeploymentRequirement deploymentRequirement = fragmentationPolicy.getDeploymentRequirement();
        this.addMap(deploymentRequirement.serializeToMap());
        */
        ArrayList<BusinessGoal> businessGoals = fragmentationPolicy.getBusinessGoals();
        for (BusinessGoal businessGoal : businessGoals) {
            this.addMap(businessGoal.serializeToMap());
        }
    }
}
