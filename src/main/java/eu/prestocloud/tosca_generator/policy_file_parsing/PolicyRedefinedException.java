package eu.prestocloud.tosca_generator.policy_file_parsing;


import java.util.logging.Level;
import java.util.logging.Logger;

public class PolicyRedefinedException extends Exception {

    public PolicyRedefinedException(int times_defined,String redefined_requirement){
        super();
        Logger.getAnonymousLogger().log(Level.SEVERE,"The "+redefined_requirement+" policy has been defined "+times_defined+" times");
    }
}
