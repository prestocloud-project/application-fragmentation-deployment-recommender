package eu.prestocloud.tosca_generator.policy_file_parsing;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PolicyRequiredException extends Exception{
    public PolicyRequiredException(String mandatory_requirement){
        super();
        Logger.getAnonymousLogger().log(Level.SEVERE,"The "+mandatory_requirement+" policy is required to be defined");
    }
}
