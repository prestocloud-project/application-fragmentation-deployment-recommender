package eu.prestocloud.tosca_generator;

import eu.prestocloud.messaging.AMQP_Publisher;
import eu.prestocloud.messaging.AMQP_Subscriber;
import eu.prestocloud.tosca_generator.capability_types.CapabilityTypesSegment;
import eu.prestocloud.tosca_generator.policy_file_parsing.PolicyRedefinedException;
import eu.prestocloud.tosca_generator.policy_file_parsing.PolicyRequiredException;
import eu.prestocloud.tosca_generator.policy_file_requirements.*;
import eu.prestocloud.tosca_generator.relationship_types.RelationshipTypesSegment;
import eu.prestocloud.tosca_generator.utility_classes.Formatting;
import eu.prestocloud.tosca_generator.utility_classes.SynchronizedBoolean;
import eu.prestocloud.tosca_generator.utility_classes.dependencies.BidirectionalCollocationDependencies;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.database.DatabaseUtils.bootstrap_database_table;
import static eu.prestocloud.database.DatabaseUtils.update_tosca_deployments_db_table;
import static eu.prestocloud.messaging.JsonInput.retrieve_input;
import static eu.prestocloud.tosca_generator.NodeGenerator.create_core_TOSCA_document;
import static eu.prestocloud.tosca_generator.NodeGenerator.initialize_node_generator;
import static eu.prestocloud.tosca_generator.utility_classes.CoordinatorNodesController.initialize_coordinator_nodes;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.*;
import static eu.prestocloud.tosca_generator.utility_classes.dependencies.DependenciesDataStructures.initialize_dependency_data_structures;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.fragments_map;
import static eu.prestocloud.utilities.UIJsonParser.get_policy_from_ui;
import static java.util.logging.Level.*;

public class ToscaGenerator {

    private static final Logger logger = Logger.getLogger(ToscaGenerator.class.getName());
    public static int tosca_deployment_counter = 1;
    private static final AMQP_Subscriber amqp_subscriber = new AMQP_Subscriber();
    private static final AMQP_Publisher amqp_publisher = new AMQP_Publisher();

    //public static int tosca_id = 0; //Although this first value is never used.
    public static boolean FIRST_RUN = true;
    public static final SynchronizedBoolean INSTANCE_TOSCA_TOPOLOGY_PARSED = new SynchronizedBoolean(true);
    public static final SynchronizedBoolean tosca_generation_scheduled = new SynchronizedBoolean(false);
    private static long current_time, timer_start = System.nanoTime();


    /**
     * Invokes the functions necessary to generate the TOSCA document
     *
     * @param args The location of the properties file to be used for deployment
     */
    private static String preferences_file_string = EMPTY;

    public static void main(String[] args) {

        Properties prop = new Properties();
        InputStream preferences_file = null;

        try {

            //if no preferences file specified as input
            if ((preferences_file_string==null || preferences_file_string.equals(EMPTY))&& (args==null || args.length<1)) {
                Logger.getAnonymousLogger().log(WARNING,"No preferences file has been specified manually, will attempt to find one in the default paths");
                if (System.getProperty("os.name").equals("Windows 10")) /*locally debugging*/ {
                    preferences_file = new FileInputStream("C:\\Users\\user\\IdeaProjects\\CodeIntrospector\\src\\main\\resources\\debug.properties");
                } else {
                    preferences_file = new FileInputStream("/home/prestocloud/configuration.properties");
                }
            }else {
                Logger.getAnonymousLogger().log(INFO,"A preferences file has been manually specified");

                if (preferences_file_string==null || preferences_file_string.equals(EMPTY)) {
                    preferences_file_string = args[0];
                    preferences_file = new FileInputStream(args[0]);
                }
                preferences_file = new FileInputStream(preferences_file_string);
            }
            prop.load(preferences_file);

            operational_input_mode = prop.getProperty("operational_input_mode");
            //use_cjdns_overlay = prop.getProperty("use_cjdns_overlay");
            broker_IP_address = prop.getProperty("broker_IP_address");
            broker_username  = prop.getProperty("broker_username");
            broker_password = prop.getProperty("broker_password");
            enforce_ssl = Boolean.parseBoolean(prop.getProperty("enforce_ssl"));
            key_storage_endpoint = prop.getProperty("key_storage_endpoint");
            if (operational_input_mode.equalsIgnoreCase("annotations")) {
                organization_username = prop.getProperty("organization_username");
            }
            key_storage_password = prop.getProperty("key_storage_password");
            vault_address = prop.getProperty("vault_address");
            vault_token = prop.getProperty("vault_token");
            topic_to_trigger_actual_deployment = prop.getProperty("topic_to_trigger_actual_deployment");
            topic_to_receive_actual_deployment_state = prop.getProperty("topic_to_receive_actual_deployment");
            topic_for_application_undeployment = prop.getProperty("topic_for_application_undeployment");
            topic_to_receive_excluded_devices = prop.getProperty("topic_to_receive_excluded_devices");
            topic_for_scale_out_adaptation = prop.getProperty("topic_for_scale_out_adaptation");
            topic_for_scale_in_adaptation = prop.getProperty("topic_for_scale_in_adaptation");
            topic_to_request_excluded_devices = prop.getProperty("topic_to_request_excluded_devices");
            topic_for_fragment_hosting_data = prop.getProperty("topic_for_fragment_hosting_data");
            /*
            keyStorePath = prop.getProperty("keyStorePath");
            keyPassphrase = prop.getProperty("keyPassphrase").toCharArray();
            trustStorePath = prop.getProperty("trustStorePath");
            trustPassphrase = prop.getProperty("trustPassphrase").toCharArray();
            */
            if (operational_input_mode !=null && operational_input_mode.equals("annotations")) {
                active_policy_file = prop.getProperty("active_policy_file");
                type_level_tosca_filename_suffix = prop.getProperty("type_level_tosca_file_name");
                annotated_application_path = prop.getProperty("annotated_application_path");
                annotated_application_name = prop.getProperty("annotated_application_name");
                base_policy_file_path = prop.getProperty("policy_file_path");
            }else if (operational_input_mode !=null && operational_input_mode.equals("json")){
                topic_to_receive_deployment_json = prop.getProperty("topic_to_receive_deployment_json");
                type_level_tosca_filename_suffix = prop.getProperty("type_level_tosca_file_name");
            }
            base_generated_tosca_path = prop.getProperty("generated_type_level_tosca_path");
            public_type_level_tosca_path = prop.getProperty("public_type_level_tosca_path");
            repository_path = prop.getProperty("repository_path");
            adaptation_interval_seconds = Integer.parseInt(prop.getProperty("adaptation_interval_seconds"));
            instance_level_tosca_parser = prop.getProperty("instance_level_tosca_parser");
            use_full_dependency_ordering = Boolean.parseBoolean(prop.getProperty("use_full_dependency_ordering"));
            //initialize_database = Boolean.parseBoolean(prop.getProperty("initialize_database"));
            run_basic_tests = Boolean.parseBoolean(prop.getProperty("run_basic_tests"));
            coordinator_node_docker_image = prop.getProperty("coordinator_node_docker_image");
            coordinator_node_docker_registry = prop.getProperty("coordinator_node_docker_registry");
            coordinator_node_docker_registry_username = prop.getProperty("coordinator_node_docker_registry_username");
            coordinator_node_docker_registry_password = prop.getProperty("coordinator_node_docker_registry_password");
            coordinator_node_ports_properties = prop.getProperty("coordinator_node_ports_properties").split(",");
            coordinator_node_environmental_variables = prop.getProperty("coordinator_node_environmental_variables").split(",");
            coordinator_node_ssh_key = prop.getProperty("coordinator_node_ssh_key");
            coordinator_node_health_check_command = prop.getProperty("coordinator_node_health_check_command");
            coordinator_node_health_check_interval = !prop.getProperty("coordinator_node_health_check_interval").equals(EMPTY)? Integer.parseInt(prop.getProperty("coordinator_node_health_check_interval")) : Integer.MAX_VALUE;
            adaptation_topics = new String [] {
                    topic_to_receive_deployment_json,
                    topic_to_receive_actual_deployment_state,
                    topic_for_application_undeployment,
                    topic_to_receive_excluded_devices,
                    topic_for_scale_out_adaptation,
                    topic_for_scale_in_adaptation
            };
        } catch (Exception ex) {
            ex.printStackTrace();

            operational_input_mode = "json";
            broker_IP_address = "52.58.107.100";
            broker_username  = "guest";
            broker_password = "guest";
            enforce_ssl = Boolean.parseBoolean(prop.getProperty("enforce_ssl"));
            topic_to_receive_deployment_json = "deployment.topology_input";
            topic_to_trigger_actual_deployment = "deployment.req";
            topic_to_request_excluded_devices = "topology_adaptation.excluded_devices.req";
            topic_to_receive_actual_deployment_state = "deployment.ack";
            topic_for_application_undeployment = "undeployment.ack";
            topic_to_receive_excluded_devices = "topology_adaptation.excluded_devices.ack";
            topic_for_scale_out_adaptation = "topology_adaptation.scale_out";
            topic_for_scale_in_adaptation = "topology_adaptation.scale_in";
            topic_to_request_excluded_devices = "topology_adaptation.excluded_devices.req";
            topic_for_fragment_hosting_data = "deployment.fragment_hosts";
/*            keyStorePath = "/home/prestocloud/afdr/input/certificates/client/key-store.p12";
            keyPassphrase = "NissaPrEstoCloud".toCharArray();
            trustStorePath = "/home/prestocloud/afdr/input/certificates/client/trust-store.p12";
            trustPassphrase = "Pr3570Cloud".toCharArray();*/
            vault_address = "3.120.91.124";
            vault_token = "s.pV8wvr4dkuq5ii476ddErT07";
            active_policy_file = "basic_policy_file_v2.policyfile";
            type_level_tosca_filename_suffix = "tosca_deployment";
            annotated_application_path = "/home/prestocloud/input/annotated_application/";
            annotated_application_name = "annotated_application.jar";
            base_policy_file_path = "/home/prestocloud/input/policy_files/";
            base_generated_tosca_path = " /home/prestocloud/output";
            public_type_level_tosca_path = "/shared/tosca_repository/";
            repository_path = "/home/prestocloud/afdr/tosca_repository/";
            instance_level_tosca_parser = "/home/prestocloud/input/presto_parser.jar";
            coordinator_node_docker_image = "coordinator_image";
            coordinator_node_docker_registry = "coordinator_registry";
            coordinator_node_docker_registry_username = EMPTY;
            coordinator_node_docker_registry_password = EMPTY;
            coordinator_node_ports_properties = new String[0];
            coordinator_node_environmental_variables = new String[0];
            coordinator_node_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC75AUrOpEajZ+/Bx8uwzFy4wwyL6rYb3WfocTwq4B7O/iCi+GB5JehBmX+AEx7EnwqMcR1Op5/KdxYp7z/M7C5Hx1x21E0FADLYhQ4kvNJ4xwDx5SsLlDbLtIhweEPDBL+d7sOl8HSJKBZGPg2JKZPK+eBOYVbnYREOTHpdzEmixYKgN71bT+ecTVySbFjraEMrTCTwrmPeSEr5MwHRCpCFgnfo/vhPTaTFI6oDlzgGDGXq6qT0HmIQUyrLa2KMD9wfZDETTs4ZmkAgdPNj1U5JKcRSSaRDzkoFsRQVQWjheSKJwhn10fNQw9vUsidEcJc8vZLp+a1ATfe5g4DiWhzPfyfF2/k5qeLGYAWPn5xh5ICQbqscNzczkV4H+zjkHwnaxrfyri9OIRd5Brd+vNVFlBgSlQuVAl4F/aNSMVjDLHgVOS5tHmYOIMyyUqZlvyt7V7JdsqwfQsRP3T+qltiTsQeELHYMJLflFOkwjuEoo1ZKJMfWpK8zS8X62DJawml+lq5+GHHAAElo02Hmy0sqYU/VXMzC8whcTvLjwW2pmj6heWRP5Szrr+5XMjfhdMRG0CrwijRhhfePnaVc9KhiSWBGLRw1k0TbWrDqwngavLmwJvCDa7yMqtEMzzVFN+/aliuZ6gukwwIxvhC8ED12vQ5V6NowYiELruLxoAG8Q== user@default";
            coordinator_node_health_check_command = "curl google.gr";
            coordinator_node_health_check_interval = 10;
            adaptation_interval_seconds = 120;
            use_full_dependency_ordering = false;
            initialize_database = true;
            run_basic_tests = false;
            adaptation_topics = new String [] {
                    topic_to_receive_deployment_json,
                    topic_to_receive_actual_deployment_state,
                    topic_for_application_undeployment,
                    topic_to_receive_excluded_devices,
                    topic_for_scale_out_adaptation,
                    topic_for_scale_in_adaptation
            };
            Logger.getAnonymousLogger().log(Level.WARNING, "Problem reading information from the properties file - reverting to default settings");
        } finally {
            if (preferences_file != null) {
                try {
                    preferences_file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        String full_type_level_tosca_file_name = EMPTY;
        String full_public_type_level_tosca_file_name = EMPTY;

        //TODO set initialize database to false for release
        initialize_database = true;
        //If database initialization is set, this has the effect of the database being recreated whenever the application is recreated as well, and storing only the latest tosca deployment counter. The tosca deployment counter is not initialized to zero each time, because the below if statement is not satisfied, and thus the old value is kept.
        int next_tosca_deployment_counter = bootstrap_database_table(tosca_deployments_db_table_name, initialize_database);
        graph_instance_hex_id = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10); //In case of a UI-based deployment, this will be later overwritten
        if (tosca_deployment_counter == 1 && !SIMULATE_NETWORK) {
            amqp_subscriber.subscribe();
        }
        if (next_tosca_deployment_counter > (tosca_deployment_counter + 1)) {
            tosca_deployment_counter = next_tosca_deployment_counter - 1; //this means that a previous instance of the DIAFDRecom was terminated abruptly, so restore the previous counter (next - 1) and try to deploy again
            full_type_level_tosca_file_name = base_generated_tosca_path + graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;
            full_public_type_level_tosca_file_name = public_type_level_tosca_path + graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;
            Logger.getAnonymousLogger().log(INFO, "Resuming from interrupted execution of older DIAFDRecom instance");


            //The data obtained by calling the below method could be also serialized to and retrieved from a database

            create_core_TOSCA_document(tosca_deployment_counter);
        }

        else {
            Logger.getAnonymousLogger().log(INFO,"Following normal topology processing flow - deployment "+(tosca_deployment_counter) + ", at time "+(System.nanoTime()-timer_start)/1000000000.0);

            //Adding here a first location in which the processing file should be saved. This location is updated based on the actual deployment input method, annotations or json
            File processing_file = new File (base_generated_tosca_path + graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type);

            ToscaMetadata tosca_metadata = new ToscaMetadata();
            tosca_metadata.addDefaultValues();

            ToscaImports tosca_imports = new ToscaImports();
            tosca_imports.add("tosca-normative-types:1.2");
            tosca_imports.add("iccs-normative-types:1.1");
            tosca_imports.add("resource-descriptions:1.0");
            tosca_imports.add("placement-constraints:1.0");


            FragmentationPolicy fragmentationPolicy;
            ToscaDocument partial_tosca_document = null;
            if (operational_input_mode.equals("annotations") && active_policy_file != null && !active_policy_file.equalsIgnoreCase("none")) {
                fragmentationPolicy = parsePolicyFile(base_policy_file_path + active_policy_file);
                initialize_node_generator(fragmentationPolicy.getMappingRequirement());
                partial_tosca_document = create_core_TOSCA_document(tosca_deployment_counter);
                //tosca_metadata.should_use_cjdns_overlay(use_cjdns_overlay);
                tosca_metadata.addCustomGraphValues();
                tosca_metadata.addRequirements(fragmentationPolicy);

                full_public_type_level_tosca_file_name = public_type_level_tosca_path+ graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;

                full_type_level_tosca_file_name = base_generated_tosca_path + graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;
                processing_file = new File(full_type_level_tosca_file_name);

            } else if (operational_input_mode.equals("json")){

                if (SIMULATE_NETWORK){
                    retrieve_input();
                }

                partial_tosca_document = create_core_TOSCA_document(tosca_deployment_counter);
                tosca_metadata.addGraphValues();
                //tosca_metadata.should_use_cjdns_overlay(use_cjdns_overlay);
                fragmentationPolicy = get_policy_from_ui();
                tosca_metadata.addRequirements(fragmentationPolicy);

                full_public_type_level_tosca_file_name = public_type_level_tosca_path+ graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;

                full_type_level_tosca_file_name = base_generated_tosca_path + graph_hex_id+UNDERSCORE+graph_instance_hex_id+UNDERSCORE+type_level_tosca_filename_suffix + UNDERSCORE + tosca_deployment_counter + type_level_tosca_file_type;
                processing_file = new File(full_type_level_tosca_file_name);

            }


            if (partial_tosca_document!=null && partial_tosca_document.isNotEmpty()) {

                CapabilityTypesSegment capability_types_segment = new CapabilityTypesSegment();
                RelationshipTypesSegment relationship_types_segment = new RelationshipTypesSegment();

                //The "real" capability type segments are created using the following commands. These capabilities and relationships are now added by Activeeon

                //CapabilityTypesSegment capability_types_segment = create_capability_types_segment();
                //RelationshipTypesSegment relationship_types_segment = create_relationship_types_segment();

                ToscaDocument tosca_document = new ToscaDocument();
                tosca_document.setMetadata(tosca_metadata);
                tosca_document.setNode_types_segment(partial_tosca_document.getNode_types_segment());
                tosca_document.setDescription("Types Description");
                tosca_document.setTosca_imports(tosca_imports);
                tosca_document.setCapability_types_segment(capability_types_segment);
                tosca_document.setRelationship_types_segment(relationship_types_segment);
                tosca_document.setTopology_template_segment(partial_tosca_document.getTopology_template_segment());


                try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(processing_file), "utf-8"))) {

                    writer.append(tosca_document.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                Logger.getAnonymousLogger().log(WARNING,"No type-level TOSCA was generated - perhaps there are no fragment definitions available");
                tosca_deployment_counter--;
            }
        }

        if  (SIMULATE_NETWORK) {
            //tosca_deployment_counter++;
        }
        else {
            JSONObject obj = new JSONObject();
            //obj.put("old_tosca_id", tosca_id++);
            obj.put("tosca_id", full_public_type_level_tosca_file_name);
            if (organization_username !=null && !organization_username.equals(EMPTY)) {
                obj.put("user", organization_username);
            }
            obj.put("application_instance_id",tosca_deployment_counter);
            System.out.println(obj.toString());
            amqp_publisher.publish(obj.toJSONString());
            update_tosca_deployments_db_table(tosca_deployment_counter, TYPE_LEVEL_TOSCA_SENT_FOR_DEPLOYMENT);
        }

        /**
         * In the below tests, the transmission delays between the simulated components of the meta-management layer have been measured to be ~12 seconds. For this reason, the interval between the parsing of the two adaptation events is set to 12 seconds. This is expected to be greater, because of the time needed to deploy new VM's
         * Additionally, only one instance-level file is currently exploited (instance_tosca_2.yaml) as the names in the first file needed to be updated
         * Furthermore, the reconfiguration cycle is:
         * Scale in/out event -> Req. excluded devices -> Excl. devices ack -> new tosca generation when the next adaptation interval is met -> deployment.req -> deployment.ack -> parsing of instance-level TOSCA -> message to RARecom with the current topology -> Scale in/out event ...
         * All events happening before t=24 reflect the first reconfiguration phase, and the others the second reconfiguration phase
         * **IMPORTANT** Note that since the data contained in the instance-level TOSCA is not evaluated to update the data-structures, and that the RARecom is simulated, in the third adaptation, the number of instances for the FaceDetector fragment will decline without a particular event back to the starting number of instances (1). For this reason the third adaptation (second effective adaptation) which adds 5 instance to FaceDetector will result in 6 instances at the end of the simulation of the fragment)
         */
        if (tosca_deployment_counter==1 && run_basic_tests){
            logger.log(INFO,"Starting adaptation tests:\n-------------------------\n");
            //simulate_horizontal_scaling_adaptation("test_fragments_FaceDetector",1,1);
            //simulate_horizontal_scaling_adaptation("test_fragments_FaceDetector",5,3); //this should not be effective as the interval between the two adaptations is too short.
            simulate_horizontal_scaling_adaptation("tf_FaceDet",5,28);
            //simulate_horizontal_scaling_adaptation("test_fragments_VideoTranscoder",1,30);

            /*
            simulate_mca_device_exclusion_adaptation(new LinkedHashMap(){{
                put("test_fragments_VideoStreamer","asf342fd, sjkk349d, kqt8s1f1, i89iaaq5, kaf9734f");
                put("test_fragments_VideoTranscoder", "bladf423, nounae23, kage219a, kresc100, o122abol, gl3eb18d");
            }},7);
            */
            // the same event is repeated at t=31

            simulate_mca_device_exclusion_adaptation(new LinkedHashMap(){{
                put("tf_VideoStr","asf342fd, sjkk349d, kqt8s1f1, i89iaaq5, kaf9734f");
                put("tf_VideoTrc", "bladf423, nounae23, kage219a, kresc100, o122abol, gl3eb18d");
            }},31);

            simulate_instance_level_tosca_parsing(12);
            //simulate_instance_level_tosca_parsing(24);
        }
        FIRST_RUN = false; //subsequent calls to the ToscaGenerator main method will be aware of the reconfiguration;
        tosca_deployment_counter++;
    }


    /**
     * This method simulates instance TOSCA parsing, after a number of seconds elapse. Normally, TOSCA parsing should be triggered by incoming messages from the Control layer, signalling the end of deployment of the topology. This method sends a message supposedly originating from the Control Layer, to trigger the instance TOSCA parsing
     * @param delay_sec The required delay in seconds for the parsing of the instance level TOSCA
     */
    private static void simulate_instance_level_tosca_parsing(int delay_sec) {

        final String adaptation_topic = topic_to_receive_actual_deployment_state;
        JSONObject adaptation_message = new JSONObject();

        Logger.getAnonymousLogger().log(INFO,"Executing simulation of tosca parsing with a delay of "+delay_sec+" sec");

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (SIMULATE_NETWORK){
                    adaptation_message.put("application_instance_id",String.valueOf(tosca_deployment_counter));
                    String message = adaptation_message.toString();
                    amqp_subscriber.test_process_received_message(message,adaptation_topic);
                }else {
                    Logger.getAnonymousLogger().log(SEVERE,"Inside scheduled code");
                    JSONObject adaptation_message = new JSONObject();
                    adaptation_message.put("application_instance_id",String.valueOf(tosca_deployment_counter));
                    String message = adaptation_message.toString();
                    amqp_publisher.publish(adaptation_topic, message);
                }
                //InstanceToscaParser.parse();
            }
        },delay_sec*1000);
    }

    private static void simulate_horizontal_scaling_adaptation(String fragment_name, int adaptation_instances, int delay_sec) {
        final String adaptation_topic = adaptation_instances>0?topic_for_scale_out_adaptation:topic_for_scale_in_adaptation;
        JSONObject adaptation_fragment = new JSONObject();
        adaptation_fragment.put("fragment_name",fragment_name);
        adaptation_fragment.put("delta_instances",String.valueOf(Math.abs(adaptation_instances)));
        String message = adaptation_fragment.toString();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (SIMULATE_NETWORK){
                    amqp_subscriber.test_process_received_message(message,adaptation_topic);
                }else {
                    amqp_publisher.publish(topic_for_scale_out_adaptation, message);
                }
            }
        }, delay_sec*1000);
    }

    private static void simulate_mca_device_exclusion_adaptation(LinkedHashMap<String,String> fragments_excluded_devices, int delay_sec) {

        JSONArray json_array = new JSONArray();
        for (HashMap.Entry<String,String> fragment_excluded_devices : fragments_excluded_devices.entrySet()) {

            String fragment_name = fragment_excluded_devices.getKey();
            String[] excluded_devices = fragment_excluded_devices.getValue().split(",");

            JSONObject adaptation_fragment = new JSONObject();
            adaptation_fragment.put("fragment_name", fragment_name);

            JSONArray excluded_devices_json_array = new JSONArray();
            for (String excluded_device : excluded_devices){
                excluded_devices_json_array.add(excluded_device.trim());
            }

            adaptation_fragment.put("excluded_devices", excluded_devices_json_array);
            json_array.add(adaptation_fragment);
        }
        String message = json_array.toString();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (SIMULATE_NETWORK){
                    amqp_subscriber.test_process_received_message(message,topic_to_receive_excluded_devices);
                }else {
                    amqp_publisher.publish(topic_to_receive_excluded_devices, message);
                }
            }
        }, delay_sec*1000);
    }

    /**
     *This method schedules the execution of the TOSCA generator with the delay specified in the configuration file of the component (unless overriden using the array parameter)
     * @param override_time_interval The delay in seconds which will shoulde elapse before starting the TOSCA generator
     */
    public static void schedule_tosca_generator(int... override_time_interval){
        int delay_seconds;
        if(override_time_interval.length>0){
            delay_seconds=override_time_interval[0];
        }else{
            delay_seconds=adaptation_interval_seconds;
        }
        synchronized (tosca_generation_scheduled) {
            if (!tosca_generation_scheduled.getValue()) {
                Timer timer = new Timer();
                tosca_generation_scheduled.setTrue();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Logger.getAnonymousLogger().log(INFO, "Running new TOSCA generator");
                        //TODO below should follow main
                        tosca_generation_scheduled.setFalse();
                        main(new String[]{});
                    }
                }, delay_seconds * 1000);
            }
        }
    }


    public static FragmentationPolicy parsePolicyFile(String filename) {
        FragmentationPolicy fragmentationPolicy = new FragmentationPolicy();

        ArrayList<BusinessGoal> businessGoals = new ArrayList<>();
        BudgetRequirement budgetRequirement = new BudgetRequirement();
        ArrayList<CollocationRequirement> collocationRequirements = new ArrayList<>();
        MappingRequirement mappingRequirement = new MappingRequirement();
        ArrayList<ProviderRequirement> providerRequirements = new ArrayList<>();
        ArrayList<ScalabilityRequirement> scalabilityRequirements = new ArrayList<>();
        DeploymentRequirement deploymentRequirement = new DeploymentRequirement();

        int business_goals = 0;
        int budget_requirements = 0;
        int mapping_requirements = 0;
        int scalability_requirements = 0;
        int deployment_requirements = 0;


        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line = br.readLine();

            int expected_indentation = 0;

            while (line != null) {
                int spaces;

                if (line.replaceAll("\\s+", "").contains("BusinessGoal" + POLICYFILE_DELIMITER)) {
                    BusinessGoal businessGoal = getBusinessGoal(br, expected_indentation);
                    businessGoals.add(businessGoal);
                    business_goals++;
                } else if (line.replaceAll("\\s+", "").contains("BudgetRequirement" + POLICYFILE_DELIMITER)) {
                    budgetRequirement = getBudgetRequirement(br, expected_indentation);
                    budget_requirements++;
                //} else if (line.replaceAll("\\s+", "").contains("CollocationRequirement" + POLICYFILE_DELIMITER)) {
                  //  CollocationRequirement collocationRequirement = getCollocationRequirement(br, expected_indentation);
                  //  collocationRequirements.add(collocationRequirement);
                } else if (line.replaceAll("\\s+", "").contains("MappingRequirement" + POLICYFILE_DELIMITER)) {
                    mappingRequirement = getMappingRequirement(br, expected_indentation);
                    mapping_requirements++;
                } else if (line.replaceAll("\\s+", "").contains("ProviderRequirement" + POLICYFILE_DELIMITER)) {
                    ProviderRequirement providerRequirement = getProviderRequirement(br, expected_indentation);
                    providerRequirements.add(providerRequirement);
                } else if (line.replaceAll("\\s+", "").contains("DeploymentRequirement" + POLICYFILE_DELIMITER)) {
                    deploymentRequirement = getDeploymentRequirement(br, expected_indentation);
                    deployment_requirements++;
                } else if (line.replaceAll("\\s+", "").contains("ScalabilityRequirements" + POLICYFILE_DELIMITER)) {
                    scalabilityRequirements = getScalabilityRequirements(br,expected_indentation);
                    scalability_requirements++;
                }else {
                    logger.log(Level.WARNING, "Unknown requirement " + line + " requested");
                }

                line = br.readLine();
            }
            try{
                if (scalability_requirements>1){
                    throw new PolicyRedefinedException(scalability_requirements,"scalability_requirements");
                }
                if (business_goals==0){
                    throw new PolicyRequiredException("business goal");
                }
                if (budget_requirements>1){
                    throw new PolicyRedefinedException(budget_requirements,"budget requirements");
                }
                //provider requirements not tested as they are optional and multiple definitions can exist
                if (mapping_requirements>1){
                    throw new PolicyRedefinedException(mapping_requirements,"mapping requirements");
                }else if(mapping_requirements==0){
                    throw new PolicyRequiredException("mapping requirements");
                }
                if (deployment_requirements>1){
                    throw new PolicyRequiredException("deployment requirements");
                }

            }catch (PolicyRedefinedException p){
                p.printStackTrace();
            }catch (PolicyRequiredException p){
                p.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        fragmentationPolicy.setBusinessGoals(businessGoals);
        fragmentationPolicy.setBudgetRequirement(budgetRequirement);
        fragmentationPolicy.setScalabilityRequirements(scalabilityRequirements);
        fragmentationPolicy.setDeploymentRequirement(deploymentRequirement);
        fragmentationPolicy.setProviderRequirements(providerRequirements);
        fragmentationPolicy.setCollocationRequirements(collocationRequirements);
        fragmentationPolicy.setMappingRequirement(mappingRequirement);

        ProviderRequirement.zero_requirement_id_counter();
        //TODO decide on the number of instances of each requirement. If some/all should be present, an exception should be thrown here for the rest.

        return fragmentationPolicy;
    }

    private static DeploymentRequirement getDeploymentRequirement(BufferedReader br, int expected_indentation) throws IOException {
        DeploymentRequirement deploymentRequirement = null;
        boolean instantiated = false;
        String line = br.readLine();

        int max_instances;
        int max_fragment_instances;
        int max_master_instances;

        int spaces = getLeadingWhitespaces(line);
        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
        while (spaces == expected_indentation) {
            String detected_attribute = check_string_for_substrings(line,"MaxInstances"+ COLON_DELIMITER,"MaxFragmentInstances"+ COLON_DELIMITER,"MaxMasterNodeInstances"+ COLON_DELIMITER);
            if (detected_attribute!=null && !detected_attribute.equals("")) {
                if (!instantiated) {
                    deploymentRequirement = new DeploymentRequirement();
                    instantiated = true;
                }
                if (detected_attribute.equals("MaxInstances"+ COLON_DELIMITER)){
                    max_instances = Integer.parseInt(line.replace("MaxInstances" + POLICYFILE_DELIMITER, "").replaceAll("^\\s+|\\s+$", ""));
                    deploymentRequirement.setMax_instances(max_instances);
                }
                else if(detected_attribute.equals("MaxFragmentInstances"+ COLON_DELIMITER)){
                    max_fragment_instances = Integer.parseInt(line.replace("MaxFragmentInstances" + POLICYFILE_DELIMITER, "").replaceAll("^\\s+|\\s+$", ""));
                    deploymentRequirement.setMax_fragment_instances(max_fragment_instances);
                }
                else if(detected_attribute.equals("MaxMasterNodeInstances"+ COLON_DELIMITER)){
                    max_master_instances = Integer.parseInt(line.replace("MaxMasterNodeInstances" + POLICYFILE_DELIMITER, "").replaceAll("^\\s+|\\s+$", ""));
                    deploymentRequirement.setMax_master_instances(max_master_instances);
                }
                else {
                    logger.log(Level.SEVERE, "Incorrect attempt to create new Deployment requirement - wrong attribute. The acceptable attributes are MaxFragmentInstances and MaxInstances");
                }
            }
            br.mark(bufferReaderReadAheadLimit);
            line = br.readLine();
            spaces = getLeadingWhitespaces(line);
        }
        //System.out.println(providerRequirement);
        if (deploymentRequirement == null) {
            logger.log(Level.SEVERE, "No body found for deployment requirement");
        }
        br.reset();
        return deploymentRequirement;
    }


    private static ArrayList<ScalabilityRequirement> getScalabilityRequirements(BufferedReader br, int expected_indentation) throws IOException {

        //Scalability requirements will be name-value pairs that will include the name of the scalability policy and a value (e.g uri) which can be used to describe it.
        ArrayList<ScalabilityRequirement> scalability_requirements= new ArrayList<>();
        String line = br.readLine();

        int spaces = getLeadingWhitespaces(line);
        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
        while (spaces == expected_indentation) {
            String [] key_value = line.replaceAll("\\s+","").split(COLON_DELIMITER,2);
            String id = key_value[0];
            String scalability_action_uri=key_value[1];
            scalability_requirements.add(new ScalabilityRequirement(id,scalability_action_uri));
            br.mark(bufferReaderReadAheadLimit);
            line = br.readLine();
            spaces = getLeadingWhitespaces(line);
        }
        if (scalability_requirements == null) {
            logger.log(Level.SEVERE, "No body found for deployment requirement");
        }
        br.reset();
        return scalability_requirements;
    }



    private static String check_string_for_substrings(String string, String... substrings) {
        for (String substring : substrings) {
            boolean current_sub_string_included = string.replaceAll("\\s+", "").contains(substring);
            if (current_sub_string_included) {
                return substring;
            }
        }
        return "";
    }


    private static BudgetRequirement getBudgetRequirement(BufferedReader br, int expected_indentation) throws IOException {
        BudgetRequirement budgetRequirement = null;
        boolean instantiated = false;
        String line = br.readLine();

        Integer cost_threshold = null;
        Integer time_period = null;

        int spaces = getLeadingWhitespaces(line);
        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
        while (spaces == expected_indentation) {
            if (line.replaceAll("\\s+", "").contains("CostThreshold" + POLICYFILE_DELIMITER)) {
                cost_threshold = Integer.valueOf(line.replaceAll("\\s+", "").replace("CostThreshold" + POLICYFILE_DELIMITER, ""));
            } else if (line.replaceAll("\\s+", "").contains("TimePeriod" + POLICYFILE_DELIMITER)) {
                time_period = Integer.parseInt(line.replaceAll("\\s+", "").replace("TimePeriod" + POLICYFILE_DELIMITER, ""));
            }

            if ((cost_threshold != null) && (time_period != null)) {
                if (instantiated) {
                    logger.log(Level.SEVERE, "Incorrect attempt to create new Budget requirement. New requirements should be tagged separately");
                } else {
                    budgetRequirement = new BudgetRequirement(time_period, cost_threshold);
                    cost_threshold = null;
                    time_period = null;
                    instantiated = true;
                }
            }

            br.mark(bufferReaderReadAheadLimit);
            line = br.readLine();
            spaces = getLeadingWhitespaces(line);
        }
        if (budgetRequirement == null) {
            logger.log(Level.SEVERE, "No body found for budget requirement");
        }
        //System.out.println(providerRequirement);
        br.reset();
        return budgetRequirement;
    }

    private static ProviderRequirement getProviderRequirement(BufferedReader br, int expected_indentation) throws IOException {
        ProviderRequirement providerRequirement = new ProviderRequirement();
        boolean instantiated = false;
        String line = br.readLine();
        String provider_name = null;
        Boolean required = null, excluded = null;
        int spaces = getLeadingWhitespaces(line);
        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
        while (spaces == expected_indentation) {
            if (line.replaceAll("\\s+", "").contains("ProviderName" + POLICYFILE_DELIMITER)) {
                provider_name = line.replaceAll("\\s+", "").replace("ProviderName" + POLICYFILE_DELIMITER, "");
            } else if (line.replaceAll("\\s+", "").contains("Required" + POLICYFILE_DELIMITER)) {
                required = Boolean.valueOf(line.replaceAll("\\s+", "").replace("Required" + POLICYFILE_DELIMITER, ""));
            } else if (line.replaceAll("\\s+", "").contains("Excluded" + POLICYFILE_DELIMITER)) {
                excluded = Boolean.valueOf(line.replaceAll("\\s+", "").replace("Excluded" + POLICYFILE_DELIMITER, ""));
            }

            //

            if (instantiated) {
                logger.log(Level.SEVERE, "Incorrect attempt to create new Provider requirement. New requirements should be tagged separately");
            } else {
                if (provider_name != null && providerRequirement.getProviderName() == null) {
                    providerRequirement.setProviderName(provider_name);
                } //TODO Decide on whether an attempt to provide the provider name twice will provoke an exception.
                if (required != null && providerRequirement.isRequired() == null) {
                    providerRequirement.setRequired(required);
                }
                if (excluded != null && providerRequirement.isExcluded() == null) {
                    providerRequirement.setExcluded(excluded);
                }
                if ((provider_name != null) && (required != null) && (excluded != null)) {
                    instantiated = true;
                }
            }

            br.mark(bufferReaderReadAheadLimit);
            line = br.readLine();
            spaces = getLeadingWhitespaces(line);
        }
        //System.out.println(providerRequirement);
        if (providerRequirement.toString() == null) {
            logger.log(Level.SEVERE, "No body found for provider requirement");
        }
        br.reset();
        return providerRequirement;
    }

    private static CollocationRequirement getCollocationRequirement(BufferedReader br, int expected_indentation) throws IOException {
        CollocationRequirement collocationRequirement = new CollocationRequirement();
        boolean instantiated = false;
        String line = br.readLine();
        int spaces = getLeadingWhitespaces(line);
        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
        while (spaces == expected_indentation) {
            String affinityTo = null, antiAffinityTo = null;
            Boolean deployOnSameResource = null;
            if (line.replaceAll("\\s+", "").contains("AffinityTo" + POLICYFILE_DELIMITER)) {
                affinityTo = line.replaceAll("\\s+", "").replace("AffinityTo" + POLICYFILE_DELIMITER, "");
            } else if (line.replaceAll("\\s+", "").contains("AntiAffinityTo" + POLICYFILE_DELIMITER)) {
                antiAffinityTo = line.replaceAll("\\s+", "").replace("AntiAffinityTo" + POLICYFILE_DELIMITER, "");
            } else if (line.replaceAll("\\s+", "").contains("DeployOnSameResource" + POLICYFILE_DELIMITER)) {
                deployOnSameResource = Boolean.valueOf(line.replaceAll("\\s+", "").replace("DeployOnSameResource" + POLICYFILE_DELIMITER, ""));
            }


            if (instantiated) {
                logger.log(Level.SEVERE, "Incorrect attempt to create new Collocation requirement. New requirements should be tagged separately");

            } else {
                if (deployOnSameResource != null && collocationRequirement.getDeployOnSameResource() == null) {
                    collocationRequirement.setDeployOnSameResource(deployOnSameResource);
                }
                if (affinityTo != null && collocationRequirement.getAffinityTo() == null) {
                    collocationRequirement.setAffinityTo(affinityTo);
                }
                if (deployOnSameResource != null && collocationRequirement.getAntiAffinityTo() == null) {
                    collocationRequirement.setAntiAffinityTo(antiAffinityTo);
                }
                if ((affinityTo != null) && (antiAffinityTo != null) && (deployOnSameResource != null)) {
                    instantiated = true;
                }

            }

            br.mark(bufferReaderReadAheadLimit);
            line = br.readLine();
            spaces = getLeadingWhitespaces(line);
        }
        //System.out.println(collocationRequirement);
        if (collocationRequirement.toString().equals("")) {
            logger.log(Level.SEVERE, "No body found for collocation requirement");
        }
        br.reset();
        return collocationRequirement;
    }

    private static BusinessGoal getBusinessGoal(BufferedReader br, int expected_indentation) throws IOException {

        BusinessGoal businessGoal = null;
        boolean instantiated = false;
        String metric = "", metricToMinimize = "", metricToMaximize = "", threshold = "", operator = "";
        String line;
        try {
            line = br.readLine();
            int spaces = getLeadingWhitespaces(line);
            expected_indentation += POLICYFILE_INDENTATION_DEPTH;
            while (spaces == expected_indentation) {

                if (line.replaceAll("\\s+", "").contains("MetricToMinimize" + POLICYFILE_DELIMITER)) {
                    if (!attributeRedefinition(metricToMinimize, "MetricToMinimize")) {
                        metricToMinimize = line.replaceAll("\\s+", "").replace("MetricToMinimize" + POLICYFILE_DELIMITER, "");
                    }
                } else if (line.replaceAll("\\s+", "").contains("MetricToMaximize" + POLICYFILE_DELIMITER)) {
                    if (!attributeRedefinition(metricToMaximize, "MetricToMaximize")) {
                        metricToMaximize = line.replaceAll("\\s+", "").replace("MetricToMaximize" + POLICYFILE_DELIMITER, "");
                    }
                } else if (line.replaceAll("\\s+", "").contains("Metric" + POLICYFILE_DELIMITER)) {
                    if (!attributeRedefinition(metric, "Metric")) {
                        metric = line.replaceAll("\\s+", "").replace("Metric" + POLICYFILE_DELIMITER, "");
                    }
                } else if (line.replaceAll("\\s+", "").contains("Threshold" + POLICYFILE_DELIMITER)) {
                    if (!attributeRedefinition(threshold, "Threshold")) {
                        threshold = line.replaceAll("\\s+", "").replace("Threshold" + POLICYFILE_DELIMITER, "");
                    }
                } else if (line.replaceAll("\\s+", "").contains("Operator" + POLICYFILE_DELIMITER)) {
                    if (!attributeRedefinition(metricToMinimize, "Operator")) {
                        operator = line.replaceAll("\\s+", "").replace("Operator" + POLICYFILE_DELIMITER, "");
                    }
                }

                if (checkBusinessGoalConstructor(metricToMaximize)) {
                    if (instantiated) {
                        logger.log(Level.SEVERE, "Incorrect attempt to create new Business Goal. New Business Goals should be tagged separately");
                    } else {
                        businessGoal = new BusinessGoal(metricToMaximize, "maximize");
                        instantiated = true;
                    }
                } else if (checkBusinessGoalConstructor(metricToMinimize)) {
                    if (instantiated) {
                        logger.log(Level.SEVERE, "Incorrect attempt to create new Business Goal. New Business Goals should be tagged separately");
                    } else {
                        businessGoal = new BusinessGoal(metricToMinimize, "minimize");
                        instantiated = true;
                    }
                } else if (checkBusinessGoalConstructor(metric, threshold, operator)) {
                    if (instantiated) {
                        logger.log(Level.SEVERE, "Incorrect attempt to create new Business Goal. New Business Goals should be tagged separately");
                    } else {
                        businessGoal = new BusinessGoal(metric, threshold, operator);
                        instantiated = true;
                    }
                }
                br.mark(bufferReaderReadAheadLimit);
                line = br.readLine();
                spaces = getLeadingWhitespaces(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (businessGoal == null) {
            logger.log(Level.SEVERE, "No body found for Business Goal");
        }
        br.reset();
        return businessGoal;
    }

    private static MappingRequirement getMappingRequirement(BufferedReader br, int expected_indentation) throws Exception {
        MappingRequirement mappingRequirement = null;
        boolean br_mark_set = false;
        int spaces;
        String line;
        //MappingRequirement loadTranslators = new ArrayList<>();

        try {
            AttributeMapping cpu_load_translator=null,memory_load_translator=null,storage_load_translator=null;
            do {
                expected_indentation += Formatting.POLICYFILE_INDENTATION_DEPTH;
                line = br.readLine();
                spaces = getLeadingWhitespaces(line); //TODO: perhaps an Exception should be thrown in case that an error occurs here, during parsing.
                while (spaces == expected_indentation) { //if a parameter is specified twice below, last value will override the first.
                    if (line.replaceAll("\\s+", "").contains("CPU" + POLICYFILE_DELIMITER)) {
                        expected_indentation += Formatting.POLICYFILE_INDENTATION_DEPTH;
                        line = br.readLine();
                        spaces = getLeadingWhitespaces(line);
                        if (spaces != expected_indentation) {
                            logger.log(Level.SEVERE, "Wrong indentation of the CPU mappings");
                            throw new Exception("Wrong Indentation of CPU mappings");
                        }
                        int very_low=0,low=0,medium=0,high=0,very_high=0;
                        while (spaces == expected_indentation) {

                            if (line.replaceAll("\\s+", "").contains("VERY_LOW" + POLICYFILE_DELIMITER)) {
                                very_low = (getIntegerValueOfTag(line, "VERY_LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("LOW" + POLICYFILE_DELIMITER)) {
                                low = (getIntegerValueOfTag(line, "LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("MEDIUM" + POLICYFILE_DELIMITER)) {
                                medium = (getIntegerValueOfTag(line, "MEDIUM"));
                            } else if (line.replaceAll("\\s+", "").contains("VERY_HIGH" + POLICYFILE_DELIMITER)) {
                                very_high = (getIntegerValueOfTag(line, "VERY_HIGH"));
                            } else if (line.replaceAll("\\s+", "").contains("HIGH" + POLICYFILE_DELIMITER)) {
                                high = (getIntegerValueOfTag(line, "HIGH"));
                            } else {
                                logger.log(Level.SEVERE, "incorrect parameter" + line.trim());
                            }
                            br.mark(bufferReaderReadAheadLimit);
                            br_mark_set = true;
                            line = br.readLine();
                            spaces = getLeadingWhitespaces(line);
                        }
                        try {
                            if (very_low<0 || low<0 || medium<0 || high<0 || very_high<0) {
                              throw new Exception("Values intended to create a mapping in the policy file are unacceptable, as a linguistic_variable had a value below 0");
                            }else{
                                cpu_load_translator = new AttributeMapping(very_low,low,medium,high,very_high);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        //Logger.getAnonymousLogger().log(INFO,"CPU"+POLICYFILE_DELIMITER+cpu_load_translator);

                        expected_indentation -= POLICYFILE_INDENTATION_DEPTH;
                    } else if (line.replaceAll("\\s+", "").contains("RAM" + POLICYFILE_DELIMITER)) {
                        expected_indentation += POLICYFILE_INDENTATION_DEPTH;

                        line = br.readLine();
                        spaces = getLeadingWhitespaces(line);
                        if (spaces != expected_indentation) {
                            logger.log(Level.SEVERE, "Wrong indentation of the RAM mappings");
                            throw new Exception("Wrong Indentation of RAM mappings");
                        }
                        int very_low=0,low=0,medium=0,high=0,very_high=0;
                        while (spaces == expected_indentation) {
                            if (line.replaceAll("\\s+", "").contains("VERY_LOW" + POLICYFILE_DELIMITER)) {
                                very_low = (getIntegerValueOfTag(line, "VERY_LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("LOW" + POLICYFILE_DELIMITER)) {
                                low = (getIntegerValueOfTag(line, "LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("MEDIUM" + POLICYFILE_DELIMITER)) {
                                medium =(getIntegerValueOfTag(line, "MEDIUM"));
                            } else if (line.replaceAll("\\s+", "").contains("VERY_HIGH" + POLICYFILE_DELIMITER)) {
                                very_high = (getIntegerValueOfTag(line, "VERY_HIGH"));
                            } else if (line.replaceAll("\\s+", "").contains("HIGH" + POLICYFILE_DELIMITER)) {
                                high = (getIntegerValueOfTag(line, "HIGH"));
                            } else {
                                logger.log(Level.SEVERE, "incorrect parameter" + line.trim());
                                break;
                            }
                            br.mark(bufferReaderReadAheadLimit);
                            br_mark_set = true;
                            line = br.readLine();
                            spaces = getLeadingWhitespaces(line);
                        }
                        try {
                            if (very_low<0 || low<0 || medium<0 || high<0 || very_high<0) {
                                throw new Exception("Values intended to create a mapping in the policy file are unacceptable, as a linguistic_variable had a value below 0");
                            }else{
                                memory_load_translator = new AttributeMapping(very_low,low,medium,high,very_high);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        //Logger.getAnonymousLogger().log(INFO,"RAM"+memory_load_translator);

                        expected_indentation -= POLICYFILE_INDENTATION_DEPTH;
                    } else if (line.replaceAll("\\s+", "").contains("DISK" + POLICYFILE_DELIMITER)) {
                        expected_indentation += POLICYFILE_INDENTATION_DEPTH;
                        line = br.readLine();
                        spaces = getLeadingWhitespaces(line);
                        if (spaces != expected_indentation) {
                            logger.log(Level.SEVERE, "Wrong indentation of the Disk mappings");
                            throw new Exception("Wrong Indentation of Disk mappings");
                        }
                        int very_low=0,low=0,medium=0,high=0,very_high=0;
                        while (spaces == expected_indentation) {
                            if (line.replaceAll("\\s+", "").contains("VERY_LOW" + POLICYFILE_DELIMITER)) {
                                very_low = (getIntegerValueOfTag(line, "VERY_LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("LOW" + POLICYFILE_DELIMITER)) {
                                low = (getIntegerValueOfTag(line, "LOW"));
                            } else if (line.replaceAll("\\s+", "").contains("MEDIUM" + POLICYFILE_DELIMITER)) {
                                medium = (getIntegerValueOfTag(line, "MEDIUM"));
                            } else if (line.replaceAll("\\s+", "").contains("VERY_HIGH" + POLICYFILE_DELIMITER)) {
                                very_high = (getIntegerValueOfTag(line, "VERY_HIGH"));
                            } else if (line.replaceAll("\\s+", "").contains("HIGH" + POLICYFILE_DELIMITER)) {
                                high = (getIntegerValueOfTag(line, "HIGH"));
                            } else {
                                logger.log(Level.SEVERE, "incorrect parameter" + line.trim());
                            }
                            br.mark(bufferReaderReadAheadLimit);
                            br_mark_set = true;
                            line = br.readLine();
                            spaces = getLeadingWhitespaces(line);
                        }
                        try {
                            if (very_low<0 || low<0 || medium<0 || high<0 || very_high<0) {
                                throw new Exception("Values intended to create a mapping in the policy file are unacceptable, as a linguistic_variable had a value below 0");
                            }else{
                                storage_load_translator = new AttributeMapping(very_low,low,medium,high,very_high);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        //Logger.getAnonymousLogger().log(INFO,"DISK"+storage_load_translator);

                        expected_indentation -= POLICYFILE_INDENTATION_DEPTH;
                    } else {
                        logger.log(Level.SEVERE, "Erroneous annotation - only CPU,RAM and Disk attributes are supported");
                        break;
                    }
                }
            } while (spaces > 0);
            if (br_mark_set) {
                br.reset();
            }
            if (cpu_load_translator!=null && memory_load_translator !=null && storage_load_translator!=null) {
                if (!validate_mappings(cpu_load_translator)) {
                    logger.log(Level.SEVERE, "Wrong characterization of cpu attributes. One of the attributes tagged with lower requirements has higher values than the others");
                }
                if (!validate_mappings(memory_load_translator)) {
                    logger.log(Level.SEVERE, "Wrong characterization of memory attributes. One of the attributes tagged with lower requirements has higher values than the others");
                }
                if (!validate_mappings(storage_load_translator)) {
                    logger.log(Level.SEVERE, "Wrong characterization of storage attributes. One of the attributes tagged with lower requirements has higher values than the others");
                }
                mappingRequirement = new MappingRequirement(cpu_load_translator,memory_load_translator,storage_load_translator);
            }
            return mappingRequirement;

        } catch (IOException e) {
            e.printStackTrace();
            return mappingRequirement;
        }
    }


    private static boolean validate_mappings(AttributeMapping attributeMapping) {
        LinkedList<Integer> sorted_list, list_to_validate = new LinkedList<>();
        list_to_validate.add(attributeMapping.getHIGH());
        list_to_validate.add(attributeMapping.getLOW());
        list_to_validate.add(attributeMapping.getMEDIUM());
        list_to_validate.add(attributeMapping.getVERY_HIGH());
        list_to_validate.add(attributeMapping.getVERY_LOW());
        Collections.sort(list_to_validate);

        return (list_to_validate.get(4) == attributeMapping.getVERY_HIGH()) && (list_to_validate.get(3) == attributeMapping.getHIGH()) && (list_to_validate.get(2) == attributeMapping.getMEDIUM()) && (list_to_validate.get(1) == attributeMapping.getLOW()) && (list_to_validate.get(0) == attributeMapping.getVERY_LOW());
    }

    private static boolean attributeRedefinition(String parsedAttribute, String attributeName) throws Exception {
        Logger logger = Logger.getLogger(ToscaGenerator.class.getName());
        if (parsedAttribute.length() != 0) {
            logger.log(Level.SEVERE, "The " + attributeName + " parameter has been defined more than once");
            throw new Exception(exceptionDescribedInLog);
        } else {
            return false;
        }
    }

    private static boolean checkBusinessGoalConstructor(String metricToOptimize) {
        return (metricToOptimize != null) && (!Objects.equals(metricToOptimize, ""));
    }

    private static boolean checkBusinessGoalConstructor(String metric, String threshold, String operator) {
        return (metric != null) && (threshold != null) && (operator != null) && (!Objects.equals(metric, "")) && (!Objects.equals(threshold, "")) && (!Objects.equals(operator, ""));
    }


    private static int getIntegerValueOfTag(String line, String tag) {
        int result;
        Logger logger = Logger.getLogger(ToscaGenerator.class.getName());
        try {
            String processed_string = line.replaceAll("\\s+", "").replace(tag, "").replace(POLICYFILE_DELIMITER, "").trim();
            //logger.log(Level.INFO,"processed string is "+processed_string);
            result = Integer.valueOf(processed_string);
        } catch (NumberFormatException n) {
            logger.log(Level.SEVERE, "Exception at string: " + line);
            result = -1;
        }
        return result;
    }

    private static int getLeadingWhitespaces(String line) {
        if (line != null && line.length() > 0) {
            int spaceCount = 0;
            for (char c : line.toCharArray()) {
                if (c == ' ') {
                    spaceCount++;
                } else {
                    break;
                }
            }
            return spaceCount;
        } else {
            return -1;
        }
    }

    public static void initialize_component(){
        fragments_map.clear();
        initialize_dependency_data_structures();
        BidirectionalCollocationDependencies.cleanDependencies();
        initialize_coordinator_nodes();
        FIRST_RUN = true;
        tosca_deployment_counter=1;
        INSTANCE_TOSCA_TOPOLOGY_PARSED.setTrue();
    }

}