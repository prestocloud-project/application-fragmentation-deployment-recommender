package eu.prestocloud.messaging;


import consumer.AmqpConsumer;
import eu.prestocloud.tosca_generator.utility_classes.fragments.Fragment;
import eu.prestocloud.tosca_parser.InstanceToscaParser;
import eu.prestocloud.utilities.UIJsonParser;
import exceptions.AlreadySubscribedException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.database.DatabaseUtils.update_tosca_deployments_db_table;
import static eu.prestocloud.tosca_generator.NodeGenerator.ui_deployment_ready;
import static eu.prestocloud.tosca_generator.ToscaGenerator.*;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;
import static eu.prestocloud.tosca_generator.utility_classes.fragments.FragmentDataStructures.fragments_map;
import static eu.prestocloud.utilities.UIJsonParser.get_processed_fragment_name;
import static java.util.logging.Level.*;

public  class AMQP_Subscriber {

    public java.util.function.BiConsumer<String, String> consumerFunction = this::process_received_message;

    public void subscribe(){
        for (String adaptation_topic : adaptation_topics) {
            if (adaptation_topic!=null && !adaptation_topic.equals(EMPTY)) {
                AmqpConsumer consumer = new AmqpConsumer(broker_IP_address,false, adaptation_topic, consumerFunction);
                consumer.setUsernameAndPassword(broker_username,broker_password);
                if (enforce_ssl) {
                    //consumer.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
                    consumer.useSSL(vault_address,vault_token);
                }
                try {
                    consumer.subscribe();
                } catch (AlreadySubscribedException e) {
                    System.out.println(e.getMessage());
                } catch (Exception e) {//catch (SubscribingFailedException e) {
                    waitXminAndRetry(1);
                    return;
                }
            }
        }
    }

    /**process_received_message is the method that gets executed on every received message
     *
     * @param message The message which has been received
     * @param topic The topic of the message received
     */
    private void process_received_message(String message, String topic) {

        Logger.getAnonymousLogger().log(INFO, "MESSAGE_RECEIVED");

        //message = message.replaceAll("application_classes","annotated_application_test_fragments");

        int adaptation_instances = 0;
        String fragment_name = "";
        Fragment fragment_for_adaptation = null;
        JSONObject json_message = null;

        //Wait for the acknowledgement of the Control Layer before processing the received message
        //The only case in which this check can be bypassed, is when a request for a new application deployment arrives, or when a new acknowledgement message is sent, or when an undeployment is requested
        synchronized (INSTANCE_TOSCA_TOPOLOGY_PARSED) {

            if (!INSTANCE_TOSCA_TOPOLOGY_PARSED.getValue()     &&
                !topic.equals(topic_to_receive_deployment_json) &&
                !topic.equals(topic_to_receive_actual_deployment_state) &&
                !topic.equals(topic_for_application_undeployment)
            ) {
                try {
                    INSTANCE_TOSCA_TOPOLOGY_PARSED.wait();
                } catch (InterruptedException i) {
                    Logger.getAnonymousLogger().log(SEVERE, "The thread expecting a notification from the instance level TOSCA parser was interrupted from waiting. The value of INSTANCE_LEVEL_TOSCA_TOPOLOGY_PARSED was " + INSTANCE_TOSCA_TOPOLOGY_PARSED + " and the stacktrace follows");
                    i.printStackTrace();
                }
            }

        }


        //Receiving message with the deployed topology by the Control layer
        if (topic.equals(topic_to_receive_deployment_json)) {

            Fragment.setFragment_node_counter(0); //reinitialize the id's which will be given to type-level TOSCA components
            if(tosca_deployment_counter>1) {
                schedule_tosca_generator();
                initialize_component(); //initialization of the application topology
            }
            try {
                json_message = parseJSONObject(message);
            }catch (Exception e){
                Logger.getAnonymousLogger().log(SEVERE,"Could not parse a JSON file which was sent from the UI. The contents of the JSON file follow:\n"+message);
            }
            synchronized (ui_deployment_ready) {
                UIJsonParser.setUi_deployment_json(json_message);
                ui_deployment_ready.setTrue();
                ui_deployment_ready.notifyAll();
            }



        }
        else if (topic.equals(topic_for_application_undeployment)){
            schedule_tosca_generator(0);
            initialize_component();//initializes the diafdrecom
        }
        else if (topic.equals(topic_to_receive_actual_deployment_state)) {

            String application_state = EMPTY;
            String instance_level_tosca_path = EMPTY;
            int deployed_tosca_id = tosca_deployment_counter-1;
            boolean bypass_instance_level_tosca;

            try {
                json_message = parseJSONObject(message);
                if (json_message.get("status")!=null) {
                    application_state = (String) json_message.get("status");
                    bypass_instance_level_tosca = false;
                }else{
                    application_state = "DEPLOYED";
                    bypass_instance_level_tosca = true;
                }
            }catch (Exception e){
                Logger.getAnonymousLogger().log(SEVERE,"Could not parse the deployment acknowledgement json object with the following contents, due to json parsing problems\n"+message);
                e.printStackTrace();
                return;
            }
            //TODO The below statement needs verification, to ensure that the particular field will be used:
            try{
                if (!bypass_instance_level_tosca) {
                    deployed_tosca_id = Integer.parseInt((String) json_message.get("application_instance_id"));
                }
            } catch (Exception e){
                Logger.getAnonymousLogger().log(SEVERE,"Could not parse the deployment acknowledgement json object with the following contents, due to the failure of parsing a proper application_instance_id field\n"+message);
                return;
            }

            if (deployed_tosca_id > (tosca_deployment_counter-1)) {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    Logger.getAnonymousLogger().log(SEVERE, "An attempt was made by the Control Layer to deploy TOSCA file " + deployed_tosca_id + " which was not submitted by the Meta-Management Layer, as the current tosca deployment counter of the Meta-Management Layer is " + tosca_deployment_counter);
                    e.printStackTrace();
                }
            } else {
                if (deployed_tosca_id < (tosca_deployment_counter-1)) {
                    Logger.getAnonymousLogger().log(WARNING, "The Control Layer has just deployed TOSCA file " + deployed_tosca_id + " whilst the Meta-management Layer has produced TOSCA file " + (tosca_deployment_counter-1));
                }
                if (application_state.equalsIgnoreCase(TYPE_LEVEL_TOSCA_DEPLOYED)) {
                    update_tosca_deployments_db_table(deployed_tosca_id, TYPE_LEVEL_TOSCA_DEPLOYED);
                }
            }
            if (application_state.equalsIgnoreCase(TYPE_LEVEL_TOSCA_DEPLOYED)) {
                if (!bypass_instance_level_tosca) {
                    instance_level_tosca_path = (String) json_message.get("tosca_file");
                }
                synchronized (INSTANCE_TOSCA_TOPOLOGY_PARSED) {
                    INSTANCE_TOSCA_TOPOLOGY_PARSED.setFalse();
                }
                Logger.getAnonymousLogger().log(INFO, "Initializing instance-tosca parsing");
                InstanceToscaParser.parse(instance_level_tosca_path); //Older version: SimpleParser.parse();
            }else if (application_state.equalsIgnoreCase(ERROR_OCCURRED)){
                Logger.getAnonymousLogger().log(WARNING, "An error has occurred while trying to deploy the application, from the side of the Orchestrator (e.g due to the inability to satisfy the entered cost and time period constraints). A resubmission of the application (if using json input) or a restart of the component will be needed, in the case that the constraints should be changed. The integer tosca document identifier (application_instance_id) will be re-initialized to 1");
                tosca_deployment_counter = 1 ;
            }


        }
        else if (topic.equals(topic_to_receive_excluded_devices)) {


            try {
                JSONArray json_array_message = parseJSONArray(message);
                for (int i = 0; i < json_array_message.size(); i++) {

                    ArrayList<String> excluded_devices = new ArrayList<>();
                    JSONObject json_fragment_object = (JSONObject) json_array_message.get(i);
                    JSONArray excluded_devices_json_array = (JSONArray) json_fragment_object.get("excluded_devices");

                    for (int j = 0; j < excluded_devices_json_array.size(); j++) {
                        excluded_devices.add(excluded_devices_json_array.get(j).toString().trim());
                    }
                    fragment_name = (String) json_fragment_object.get("fragment_name");
                    fragment_name = get_processed_fragment_name(fragment_name);
                    fragment_for_adaptation = fragments_map.get(fragment_name);
                    if (fragment_for_adaptation != null) {
                        synchronized (fragment_for_adaptation) {
                            fragment_for_adaptation.setExcluded_edge_device_identifiers(excluded_devices);
                        }
                    } else {
                        Logger.getAnonymousLogger().log(WARNING, "Excluded devices were described for fragment " + fragment_for_adaptation + " which is not described in the topology");
                    }
                }


            } catch (Exception e) {
                if (fragment_for_adaptation == null) {
                    Logger.getAnonymousLogger().log(Level.SEVERE, "The adaptation message " + message + " concerning fragment " + fragment_name + " cannot be processed");
                    Logger.getAnonymousLogger().log(SEVERE, "The fragments recognized by the DIAFDRecom are the following:");
                    for (Map.Entry<String, Fragment> entry : fragments_map.entrySet()) {
                        Logger.getAnonymousLogger().log(SEVERE, entry.getValue().getProcessed_fragment_name());
                    }
                    return;
                }
                Logger.getAnonymousLogger().log(Level.WARNING, "Could not process message " + message + " on topic " + topic);
                e.printStackTrace();
                return;
            }
            schedule_tosca_generator();


        }
        else if (topic.equals(topic_for_scale_out_adaptation) || topic.equals(topic_for_scale_in_adaptation)) {

            synchronized (tosca_generation_scheduled) {
                try {
                    json_message = parseJSONObject(message);
                    adaptation_instances = topic.equals(topic_for_scale_out_adaptation) ? ((Long) json_message.get("delta_instances")).intValue() : -(((Long) json_message.get("delta_instances")).intValue());
                } catch (Exception e) {
                    Logger.getAnonymousLogger().log(Level.WARNING, "Could not process message " + message + " on topic " + topic);
                    return;
                }

                //Only proceed to an adaptation of the topology if there will be a modification of the number of instances
                if (Math.abs(adaptation_instances)>0) {

                    fragment_name = (String) json_message.get("fragment_name");
                    if (fragment_name != null) {
                        fragment_name = get_processed_fragment_name(fragment_name);
                        fragment_for_adaptation = fragments_map.get(fragment_name);
                        if (fragment_for_adaptation!=null) {
                            synchronized (fragment_for_adaptation) {

                                int new_fragment_lower_instances = fragment_for_adaptation.getCurrent_instances() + adaptation_instances;
                                //Only allow fragment adaptation if no previous adaptation is pending
                                if (fragment_for_adaptation.isAdaptation_pending()) {

                                } else {

                                    fragment_for_adaptation.setCurrent_instances(new_fragment_lower_instances);
                                    fragment_for_adaptation.setAdaptation_pending(true);
                                }
                            }
                            request_excluded_devices();
                        }else{
                            Logger.getAnonymousLogger().log(INFO, "An adaptation message "+message+" \n arrived for fragment " + fragment_name + " but no information on the processed fragment name which is "+fragment_for_adaptation+" can be found - the message is ignored");
                        }
                    } else {
                        Logger.getAnonymousLogger().log(INFO, "An adaptation message "+message+" \n arrived for fragment " + fragment_name + " but no information on this fragment is currently available - the message is ignored");
                    }
                }
            }
        }
    }

    private JSONObject parseJSONObject(String message) {
        JSONObject json_object = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            json_object = (JSONObject) parser.parse(message);
        }catch (Exception e){
            e.printStackTrace();
        }
        return json_object;
    }

    private JSONArray parseJSONArray(String message) {
        JSONArray json_array = new JSONArray();
        JSONParser parser = new JSONParser();
        try {
            json_array = (JSONArray) parser.parse(message);
        }catch (Exception e){
            e.printStackTrace();
        }
        return json_array;
    }

    private void request_excluded_devices() {
        JSONArray request_devices = new JSONArray();
        request_devices.add("requesting_excluded_devices");
        AMQP_Publisher amqp_publisher = new AMQP_Publisher();
        if (!SIMULATE_NETWORK) {
            amqp_publisher.publish(topic_to_request_excluded_devices, request_devices.toString());
            Logger.getAnonymousLogger().log(WARNING,"Requested excluded devices from the Mobile Context Analyzer");
        }else{
            Logger.getAnonymousLogger().log(WARNING,"Simulated request for excluded devices from the Mobile Context Analyzer");
        }

    }

    private void waitXminAndRetry(int minutes){
        try{
            Thread.sleep(minutes*60*1000);
            subscribe();
        }catch (Exception i){
            waitXminAndRetry(1);
        }
    }

    public void test_process_received_message(String message, String topic){

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                process_received_message(message,topic);
            }
        });
        t1.start();
    }

}
