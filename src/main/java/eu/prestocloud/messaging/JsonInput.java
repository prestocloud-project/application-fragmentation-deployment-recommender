package eu.prestocloud.messaging;

import eu.prestocloud.utilities.UIJsonParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static eu.prestocloud.tosca_generator.NodeGenerator.ui_deployment_ready;

public class JsonInput {
    public static void retrieve_input(){


        JFrame f = new JFrame("JSON contents input");
        JTextArea json_message_input = new JTextArea(20, 20);
        JScrollPane scrollableTextArea = new JScrollPane(json_message_input);

        scrollableTextArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        JButton b=new JButton("Submit JSON");
        b.setBounds(100,300,120,30);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                synchronized (ui_deployment_ready) {
                    String json_message = json_message_input.getText();
                    try {
                        UIJsonParser.setUi_deployment_json((JSONObject) new JSONParser().parse(json_message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ui_deployment_ready.setTrue();
                    ui_deployment_ready.notifyAll();
                }
                f.dispose();
            }
        });

        f.setLayout(new FlowLayout());
        f.getContentPane().add(scrollableTextArea);
        f.getContentPane().add(b);
        f.setSize(450,450);
        f.setVisible(true);

    }
}
