package eu.prestocloud.messaging;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import static eu.prestocloud.configuration.Constants.*;
import static java.util.logging.Level.INFO;

public class RestCommunicator {
    public static void uploadSSHkey(String key_name, String key_payload){

        HttpClient httpclient = HttpClients.createDefault();
        //1. Acquire a session ID from the Workflows & scheduling suite

        String endpoint_to_acquire_sessionid = "http://"+key_storage_endpoint+"/rest/scheduler/login/";
        String session_id = "";
        try {
            HttpResponse session_id_response;
            HttpPost post_for_sessionid = new HttpPost(endpoint_to_acquire_sessionid);

            HttpEntity session_id_entity = new StringEntity("username="+ organization_username +"&password="+key_storage_password, ContentType.APPLICATION_FORM_URLENCODED);
            post_for_sessionid.setEntity(session_id_entity);

            if(!SIMULATE_NETWORK) {
                session_id_response = httpclient.execute(post_for_sessionid);
                InputStream response_stream = session_id_response.getEntity().getContent();
                StringWriter session_id_writer = new StringWriter();
                IOUtils.copy(response_stream, session_id_writer);
                session_id = session_id_writer.toString();
            }

            //Logger.getAnonymousLogger().log(INFO,"Session id has been retrieved! sessionId="+session_id);

        }catch (Exception e){
            e.printStackTrace();
            Logger.getAnonymousLogger().log(Level.SEVERE,"The SSH key "+key_payload+" could not be uploaded as a sessionId cannot be retrieved. Perhaps the provided credentials are not correct?");
        }finally {

            //2. Upload the SSH key
            String endpoint_to_upload_ssh_key = "http://" + key_storage_endpoint + "/rest/scheduler/credentials/" + key_name;
            HttpPost post_to_upload_key = new HttpPost(endpoint_to_upload_ssh_key);
            HttpResponse ssh_key_upload_response;
            try {

                post_to_upload_key.setEntity(new StringEntity("value="+key_payload, ContentType.APPLICATION_FORM_URLENCODED));
                post_to_upload_key.addHeader("sessionId",session_id);
                if(!SIMULATE_NETWORK) {
                    ssh_key_upload_response = httpclient.execute(post_to_upload_key);
                    Logger.getAnonymousLogger().log(INFO,"SSH key "+key_name+" has been uploaded! The response from the server is: "+ssh_key_upload_response.toString());
                }else {
                    Logger.getAnonymousLogger().log(INFO,"Simulation of SSH key "+key_name+" upload is complete.");
                }


            } catch (Exception e) {
                e.printStackTrace();
                Logger.getAnonymousLogger().log(Level.SEVERE,"The SSH key "+key_payload+" could not be uploaded. Can the IP address of the particular machine communicate with the secure key storage?");
            }
        }

    }
    public static void uploadDockerRegistryUsernamePassword (String docker_registry_name, String username,String password){

        String registry_username_endpoint = "prestocloud_"+docker_registry_name+"_username";
        String registry_password_endpoint = "prestocloud_"+docker_registry_name+"_password";

        HttpClient httpclient = HttpClients.createDefault();
        //1. Acquire a session ID from the Workflows & scheduling suite

        String endpoint_to_acquire_sessionid = "http://"+key_storage_endpoint+"/rest/scheduler/login/";
        String session_id = "";
        try {
            HttpResponse session_id_response;
            HttpPost post_for_sessionid = new HttpPost(endpoint_to_acquire_sessionid);

            HttpEntity session_id_entity = new StringEntity("username="+ organization_username +"&password="+key_storage_password, ContentType.APPLICATION_FORM_URLENCODED);
            post_for_sessionid.setEntity(session_id_entity);

            session_id_response = httpclient.execute(post_for_sessionid);
            InputStream response_stream = session_id_response.getEntity().getContent();
            StringWriter session_id_writer = new StringWriter();
            IOUtils.copy(response_stream,session_id_writer);
            session_id = session_id_writer.toString();

            //Logger.getAnonymousLogger().log(INFO,"Session id has been retrieved! sessionId="+session_id);

        }catch (Exception e){
            e.printStackTrace();
            Logger.getAnonymousLogger().log(Level.SEVERE,"The username and password details for registry "+docker_registry_name+" could not be uploaded as a sessionId cannot be retrieved. Perhaps the provided credentials are not correct?");
        }finally {

            //2. Upload the SSH username and password
            String complete_endpoint_of_registry_username = "http://" + key_storage_endpoint + "/rest/scheduler/credentials/" + registry_username_endpoint;
            String complete_endpoint_of_registry_password = "http://" + key_storage_endpoint + "/rest/scheduler/credentials/" + registry_password_endpoint;
            HttpPost post_to_upload_username = new HttpPost(complete_endpoint_of_registry_username);
            HttpPost post_to_upload_password = new HttpPost(complete_endpoint_of_registry_password);
            HttpResponse post_response;
            try {

                post_to_upload_username.setEntity(new StringEntity("value="+username, ContentType.APPLICATION_FORM_URLENCODED));
                post_to_upload_username.addHeader("sessionId",session_id);
                post_response = httpclient.execute(post_to_upload_username);

                Logger.getAnonymousLogger().log(INFO,"Information on username of docker registry "+docker_registry_name+" has been uploaded! The response from the server is: "+post_response.toString());

                post_to_upload_password.setEntity(new StringEntity("value="+password, ContentType.APPLICATION_FORM_URLENCODED));
                post_to_upload_password.addHeader("sessionId",session_id);
                post_response = httpclient.execute(post_to_upload_password);

                Logger.getAnonymousLogger().log(INFO,"Information on password of docker registry "+docker_registry_name+" has been uploaded! The response from the server is: "+post_response.toString());


            } catch (Exception e) {
                e.printStackTrace();
                Logger.getAnonymousLogger().log(Level.SEVERE,"Username or password information for docker registry "+docker_registry_name+" could not be uploaded. Can the IP address of the particular machine communicate with the secure key storage?");
            }
        }

    }
}
