package eu.prestocloud.messaging;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;

import static eu.prestocloud.configuration.Constants.base_generated_tosca_path;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RepositoryCommunicationTest {
    private static String REPOSITORY_STORAGE_PATH = base_generated_tosca_path;
    private static String INSTANCE_LEVEL_TOSCA_NAME = "tosca_deployment_1_instance_test.yaml";
    private static String INSTANCE_LEVEL_TOSCA_DUMMY_CONTENT = "Test file content";
    @BeforeAll

    static void initializeExternalResources() {
        System.out.println("Initializing...");
        REPOSITORY_STORAGE_PATH = "C:\\Users\\user\\Desktop\\PrEstoCloud\\Latest_TOSCA_and_jar_export\\";
        try{
            File type_level_TOSCA_test = new File(REPOSITORY_STORAGE_PATH+INSTANCE_LEVEL_TOSCA_NAME);
            BufferedWriter bw = new BufferedWriter(new FileWriter(type_level_TOSCA_test));
            bw.write(INSTANCE_LEVEL_TOSCA_DUMMY_CONTENT);
            bw.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void test_RepositoryCommunication() {
        File type_level_TOSCA_test= new File(REPOSITORY_STORAGE_PATH+INSTANCE_LEVEL_TOSCA_NAME);
        try {
            BufferedReader br = new BufferedReader(new FileReader(type_level_TOSCA_test));
            String s = br.readLine();
            assertEquals(s, INSTANCE_LEVEL_TOSCA_DUMMY_CONTENT);
            type_level_TOSCA_test.delete();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}