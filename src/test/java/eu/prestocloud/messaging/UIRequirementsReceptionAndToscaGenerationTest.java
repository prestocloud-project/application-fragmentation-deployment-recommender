package eu.prestocloud.messaging;

import consumer.AmqpConsumer;
import eu.prestocloud.tosca_generator.ToscaDocument;
import eu.prestocloud.tosca_generator.ToscaImports;
import eu.prestocloud.tosca_generator.ToscaMetadata;
import eu.prestocloud.tosca_generator.capability_types.CapabilityTypesSegment;
import eu.prestocloud.tosca_generator.policy_file_requirements.FragmentationPolicy;
import eu.prestocloud.tosca_generator.relationship_types.RelationshipTypesSegment;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import producer.AmqpProducer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static eu.prestocloud.configuration.Constants.operational_input_mode;
import static eu.prestocloud.tosca_generator.NodeGenerator.*;
import static eu.prestocloud.tosca_generator.ToscaGenerator.tosca_deployment_counter;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;
import static eu.prestocloud.utilities.UIJsonParser.get_policy_from_ui;
import static eu.prestocloud.utilities.UIJsonParser.setUi_deployment_json;

public class UIRequirementsReceptionAndToscaGenerationTest {
    private static String TOPOLOGY_INPUT_TOPIC;
    private static String BROKER_IP_ADDRESS;
    private static String example_deployment_input_file;
    private static JSONObject received_topology_input;
    private static ToscaDocument tosca_document;


    @BeforeAll
    static void initializeExternalResources() {

        TOPOLOGY_INPUT_TOPIC = "deployment.input";
        BROKER_IP_ADDRESS = "3.120.91.124";

        try {
            //example_deployment_input_file = new String(Files.readAllBytes(Paths.get("test.json")));
            try(InputStream input_ui_json = UIRequirementsReceptionAndToscaGenerationTest.class.getResourceAsStream("/test.json")){
                List<String> doc =
                        new BufferedReader(new InputStreamReader(input_ui_json,
                                StandardCharsets.UTF_8)).lines().collect(Collectors.toList());
                example_deployment_input_file = EMPTY;
                for (String s : doc){
                    example_deployment_input_file = example_deployment_input_file+s;
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void test_ReceiveTopology_ToscaGeneration(){


        BiConsumer<String, String> message_handling_function = this::parse_actual_deployment_message;

        AmqpConsumer situations_consumer = new AmqpConsumer(BROKER_IP_ADDRESS, false,message_handling_function);
        situations_consumer.setUsernameAndPassword("nissatech","9r3570cl0ud!");
        String published_tosca_id ;

        //TODO Create a similar consumer if needed to retrieve information from monitoring data
        try {

            situations_consumer.subscribe(TOPOLOGY_INPUT_TOPIC);
            AmqpProducer producer = new AmqpProducer(BROKER_IP_ADDRESS,false);
            producer.publish(example_deployment_input_file,TOPOLOGY_INPUT_TOPIC);
            //allow some time for the message to be sent and received

            Thread.sleep(5000);

            assert (tosca_document!=null && !tosca_document.toString().equals(EMPTY));

            System.out.println("\n--------------------------------------------------------------------------------\n");
            System.out.println("TOSCA DOCUMENT DUMP");
            System.out.println("--------------------------------------------------------------------------------\n");

            System.out.println(tosca_document.toString());
        }catch (Exception e){
            published_tosca_id = "-1";
            e.printStackTrace();
        }

        //assertEquals(received_topology_input,published_tosca_id);
    }

    public ToscaDocument parse_actual_deployment_message(String message, String topic){

        operational_input_mode = "json";
        ToscaDocument partial_tosca_document;
        FragmentationPolicy fragmentationPolicy;
        ToscaMetadata tosca_metadata = new ToscaMetadata();
        try {

            synchronized (ui_deployment_ready) {
                received_topology_input = (JSONObject) new JSONParser().parse(message);
                setUi_deployment_json(received_topology_input);
                ui_deployment_ready.setTrue();
                //ui_deployment_ready.notifyAll();
            }
            partial_tosca_document = create_core_TOSCA_document(tosca_deployment_counter);

            fragmentationPolicy = get_policy_from_ui();
            tosca_metadata.addRequirements(fragmentationPolicy);

            if (partial_tosca_document!=null && partial_tosca_document.isNotEmpty()) {

                CapabilityTypesSegment capability_types_segment = new CapabilityTypesSegment();
                RelationshipTypesSegment relationship_types_segment = new RelationshipTypesSegment();

                tosca_document = new ToscaDocument();
                tosca_document.setMetadata(tosca_metadata);
                tosca_document.setNode_types_segment(partial_tosca_document.getNode_types_segment());
                tosca_document.setDescription("Types Description");
                tosca_document.setTosca_imports(new ToscaImports());
                tosca_document.setCapability_types_segment(capability_types_segment);
                tosca_document.setRelationship_types_segment(relationship_types_segment);
                tosca_document.setTopology_template_segment(partial_tosca_document.getTopology_template_segment());


            }

        }catch (ParseException p){
            p.printStackTrace();
        }
        return  tosca_document;
    }
}
