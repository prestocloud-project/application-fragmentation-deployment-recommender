package eu.prestocloud.messaging;

import org.junit.jupiter.api.Test;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.messaging.RestCommunicator.uploadSSHkey;

class RestCommunicatorTest {

    @Test
    void uploadSSHkey_test() {
        key_storage_endpoint = "34.251.56.14:8080";
        organization_username = "";
        key_storage_password = "";
        if (organization_username.equals("")||key_storage_password.equals("")){
            assert false; //fill in the username and the password of the secure storage before testing
        }
        uploadSSHkey("testkey","ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1uCM+WTrtr82waAez9ZrPnvIyArAVk5AxOXkS8E4genxu1D7YKK2prLDl8xOzOJmrWkfHtzSSDH+quxAaHCmdz/fMsiDt3CzsRLn1f5GqXy4BM+IvVd8M4585s/hRUmDEGohlt9Ro3gnE65zPAjz35pwkBLIWXZ4tSbhQnUJEjMlKjAtsEXIlJeC1YSTMNhvaBnB1qOMxwZbOUEdhxlbezIVlugLxrdhlEDDJBpR5xwSDgFwEKO/Z9eE+cLAVNEetShenHZevctMHcdEGFBie8MOF2hiKD3kKcPykH6ULPw2hs9seiIz51coGAcX5k3YHxvi7ngVqHjuQsRqUIe97 admin");
    }
}