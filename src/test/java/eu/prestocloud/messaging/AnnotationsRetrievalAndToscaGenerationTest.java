package eu.prestocloud.messaging;

import eu.prestocloud.tosca_generator.ToscaDocument;
import eu.prestocloud.tosca_generator.ToscaImports;
import eu.prestocloud.tosca_generator.ToscaMetadata;
import eu.prestocloud.tosca_generator.capability_types.CapabilityTypesSegment;
import eu.prestocloud.tosca_generator.policy_file_requirements.FragmentationPolicy;
import eu.prestocloud.tosca_generator.relationship_types.RelationshipTypesSegment;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

import static eu.prestocloud.configuration.Constants.*;
import static eu.prestocloud.tosca_generator.NodeGenerator.*;
import static eu.prestocloud.tosca_generator.ToscaGenerator.parsePolicyFile;
import static eu.prestocloud.tosca_generator.ToscaGenerator.tosca_deployment_counter;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;


public class AnnotationsRetrievalAndToscaGenerationTest {
    private static String BROKER_IP_ADDRESS;
    private static ToscaDocument tosca_document;
    private static String active_policy_file;


    @BeforeAll
    static void initializeExternalResources() {

        annotated_application_name = "\\test.jar";
        BROKER_IP_ADDRESS = "3.120.91.124";
        annotated_application_path = "/";
        key_storage_endpoint = "34.251.56.14:8080";
        coordinator_node_docker_image = "coordinator_image";
        coordinator_node_docker_registry = "coordinator_registry";
        coordinator_node_ports_properties = new String[0];
        coordinator_node_environmental_variables = new String[0];
        coordinator_node_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC75AUrOpEajZ+/Bx8uwzFy4wwyL6rYb3WfocTwq4B7O/iCi+GB5JehBmX+AEx7EnwqMcR1Op5/KdxYp7z/M7C5Hx1x21E0FADLYhQ4kvNJ4xwDx5SsLlDbLtIhweEPDBL+d7sOl8HSJKBZGPg2JKZPK+eBOYVbnYREOTHpdzEmixYKgN71bT+ecTVySbFjraEMrTCTwrmPeSEr5MwHRCpCFgnfo/vhPTaTFI6oDlzgGDGXq6qT0HmIQUyrLa2KMD9wfZDETTs4ZmkAgdPNj1U5JKcRSSaRDzkoFsRQVQWjheSKJwhn10fNQw9vUsidEcJc8vZLp+a1ATfe5g4DiWhzPfyfF2/k5qeLGYAWPn5xh5ICQbqscNzczkV4H+zjkHwnaxrfyri9OIRd5Brd+vNVFlBgSlQuVAl4F/aNSMVjDLHgVOS5tHmYOIMyyUqZlvyt7V7JdsqwfQsRP3T+qltiTsQeELHYMJLflFOkwjuEoo1ZKJMfWpK8zS8X62DJawml+lq5+GHHAAElo02Hmy0sqYU/VXMzC8whcTvLjwW2pmj6heWRP5Szrr+5XMjfhdMRG0CrwijRhhfePnaVc9KhiSWBGLRw1k0TbWrDqwngavLmwJvCDa7yMqtEMzzVFN+/aliuZ6gukwwIxvhC8ED12vQ5V6NowYiELruLxoAG8Q== user@default";
        coordinator_node_health_check_command = "curl google.gr";
        coordinator_node_health_check_interval = 10;


        try {
            URL res = AnnotationsRetrievalAndToscaGenerationTest.class.getClassLoader().getResource("test.policyfile");
            File file = Paths.get(res.toURI()).toFile();
            active_policy_file = file.getAbsolutePath();

            res = AnnotationsRetrievalAndToscaGenerationTest.class.getClassLoader().getResource(annotated_application_name);
            file = Paths.get(res.toURI()).toFile();
            annotated_application_path = file.getParent();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void test_RetrieveAnnotations_ToscaGeneration(){


        try {

            //AmqpProducer producer = new AmqpProducer(BROKER_IP_ADDRESS);
            //producer.publish(example_deployment_input_file,TOPOLOGY_INPUT_TOPIC);


            operational_input_mode = "annotations";
            ToscaDocument tosca_document = null, partial_tosca_document;
            FragmentationPolicy fragmentationPolicy;
            ToscaMetadata tosca_metadata = new ToscaMetadata();

            fragmentationPolicy = parsePolicyFile(active_policy_file);
            initialize_node_generator(fragmentationPolicy.getMappingRequirement());

            partial_tosca_document = create_core_TOSCA_document(tosca_deployment_counter);
            tosca_metadata.addRequirements(fragmentationPolicy);

            if (partial_tosca_document!=null && partial_tosca_document.isNotEmpty()) {

                CapabilityTypesSegment capability_types_segment = new CapabilityTypesSegment();
                RelationshipTypesSegment relationship_types_segment = new RelationshipTypesSegment();

                tosca_document = new ToscaDocument();
                tosca_document.setMetadata(tosca_metadata);
                tosca_document.setNode_types_segment(partial_tosca_document.getNode_types_segment());
                tosca_document.setDescription("Types Description");
                tosca_document.setTosca_imports(new ToscaImports());
                tosca_document.setCapability_types_segment(capability_types_segment);
                tosca_document.setRelationship_types_segment(relationship_types_segment);
                tosca_document.setTopology_template_segment(partial_tosca_document.getTopology_template_segment());


            }

            assert (tosca_document!=null && !tosca_document.toString().equals(EMPTY));

            System.out.println("\n--------------------------------------------------------------------------------\n");
            System.out.println("TOSCA DOCUMENT DUMP");
            System.out.println("--------------------------------------------------------------------------------\n");

            System.out.println(tosca_document.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}

