package eu.prestocloud.messaging;

import consumer.AmqpConsumer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import producer.AmqpProducer;

import java.util.function.BiConsumer;

import static eu.prestocloud.messaging.Test_Publisher.publish;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnnounceNewDeploymentRecomTest {
    private static String DEPLOYMENT_ANNOUNCEMENT_TOPIC, DEPLOYMENT_PERFORMED_TOPIC;
    private static String BROKER_IP_ADDRESS;
    private static String example_deployment_announcement;
    private static String received_instance_level_deployment;
    private static String expected_control_layer_response;


    @BeforeAll
    static void initializeExternalResources() {
        DEPLOYMENT_ANNOUNCEMENT_TOPIC = "deployment.req";
        DEPLOYMENT_PERFORMED_TOPIC = "deployment.ack";
        BROKER_IP_ADDRESS = "3.120.91.124";

        example_deployment_announcement = "{\n" +
                "\"tosca_id\":\"/home/user/prestocloud/tosca_deployment_1.yaml\"\n" +
                "}";

        expected_control_layer_response = "{\n" +
                "\"application_instance_id\":\"1\"\n" +
                "}";

        received_instance_level_deployment = "";
    }

    @Test
    public void test_SubscribeToEvents_PublishSituations(){


        BiConsumer<String, String> message_handling_function = this::parse_actual_deployment_message;

        AmqpConsumer situations_consumer = new AmqpConsumer(BROKER_IP_ADDRESS,false, message_handling_function);
        situations_consumer.setUsernameAndPassword("nissatech","9r3570cl0ud!");
        String published_tosca_id ;

        //TODO Create a similar consumer if needed to retrieve information from monitoring data
        try {
            situations_consumer.subscribe(DEPLOYMENT_PERFORMED_TOPIC);
            AmqpProducer producer = new AmqpProducer(BROKER_IP_ADDRESS,false);
            producer.publish(example_deployment_announcement,DEPLOYMENT_ANNOUNCEMENT_TOPIC);
            producer.publish(expected_control_layer_response,DEPLOYMENT_PERFORMED_TOPIC);
            //allow some time for the message to be sent and received

            Thread.sleep(2000);
            published_tosca_id = ((String)((JSONObject)(new JSONParser().parse(example_deployment_announcement))).get("tosca_id")).replaceAll("^(.*)tosca_deployment_",EMPTY).replaceAll("\\.yaml$",EMPTY);
        }catch (Exception e){
            published_tosca_id = "-1";
            e.printStackTrace();
        }

        assertEquals(received_instance_level_deployment,published_tosca_id);
    }

    public void parse_actual_deployment_message(String message, String topic){
        try {
            JSONObject received_deployment_performed = (JSONObject) new JSONParser().parse(message);
            received_instance_level_deployment = (String) received_deployment_performed.get("application_instance_id");
            System.out.println("The value received is "+received_instance_level_deployment);
        }catch (ParseException p){
            p.printStackTrace();
        }
    }

}
