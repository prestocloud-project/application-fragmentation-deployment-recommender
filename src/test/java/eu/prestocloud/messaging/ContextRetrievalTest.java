package eu.prestocloud.messaging;

import consumer.AmqpConsumer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import producer.AmqpProducer;

import java.util.function.BiConsumer;

import static eu.prestocloud.messaging.Test_Publisher.publish;
import static eu.prestocloud.tosca_generator.utility_classes.Formatting.EMPTY;

public class ContextRetrievalTest {

    private static String REQUEST_CONTEXT_TOPIC;
    private static String RETRIEVE_CONTEXT_TOPIC;
    private static final boolean MCA_SIMULATED = true;
    private static String BROKER_IP_ADDRESS;
    private static String context_received,example_context_returned ;

    @BeforeAll

    static void initializeExternalResources() {
        System.out.println("Initializing external resources...");
        REQUEST_CONTEXT_TOPIC = "excluded_devices.req";
        RETRIEVE_CONTEXT_TOPIC = "excluded_devices.ack";
        BROKER_IP_ADDRESS = "3.120.91.124";
        example_context_returned  =
                "[{ \"fragment_name\":\"test_fragments_VideoStreamer\", \"excluded_devices\":[\n" +
                "\"asf342fd\", \"sjkk349d\", \"kqt8s1f1\", \"i89iaaq5\", \"kaf9734f\"\n" +
                "]},\n" +
                "{ \"fragment_name\":\"test_fragments_VideoTranscoder\", \"excluded_devices\":[\n" +
                "\"bladf423\", \"nounae23\", \"kage219a\", \"kresc100\", \"o122abol\", \"gl3eb18d\"\n" +
                "]}\n" +
                "]";

    }

    @Test
    public void test_RetrieveContext(){


        BiConsumer<String, String> message_handling_function = this::return_message;
        AmqpProducer producer = new AmqpProducer(BROKER_IP_ADDRESS,false);
        AmqpConsumer context_consumer = new AmqpConsumer(BROKER_IP_ADDRESS,false, message_handling_function);

        //TODO Create a similar consumer if needed to retrieve information from monitoring data
        try {
            context_consumer.setUsernameAndPassword("nissatech","9r3570cl0ud!");
            context_consumer.subscribe(RETRIEVE_CONTEXT_TOPIC);
            producer.publish("[\"requesting excluded devices\"]",REQUEST_CONTEXT_TOPIC);
            if (MCA_SIMULATED) {
                producer.publish(example_context_returned,RETRIEVE_CONTEXT_TOPIC);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        //allow some time for the message to be sent and received
        try {
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }

        assert(context_received!=null && !context_received.equals(EMPTY));

    }

    public void return_message(String message, String topic){
        if (topic.startsWith(RETRIEVE_CONTEXT_TOPIC)) {
            context_received = message;
        }
    }



}
