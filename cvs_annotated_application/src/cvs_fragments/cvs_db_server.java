package cvs_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;


@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.LOW,
        storageLoad = PrestoFragmentation.StorageLoad.MEDIUM,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.none,
        cloud_docker_image = "cvs_db_server:latest",
        cloud_docker_registry = "hub.docker.com/r/salmant",
        cloud_environmental_variables = {},
        cloud_ports_properties = {
                "7000","7000","TCP",
                "7001","7001","TCP",
                "7199","7199","TCP",
                "9160","9160","TCP",
                "9042","9042","TCP",
                "8012","8012","TCP",
                "61621","61621","TCP",
        }
)
public class cvs_db_server {
}
