package cvs_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        onloadable = true,
        offloadable = true,
        elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.faas,
        precededBy = {"cvs_fragments.cvs_notification_server","cvs_fragments.cvs_sensors_arm"},
        cloud_docker_image = "cvs_driver_dynamics_x86:latest",
        cloud_docker_registry = "hub.docker.com/r/salmant",
        cloud_environmental_variables = {
                "SensorPort","6789",
                "ServicePort","7936",
                "Threshold","0.3",
                "ProcessIP","127.0.0.1",
                "NS_IP","get_property: [cvs_fragments.cvs_notification_server,host,network,addresses,1]",
                "NS_PORT","get_property: [cvs_fragments.cvs_notification_server,docker_cloud,ports,1,published]",
                "DB_IP","get_property: [cvs_fragments.cvs_db_server,host,network,addresses,1]",
                "TravelId","65440984",
                "GPS_Counter","30",
                "Aggregate","yes",
                "Process","yes"
        },
        cloud_ports_properties = {
                "6789","6789","TCP",
                "7936","7936","TCP",
        },
        edge_docker_image = "cvs_driver_dynamics_x86:latest",
        edge_docker_registry = "hub.docker.com/r/salmant",
        edge_environmental_variables = {
                "SensorPort","6789",
                "ServicePort","7936",
                "Threshold","0.3",
                "ProcessIP","127.0.0.1",
                "NS_IP","get_property: [cvs_fragments.cvs_notification_server,host,network,addresses,1]",
                "NS_PORT","get_property: [cvs_fragments.cvs_notification_server,docker_cloud,ports,1,published]",
                "DB_IP","get_property: [cvs_fragments.cvs_db_server,host,network,addresses,1]",
                "TravelId","65440984",
                "GPS_Counter","30",
                "Aggregate","yes",
                "Process","yes"
        },
        edge_ports_properties = {
                "6789","6789","TCP",
                "7936","7936","TCP",
        }
)
public class cvs_driver_dynamics {
}
