package cvs_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.MEDIUM,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        offloadable = false,
        onloadable = true,
        elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.faas,
        precededBy = {"cvs_fragments.cvs_notification_server"},
        edge_docker_image = "cvs_sensors_arm:latest",
        edge_docker_registry = "hub.docker.com/r/salmant",
        edge_environmental_variables = {
                "SensorPort","6789",
                "DriverDynamicsIP","get_property: [cvs_fragments.cvs_driver_dynamics,host,network,addresses,1]",
        },
        edge_ports_properties = {
                "6789","6789","TCP",
        }
)
public class cvs_sensors_arm {
}
