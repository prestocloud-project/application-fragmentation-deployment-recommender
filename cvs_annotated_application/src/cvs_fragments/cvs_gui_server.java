package cvs_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.VERY_LOW,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.none,
        precededBy = {"cvs_fragments.cvs_notification_server"},
        cloud_docker_image = "cvs_gui_server:latest",
        cloud_docker_registry = "hub.docker.com/r/salmant",
        cloud_environmental_variables = {
                "NS_SERVER","get_property: [cvs_fragments.cvs_notification_server,host,network,addresses,1]",
                "NS_PORT","get_property: [cvs_fragments.cvs_notification_server,docker_cloud,ports,1,published]",
                "DB_SERVER","get_property: [cvs_fragments.db_server,host,network,addresses,1]",

        },
        cloud_ports_properties = {
                "80","8080","TCP",
                "3306","3306","TCP",
        }
)
public class cvs_gui_server {
}
