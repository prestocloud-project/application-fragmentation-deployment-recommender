package cvs_fragments;

import eu.prestocloud.annotations.PrestoFragmentation;

@PrestoFragmentation(
        memoryLoad = PrestoFragmentation.MemoryLoad.MEDIUM,
        cpuLoad = PrestoFragmentation.CPULoad.VERY_LOW,
        storageLoad = PrestoFragmentation.StorageLoad.VERY_LOW,
        onloadable = false,
        offloadable = true,
        elasticity_mechanism = PrestoFragmentation.Elasticity_mechanism.faas,
        precededBy = {"cvs_fragments.cvs_db_server"},
        cloud_docker_image = "cvs_notification_server:latest",
        cloud_docker_registry = "hub.docker.com/r/salmant",
        cloud_environmental_variables = {},
        cloud_ports_properties = {
                "8080","8282","TCP",
        }
)
public class cvs_notification_server {
}
